<div class="row justify-content-center mb-4">
    <div class="col-4">
        <form action="{{$redirectUrl}}" method="GET">
            <div class="input-group input-group-outline">
                <label for="opd" class="d-block">Perangkat Daerah</label>
                @can('access-other-opd')
                    <select name="opd" id="opd" class="form-control" required>
                        @foreach ($opds as $opd)
                            <option value="{{ $opd->id }}" {{ $selectedOpd == $opd->id ? "selected" : "" }}>{{ $opd->name }}</option>
                        @endforeach
                    </select>
                @else
                    <select name="opd" id="opd" class="form-control" required disabled>
                        @foreach ($opds as $opd)
                            <option value="{{ $opd->id }}" {{ $selectedOpd == $opd->id ? "selected" : "" }}>{{ $opd->name }}</option>
                        @endforeach
                    </select>
                @endcan
            </div>

            @can('access-other-opd')
                <div class="input-group input-group-outline">
                    <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                </div>
            @endcan
        </form>
    </div>
</div>

@push('scripts')
<script>
selectPlaceholder('#opd', 'Pilih PD');
</script>
@endpush
