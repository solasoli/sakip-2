<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">{{ $title }}</span>
    <nav class="mr-4">
        @foreach ($items as $item)
            @if ($loop->last)
                <span class="breadcrumb-item" style="color: #000;">{{ $item['label'] }}</span>
            @else
                <a class="breadcrumb-item" href="{{ $item['href'] }}">{{ $item['label'] }}</a>
            @endif
        @endforeach
    </nav>
</div>
