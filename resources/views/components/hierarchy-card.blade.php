<div class="card mt-3">
    <div class="card-header bg-primary text-light style="position: relative; z-index: 999;">
        <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
            <a href="#" class="info_btn">
                <i class="fa fa-info-circle"></i>
            </a>&emsp;
            <i class="fa fa-angle-double-right mr-2"></i> {{ $title }}
        </span>
        {{$action??null}}
    </div>
    <div class="alert show-hidden info_bar" style="display: flex; flex-direction: column" role="alert">
        @foreach ($hierarchy as $hierarchyKey => $hierarchyValue)
            @if ($hierarchyValue != null)
                <span class="hierarki_m">~ {{ $hierarchyKey }} &nbsp;
                    <i class="fa fa-caret-right">&emsp;</i>{{ $hierarchyValue }}
                </span>
            @endif
        @endforeach
    </div>
    <div class="card-body">
        {{$slot}}
    </div>
</div>

@push('css')
    <style media="screen">
        .info_bar {
            position: absolute;
            top: 50px;
            z-index: 998;
            border-radius: 0;
            width: 100%;
            background-image: linear-gradient(195deg, #42424a 0%, #191919 100%);
        }

        #card-body-info {
            transition: .3s;
        }

        .relative-info {
            transition: .3s;
        }

    </style>
@endpush
@push('scripts')
    <script>
        // to make sure this function only run once
        if (typeof getInfoHierarki == 'undefined') {
            getInfoHierarki('.info_btn');
            function getInfoHierarki(btn) {
                let info = document.querySelectorAll(btn);
                info.forEach(res => {
                    res.addEventListener('click', () => {
                        const card = res.closest('.card')
                        const card_header = card.querySelector('.card-header')
                        const info_bar = card_header.nextElementSibling
                        const card_body = card.querySelector('.card-body')
                        const height_info_bar = info_bar.offsetHeight

                        info_bar.classList.toggle('show-hidden')
                        card_body.style.transition = '.3s';
                        if (!info_bar.classList.contains('show-hidden')) {
                            card_body.style.marginTop = `${height_info_bar - 10}px`;
                        } else {
                            card_body.style.marginTop = `0`;
                        }
                    })
                })
            }
        }
    </script>
@endpush
