<div class="card">
    <div class="card-header">
        <span>{{$headline}}</span>
    </div>

    @php
        $isEdit = isset($user);
        $url = $isEdit
            ? route('users.update', ['user' => $user->id])
            : route('users.store');
    @endphp
    <form action="{{$url}}" method="POST">
        @csrf
        @if ($isEdit)
            @method('PUT')
        @endif

        <div class="card-body">
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="name">Nama</label>
                    <input
                        type="text"
                        id="name"
                        value="{{ $user->name ?? null }}"
                        class="form-control"
                        name="name">
                </div>
            </div>
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="email">Email</label>
                    <input
                        type="text"
                        id="email"
                        value="{{ $user->email ?? null }}"
                        class="form-control"
                        name="email">
                </div>
            </div>
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="username">Username</label>
                    <input
                        type="text"
                        id="username"
                        value="{{ $user->username ?? null }}"
                        class="form-control"
                        name="username">
                </div>
            </div>
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="opd">OPD</label>
                    <select name="opd" id="opd" class="form-control">
                        @foreach ($opds as $item)
                            @php
                                $isChecked = $isEdit
                                    ? $user->id_opd == $item->id
                                    : false;
                            @endphp
                            <option value="{{ $item->id }}" {{ $isChecked ? 'selected' : '' }}>{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="role">Role</label>
                    <select name="role" id="role" class="form-control">
                        @foreach ($roles as $item)
                            @php
                                $isChecked = $isEdit
                                    ? $user->role == $item['value']
                                    : false;
                            @endphp
                            <option value="{{ $item['value'] }}" {{ $isChecked ? 'selected' : '' }}>{{ $item['name'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button class="btn btn-dark cancel" onclick="return window.history.back()" type="button">Cancel</button>
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
</div>

@push('scripts')
    <script>
        selectPlaceholder('#opd', 'Pilih OPD');
        selectPlaceholder('#role', 'Pilih Role');
    </script>
@endpush
