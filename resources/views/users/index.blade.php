@extends('layouts/app')
@section('title')
    Administrator
@endsection
@section('content')

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Administrator</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <span class="breadcrumb-item" style="color: #000;">Administrator</span>
    </nav>
</div>
<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Administrator</h6>
            <a href="{{ route('users.create') }}">
                <button class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Tambah</button>
            </a>
        </div>
        <div class="card-body">
            @include('partials.message')
            <div class="row">
                <div class="col-sm-12">
					<div class="table-responsive">
						{!! $html->table() !!}
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
{!! $html->scripts() !!}
@endsection
