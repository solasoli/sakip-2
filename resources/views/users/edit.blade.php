@extends('layouts/app')
@section('title')
    Ubah Administrator
@endsection
@section('content')
<div class="container-fluid pb-2 mt-4">

    @include('partials.message')
    @include('partials.error-message')
    @include('users.form', [
        "headline" => "Form Ubah Administrator $user->name",
    ])
</div>
@endsection
