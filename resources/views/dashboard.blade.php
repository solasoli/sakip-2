@extends('layouts.app')
@section('title')
Dashboard
@endsection
@section('content')
<style media="screen">
.modal-lg {
    width: 750px !important;
}
.text-kiri{
    text-align: left;
}
</style>
<div class="col-12">
    <div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
        <span class="brand ml-4">Dashboard</span>
        <nav class="mr-4">
            <a class="breadcrumb-item" href="/">Dashboard</a>
        </nav>
    </div>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-header">
                        <p class="text-dark">Selamat Datang.</p>
                    </div>
                    <div class="container">
                            <table class="table table-bordered table-hove">
                                <thead>
                                    <tr>
                                        <td class="text-dark">Username</td>
                                        <td class="text-center text-dark">:</td>
                                        <td style="text-align: left;">{{ Auth::user()->username }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-dark">Nama</td>
                                        <td class="text-center text-dark">:</td>
                                        <td style="text-align: left;">{{ Auth::user()->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-dark">Role</td>
                                        <td class="text-center text-dark">:</td>
                                        <td style="text-align: left;">{{ Auth::user()->role }}</td>
                                    </tr>
                                </thead>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection