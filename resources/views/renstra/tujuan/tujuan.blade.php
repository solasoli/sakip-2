@extends('layouts.app')
@section('title')
    Renstra Tujuan
@endsection
@section('content')
<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Renstra Tujuan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Renstra</a>
        <span class="breadcrumb-item" style="color: #000;">Tujuan</span>
    </nav>
</div>
<style media="screen">
    .info_bar {
        position: absolute;
        top: 50px;
        z-index: 998;
        border-radius: 0;
        width: 100%;
        background-image: linear-gradient(195deg, #42424a 0%, #191919 100%);
    }

    #card-body-info {
        transition: .3s;
    }

    .relative-info {
        transition: .3s;
    }
</style>
<div class="container-fluid pb-2 mt-4">

    @include('components.opd-selection',[
        'opds' => $opd,
        'selectedOpd' => $opd_selected,
        'redirectUrl' => route('renstra.tujuan.index')
    ])

    @include('partials.error-message')
    @include('partials.message')

    <div class="card">
        @if (!is_null(periode()))
            <div class="card-body">
                <div class="card-header">
                    <h6 class="card-title">Renstra Tujuan Tingkat Kabupaten Periode {{ periode()->dari }} -
                        {{ periode()->sampai }}</h6>
                    <a href="{{ url('') }}/renstra/tujuan/add" class="btn btn-primary btn-sm" style="background-color: #4caf50;">
                        <i class="fa fa-plus"></i>
                        <span>Tambah Tujuan</span>
                    </a>
                </div>
                <hr>

                @if($tujuan_count > 0)
                    @foreach ($hierarki as $idx => $item)
                        @if(!is_null($item->tujuanRenstra->first()))
                        <div class="card mt-2">
                            <div class="card-header bg-primary text-light" style="position: relative; z-index: 999;">
                                <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                                    <a href="#" class="info_btn">
                                        <i class="fa fa-info-circle"></i>
                                    </a>&emsp;
                                    <i class="fa fa-angle-double-right"></i> &nbsp; Sasaran : &nbsp; {{ $item->name }}
                                </span>
                            </div>
                            <!-- Hierarki -->
                            <div class="alert show-hidden info_bar mt-4"
                                style="display: flex; flex-direction: column" role="alert">
                                <span class="hierarki_m">~ / Misi &nbsp;
                                    <i class="fa fa-caret-right">&emsp;</i>{{ $item->tujuan->misi->misi }}
                                </span>
                                <span class="hierarki_m">~ / Perangkat Daerah &nbsp;
                                    <i class="fa fa-caret-right">&emsp;</i>{{ $item->tujuanRenstra->first()->opd->name }}
                                </span>
                                <span class="hierarki_m">~ RPJMD / Tujuan &nbsp;
                                    <i class="fa fa-caret-right">&emsp;</i>{{ $item->tujuan->name }}
                                </span>
                                <span class="hierarki_m">~ RPJMD / Sasaran &nbsp;
                                    <i class="fa fa-caret-right">&emsp;</i>{{ $item->name }}
                                </span>
                                <span class="hierarki_m">~ RPJMD / Prioritas Pembangunan &nbsp;
                                    <i class="fa fa-caret-right">&emsp;</i>
                                    @if(!is_null($item->prioritasPembangunan) && !is_null($item->prioritasPembangunan->mstPrioritasPembangunan))
                                        {{ $item->prioritasPembangunan->mstPrioritasPembangunan->name }}
                                    @else
                                        <span class="text-warning">Prioritas Pembangunan belum ditetapkan! &nbsp; klik
                                        <a href="{{ url('') }}/rpjmd/prioritas-pembangunan" class="text-info">disini</a> untuk menetapkan</span>
                                    @endif
                                </span>
                            </div>
                            <!-- end hierarki -->

                            @foreach ($item->tujuanRenstra as $val)
                            <div class="card-body">
                                <div class="card">
                                    <div class="card-header">
                                        <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                                            &nbsp; Tujuan Perangkat Daerah : &nbsp; {{ $val->name }}
                                        </span>

                                        <!-- button edit & delete -->
                                        <div class="aksi">
                                            <!-- button delete -->
                                            <form action="{{ url('') }}/renstra/tujuan/delete/{{ hashID($val->id) }}" method="POST" class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button class="btn btn-danger btn-sm" title="Hapus" type="submit" onclick="return confirm('Lanjutkan menghapus data?')">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </form>

                                            <!-- button edit -->
                                            <form action="{{ url('') }}/renstra/tujuan/edit/{{ hashID($val->id) }}" method="POST" class="d-inline">
                                                @csrf
                                                <button class="btn btn-warning btn-sm" title="Edit" type="submit">
                                                    EDIT
                                                </button>
                                            </form>
                                        </div>
                                        <!-- end button  -->

                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <th>Indikator Tujuan</th>
                                                    <th>Satuan</th>
                                                    <th>Kondisi Awal</th>
                                                    @foreach($periode_per_tahun as $tahun)
                                                    <th width="125" class="text-center">Target {{ $tahun->tahun }}</th>
                                                    @endforeach
                                                    <th>Kondisi Akhir</th>
                                                </thead>
                                                <tbody>
                                                    @foreach ($val->indikator as $itemIndikator)
                                                    <tr>
                                                        <td>{{ $itemIndikator->indikator }}</td>
                                                        <td>{{ $itemIndikator->satuan->name }}</td>
                                                        <td>{{ $itemIndikator->kondisi_awal }}</td>
                                                        @foreach ($itemIndikator->target as $itemTarget)
                                                        <td>{{ $itemTarget->target }}</td>
                                                        @endforeach
                                                        <td>{{ $itemIndikator->kondisi_akhir }}</td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </div>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        @endif
                    @endforeach
                @else
                    <div class="alert alert-warning" role="alert">
                        Tidak ada data!
                    </div>
                @endif
            </div>
        @endif
    </div>
</div>
@endsection

@section('scripts')
    <script>
        // create and get hierarki
        function getInfoHierarki(btn) {
            let info = document.querySelectorAll(btn);
            info.forEach(res => {
                res.addEventListener('click', () => {
                    const card = res.closest('.card')
                    const card_header = card.querySelector('.card-header')
                    const info_bar = card_header.nextElementSibling
                    const card_body = card.querySelector('.card-body')
                    const height_info_bar = info_bar.offsetHeight

                    info_bar.classList.toggle('show-hidden')
                    card_body.style.transition = '.3s';
                    if (!info_bar.classList.contains('show-hidden')) {
                        card_body.style.marginTop = `${height_info_bar - 10}px`;
                    } else {
                        card_body.style.marginTop = `0`;
                    }
                })
            })
        }
        getInfoHierarki('.info_btn');
    </script>
@endsection
