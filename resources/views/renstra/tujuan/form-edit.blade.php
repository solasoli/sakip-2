@extends('layouts.app')
@section('title')
    Form Edit Renstra Tujuan
@endsection
@section('content')
<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Renstra Tujuan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Renstra</a>
        <span class="breadcrumb-item">Tujuan</span>
        <span class="breadcrumb-item" style="color: #000;">Edit</span>
    </nav>
</div>
<div class="container-fluid pb-2 mt-4">
    @foreach ($tujuan as $item)
    <form action="{{ url('') }}/renstra/tujuan/update/{{ hashID($item->id) }}" method="POST" class="edit_renstra_tujuan">
    @csrf
    @method('patch')
    <div class="card">
        <div class="card-header">
            <span>Form Edit Renstra Tujuan</span>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <div class="input-group input-group-outline">
                        <label for="">Pilih Perangkat Daerah</label>
                        <select name="opd" id="opd" class="opd">
                            @foreach ($opd as $val)
                            <option value="{{ $val->id }}" {{ $val->id == $item->id_mst_opd ? "selected" : "" }}>{{ $val->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group input-group-outline">
                        <label for="">Sasaran Kabupaten</label>
                        <select name="sasaran" id="sasaran" class="sasaran">
                            @foreach ($sasaran as $val)
                            <option value="{{ $val->id }}" {{ $val->id == $item->id_rpjmd_sasaran ? "selected" : "" }}>{{ $val->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group input-group-outline">
                        <label for="">Perangkat Daerah</label>
                        <input type="text" name="tujuan" id="tujuan" value="{{ $item->name }}" class="form-control">
                    </div>
                </div>
            </div>
            <div class="place-duplicate">

                @foreach ($item->indikator as $indikator)
                <div class="duplicate">
                    <div style="display: flex; align-items: end;">
                        <table class="table-responsive pb-3">
                            <tr>
                                <th class="p-3">Indikator tujuan * : </th>
                                <th class="p-3">Satuan * : </th>
                                <th class="p-3">Kondisi Awal * : </th>
                                @foreach($periode_per_tahun as $idx => $item)
                                    <th class="p-3">Target {{ $item->tahun }} * : </th>
                                @endforeach
                                <th class="p-3">Kondisi Akhir * : </th>
                            </tr>
                            <tr>
                                <td class="pr-2">
                                    <div class="input-group input-group-outline">
                                        <input type="text" id="indikator" value="{{ $indikator->indikator }}" class="form-control" name="indikator[]">
                                        @error('indikator.*')
                                        <div class="text-danger">
                                            <small>Indikator sasaran tidak boleh kosong!</small>
                                        </div>
                                        @enderror
                                    </div>
                                </td>
                                <td class="pr-2">
                                    <div class="input-group input-group-outline">
                                        <select name="satuan[]" id="satuan" class="form-control satuan">
                                            @foreach ($satuan as $val)
                                            <option value="{{ $val->id }}" {{ $indikator->id_satuan == $val->id ? "selected" : "" }}>{{ $val->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('satuan.*')
                                        <div class="text-danger">
                                            <small>Tidak boleh kosong!</small>
                                        </div>
                                        @enderror
                                    </div>
                                </td>
                                <td class="pr-2">
                                    <div class="input-group input-group-outline">
                                        <input type="text" class="form-control" value="{{ $indikator->kondisi_awal }}" name="awal[]" id="awal">
                                    </div>
                                </td>
                                @foreach($periode_per_tahun as $idx => $item)
                                    <td class="pr-2">
                                        <div class="input-group input-group-outline">
                                            <input type="text" class="form-control" id="target{{ $loop->iteration }}" value="{{ $indikator->target[$idx]->target }}" name="target[]">
                                        </div>
                                    </td>
                                @endforeach
                                <td class="pr-2">
                                    <div class="input-group input-group-outline">
                                        <input type="text" class="form-control" value="{{ $indikator->kondisi_akhir }}" name="akhir[]" id="akhir">
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="input-group input-group-outline close-wrapper" hidden>
                            <button class="close-duplicate btn btn-danger btn-sm mt-4 ml-2"><i class="fa fa-close"></i></button>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            <div class="input-group input-group-outline row">
                <div class="col-sm-12">
                    <button type="button" class="btn btn-success btn-sm duplicate-btn">
                        <i class="fa fa-plus"></i> Tambah Indikator</button>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="button" class="btn btn-dark cancel">Cancel</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form>
    @endforeach
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        const btn_cancel = document.querySelector('.cancel');
        btn_cancel.addEventListener('click', () => {
            window.history.back();
        });

        show_remove_btn('.close-wrapper', 'hidden');
        $('.duplicate-btn').click(() => {
            clone_el();
            show_remove_btn('.close-wrapper', 'hidden');
        })

        // function show remove button after click duplicate-btn
        function show_remove_btn(el, attr) {
            const close_btn = document.querySelectorAll(el);
            close_btn.forEach((res, idx) => {
                if (idx > 0) {
                    res.removeAttribute(attr)
                }
            })
        }
        // end //

        // function clone element 
        const clone_el = function() {
            const el_duplicate = document.querySelector('.duplicate');
            const select_clone = el_duplicate.querySelectorAll('select.satuan');
            let span_select = el_duplicate.querySelectorAll('span.select2');
            span_select.forEach(res => res.remove());

            select_clone.forEach(res => {
                res.classList.remove('satuan');
                res.classList.add('satuan-cloned');
            })

            const clone = el_duplicate.cloneNode(true);
            document.querySelector('.place-duplicate').appendChild(clone);
            clean_el_clone(clone);
            selectPlaceholder('.satuan-cloned', 'Satuan');
        }
        // end //

        // clear value after clone
        const clean_el_clone = function(el) {
            let check = el.querySelectorAll('input[type=checkbox]');
            let input = el.querySelectorAll('input[type=text]');

            let jml_clone = document.querySelectorAll('.duplicate').length;

            check.forEach(res => res.checked = false)
            input.forEach(res => (!res.classList.contains('check')) ? res.value = '' : checkboxValue())
        }
        // end //

        // remove element clone
        closeElementClone('close-duplicate', '.duplicate');
        function closeElementClone(c_btn, c_parent) {
            document.addEventListener('click', function(e) {
                if (e.target.classList.contains(c_btn) || e.target.classList.contains('fa-close')) {
                    e.preventDefault();
                    this.activeElement.closest(c_parent).remove();
                }
            })
        }
        // end //

        // select2 using placeholder
        selectPlaceholder('.satuan', "Satuan")
        selectPlaceholder('.sasaran', "Sasaran Kabupaten")
        selectPlaceholder('.opd', "Pilih OPD")
        // end //

        inputValidate('.edit_renstra_tujuan', ['input', 'select', 'textarea']);
    });
</script>
@endsection