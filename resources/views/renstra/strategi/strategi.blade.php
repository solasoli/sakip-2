@extends('layouts.app')
@section('title')
    Strategi
@endsection
@section('content')
<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Resntra Strategi</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Renstra</a>
        <span class="breadcrumb-item" style="color: #000;">Strategi</span>
    </nav>
</div>
<style media="screen">
    .info_bar {
        position: absolute;
        top: 50px;
        z-index: 998;
        border-radius: 0;
        width: 100%;
        background-color: rgb(41, 77, 100);
    }

    #card-body-info {
        transition: .3s;
    }

    .relative-info {
        transition: .3s;
    }
</style>
<div class="container-fluid pb-2 mt-4">
    @include('components.opd-selection',[
        'opds' => $opd,
        'selectedOpd' => $opd_selected,
        'redirectUrl' => route('renstra.strategi.index')
    ])
    @include('partials.error-message')
    @include('partials.message')
    <div class="card mb-4">
        @if (!is_null(periode()))
        <form action="{{ url('') }}/renstra/strategi/create" method="POST" class="add-renstra-strategi">
            <input type="hidden" name="opd_selected" value="{{$opd_selected}}"/>
            <div class="card-body">
                <div class="card-header">
                    <h6 class="card-title">Renstra Strategi Tingkat Kabupaten Periode {{ periode()->dari }} -
                        {{ periode()->sampai }}</h6>
                </div>
                <hr>
                @if(!is_null($hierarki->first()))
                    @csrf
                    @foreach ($hierarki as $idx => $item)
                        @if(!is_null($item->renstraTujuan->first()))
                            <div class="card mt-3">
                                <div class="card-header bg-primary text-light" style="position: relative; z-index: 999;">
                                    <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                                        <a href="#" class="info_btn">
                                            <i class="fa fa-info-circle"></i>
                                        </a>&emsp;
                                        <i class="fa fa-angle-double-right"></i> &nbsp; Sasaran Perangkat Daerah : &nbsp; {{ $item->name }}
                                    </span>
                                </div>
                                <div class="alert show-hidden info_bar"
                                    style="display: flex; flex-direction: column" role="alert">
                                    <span class="hierarki_m">~ / Misi &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->renstraTujuan->rpjmdSasaran->tujuan->misi->misi }}
                                    </span>
                                    <span class="hierarki_m">~ RPJMD / Tujuan &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->renstraTujuan->rpjmdSasaran->tujuan->name }}
                                    </span>
                                    <span class="hierarki_m">~ RPJMD / Sasaran &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->renstraTujuan->rpjmdSasaran->name }}
                                    </span>
                                    {{-- @dd($item->renstraTujuan->rpjmdSasaran->prioritasPembangunan) --}}
                                    <span class="hierarki_m">~ RPJMD / Prioritas Pembangunan &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>
                                        @if(!is_null($item->renstraTujuan->rpjmdSasaran->prioritasPembangunan) && !is_null($item->renstraTujuan->rpjmdSasaran->prioritasPembangunan->mstPrioritasPembangunan))
                                            {{ $item->renstraTujuan->rpjmdSasaran->prioritasPembangunan->mstPrioritasPembangunan->name }}
                                        @else
                                            <span class="text-warning">Prioritas Pembangunan belum ditetapkan! &nbsp; klik
                                            <a href="{{ url('') }}/rpjmd/prioritas-pembangunan" class="text-info">disini</a> untuk menetapkan</span>
                                        @endif
                                    </span>
                                    <span class="hierarki_m">~ / Perangkat Daerah &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->renstraTujuan->opd->name }}
                                    </span>
                                    <span class="hierarki_m">~ Renstra / Tujuan &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->renstraTujuan->name }}
                                    </span>
                                    <span class="hierarki_m">~ Renstra / Sasaran &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->name }}
                                    </span>
                                </div>
                                <div class="card-body">
                                    <div class="card">
                                        <div class="card-header">
                                            <span>Strategi  PD</span>
                                        </div>
                                        <div class="card-body">
                                            <input type="number" value="{{ $item->id }}" name="sasaran_{{ $idx }}" hidden>

                                            @if(!is_null($item->renstraStrategi->first()))
                                                @foreach ($item->renstraStrategi as $val)
                                                <div class="duplicate">
                                                    <div class="form-group my-2">
                                                        <div class="input-wrapper {{ $loop->iteration > 1 ? 'd-flex' : '' }} input-group input-group-outline">
                                                            <input type="text" id="strategi{{ $loop->iteration }}" class="form-control" value="{{ $val->name }}" name="strategi_{{ $idx }}[]" autocomplete="off">
                                                            @if($loop->iteration > 1)
                                                            <button class="btn btn-danger btn-sm ml-2 close-clone"><i class="fa fa-close"></i></button>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                @else
                                                <div class="duplicate my-2">
                                                    <div class="form-group mt-2">
                                                        <div class="input-wrapper input-group input-group-outline">
                                                            <input type="text" id="strategi{{ $loop->iteration }}" class="form-control" name="strategi_{{ $idx }}[]" autocomplete="off">
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="clone-wrapper">
                                            </div>
                                            <button type="button" class="clone-strategi btn btn-primary btn-sm">
                                                <i class="fa fa-plus"></i> Tambah Poin</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            @if($loop->iteration == 1)
                            <div class="alert alert-warning" role="alert">
                                Isi Terlebih Dahulu Renstra Tujuan! <br> Klik <a href="{{ url('') }}/renstra/sasaran/add">disini </a>untuk mengisi
                            </div>
                            @endif
                        @endif
                    @endforeach
                @else
                <div class="alert alert-warning" role="alert">
                    Tidak Ada Data
                </div>
                @endif
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="" class="btn btn-dark">Cancel</a>
            </div>
        </form>
        @endif
    </div>
</div>
@endsection

@section('scripts')
    <script>
        // create and get hierarki
        getInfoHierarki('.info_btn');
        function getInfoHierarki(btn) {
            let info = document.querySelectorAll(btn);
            info.forEach(res => {
                res.addEventListener('click', () => {
                    const card = res.closest('.card')
                    const card_header = card.querySelector('.card-header')
                    const info_bar = card_header.nextElementSibling
                    const card_body = card.querySelector('.card-body')
                    const height_info_bar = info_bar.offsetHeight

                    info_bar.classList.toggle('show-hidden')
                    card_body.style.transition = '.3s';
                    if (!info_bar.classList.contains('show-hidden')) {
                        card_body.style.marginTop = `${height_info_bar - 10}px`;
                    } else {
                        card_body.style.marginTop = `0`;
                    }
                })
            })
        }

        function clone() {
            const btn = document.querySelectorAll('.clone-strategi');
            btn.forEach(res => {
                res.addEventListener('click', function() {
                    let parent = this.parentElement;
                    let clone = parent.querySelector('.duplicate')
                        .cloneNode(true);

                    // clean el validate
                    const el_validate = clone.querySelectorAll('.validate');
                    el_validate.forEach(res => res.remove());

                    const input = clone.querySelector('input');
                    input.style.border = '1px solid #aaa';
                    const clone_wrapper = parent.querySelector('.clone-wrapper');

                    input.value = '';
                    clone.querySelector('.input-wrapper').classList.add('d-flex');
                    const fix_el_clone = addBtnClose(clone);
                    clone_wrapper.append(fix_el_clone);
                });
            });
        }
        clone();

        function addBtnClose(el)
        {
            const btn = document.createElement('button');
            const i = document.createElement('i');

            btn.classList.add('btn', 'btn-danger', 'btn-sm', 'ml-2', 'close-clone');
            i.classList.add('fa', 'fa-close');

            btn.append(i);
            el.querySelector('.input-wrapper').append(btn);

            return el;
        }

        closeElClone();
        function closeElClone()
        {
            document.addEventListener('click', function(e) {
                const target = e.target.classList;
                if(target.contains('close-clone') || target.contains('fa-close')) {
                    e.target.closest('.input-group').remove();
                }
            });
        }

        //validasi input

        inputValidate('.add-renstra-strategi', ['input'])
    </script>
@endsection
