@extends('layouts.app')
@section('title')
    {{ $title }}
@endsection
@section('content')
@include('renstra.program.parts.modal')
    <div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
        <span class="brand ml-4">Renstra Program</span>
        <nav class="mr-4">
            <a class="breadcrumb-item" href="/">Dashboard</a>
            <a class="breadcrumb-item" href="#">Renstra</a>
            <span class="breadcrumb-item" style="color: #000;">Program</span>
        </nav>
    </div>
    <style media="screen">
        .info_bar {
            position: absolute;
            top: 50px;
            z-index: 998;
            border-radius: 0;
            width: 100%;
            background-color: rgb(41, 77, 100);
        }

        #card-body-info {
            transition: .3s;
        }

        .relative-info {
            transition: .3s;
        }

    </style>

    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection',[
            'opds' => $opd,
            'selectedOpd' => $selected_opd,
            'redirectUrl' => route('renstra.program.program.index')
        ])
        @include('partials.error-message')
        @include('partials.message')
        <div class="card mb-4">
            @if (!is_null(periode()))
                <div class="card-body">
                    <div class="card-header">
                        <h6 class="card-title">Renstra Program Tingkat Kabupaten Periode {{ periode()->dari }} -
                            {{ periode()->sampai }}</h6>
                        <button class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalCenter">
                            <i class="fa fa-plus"></i> Tambah
                        </button>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success flashMessages" role="alert">
                            {{ $message }}
                        </div>
                    @endif
                    <hr>

                    @foreach ($hierarki as $item)
                        @if (!is_null($item->programRenstra->first()))
                            <div class="card mt-3">
                                <div class="card-header bg-primary text-light">
                                    <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                                        <a href="#" class="info_btn">
                                            <i class="fa fa-info-circle"></i>
                                        </a>&emsp;
                                        <i class="fa fa-angle-double-right"></i> &nbsp; Sasaran Perangkat Daerah : &nbsp;
                                        {{ $item->name }}
                                    </span>
                                </div>

                                @php
                                    $prioritas_pembangunan = $item->renstraTujuan->rpjmdSasaran->prioritasPembangunan;
                                @endphp
                                <div class="alert show-hidden info_bar" style="display: flex; flex-direction: column"
                                    role="alert">
                                    <span class="hierarki_m">~ / Misi &nbsp;
                                        <i
                                            class="fa fa-caret-right">&emsp;</i>{{ $item->renstraTujuan->rpjmdSasaran->tujuan->misi->misi }}
                                    </span>
                                    <span class="hierarki_m">~ RPJMD / Tujuan &nbsp;
                                        <i
                                            class="fa fa-caret-right">&emsp;</i>{{ $item->renstraTujuan->rpjmdSasaran->tujuan->name }}
                                    </span>
                                    <span class="hierarki_m">~ RPJMD / Sasaran &nbsp;
                                        <i
                                            class="fa fa-caret-right">&emsp;</i>{{ $item->renstraTujuan->rpjmdSasaran->name }}
                                    </span>
                                    <span class="hierarki_m">~ RPJMD / Prioritas Pembangunan &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>
                                        @if (!is_null($prioritas_pembangunan) && !is_null($prioritas_pembangunan->mstPrioritasPembangunan))
                                            {{ $prioritas_pembangunan->mstPrioritasPembangunan->name }}
                                        @else
                                            <span class="text-warning">Prioritas Pembangunan belum ditetapkan! &nbsp; klik
                                                <a href="{{ url('') }}/rpjmd/prioritas-pembangunan"
                                                    class="text-info">disini</a> untuk menetapkan</span>
                                        @endif
                                    </span>
                                    <span class="hierarki_m">~ / Perangkat Daerah &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->renstraTujuan->opd->name }}
                                    </span>
                                    <span class="hierarki_m">~ Renstra / Tujuan &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->renstraTujuan->name }}
                                    </span>
                                    <span class="hierarki_m">~ Renstra / Sasaran &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->name }}
                                    </span>
                                </div>

                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered " width="100%"
                                            id="dtHorizontalExample">
                                            <tr>
                                                <th>Program</th>
                                                @foreach ($periode_per_tahun as $val)
                                                    <th>Anggaran {{ $val->tahun }}</th>
                                                @endforeach
                                                <th>Anggaran Renja</th>
                                                <th>Sumber Anggaran</th>
                                                <th>Aksi</th>
                                            </tr>
                                            @foreach ($item->programRenstra as $val)
                                                <tr>
                                                    <td>{{ $val->programRpjmd->mstProgram->name }}</td>
                                                    @foreach ($periode_per_tahun as $th)
                                                        @php $anggaran = $val->anggaranRenstra->firstWhere('id_periode_per_tahun', $th->id) @endphp
                                                        <td>{{ $anggaran->nilai ?? 0 }}</td>
                                                    @endforeach
                                                    <td>
                                                        <a href="#" data-bs-toggle="modal"
                                                            data-bs-target="#anggaranRenja{{ $val->id }}">Selengkapnya</a>
                                                    </td>
                                                    <td>
                                                        <a href="#">Selengkapnya</a>
                                                    </td>
                                                    <td>
                                                        <a href="#" class="btn btn-warning btn-sm" data-bs-toggle="modal"
                                                            data-bs-target="#modalEdit{{ $val->id }}">Edit</a>
                                                        <form
                                                            action="{{ url('') }}/renstra/program/delete/{{ hashID($val->id) }}"
                                                            method="POST" class="d-inline">
                                                            @csrf
                                                            @method('delete')
                                                            <button class="btn btn-danger btn-sm" type="submit"
                                                                onclick="return confirm('Lanjukan menghapus data?')">Hapus</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                    @foreach ($item->programRenstra as $val)
                                        <!-- Modal edit -->
                                        <div class="modal fade" id="modalEdit{{ $val->id }}" tabindex="-1"
                                            role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <form
                                                        action="{{ url('') }}/renstra/program/edit/{{ hashID($val->id) }}"
                                                        method="POST">
                                                        @csrf
                                                        @method('patch')
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Tambah
                                                                Program</h5>
                                                            <button type="button" class="close"
                                                                data-bs-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="input-group mt-2 selectSasaran">
                                                                <label for="">Pilih Renstra Sasaran</label>
                                                                <select name="sasaran" class="sasaran">
                                                                    <option value=""></option>
                                                                    @foreach ($sasaran as $sr)
                                                                        <option value="{{ $sr->id }}"
                                                                            {{ $item->id == $sr->id ? 'selected' : '' }}>
                                                                            {{ $sr->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="input-group mt-2 selectProgram">
                                                                <label for="">Pilih Renstra Program</label>
                                                                <select name="program" class="programS">
                                                                    <option value=""></option>
                                                                    @foreach ($program as $pr)
                                                                        <option value="{{ $pr->id }}"
                                                                            {{ $pr->id == $val->id_rpjmd_program ? 'selected' : '' }}>
                                                                            {{ $pr->mstProgram->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <label for="">Sumber Anggaran</label>
                                                            <div class="duplicate">
                                                                <div class="input-group mt-2">
                                                                    <div class="input-wrapper">
                                                                        <select class="custom-select" id="inputGroupSelect01">
                                                                            <option selected>Choose...</option>
                                                                            <option value="1">One</option>
                                                                            <option value="2">Two</option>
                                                                            <option value="3">Three</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="clone-wrapper">
                                                            </div>
                                                            <button type="button" class="clone-program btn btn-primary btn-sm">
                                                                <i class="fa fa-plus"></i> Tambah Sumber Anggaran</button>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-primary">Submit</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Modal Anggaran Renja -->
                                        @include('partials.modal-table', [
                                            "title" => "Anggaran Renja",
                                            "modalId" => "anggaranRenja$val->id",
                                            "tableHeads" => [
                                                "Kegiatan",
                                                ...$periode_per_tahun->map(function($periode) {return $periode->tahun;}),
                                            ],
                                            "tableData" => [
                                                [
                                                    $val->programRpjmd->mstProgram->name,
                                                    ...$periode_per_tahun->map(function($periode) use($val){
                                                        $anggaranRenja = $val->anggaranRenja;
                                                        $selectedAnggaran = $anggaranRenja->firstWhere('id_periode_per_tahun', $periode->id);
                                                        return $selectedAnggaran->nilai;
                                                    })
                                                ]
                                            ]
                                        ])
                                    @endforeach

                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            @endif
        </div>
    </div>

    <!-- Modal add -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form action="{{ url('') }}/renstra/program/add" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Tambah Program</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="input-group mt-2" id="addSelectSasaran">
                            <select name="sasaran" class="sasaran">
                                <option value=""></option>
                                @foreach ($sasaran as $item)
                                    <option value="{{ $item->id }}">
                                        {{ $item->name }} | {{ $item->renstraTujuan->opd->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="input-group mt-2" id="addSelectProgram">
                            <select name="program" class="programS">
                                <option value=""></option>
                                @foreach ($program as $item)
                                    <option value="{{ $item->id }}">{{ $item->mstProgram->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        selectPlaceholder('#opd', 'Pilih PD');
        selectUsingParent('.sasaran', '#addSelectProgram', 'Pilih Renstra Sasaran');
        selectUsingParent('.programS', '#addSelectSasaran', 'Pilih Renstra Program');
        // create and get hierarki
        getInfoHierarki('.info_btn');

        function getInfoHierarki(btn) {
            let info = document.querySelectorAll(btn);
            info.forEach(res => {
                res.addEventListener('click', () => {
                    const card = res.closest('.card')
                    const card_header = card.querySelector('.card-header')
                    const info_bar = card_header.nextElementSibling
                    const card_body = card.querySelector('.card-body')
                    const height_info_bar = info_bar.offsetHeight

                    info_bar.classList.toggle('show-hidden')
                    card_body.style.transition = '.3s';
                    if (!info_bar.classList.contains('show-hidden')) {
                        card_body.style.marginTop = `${height_info_bar - 10}px`;
                    } else {
                        card_body.style.marginTop = `0`;
                    }
                });
            });
        }

        function clone() {
            const btn = document.querySelectorAll('.clone-program');
            btn.forEach(res => {
                res.addEventListener('click', function() {
                    let parent = this.parentElement;
                    let clone = parent.querySelector('.duplicate')
                        .cloneNode(true);

                    // clean el validate
                    const el_validate = clone.querySelectorAll('.validate');
                    el_validate.forEach(res => res.remove());

                    const input = clone.querySelector('select');
                    input.style.border = '1px solid #aaa';
                    const clone_wrapper = parent.querySelector('.clone-wrapper');

                    input.value = '';
                    clone.querySelector('.input-wrapper').classList.add('d-flex');
                    const fix_el_clone = addBtnClose(clone);
                    clone_wrapper.append(fix_el_clone);
                });
            });
        }
        clone();

        function addBtnClose(el)
        {
            const btn = document.createElement('button');
            const i = document.createElement('i');

            btn.classList.add('btn', 'btn-danger', 'btn-sm', 'ml-2', 'close-clone');
            i.classList.add('fa', 'fa-close');

            btn.append(i);
            el.querySelector('.input-wrapper').append(btn);

            return el;
        }

        closeElClone();
        function closeElClone()
        {
            document.addEventListener('click', function(e) {
                const target = e.target.classList;
                if(target.contains('close-clone') || target.contains('fa-close')) {
                    e.target.closest('.input-group mt-2').remove();
                }
            });
        }

    </script>
@endsection
