@extends('layouts.app')
@section('title')
    Form Tambah Renstra Sasaran program
@endsection
@section('content')
<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Renstra Sasaran Program</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Renstra</a>
        <span class="breadcrumb-item">Program</span>
        <span class="breadcrumb-item">Sasaran Program</span>
        <span class="breadcrumb-item" style="color: #000;">Tambah</span>
    </nav>
</div>

<style>
    button.badge {
        border: none;
        padding: 3px;
    }
</style>

@foreach ($data as $data)
<div class="container-fluid pb-2 mt-4">
    <div class="card">
        <div class="card-header">
            <span>Form Tambah Renstra Sasaran Program</span>
        </div>
        <form action="{{ url('') }}/renstra/program/sasaran-program/edit/{{ hashID($data->id) }}" method="POST" class="add_renstra_sasaran">
            @csrf
            @method('patch')
            <div class="card-body">
                <div class="input-group input-group-outline">
                    <div class="col-4">
                        <label for="opd">Perangkat Daerah</label>
                        <select name="opd" id="opd" class="opd">
                            <option value=""></option>
                            @foreach ($opd as $item)
                            <option value="{{ $item->id }}" {{ $data->id_opd == $item->id ? "selected" : "" }}>{{ $item->name }}</option> 
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="input-group input-group-outline">
                    <div class="col-4">
                        <label for="program">Program PD</label>
                        <select name="program" id="program" class="program">
                            <option value=""></option>
                            @foreach ($program as $item)
                            <option value="{{ $item->id }}" {{ $data->id_program_renstra == $item->id ? "selected" : "" }}>{{ $item->programRpjmd->mstProgram->name }} | (sasaran){{ $item->sasaranRenstra->name }}</option> 
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="input-group input-group-outline">
                    <div class="col-4">
                        <label for="sasaran_program">Sasaran Program</label>
                        <input type="text" id="sasaran_program" class="form-control" name="sasaran_program" value="{{ $data->name }}">
                    </div>
                </div>
                <div class="place-duplicate">
                    @foreach ($data->indikator as $indikator)
                    <div class="duplicate">
                        <div class="input-group input-group-outline close-wrapper" hidden>
                            <button class="close-duplicate badge badge-danger mt-4 ml-3 p-2">
                                <i class="fa fa-times"></i> Hapus Form
                            </button>
                        </div>
                        <div style="display: flex; align-items: center;">
                            <div class="input-group input-group-outline ml-3">
                                <label for="indikator">Indikator Program * : </label>
                                <input type="text" id="indikator" class="form-control" name="indikator[]" value="{{ $indikator->indikator }}">
                                @error('indikator.*')
                                <div class="text-danger">
                                    <small>Indikator sasaran tidak boleh kosong!</small>
                                </div>
                                @enderror
                            </div>
                            
                            <div class="input-group input-group-outline ml-2">
                                <label for="satuan">Satuan * : </label>
                                <select name="satuan[]" id="satuan" class="form-control satuan">
                                    <option value=""></option>
                                    @foreach ($satuan as $item)
                                    <option value="{{ $item->id }}" {{ $indikator->id_satuan == $item->id ? "selected" : "" }}>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                @error('satuan.*')
                                <div class="text-danger">
                                    <small>Tidak boleh kosong!</small>
                                </div>
                                @enderror
                            </div>

                            <div class="input-group input-group-outline ml-2">
                                <label for="pk" class="check pk">PK</label>
                                <div class="switch d-flex ml-2">
                                    <input type="checkbox" class="check form-control" {{ $indikator->is_pk > 0 ? "checked" : "" }} id="pk"/>
                                    <input type="text" name="pk[]" class="pk check" hidden>
                                </div>
                            </div>
                            <div class="input-group input-group-outline ml-2">
                                <label for="iku" class="check iku">IKU</label>
                                <div class="switch d-flex ml-2">
                                    <input type="checkbox" class="check form-control" {{ $indikator->is_iku > 0 ? "checked" : "" }} id="iku"/>
                                    <input type="text" name="iku[]" class="iku check" hidden>
                                </div>
                            </div>
                        </div>
                        <div style="display: flex; align-items: center;">
                            <div class="input-group input-group-outline px-3">
                                <label for="pengukuran">Cara Pengukuran * : </label><br>
                                <textarea name="cara_pengukuran[]" id="pengukuran" style="height: 80px" cols="80" rows="30">{{ $indikator->cara_pengukuran }}</textarea>
                                @error('pengukuran.*')
                                <div class="text-danger">
                                    <small>Cara pengukuran tidak boleh kosong!</small>
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div style="display: flex; align-items: center;">
                            <div class="input-group input-group-outline ml-3" style="width: 150px">
                                <label for="awal">Kondisi Awal * : </label>
                                <input type="text" id="awal" class="form-control" name="awal[]" value="{{ $indikator->awal }}">
                            </div>
                            @foreach($periode_per_tahun as $idx => $item)
                            <div class="input-group input-group-outline ml-3" style="width: 120px">
                                <label for="target">Target {{ $item->tahun }} * : </label>
                                <input type="text" id="target" class="form-control" id="target{{ $loop->iteration }}" value="{{ $indikator->target[$idx]->target }}" name="target[]">
                            </div>
                            @endforeach
                            <div class="input-group input-group-outline ml-3" style="width: 150px">
                                <label for="akhir">Kondisi Akhir * : </label>
                                <input type="text" id="akhir" class="form-control" name="akhir[]" value="{{ $indikator->akhir }}">
                            </div>
                        </div>
                        <hr>
                    </div>
                    @endforeach
                </div>
                <div class="input-group input-group-outline row mt-3">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-success btn-sm duplicate-btn">
                            <i class="fa fa-plus"></i> Tambah Indikator</button>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-dark cancel" onclick="return window.history.back()" type="button">Cancel</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </form>
    </div>
</div>
@endforeach
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        checkboxValue();

        show_remove_btn('.close-wrapper', 'hidden');
        $('.duplicate-btn').click(() => {
            clone_el();
            show_remove_btn('.close-wrapper', 'hidden');
        })

        closeElementClone('close-duplicate', '.duplicate');

        // function show remove button after click duplicate-btn
        function show_remove_btn(el, attr) {
            const close_btn = document.querySelectorAll(el);
            close_btn.forEach((res, idx) => {
                if (idx > 0) {
                    res.removeAttribute(attr)
                }
            })
        }
        // end //

        // function clone element 
        const clone_el = function() {
            const el_duplicate = document.querySelector('.duplicate');
            const select_clone = el_duplicate.querySelectorAll('select.satuan');
            let span_select = el_duplicate.querySelectorAll('span.select2');
            span_select.forEach(res => res.remove());

            select_clone.forEach(res => {
                res.classList.remove('satuan');
                res.classList.add('satuan-cloned');
            })

            const clone = el_duplicate.cloneNode(true);
            document.querySelector('.place-duplicate').appendChild(clone);
            clean_el_clone(clone);
            selectPlaceholder('.satuan-cloned', 'Satuan');
        }
        // end //
        
        // clear value after clone
        const clean_el_clone = function(el) {
            let check = el.querySelectorAll('input[type=checkbox]');
            let input = el.querySelectorAll('input[type=text]');
            let option = el.querySelectorAll('option[selected]');
            
            let jml_clone = document.querySelectorAll('.duplicate').length;

            correspondingLabelInput(el, 'label.pk', 'input#pk', `pk_${jml_clone}`);
            correspondingLabelInput(el, 'label.iku', 'input#iku', `iku_${jml_clone}`);
            
            option.forEach(res => res.removeAttribute('selected'));
            check.forEach(res => res.checked = false)
            input.forEach(res => (!res.classList.contains('check')) ? res.value = '' : checkboxValue())
            el.querySelector('textarea').value = '';
        }
        // end //
       
        // create for label and id input (matching)
        const correspondingLabelInput = function(base_el, label_el, input_el, str) {
            const _label = base_el.querySelector(label_el);
            const _input = base_el.querySelector(input_el);

            _label.removeAttribute('for');
            _input.removeAttribute('id');

            _label.setAttribute('for', str);
            _input.setAttribute('id', str);
        }
        // end //

        // remove element clone
        function closeElementClone(c_btn, c_parent) {
            document.addEventListener('click', function(e) {
                if (e.target.classList.contains(c_btn) || e.target.classList.contains('fa-close')) {
                    e.preventDefault();
                    this.activeElement.closest(c_parent).remove();
                }
            })
        }
        // end //

        //validate
        const form = '.add_renstra_sasaran';
        inputValidate(form, ['input', 'select', 'textarea']);

        // select2 using placeholder
        selectPlaceholder('.program', 'Pilih Program')
        selectPlaceholder('.satuan', "Satuan")
        selectPlaceholder('.opd', "Pilih PD")
        // end //
    });
</script>
@endsection