@extends('layouts.app')
@section('title')
    {{ $title }}
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Sasaran Program",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "Renstra", "href" => "#"],
            [ "label" => "Program", "href" => "#"],
            [ "label" => "Sasaran Program", "href" => "#"],
        ]
    ])
    <style media="screen">
        .info_bar {
            position: absolute;
            top: 50px;
            z-index: 998;
            border-radius: 0;
            width: 100%;
            background-image: linear-gradient(195deg, #42424a 0%, #191919 100%);
        }

        #card-body-info {
            transition: .3s;
        }

        .relative-info {
            transition: .3s;
        }

    </style>
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection',[
            'opds' => $opd,
            'selectedOpd' => $opd_selected,
            'redirectUrl' => route('renstra.program.sasaran_program.index')
        ])
        @include('partials.error-message')
        @include('partials.message')
        <div class="card">
            <div class="card-header">
                <h6 class="card-title">Renstra Sasaran Program & Indikator Program PD</h6>
                <a href="{{ url('') }}/renstra/program/sasaran-program/add" class="btn btn-primary btn-sm" style="background-color: #4caf50;">
                    <i class="fa fa-plus"></i>
                    <span>Tambah</span>
                </a>
            </div>
            <div class="card-body">
            @if(!is_null($hierarki->first()))
                @csrf
                    @if (!is_null($hierarki->first()))
                        @csrf
                        @foreach ($hierarki as $idx => $item)
                            @php $periodePerTahun = $item->getPeriode()->periodePerTahun @endphp
                            @if (!is_null($item->first()))
                                <div class="card mt-3">
                                    <div class="card-header bg-primary text-light"
                                        style="position: relative; z-index: 999;">
                                        <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                                            <a href="#" class="info_btn">
                                                <i class="fa fa-info-circle"></i>
                                            </a>&emsp;
                                            <i class="fa fa-angle-double-right"></i> &nbsp; Sasaran Program PD : &nbsp;
                                            {{ $item->name }}
                                        </span>
                                        <!-- button aksi -->
                                        <div class="aksi">
                                            <!-- button delete -->
                                            <form
                                                action="{{ url('') }}/renstra/program/sasaran-program/delete/{{ hashID($item->id) }}"
                                                method="POST" class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button class="btn btn-danger btn-sm" type="submit"
                                                    onclick="return confirm('Lanjukan menghapus data?')"
                                                    title="Hapus">
                                                    Hapus
                                                </button>
                                            </form>

                                            <!-- button update -->
                                            <form
                                                action="{{ url('') }}/renstra/program/sasaran-program/edit/{{ hashID($item->id) }}"
                                                method="POST" class="d-inline">
                                                @csrf
                                                <button class="btn btn-warning btn-sm" title="Edit">
                                                    Edit
                                                </button>
                                            </form>
                                        </div>
                                        <!-- end aksi -->
                                    </div>
                                    @php
                                        $obj = $item->program->sasaranRenstra->renstraTujuan->rpjmdSasaran;
                                    @endphp
                                    <div class="alert show-hidden info_bar" style="display: flex; flex-direction: column" role="alert">
                                        <span class="hierarki_m">~ / Misi &nbsp;
                                            <i
                                                class="fa fa-caret-right">&emsp;</i>{{ $obj->tujuan->misi->misi }}
                                        </span>
                                        <span class="hierarki_m">~ RPJMD / Tujuan &nbsp;
                                            <i
                                                class="fa fa-caret-right">&emsp;</i>{{ $obj->tujuan->name }}
                                        </span>
                                        <span class="hierarki_m">~ RPJMD / Sasaran &nbsp;
                                            <i class="fa fa-caret-right">&emsp;</i>{{ $obj->name }}
                                        </span>
                                        <span class="hierarki_m">~ RPJMD / Prioritas Pembangunan &nbsp;
                                            <i class="fa fa-caret-right">&emsp;</i>
                                            @if (!is_null($obj->prioritasPembangunan) && !is_null($obj->prioritasPembangunan->mstPrioritasPembangunan))
                                                {{ $obj->prioritasPembangunan->mstPrioritasPembangunan->name }}
                                            @else
                                                <span class="text-warning">Prioritas Pembangunan belum ditetapkan! &nbsp;
                                                    klik
                                                    <a href="{{ url('') }}/rpjmd/prioritas-pembangunan"
                                                        class="text-info">disini</a> untuk menetapkan</span>
                                            @endif
                                        </span>
                                        <span class="hierarki_m">~ / Perangkat Daerah &nbsp;
                                            <i class="fa fa-caret-right">&emsp;</i>{{ $item->opd->name }}
                                        </span>
                                        <span class="hierarki_m">~ Renstra / Tujuan &nbsp;
                                            <i class="fa fa-caret-right">&emsp;</i>{{ $item->program->sasaranRenstra->renstraTujuan->name }}
                                        </span>
                                        <span class="hierarki_m">~ Renstra / Sasaran &nbsp;
                                            <i class="fa fa-caret-right">&emsp;</i>{{ $item->program->sasaranRenstra->name }}
                                        </span>
                                        <span class="hierarki_m">~ Renstra / Program &nbsp;
                                            <i class="fa fa-caret-right">&emsp;</i>{{ $item->program->programRpjmd->mstProgram->name }}
                                        </span>
                                    </div>

                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-responsive">
                                            <thead>
                                                <tr>
                                                    <th>Indikator Program</th>
                                                    <th>Satuan</th>
                                                    <th>PK</th>
                                                    <th>IKU</th>
                                                    <th>Cara Pengukuran</th>
                                                    <th>Kondisi Awal</th>
                                                    @foreach ($periode_per_tahun as $tahun)
                                                        <th width="125" class="text-center">Target
                                                            {{ $tahun->tahun }}</th>
                                                    @endforeach
                                                    <th>Kondisi Akhir</th>
                                                    <th>Target Renja</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($item->indikator->where('id_sasaran_program', $item->id) as $idx => $itemIndikator)
                                                    <tr>
                                                        <td>{{ $itemIndikator->indikator }}</td>
                                                        <td>{{ $itemIndikator->satuan->name }}</td>
                                                        <td>{!! $itemIndikator->is_pk > 0 ? '&check;' : '&times;' !!}</td>
                                                        <td>{!! $itemIndikator->is_iku > 0 ? '&check;' : '&times' !!}</td>
                                                        <td>
                                                            <a href="#" data-bs-toggle="modal" data-bs-target="#caraPengukuran{{ $itemIndikator->id }}">
                                                                Selengkapnya
                                                                </a>
                                                        </td>
                                                        <td>{{ $itemIndikator->awal }}</td>
                                                        @foreach ($itemIndikator->target as $itemTarget)
                                                            <td class="text-center">
                                                                {{ $itemTarget->target }}</td>
                                                        @endforeach
                                                        <td>{{ $itemIndikator->akhir }}</td>
                                                        <td>
                                                            <button class="btn btn-primary btn-sm" data-bs-toggle="modal"
                                                                data-bs-target="#targetRenja{{ $itemIndikator->id }}">
                                                                <i class="fa fa-plus"></i> Tambah Target
                                                            </button>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                        
                                        {{-- //Modal --}}
                                        @foreach ($item->indikator->where('id_sasaran_program', $item->id) as $idx => $indikator)
                                            <div class="modal" tabindex="-1" role="dialog"
                                                id="targetRenja{{ $indikator->id }}">
                                                <div class="modal-dialog" role="document" style="max-width:800px">
                                                    <div class="modal-content">
                                                        <form
                                                            action="{{ route('renstra.program.sasaran_program.indikator.renja', [
                                                                'sasaranProgram' => $item->id,
                                                                'indikator' => $indikator->id,
                                                            ]) }}"
                                                            method="POST">
                                                            @csrf
                                                            <div class="modal-header">
                                                                <h5 class="modal-title">Target Renja Indikator Sasaran</h5>
                                                                <button type="button" class="close" data-bs-dismiss="modal"
                                                                    aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="table-responsive">
                                                                    <table class="table table-bordered">
                                                                    <thead>
                                                                        <tr>
                                                                            <th rowspan="2">Indikator Sasaran</th>
                                                                            <th colspan="{{ $periodePerTahun->count() }}">Target Renja
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            @foreach ($periodePerTahun as $periode)
                                                                                <th>{{ $periode->tahun }}</th>
                                                                            @endforeach
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>{{ $indikator->indikator }}</td>
                                                                            @foreach ($periodePerTahun as $periode)
                                                                                @php $targetRenja = $indikator->targetRenja->firstWhere('id_periode_per_tahun', $periode->id); @endphp
                                                                                <input type="hidden"
                                                                                    name="renja[{{ $loop->index }}][id_tahun]"
                                                                                    value="{{ $periode->id }}">
                                                                                <td><input type="text" style="max-width:70px"
                                                                                        name="renja[{{ $loop->index }}][value]"
                                                                                        {{ $targetRenja != null ? "value=$targetRenja->nilai" : '' }} />
                                                                                </td>
                                                                            @endforeach
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="modal-footer justify-content-start">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Cancel</button>
                                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal fade" id="caraPengukuran{{ $itemIndikator->id }}"
                                                tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                                aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Detail
                                                                Cara Pengukuran</h5>
                                                            <button type="button" class="close"
                                                                data-bs-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <span>
                                                                Indikator Sasaran : <br />
                                                                {{ $itemIndikator->indikator }}
                                                            </span>
                                                            <br>
                                                            <br>
                                                            <span>
                                                                Cara Pengukuran : <br />
                                                                {{ $itemIndikator->cara_pengukuran }}
                                                            </span>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary"
                                                                data-bs-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    @else
                        <div class="alert alert-warning" role="alert">
                            Tidak ada data!
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        // create and get hierarki
        function getInfoHierarki(btn) {
            let info = document.querySelectorAll(btn);
            info.forEach(res => {
                res.addEventListener('click', () => {
                    const card = res.closest('.card')
                    const card_header = card.querySelector('.card-header')
                    const info_bar = card_header.nextElementSibling
                    const card_body = card.querySelector('.card-body')
                    const height_info_bar = info_bar.offsetHeight

                    info_bar.classList.toggle('show-hidden')
                    card_body.style.transition = '.3s';
                    if (!info_bar.classList.contains('show-hidden')) {
                        card_body.style.marginTop = `${height_info_bar - 10}px`;
                    } else {
                        card_body.style.marginTop = `0`;
                    }
                })
            })
        }
        getInfoHierarki('.info_btn');
    </script>
@endsection
