<div class="modal fade" id="tambah-target" tabindex="-1" aria-labelledby="tambah-anggaran" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tambah-anggaran1">Target Renja Indikator Program</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th rowspan="2">Indikator Program</th>
                                <th colspan="5">Anggaran Renja</th>
                            </tr>
                            <tr>
                                <th class="text-center">2020</th>
                                <th class="text-center">2021</th>
                                <th class="text-center">2022</th>
                                <th class="text-center">2023</th>
                                <th class="text-center">2024</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Indikator 1</td>
                                <td class="text-center"><input type="text" class="form-control" id=""></td>
                                <td class="text-center"><input type="text" class="form-control" id=""></td>
                                <td class="text-center"><input type="text" class="form-control" id=""></td>
                                <td class="text-center"><input type="text" class="form-control" id=""></td>
                                <td class="text-center"><input type="text" class="form-control" id=""></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>