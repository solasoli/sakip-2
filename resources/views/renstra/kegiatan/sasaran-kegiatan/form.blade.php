<div class="card">
    <div class="card-header">
        <span>{{$headline}}</span>
    </div>

    @php
        $isEdit = isset($kegiatan);
        $url = $isEdit ? route('renstra.kegiatan.sasaran_kegiatan.update', [ "sasaranKegiatanRenstra" => $kegiatan->id ]) : route('renstra.kegiatan.sasaran_kegiatan.store');
    @endphp
    <form action="{{$url}}" method="POST" x-data="form">
        @csrf
        @if ($isEdit)
            @method('PUT')
        @endif

        <div class="card-body">
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="opd">Perangkat Daerah</label>
                    <select name="opd" id="opd" class="opd form-control" x-model="selectedOpd">
                        @foreach ($opds as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="opd">Kegiatan PD</label>
                    <select name="kegiatan" class="form-control" id="kegiatan"></select>
                </div>
            </div>
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="sasaran_kegiatan">Sasaran Kegiatan</label>
                    <input
                        x-model="sasaranKegiatan"
                        type="text"
                        id="sasaran"
                        class="form-control"
                        name="sasaran">
                </div>
            </div>
            <template x-for="(indikatorItem, index) in indikator">
                <div>
                    <input type="hidden" :name="`indikator[${index}][id]`" :value="indikatorItem.id">
                    <template x-if="index != 0">
                        <div class="input-group input-group-outline close-wrapper">
                            <button @click.prevent="hapusIndikator(index)" class="close-duplicate btn btn-primary mt-4 ml-3 p-2">
                                <i class="fa fa-times"></i> Hapus Form
                            </button>
                        </div>
                    </template>
                    <div class="place-duplicate">
                        <div class="duplicate">
                            <div style="display: flex; align-items: center;">
                                <div class="input-group ml-3">
                                    <label for="indikator">Indikator sasaran * : </label>
                                    <div class="input-group input-group-outline">
                                        <input type="text" id="indikator" class="form-control" :name="`indikator[${index}][nama]`">
                                        @error('indikator.*')
                                        <div class="text-danger">
                                            <small>Indikator sasaran tidak boleh kosong!</small>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="input-group ml-2">
                                    <label :for="`satuan_${index}`" class="block">Satuan * : </label>
                                        <select :id="`satuan_${index}`" class="form-control" :name="`indikator[${index}][satuan]`">
                                            <template x-for="satuanItem in satuan">
                                                <option :value="satuanItem.id"><span x-text="satuanItem.name"></span></option>
                                            </template>
                                        </select>
                                </div>
                                <div class="input-group input-group-outline form-check ml-3" style="width: 100px">
                                    <label class="check pk ml-2" for="pk">PK</label>
                                    <div class="switch d-flex ml-2">
                                        <input type="checkbox" x-model="indikatorItem.is_pk" class="check form-control form-check-input" :id="`pk_${index}`" :name="`indikator[${index}][is_pk]`"/>
                                        <input type="text" class="pk check" hidden>
                                    </div>
                                </div>
                                <div class="input-group input-group-outline form-check ml-3" style="width: 120px">
                                    <label class="check iku ml-2" for="iku">IKU</label>
                                    <div class="switch d-flex ml-2">
                                        <input type="checkbox" x-model="indikatorItem.is_iku" class="check form-control form-check-input" :id="`iku_${index}`" :name="`indikator[${index}][is_iku]`"/>
                                        <input type="text"  class="iku check" hidden>
                                    </div>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>

                    <div style="display: flex; align-items: center;">
                        <div class="input-group input-group-outline px-3">
                            <label for="pengukuran">Cara Pengukuran * : </label><br>
                            <textarea x-model="indikatorItem.cara_pengukuran" style="height: 80px" cols="80" rows="30" :name="`indikator[${index}][cara_pengukuran]`"></textarea>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <div style="display: flex; align-items: center;">
                            <div class="input-group ml-3" style="width: 150px">
                                <label for="awal">Kondisi Awal * : </label>
                                    <div class="input-group input-group-outline">
                                        <input type="text" class="form-control" x-model="indikatorItem.target_awal":name="`indikator[${index}][target_awal]`" >
                                    </div>

                            </div>
                            <template x-for="(tahun, indexTahun) in indikatorItem.target_pertahun">
                                <div class="input-group ml-3" style="width: 120px">
                                    <label for="target"><span x-text="`Target ${tahun.label} * :`"></span></label>
                                    <input type="hidden" :name="`indikator[${index}][target][${indexTahun}][id]`" :value="tahun.id">
                                    <input type="hidden" :name="`indikator[${index}][target][${indexTahun}][id_tahun]`" :value="tahun.id_tahun">
                                        <div class="input-group input-group-outline">
                                            <input type="text" id="target" class="form-control" x-model="tahun.value" :name="`indikator[${index}][target][${indexTahun}][value]`"/>
                                        </div>

                                </div>
                            </template>
                            <div class="input-group  ml-3" style="width: 150px">
                                <label for="akhir">Kondisi Akhir * : </label>
                                 <div class="input-group input-group-outline">
                                    <input type="text" class="form-control" x-model="indikatorItem.target_akhir" :name="`indikator[${index}][target_akhir]`"/>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </template>
            <template x-if="selectedKegiatan != null">
                <div class="input-group input-group-outline close-wrapper">
                    <button @click.prevent="tambahIndikator()" class="close-duplicate badge badge-success mt-4 ml-3 p-2" style="background-color: #4caf50;">
                        <!-- <i class="fa fa-times"></i> --> + Tambah Indikator
                    </button>
                </div>
            </template>
        </div>
        <div class="card-footer">
            <button class="btn btn-dark cancel" onclick="return window.history.back()" type="button">Cancel</button>
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
</div>

<script>
    document.addEventListener('alpine:init', () => {
        Alpine.data('form',() => {
            return {
                selectedOpd: null,
                selectedKegiatan: null,
                selectedKegiatanPeriodeTahun: [],
                sasaranKegiatan: '{{$isEdit ? $kegiatan->name : ''}}',
                indikator:[],
                fetchedKegiatan: [],
                satuan: @json($satuan),
                init() {
                    this.initializeOpd()
                    this.initializeKegiatan()
                },
                tambahIndikator(prefilledIndikator){
                    this.indikator.push( prefilledIndikator || {
                        id: null,
                        cara_pengukuran: '',
                        nama: '',
                        satuan: 1,
                        is_pk: false,
                        is_iku: false,
                        target_awal: null,
                        target_akhir: null,
                        target_pertahun: this.selectedKegiatanPeriodeTahun.map(tahun => {
                            return {
                                id: null,
                                id_tahun: tahun.id,
                                label: tahun.tahun,
                                value: null
                            }
                        })
                    });

                    const latestIndex = this.indikator.length - 1

                    this.$nextTick(() => {
                        const satuanSelect = $(`#satuan_${latestIndex}`).select2()

                        satuanSelect.val(this.indikator[latestIndex].satuan).trigger("change");

                        satuanSelect.on('select2:select', (event) => {
                            this.indikator[latestIndex].satuan = event.target.value
                        })

                        // this watcher make error
                        // this.$watch(`indikator[${latestIndex}].satuan`, (value) => {
                        //     satuanSelect.val(value).trigger("change");
                        // });
                    })
                },
                hapusIndikator(index) {
                    this.indikator.splice(index, 1)
                },
                initializeOpd() {
                    const opdSelect = $('#opd').select2()

                    opdSelect.on('select2:select', (event) => {
                        this.selectedOpd = event.target.value
                        this.selectedKegiatan = null;
                        this.indikator = [];
                        this.selectedKegiatanPeriodeTahun = [];
                    })

                    this.$watch('selectedOpd', (value) => {
                        opdSelect.val(value).trigger("change");
                    });

                    @if($isEdit)
                        this.selectedOpd = {{$kegiatan->getPerangkatDaerah()->id}}
                    @else
                        this.selectedOpd = {{$opds->first()->id}}
                    @endIf
                },
                initializeKegiatan() {
                    const kegiatanSelect = $('#kegiatan').select2({
                        ajax:{
                            url: (params) => {
                                return `/api/opds/${this.selectedOpd}/renstra/kegiatan`;
                            },
                            dataType: 'json',
                            processResults: (data) => {
                                this.fetchedKegiatan = data.data.kegiatan
                                return {
                                    results: data.data.kegiatan.map(data => {
                                        return {
                                            text: data.mst_kegiatan.name,
                                            id: data.id
                                        }
                                    })
                                }
                            }
                        },
                    })

                    kegiatanDropdownSelected = (value, tambahIndikator = false) => {
                        this.selectedKegiatan = value
                        const selectedKegiatan = this.fetchedKegiatan.find(item => item.id == value)
                        this.selectedKegiatanPeriodeTahun = selectedKegiatan.tahun_periode
                        this.indikator = [];
                        if(tambahIndikator) this.tambahIndikator();
                    }

                    kegiatanSelect.on('select2:select', (event) => {
                        kegiatanDropdownSelected(event.target.value)
                    })

                    this.$watch('selectedKegiatan', (value) => {
                        kegiatanSelect.val(value).trigger("change");
                    });

                    @if($isEdit)
                        const option = new Option('{{ preg_replace('/\r/', '', $kegiatan->kegiatan->mstKegiatan->name)}}', {{$kegiatan->kegiatan->id}}, true, true);
                        kegiatanSelect.append(option).trigger('change');
                        kegiatanSelect.val({{$kegiatan->kegiatan->id}}).trigger("change");
                        this.fetchedKegiatan = [@json($transformedKegiatan)]
                        kegiatanDropdownSelected({{$kegiatan->kegiatan->id}})
                        @foreach ($kegiatan->indikator as $indikator)
                            @php
                                $targetPertahun = $indikator->target->map(function($target) {
                                    return [
                                        "id" => $target->id,
                                        "id_tahun" => $target->tahun->id,
                                        "label" => $target->tahun->tahun,
                                        "value" => $target->target
                                    ];
                                });
                            @endphp
                            this.tambahIndikator({
                                id: {{$indikator->id}},
                                cara_pengukuran: '{{$indikator->cara_pengukuran}}',
                                nama: '{{$indikator->name}}',
                                satuan: {{$indikator->id_satuan}},
                                is_pk: {{$indikator->is_pk ? 'true' : 'false'}},
                                is_iku: {{$indikator->is_iku ? 'true' :  'false'}},
                                target_awal: '{{$indikator->target_awal}}',
                                target_akhir: '{{$indikator->target_akhir}}',
                                target_pertahun: @json($targetPertahun)
                            })
                        @endforeach
                    @endif
                }
            }
        })
    })
</script>
