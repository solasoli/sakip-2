@extends('layouts.app')
@section('title')
    Sasaran Kegiatan
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Sasaran Kegiatan",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "Renstra", "href" => "#"],
            [ "label" => "Kegiatan", "href" => "#"],
            [ "label" => "Sasaran Kegiatan", "href" => "#"],
        ]
    ])
    <style media="screen">
        .info_bar {
            position: absolute;
            top: 5px;
            z-index: 998;
            border-radius: 0;
            width: 100%;
            background-color: rgb(41, 77, 100);
        }

        #card-body-info {
            transition: .3s;
        }

        .relative-info {
            transition: .3s;
        }

        ul {
            padding: 0;
            margin: 0;
            padding-left: 15px;
        }

    </style>
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection', [
            'redirectUrl' => route('renstra.kegiatan.sasaran_kegiatan.index')
        ])
        @include('partials.error-message')
        @include('partials.message')
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-header">
                    <h6 class="card-title">Data Sasaran & Indikator Kegiatan PD</h6>
                    <a href="{{ route('renstra.kegiatan.sasaran_kegiatan.create') }}" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i>
                        <span>Tambah</span>
                    </a>
                </div>
                <hr>
                @foreach ($sasaranKegiatan as $sasaran)
                    @php
                        $hierarchy = [
                            '/ Misi' => $sasaran->getMisi()->misi,
                            'RPJMD / Tujuan' => $sasaran->getRpjmdTujuan()->name,
                            'RPJMN / Sasaran' => $sasaran->getRpjmdSasaran()->name,
                            'RPJMD / Prioritas Pembangunan' => $sasaran->getPrioritasPembangunan()->name,
                            '/ Perangkat Daerah' => $sasaran->getPerangkatDaerah()->name,
                            'Renstra / Tujuan' => $sasaran->getRenstraTujuan()->name,
                            'Renstra / Sasaran' => $sasaran->getRenstraSasaran()->name,
                            'Renstra / Program' => $sasaran->getMstProgram()->name,
                            'Renstra / Kegiatan' => $sasaran->getMstKegiatan()->name,
                        ];
                        $title = 'Sasaran Kegiatan : ' . $sasaran->name;
                        $itemId = "sasaran$sasaran->id";
                        $periodePerTahun = $sasaran->getPeriode()->periodePerTahun;
                    @endphp
                    <x-hierarchy-card :title="$title" :hierarchy="$hierarchy" class="">
                        <x-slot name="action">
                            <div class="aksi">
                                <!-- button delete -->
                                <form
                                    action="{{ route('renstra.kegiatan.sasaran_kegiatan.destroy', ['kegiatan' => $sasaran->id]) }}"
                                    method="POST" class="d-inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger btn-sm" type="submit"
                                        onclick="return confirm('Lanjukan menghapus data?')" title="Hapus">
                                        <!-- <i class="fa fa-trash"></i> -->Hapus
                                    </button>
                                </form>

                                <!-- button update -->
                                <form action="{{route('renstra.kegiatan.sasaran_kegiatan.edit', ["kegiatan" => $sasaran->id])}}" method="GET"
                                    class="d-inline">
                                    <button class="btn btn-warning btn-sm" title="Edit">
                                    <!-- <i class="fa fa-pencil"></i> -->Edit
                                    </button>
                                </form>
                            </div>
                        </x-slot>

                        <div class="card-body text-center table-responsive">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <th>Indikator Kegiatan</th>
                                        <th>Satuan</th>
                                        <th>PK</th>
                                        <th>IKU</th>
                                        <th>Cara pengukuran</th>
                                        <th>Kondisi Awal</th>
                                        @foreach ($periodePerTahun as $periode)
                                            <th>Target {{ $periode->tahun }}</th>
                                        @endforeach
                                        <th>Kondisi Akhir</th>
                                        <th>Target Renja</th>
                                    </tr>
                                    @foreach ($sasaran->indikator as $indikator)
                                        <tr>
                                            <td>{{ $indikator->name }}</td>
                                            <td>{{ $indikator->satuan->name }}</td>
                                            <td>{{ $indikator->is_pk ? '✓' : '-' }}</td>
                                            <td>{{ $indikator->is_iku ? '✓' : '-' }}</td>
                                            <td>
                                                <a href="#" data-bs-toggle="modal"
                                                    data-bs-target="#caraPengukuran{{ $indikator->id }}"
                                                    style="text-decoration: none">
                                                    Selengkapnya
                                                </a>
                                            </td>
                                            <td>{{ $indikator->target_awal }}</td>
                                            @foreach ($periodePerTahun as $periode)
                                                @php $target = $indikator->target->firstWhere('id_periode_per_tahun', $periode->id); @endphp
                                                <th>{{ $target ? $target->target : '-' }}</th>
                                            @endforeach
                                            <td>{{ $indikator->target_akhir }}</td>
                                            <td><button class="btn btn-primary btn-sm" data-bs-toggle="modal"
                                                    data-bs-target="#targetRenja{{ $indikator->id }}"><i
                                                        class="fa fa-plus"></i> Tambah Target</button></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @foreach ($sasaran->indikator as $indikator)
                                <div class="modal fade" id="caraPengukuran{{ $indikator->id }}" tabindex="-1"
                                    role="dialog" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Detail
                                                    Cara Pengukuran</h5>
                                                <button type="button" class="close" data-bs-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div>
                                                    Indikator Kegiatan :
                                                </div>
                                                <div>
                                                    {{ $indikator->name }}
                                                </div>
                                                <br>
                                                <br>
                                                <span>
                                                    Cara Pengukuran : <br />
                                                    {{ $indikator->cara_pengukuran }}
                                                </span>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal" tabindex="-1" role="dialog"
                                    id="targetRenja{{ $indikator->id }}">
                                    <div class="modal-dialog" role="document" style="max-width:800px">
                                        <div class="modal-content">
                                            <form
                                                action="{{ route('renstra.kegiatan.sasaran_kegiatan.indikator.renja', [
                                                    'sasaranKegiatan' => $sasaran->id,
                                                    'indikator' => $indikator->id,
                                                ]) }}"
                                                method="POST">
                                                @csrf
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Target Renja Indikator Sasaran</h5>
                                                    <button type="button" class="close" data-bs-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th rowspan="2">Indikator Sasaran</th>
                                                                    <th colspan="{{ $periodePerTahun->count() }}">Target Renja
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    @foreach ($periodePerTahun as $periode)
                                                                        <th>{{ $periode->tahun }}</th>
                                                                    @endforeach
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td>{{ $indikator->name }}</td>
                                                                    @foreach ($periodePerTahun as $periode)
                                                                        @php $targetRenja = $indikator->targetRenja->firstWhere('id_periode_per_tahun', $periode->id); @endphp
                                                                        <input type="hidden"
                                                                            name="renja[{{ $loop->index }}][id_tahun]"
                                                                            value="{{ $periode->id }}">
                                                                        <td><input type="text" style="max-width:70px"
                                                                                name="renja[{{ $loop->index }}][value]"
                                                                                {{ $targetRenja != null ? "value=$targetRenja->nilai" : '' }} />
                                                                        </td>
                                                                    @endforeach
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer justify-content-start">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-bs-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </x-hierarchy-card>
                @endforeach
            </div>
        </div>
    </div>
@endsection
