@extends('layouts.app')
@section('title')
    Tambah Sasaran Kegiatan
@endsection
@section('content')
<div class="container-fluid pb-2 mt-4">

    @include('partials.error-message')

    @include('renstra.kegiatan.sasaran-kegiatan.form', [
        "headline" => "Form Tambah Sasaran Kegiatan PD"
    ])
</div>
@endsection
