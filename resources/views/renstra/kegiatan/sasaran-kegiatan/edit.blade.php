@extends('layouts.app')
@section('title')
    Ubah Sasaran Kegiatan
@endsection
@section('content')
<div class="container-fluid pb-2 mt-4">

    @if($errors->any())
        <div class="alert alert-danger">
            <p><strong>Opps Something went wrong</strong></p>
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
    @include('renstra.kegiatan.sasaran-kegiatan.form', [
        "headline" => "Form Ubah Sasaran Kegiatan PD"
    ])
</div>
@endsection
