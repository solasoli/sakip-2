@extends('layouts.app')
@section('title')
    Form Tambah Renstra Kegiatan
@endsection
@section('content')
<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Renstra Kegiatan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Renstra</a>
        <span class="breadcrumb-item">Kegiatan</span>
        <span class="breadcrumb-item">Kegiatan</span>
        <span class="breadcrumb-item" style="color: #000;">Edit</span>
    </nav>
</div>

<style>
    button.badge {
        border: none;
        padding: 3px;
    }
</style>

@foreach ($data as $data)
<div class="container-fluid pb-2 mt-4">
    <div class="card">
        <div class="card-header">
            <span>Form Tambah Renstra Kegiatan</span>
        </div>
        <form action="{{ url('') }}/renstra/kegiatan/kegiatan/edit/{{ $data->id }}" method="POST" class="add_renstra_kegiatan">
            @csrf
            @method('patch')
            <div class="card-body">
                <div class="col-4">
                    <label for="opd">Perangkat Daerah</label>
                    <div class="input-group">
                        <select name="opd" id="opd" class="opd">
                            <option value=""></option>
                            @foreach ($opd as $item)
                            <option value="{{ $item->id }}" {{ $data->id_mst_opd == $item->id ? "selected" : "" }}>{{ $item->name }}</option> 
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-4">
                    <label for="program">Program PD</label>
                    <div class="input-group">
                        <select name="program" id="program" class="program">
                            <option value=""></option>
                            @foreach ($program as $item)
                            <option value="{{ $item->id }}" {{ $data->id_renstra_program == $item->id ? "selected" : "" }}>{{ $item->programRpjmd->mstProgram->name }} | (sasaran){{ $item->sasaranRenstra->name }}</option> 
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-4">
                    <label for="kegiatan">Kegiatan PD</label>
                    <div class="input-group">
                        <select name="kegiatan" id="kegiatan" class="kegiatan">
                            <option value=""></option>
                            @foreach ($kegiatan as $item)
                            <option value="{{ $item->id }}" {{ $data->id_mst_kegiatan == $item->id ? "selected" : "" }}>{{ $item->name }}</option> 
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="place-duplicate">
                    @foreach ($data->sumberAnggaran as $sa)
                    <div class="duplicate">
                        <div class="col-4">
                            <label for="anggaran">Sumber Anggaran</label>
                            <div class="input-group">
                                <div class="select-wrapper d-flex">
                                    <select name="anggaran[]" id="anggaran" class="anggaran">
                                        <option value=""></option>
                                        @foreach ($anggaran as $item)
                                        <option value="{{ $item->id }}" {{ $sa->id_mst_sumber_anggaran == $item->id ? "selected" : "" }}>{{ $item->name }}</option> 
                                        @endforeach
                                    </select>
                                    <button class="btn btn-danger btn-sm ml-2 btn-close" type="button" {{ $loop->iteration > 1 ? "" : "hidden" }}>
                                        <i class="fa fa-close"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="input-group">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-success btn-sm duplicate-btn mt-2">
                            <i class="fa fa-plus"></i> Tambah Sumber Anggaran</button>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-dark cancel" onclick="return window.history.back()" type="button">Cancel</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </form>
    </div>
</div>
@endforeach
@endsection

@section('scripts')
    <script>
        selectPlaceholder('.opd', 'Pilih PD');
        selectPlaceholder('.program', 'Pilih Program PD');
        selectPlaceholder('.kegiatan', 'Pilih Kegiatan PD');
        selectPlaceholder('.anggaran', 'Pilih Sumber Anggaran');
        inputValidate('.add_renstra_kegiatan', ['select']);

        (function cloneEl() {
            const btn = document.querySelector('.duplicate-btn');
            btn.addEventListener('click', function() {
                const el = document.querySelector('.duplicate');
                const plc = document.querySelector('.place-duplicate');
                let clone = el.cloneNode(true);

                let el_fix = new cleanElClone(clone);
                plc.append(el_fix);
                selectPlaceholder('.anggaran-cloned', 'Pilih Sumber Anggaran');
            });
        })();

        function cleanElClone(clone) {
            clone.querySelector('span.select2').remove();
            const text_validate = clone.querySelectorAll('small')
                .forEach(res => res.remove());

            const selectSumberAnggaran = clone.querySelector('select.anggaran');
            selectSumberAnggaran.classList.add('anggaran-cloned');
            // remove attribute
            clone.querySelector('.btn-close').removeAttribute('hidden');
            clone.querySelector('option[selected]').removeAttribute('selected');

            return clone;
        }

        (function closeClone() {
            document.addEventListener('click', function(e) {
                if(e.target.classList.contains('btn-close') || e.target.classList.contains('fa-close')) {
                    this.activeElement.closest('.duplicate').remove();
                }
            });
        })();
    </script>
@endsection