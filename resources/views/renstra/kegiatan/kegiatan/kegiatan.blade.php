@extends('layouts.app')
@section('title')
    {{ $title }}
@endsection
@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
        <span class="brand ml-4">Renstra Kegiatan</span>
        <nav class="mr-4">
            <a class="breadcrumb-item" href="/">Dashboard</a>
            <a class="breadcrumb-item" href="#">Renstra</a>
            <a class="breadcrumb-item" href="#">Kegiatan</a>
            <span class="breadcrumb-item" style="color: #000;">Kegiatan</span>
        </nav>
    </div>
    <style media="screen">
        .info_bar {
            position: absolute;
            top: 50px;
            z-index: 998;
            border-radius: 0;
            width: 100%;
            background-color: rgb(41, 77, 100);
        }

        #card-body-info {
            transition: .3s;
        }

        .relative-info {
            transition: .3s;
        }

        ul {
            padding: 0;
            margin: 0;
            padding-left: 15px;
        }

    </style>

    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection',[
            'opds' => $opd,
            'selectedOpd' => $selected_opd,
            'redirectUrl' => route('renstra.kegiatan.index')
        ])

        @include('partials.error-message')
        @include('partials.message')

        <div class="card mb-4">
            @if (!is_null(periode()))
                <div class="card-body">
                    <div class="card-header">
                        <h6 class="card-title">Data Kegiatan Renstra PD</h6>
                        <a href="{{ url('') }}/renstra/kegiatan/kegiatan/add" class="btn btn-primary btn-sm">
                            <i class="fa fa-plus"></i> Tambah
                        </a>
                    </div>
                    <hr>

                    @foreach ($hierarki as $item)
                        @if (!is_null($item->kegiatanRenstra->first()))
                            <div class="card mt-3">
                                <div class="card-header bg-primary text-light">
                                    <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                                        <a href="#" class="info_btn">
                                            <i class="fa fa-info-circle"></i>
                                        </a>&emsp;
                                        <i class="fa fa-angle-double-right"></i> &nbsp; Program OPD : &nbsp;
                                        {{ $item->programRpjmd->mstProgram->name }}
                                    </span>
                                </div>

                                @php
                                    $prioritas_pembangunan = $item->sasaranRenstra->renstraTujuan->rpjmdSasaran->prioritasPembangunan;
                                    $other = $item->sasaranRenstra->renstraTujuan->rpjmdSasaran;
                                @endphp
                                <div class="alert show-hidden info_bar" style="display: flex; flex-direction: column"
                                    role="alert">
                                    <span class="hierarki_m">~ / Misi &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $other->tujuan->misi->misi }}
                                    </span>
                                    <span class="hierarki_m">~ RPJMD / Tujuan &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $other->tujuan->name }}
                                    </span>
                                    <span class="hierarki_m">~ RPJMD / Sasaran &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $other->name }}
                                    </span>
                                    <span class="hierarki_m">~ RPJMD / Prioritas Pembangunan &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>
                                        @if (!is_null($prioritas_pembangunan) && !is_null($prioritas_pembangunan->mstPrioritasPembangunan))
                                            {{ $prioritas_pembangunan->mstPrioritasPembangunan->name }}
                                        @else
                                            <span class="text-warning">Prioritas Pembangunan belum ditetapkan! &nbsp; klik
                                                <a href="{{ url('') }}/rpjmd/prioritas-pembangunan"
                                                    class="text-info">disini</a> untuk menetapkan</span>
                                        @endif
                                    </span>
                                    <span class="hierarki_m">~ / Perangkat Daerah &nbsp;
                                        <i
                                            class="fa fa-caret-right">&emsp;</i>{{ $item->sasaranRenstra->renstraTujuan->opd->name }}
                                    </span>
                                    <span class="hierarki_m">~ Renstra / Tujuan &nbsp;
                                        <i
                                            class="fa fa-caret-right">&emsp;</i>{{ $item->sasaranRenstra->renstraTujuan->name }}
                                    </span>
                                    <span class="hierarki_m">~ Renstra / Sasaran &nbsp;
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->sasaranRenstra->name }}
                                    </span>
                                    <span class="hierarki_m">~ Renstra / Program &nbsp;
                                        <i
                                            class="fa fa-caret-right">&emsp;</i>{{ $item->programRpjmd->mstProgram->name }}
                                    </span>
                                </div>

                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" width="100%"
                                            id="dtHorizontalExample">
                                            <tr>
                                                <th>Kegiatan</th>
                                                @foreach ($periode_per_tahun as $val)
                                                    <th>Anggaran {{ $val->tahun }}</th>
                                                @endforeach
                                                <th>Sumber Anggaran</th>
                                                <th>Anggaran Renja</th>
                                                <th>Aksi</th>
                                            </tr>
                                            @foreach ($item->kegiatanRenstra->where('is_deleted', 0) as $val)
                                                <tr>
                                                    <td>{{ $val->mstKegiatan->name }}</td>
                                                    @foreach ($periode_per_tahun as $th)
                                                        @php $anggaran = $val->anggaranRenstra->firstWhere('id_periode_per_tahun', $th->id) @endphp
                                                        <td>{{ $anggaran->nilai ?? 0 }}</td>
                                                    @endforeach
                                                    <td>
                                                        <a href="#" data-bs-toggle="modal"
                                                            data-bs-target="#sumberAnggaran{{ $val->id }}">Selengkapnya</a>
                                                    </td>
                                                    <td>
                                                        <a href="#" data-bs-toggle="modal"
                                                            data-bs-target="#anggaranRenja{{ $val->id }}">Selengkapnya</a>
                                                    </td>
                                                    <td>
                                                        <a href="{{ url('') }}/renstra/kegiatan/kegiatan/edit/{{ hashID($val->id) }}"
                                                            class="btn btn-warning btn-sm">Edit</a>
                                                        <form
                                                            action="{{ url('') }}/renstra/kegiatan/kegiatan/delete/{{ hashID($val->id) }}"
                                                            method="POST" class="d-inline">
                                                            @csrf
                                                            @method('delete')
                                                            <button class="btn btn-danger btn-sm" type="submit"
                                                                onclick="return confirm('Lanjukan menghapus data?')">Hapus</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>

                            @foreach ($item->kegiatanRenstra as $val)
                                <!-- Modal Sumber Anggaran -->
                                <div class="modal fade" id="sumberAnggaran{{ $val->id }}" tabindex="-1"
                                    role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLongTitle">Detail Sumber Anggaran
                                                </h5>
                                                <button type="button" class="close" data-bs-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="input-group input-group-outline">
                                                    <span>Kegiatan :</span><br>
                                                    <span>{{ $val->mstKegiatan->name }}</span>
                                                </div>
                                                <div class="input-group input-group-outline">
                                                    <span>Sumber Anggaran :</span><br>
                                                    @foreach ($val->sumberAnggaran as $sa)
                                                        <ul>
                                                            <li>{{ $sa->mstSumberAnggaran->name }}</li>
                                                        </ul>
                                                    @endforeach
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal Anggaran Renja -->
                                @include('partials.modal-table', [
                                    "title" => "Anggaran Renja",
                                    "modalId" => "anggaranRenja$val->id",
                                    "tableHeads" => [
                                        "Kegiatan",
                                        ...$periode_per_tahun->map(function($periode) {return $periode->tahun;}),
                                    ],
                                    "tableData" => [
                                        [
                                            $val->mstKegiatan->name,
                                            ...$periode_per_tahun->map(function($periode) use($val){
                                                $anggaranRenja = $val->anggaranRenja;
                                                $selectedAnggaran = $anggaranRenja->firstWhere('id_periode_per_tahun', $periode->id);
                                                return $selectedAnggaran->nilai;
                                            })
                                        ]
                                    ]
                                ])
                            @endforeach

                        @endif
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        selectPlaceholder('#opd', 'Pilih OPD');
        selectUsingParent('.sasaran', '#addSelectProgram', 'Pilih Renstra Sasaran');
        selectUsingParent('.programS', '#addSelectSasaran', 'Pilih Renstra Program');
        // create and get hierarki
        getInfoHierarki('.info_btn');

        function getInfoHierarki(btn) {
            let info = document.querySelectorAll(btn);
            info.forEach(res => {
                res.addEventListener('click', () => {
                    const card = res.closest('.card')
                    const card_header = card.querySelector('.card-header')
                    const info_bar = card_header.nextElementSibling
                    const card_body = card.querySelector('.card-body')
                    const height_info_bar = info_bar.offsetHeight

                    info_bar.classList.toggle('show-hidden')
                    card_body.style.transition = '.3s';
                    if (!info_bar.classList.contains('show-hidden')) {
                        card_body.style.marginTop = `${height_info_bar - 10}px`;
                    } else {
                        card_body.style.marginTop = `0`;
                    }
                });
            });
        }
    </script>
@endsection
