@extends('layouts.app')
@section('title')
    Form Tambah Renstra Kegiatan
@endsection
@section('content')
<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Renstra Kegiatan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Renstra</a>
        <span class="breadcrumb-item">Kegiatan</span>
        <span class="breadcrumb-item">Kegiatan</span>
        <span class="breadcrumb-item" style="color: #000;">Tambah</span>
    </nav>
</div>

<style>
    button.badge {
        border: none;
        padding: 3px;
    }
</style>

<div class="container-fluid pb-2 mt-4">
    <div class="card">
        <div class="card-header">
            <span>Form Tambah Renstra Kegiatan</span>
        </div>
        <form action="{{ url('') }}/renstra/kegiatan/kegiatan/add" method="POST" class="add_renstra_kegiatan">
            @csrf
            <div class="card-body">
                <div class="col-4">
                    <div class="input-group">
                        <label for="opd">Perangkat Daerah</label>
                        <select name="opd" id="opd" class="opd">
                            <option value=""></option>
                            @foreach ($opd as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option> 
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-4">
                    <div class="input-group">
                        <label for="program">Program PD</label>
                        <select name="program" id="program" class="program">
                            <option value=""></option>
                            @foreach ($program as $item)
                            <option value="{{ $item->id }}">{{ $item->programRpjmd->mstProgram->name }} | (sasaran){{ $item->sasaranRenstra->name }}</option> 
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-4">
                    <div class="input-group">
                        <label for="kegiatan">Kegiatan PD</label>
                        <select name="kegiatan" id="kegiatan" class="kegiatan">
                            <option value=""></option>
                            @foreach ($kegiatan as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option> 
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="place-duplicate">
                    <div class="duplicate">
                        <div class="col-4">
                            <div class="input-group">
                                <label for="anggaran">Sumber Anggaran</label>
                                <div class="select-wrapper d-flex input-group">
                                    <select name="anggaran[]" id="anggaran" class="anggaran">
                                        <option value=""></option>
                                        @foreach ($anggaran as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option> 
                                        @endforeach
                                    </select>
                                    <button class="btn btn-danger btn-sm ml-2 btn-close" type="submit" hidden>
                                        <i class="fa fa-close">Hapus</i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="input-group">
                    <div class="col-sm-12 mt-2">
                        <button type="button" class="btn btn-success btn-sm duplicate-btn">
                            <i class="fa fa-plus"></i> Tambah Sumber Anggaran</button>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-dark cancel" onclick="return window.history.back()" type="button">Cancel</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        selectPlaceholder('.opd', 'Pilih PD');
        selectPlaceholder('.program', 'Pilih Program PD');
        selectPlaceholder('.kegiatan', 'Pilih Kegiatan PD');
        selectPlaceholder('.anggaran', 'Pilih Sumber Anggaran');
        inputValidate('.add_renstra_kegiatan', ['select']);

        (function cloneEl() {
            const btn = document.querySelector('.duplicate-btn');
            btn.addEventListener('click', function() {
                const el = document.querySelector('.duplicate');
                const plc = document.querySelector('.place-duplicate');
                let clone = el.cloneNode(true);

                clone.querySelector('span.select2').remove();
                const text_validate = clone.querySelectorAll('small')
                    .forEach(res => res.remove());

                const selectSumberAnggaran = clone.querySelector('select.anggaran');
                selectSumberAnggaran.classList.add('anggaran-cloned');
                clone.querySelector('.btn-close').removeAttribute('hidden');
                plc.append(clone);
                selectPlaceholder('.anggaran-cloned', 'Pilih Sumber Anggaran');
            });
        })();

        (function closeClone() {
            document.addEventListener('click', function(e) {
                if(e.target.classList.contains('btn-close') || e.target.classList.contains('fa-close')) {
                    this.activeElement.closest('.duplicate').remove();
                }
            });
        })();
    </script>
@endsection