@extends('layouts.app')
@section('title')
    {{ $title }}
@endsection
@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
        <span class="brand ml-4">Renstra Sasaran</span>
        <nav class="mr-4">
            <a class="breadcrumb-item" href="/">Dashboard</a>
            <a class="breadcrumb-item" href="#">Renstra</a>
            <span class="breadcrumb-item" style="color: #000;">Sasaran</span>
        </nav>
    </div>
    <style media="screen">
        .info_bar {
            position: absolute;
            top: 50px;
            z-index: 998;
            border-radius: 0;
            width: 100%;
            background-image: linear-gradient(195deg, #42424a 0%, #191919 100%);
        }

        #card-body-info {
            transition: .3s;
        }

        .relative-info {
            transition: .3s;
        }

    </style>
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection',[
            'opds' => $opd,
            'selectedOpd' => $opd_selected,
            'redirectUrl' => route('renstra.sasaran.index')
        ])
        @include('partials.error-message')
        @include('partials.message')
        <div class="card">
            <div class="card-header">
                <h6 class="card-title">Renstra Sasaran Tingkat Kabupaten Periode {{ periode()->dari }} -
                    {{ periode()->sampai }}</h6>
                <a href="{{ url('') }}/renstra/sasaran/add" class="btn btn-primary btn-sm" style="background-color: #4caf50;">
                    <i class="fa fa-plus"></i>
                    <span>Tambah</span>
                </a>
            </div>
            <div class="card-body">
                @if (!is_null($hierarki->first()))
                    @csrf
                    @if (!is_null($hierarki->first()))
                        @csrf
                        {{-- @dd($hierarki) --}}
                        @foreach ($hierarki as $idx => $item)
                            @if (!is_null($item->renstraSasaran->first()))
                                <div class="card {{ $loop->iteration > 1 ? 'mt-3' : '' }}">
                                    <div class="card-header bg-primary text-light"
                                        style="position: relative; z-index: 999;">
                                        <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                                            <a href="#" class="info_btn">
                                                <i class="fa fa-info-circle"></i>
                                            </a>&emsp;
                                            <i class="fa fa-angle-double-right"></i> &nbsp; Perangkat Daerah : &nbsp;
                                            {{ $item->name }}
                                        </span>
                                    </div>
                                    <div class="alert show-hidden info_bar mt-4" style="display: flex; flex-direction: column"
                                        role="alert">
                                        <span class="hierarki_m">~ / Misi &nbsp;
                                            <i
                                                class="fa fa-caret-right">&emsp;</i>{{ $item->rpjmdSasaran->tujuan->misi->misi }}
                                        </span>
                                        <span class="hierarki_m">~ RPJMD / Tujuan &nbsp;
                                            <i
                                                class="fa fa-caret-right">&emsp;</i>{{ $item->rpjmdSasaran->tujuan->name }}
                                        </span>
                                        <span class="hierarki_m">~ RPJMD / Sasaran &nbsp;
                                            <i class="fa fa-caret-right">&emsp;</i>{{ $item->rpjmdSasaran->name }}
                                        </span>
                                        <span class="hierarki_m">~ RPJMD / Prioritas Pembangunan &nbsp;
                                            <i class="fa fa-caret-right">&emsp;</i>
                                            @if (!is_null($item->rpjmdSasaran->prioritasPembangunan) && !is_null($item->rpjmdSasaran->prioritasPembangunan->mstPrioritasPembangunan))
                                                {{ $item->rpjmdSasaran->prioritasPembangunan->mstPrioritasPembangunan->name }}
                                            @else
                                                <span class="text-warning">Prioritas Pembangunan belum ditetapkan! &nbsp;
                                                    klik
                                                    <a href="{{ url('') }}/rpjmd/prioritas-pembangunan"
                                                        class="text-info">disini</a> untuk menetapkan</span>
                                            @endif
                                        </span>
                                        <span class="hierarki_m">~ / Perangkat Daerah &nbsp;
                                            <i class="fa fa-caret-right">&emsp;</i>{{ $item->opd->name }}
                                        </span>
                                        <span class="hierarki_m">~ Renstra / Tujuan &nbsp;
                                            <i class="fa fa-caret-right">&emsp;</i>{{ $item->name }}
                                        </span>
                                    </div>

                                    <div class="card-body">
                                        @foreach ($item->renstraSasaran as $itemSasaran)
                                            <div class="card mt-3">
                                                <div class="card-header">
                                                    <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                                                        &nbsp; Sasaran Perangkat Daerah : &nbsp; {{ $itemSasaran->name }}
                                                    </span>

                                                    <!-- button aksi -->
                                                    <div class="aksi">
                                                        <!-- button delete -->
                                                        <form
                                                            action="{{ url('') }}/renstra/sasaran/delete/{{ hashID($itemSasaran->id) }}"
                                                            method="POST" class="d-inline">
                                                            @csrf
                                                            @method('delete')
                                                            <button class="btn btn-danger btn-sm" type="submit"
                                                                onclick="return confirm('Lanjukan menghapus data?')"
                                                                title="Hapus">
                                                                Hapus
                                                            </button>
                                                        </form>

                                                        <!-- button update -->
                                                        <form
                                                            action="{{ url('') }}/renstra/sasaran/edit/{{ hashID($itemSasaran->id) }}"
                                                            method="POST" class="d-inline">
                                                            @csrf
                                                            <button class="btn btn-warning btn-sm" title="Edit">
                                                                Edit
                                                            </button>
                                                        </form>
                                                    </div>
                                                    <!-- end aksi -->
                                                </div>
                                                <div class="card-body table-responsive">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>Indikator Sasaran</th>
                                                                <th>PK</th>
                                                                <th>IKU</th>
                                                                <th>IR</th>
                                                                <th>Satuan</th>
                                                                <th>Cara Pengukuran</th>
                                                                <th>Kondisi Awal</th>
                                                                @foreach ($periode_per_tahun as $tahun)
                                                                    <th width="125" class="text-center">Target
                                                                        {{ $tahun->tahun }}</th>
                                                                @endforeach
                                                                <th>Kondisi Akhir</th>
                                                                <th>Target Renja</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach ($itemSasaran->indikator->where('id_sasaran', $itemSasaran->id) as $idx => $itemIndikator)
                                                                <tr>
                                                                    <td>{{ $itemIndikator->indikator }}</td>
                                                                    <td>{!! $itemIndikator->is_pk > 0 ? '&check;' : '&times;' !!}</td>
                                                                    <td>{!! $itemIndikator->is_iku > 0 ? '&check;' : '&times' !!}</td>
                                                                    <td>{!! $itemIndikator->is_ir > 0 ? '&check;' : '&times' !!}</td>
                                                                    <td>{{ $itemIndikator->satuan->name }}</td>
                                                                    <td>
                                                                        <a href="#" data-toggle="modal"
                                                                            data-target="#caraPengukuran{{ $itemIndikator->id }}"
                                                                            style="text-decoration: none">
                                                                            Selengkapnya
                                                                        </a>
                                                                    </td>
                                                                    <td>{{ $itemIndikator->awal }}</td>
                                                                    @foreach ($itemIndikator->target as $itemTarget)
                                                                        <td class="text-center">
                                                                            {{ $itemTarget->target }}</td>
                                                                    @endforeach
                                                                    <th>Kondisi Akhir</th>
                                                                    <th>Target Renja</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($itemSasaran->indikator->where('id_sasaran', $itemSasaran->id) as $idx => $itemIndikator)
                                                                    <tr>
                                                                        <td>{{ $itemIndikator->indikator }}</td>
                                                                        <td>{!! $itemIndikator->is_pk > 0 ? '&check;' : '&times;' !!}</td>
                                                                        <td>{!! $itemIndikator->is_iku > 0 ? '&check;' : '&times' !!}</td>
                                                                        <td>{{ $itemIndikator->satuan->name }}</td>
                                                                        <td>
                                                                            <a href="#" data-bs-toggle="modal"
                                                                                data-bs-target="#caraPengukuran{{ $itemIndikator->id }}"
                                                                                style="text-decoration: none">
                                                                                Selengkapnya
                                                                            </a>
                                                                        </td>
                                                                        <td>{{ $itemIndikator->awal }}</td>
                                                                        @foreach ($itemIndikator->target as $itemTarget)
                                                                            <td class="text-center">
                                                                                {{ $itemTarget->target }}</td>
                                                                        @endforeach
                                                                        <td>{{ $itemIndikator->akhir }}</td>
                                                                        <td>
                                                                            <button type="button"
                                                                                class="btn btn-success btn-sm duplicate-btn"
                                                                                data-bs-toggle="modal" data-bs-target="#renja{{$itemIndikator->id}}">
                                                                                <i class="fa fa-plus"></i> Tambah
                                                                                Target</button>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            {{-- //Modal --}}
                                            @foreach ($itemSasaran->indikator->where('id_sasaran', $itemSasaran->id) as $idx => $itemIndikator)
                                                @include('renstra.sasaran.parts.renja-modal', [
                                                    "modalId" => "renja$itemIndikator->id",
                                                    "sasaran" => $itemSasaran,
                                                    "indikator" => $itemIndikator
                                                ])
                                                <div class="modal fade" id="caraPengukuran{{ $itemIndikator->id }}"
                                                    tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
                                                    aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">Detail
                                                                    Cara Pengukuran</h5>
                                                                <button type="button" class="close"
                                                                    data-bs-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <span>
                                                                    Indikator Sasaran : <br />
                                                                    {{ $itemIndikator->indikator }}
                                                                </span>
                                                                <br>
                                                                <br>
                                                                <span>
                                                                    Cara Pengukuran : <br />
                                                                    {{ $itemIndikator->cara_pengukuran }}
                                                                </span>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary"
                                                                    data-bs-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endforeach
                                    </div>
                                </div>
                            @else
                                @if ($loop->iteration == 1)
                                    <div class="alert alert-warning" role="alert">
                                        Tidak ada data!
                                    </div>
                                @endif
                            @endif
                        @endforeach
                    @else
                        <div class="alert alert-warning" role="alert">
                            Tidak ada data!
                        </div>
                    @endif
                @endif
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="dataNone" tabindex="-1" role="dialog" aria-labelledby="dataNoneTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title text-danger" id="exampleModalLongTitle">Warning!</h6>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span>Tidak ada data pada "~/Renstra/Tujuan"</span>
                    <br>
                    <span>Klik <a href="{{ url('') }}/renstra/tujuan/add">disini</a> untuk menambah data rensta
                        tujuan</span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('scripts')
    <script>
        // create and get hierarki
        function getInfoHierarki(btn) {
            let info = document.querySelectorAll(btn);
            info.forEach(res => {
                res.addEventListener('click', () => {
                    const card = res.closest('.card')
                    const card_header = card.querySelector('.card-header')
                    const info_bar = card_header.nextElementSibling
                    const card_body = card.querySelector('.card-body')
                    const height_info_bar = info_bar.offsetHeight

                    info_bar.classList.toggle('show-hidden')
                    card_body.style.transition = '.3s';
                    if (!info_bar.classList.contains('show-hidden')) {
                        card_body.style.marginTop = `${height_info_bar - 10}px`;
                    } else {
                        card_body.style.marginTop = `0`;
                    }
                })
            })
        }
        getInfoHierarki('.info_btn');
    </script>
@endsection
