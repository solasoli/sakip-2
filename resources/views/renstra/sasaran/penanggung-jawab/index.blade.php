@extends('layouts.app')
@section('title')
    Penanggung Jawab Sasaran
@endsection
@section('content')
    <style media="screen">
        .modal-lg{
            width: 750px !important;
        }
        .info_bar {
            position: absolute;
            top: 50px;
            z-index: 998;
            border-radius: 0;
            width: 100%;
            background-image: linear-gradient(195deg, #42424a 0%, #191919 100%);
        }
        #card-body-info {
            transition: .3s;
        }
        .relative-info {
            transition: .3s;
        }
    </style>
    @include('components.content-header', [
        "title" => "Penanggung Jawab Sasaran",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "Renstra", "href" => "#"],
            [ "label" => "Sasaran", "href" => "#"],
            [ "label" => "Penanggung Jawab Sasaran", "href" => "#"],
        ]
    ])
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection', [
            'redirectUrl' => route('renstra.sasaran.penanggung_jawab.index')
        ])
    </div>
    @if($errors->any())
        <div class="alert alert-danger">
            <p><strong>Opps Something went wrong</strong></p>
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
    <div class="card mb-4">
        @if ($sasaranIndikator->count() > 0)
            <form action="{{ route('renstra.sasaran.penanggung_jawab.mass_update') }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="card-header">
                        <h6 class="card-title">Data Penanggung Jawab Perangkat Daerah</h6>
                    </div>
                    <hr>
                    @foreach ($sasaranIndikator as $indikator)
                        @php
                            $hierarchy = [
                                '/ Misi' => $indikator->getMisi()->misi,
                                'RPJMD / Tujuan' => $indikator->getRpjmdTujuan()->name,
                                'RPJMN / Sasaran' => $indikator->getRpjmdSasaran()->name,
                                'RPJMD / Prioritas Pembangunan' => $indikator->getPrioritasPembangunan()->name,
                                '/ Perangkat Daerah' => $indikator->getPerangkatDaerah()->name,
                                'Renstra / Tujuan' => $indikator->getRenstraTujuan()->name,
                                'Renstra / Sasaran' => $indikator->getRenstraSasaran()->name,
                            ];
                            $title = 'Indikator Program ' . $indikator->getPerangkatDaerah()->name . ' : ' . $indikator->indikator;
                            $itemId = "indikator$indikator->id";
                            $indikatorIndex = $loop->index;
                        @endphp
                        <x-hierarchy-card :title="$title" :hierarchy="$hierarchy" class="info_bar">
                            <div x-data="{{ $itemId }}">
                                <div class="table-responsive">                                
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Penanggungjawab</th>
                                                @foreach ($periode->periodePerTahun as $tahun)
                                                    @php $selected = $indikator->target->firstWhere('id_periode_per_tahun', $tahun->id) @endphp
                                                    <th>Target {{ $tahun->tahun }} ( {{ $selected ? $selected->target : '-' }}  )</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <input type="hidden" name="indikator[{{ $indikatorIndex }}][id]" value="{{$indikator->id}}"/>
                                            <template x-for="(pegawai, index) in penanggungJawab" :key="index">
                                                <tr>
                                                    <td class="flex">
                                                        <input
                                                            type="hidden"
                                                            :value="pegawai.id"
                                                            :name="'indikator[{{ $indikatorIndex }}][penanggung_jawab]['+index+'][id]'"/>
                                                        <select
                                                            :id="'{{ $itemId }}_'+index"
                                                            :name="'indikator[{{ $indikatorIndex }}][penanggung_jawab]['+index+'][pegawai_id]'"
                                                            class="flex-grow mb-2 w-full">
                                                        </select>
                                                        <button
                                                            @click="hapusPenanggungJawab(index)"
                                                            type="button"
                                                            class="flex-none btn btn-primary btn-sm  ml-2">
                                                            <i class="fa fa-trash"></i>
                                                        </button>
                                                    </td>
                                                    @foreach ($periode->periodePerTahun as $key => $tahun)
                                                        <td>
                                                            <input
                                                                type="hidden"
                                                                :name="'indikator[{{ $indikatorIndex }}][penanggung_jawab]['+index+'][target][{{ $key }}][periode_id]'"
                                                                value="{{ $tahun->id }}"/>
                                                            <div class="input-group input-group-outline">
                                                                <input
                                                                :name="'indikator[{{ $indikatorIndex }}][penanggung_jawab]['+index+'][target][{{ $key }}][target]'"
                                                                type="text"
                                                                class="form-control"
                                                                :value="getTarget(index, {{ $tahun->id }})"
                                                                @keyup="event => ubahTarget(index, {{ $tahun->id }}, event.target.value)"/>
                                                            </div>
                                                        </td>
                                                    @endforeach
                                                </tr>
                                            </template>
                                        </tbody>
                                    </table>
                                </div>
                                <button type="button" class="btn btn-primary btn-sm btn_clone" @click="tambahPenanggungJawab()">
                                <i class="fa fa-plus"></i> Tambah Penanggung Jawab
                                </button>
                            </div>
                            <script>
                                document.addEventListener('alpine:init', () => {
                                    Alpine.data("{{ $itemId }}", () => {
                                        return {
                                            init() {
                                                const initialPenanggungJawab = (@json($indikator->sasaranIndikatorPenanggungJawab))
                                                        .map(penanggungJawab => ({
                                                            id: penanggungJawab.id,
                                                            pegawai_id: penanggungJawab.pegawai.id,
                                                            target: penanggungJawab.target.map(target => ({
                                                                periode_id: target.id_periode_per_tahun,
                                                                target: target.target
                                                            }))
                                                        }));

                                                if(initialPenanggungJawab.length == 0) {
                                                    this.tambahPenanggungJawab()
                                                } else {
                                                    initialPenanggungJawab.forEach((initial) => {
                                                        this.tambahPenanggungJawab(initial)
                                                    })
                                                }
                                            },
                                            baseComponentId: '{{ $itemId }}',
                                            penanggungJawab: [],
                                            tambahPenanggungJawab(penanggungJawabObject) {
                                                this.penanggungJawab.push(penanggungJawabObject || {
                                                    id: null,
                                                    pegawai_id: null,
                                                    target: []
                                                })
                                                const latestIndex = this.penanggungJawab.length - 1

                                                this.$nextTick(() => {
                                                    const select2 = $(`#${this.baseComponentId}_${latestIndex}`).select2({
                                                        width: '100%',
                                                        ajax: {
                                                            url: "{{ route('api.opds.pegawai', [ 'opd' => $selectedOpd, 'sasaran' => 'renstra' ]) }}",
                                                            dataType: 'json',
                                                            data: function(params) {
                                                                return {
                                                                    search: params.term
                                                                }
                                                            },
                                                            processResults: function(data) {
                                                                return {
                                                                    results: data.data.pegawai.map(data => {
                                                                        return {
                                                                            text: data.nama,
                                                                            id: data.id
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        }
                                                    })

                                                    // prepopulate initial options so that user name available in dropdown options
                                                    if(penanggungJawabObject != null) {
                                                        const initialData = @json($indikator->sasaranIndikatorPenanggungJawab);
                                                        initialData.forEach(item => {
                                                            const option = new Option(item.pegawai.nama, item.pegawai.id, true, true)
                                                            select2.append(option).trigger('change')
                                                        });
                                                        select2.val(penanggungJawabObject.pegawai_id).trigger("change");
                                                    }

                                                    // AlpineJS and Select2 is entirely different library
                                                    // so there is no way for this two library to communicate each other
                                                    // because of this, i create a listener to listen for select2 value change
                                                    // if select2 changed then modify value on alpinejs
                                                    // or alpinejs value changed, listen it from watcher, then modify select2 value

                                                    select2.on("select2:select", (event) => {
                                                        this.penanggungJawab[latestIndex].pegawai_id = event.target.value
                                                    });

                                                    this.$watch(`penanggungJawab[${latestIndex}]`, (value) => {
                                                        select2.val(value.pegawai_id).trigger("change");
                                                    });
                                                })
                                            },
                                            hapusPenanggungJawab(index) {
                                                this.penanggungJawab.splice(index, 1)
                                            },
                                            ubahTarget(penanggungJawabIndex, periodeTahunId, target) {
                                                const existingIndex = this.penanggungJawab[penanggungJawabIndex]
                                                    .target.findIndex(target => target.periode_id == periodeTahunId)

                                                if(existingIndex != -1) {
                                                    // change value first
                                                    this.penanggungJawab[penanggungJawabIndex].target[existingIndex].target = target
                                                    // then rebuild the array
                                                    this.penanggungJawab[penanggungJawabIndex].target = [ ...this.penanggungJawab[penanggungJawabIndex].target ]
                                                } else {
                                                    this.penanggungJawab[penanggungJawabIndex].target.push({
                                                        periode_id: periodeTahunId,
                                                        target
                                                    })
                                                }
                                            },
                                            getTarget(penanggungJawabIndex, periodeTahunId) {
                                                const target = this.penanggungJawab[penanggungJawabIndex]
                                                    .target.find(target => target.periode_id == periodeTahunId)

                                                return target ? target.target : ''
                                            }
                                        }
                                    })
                                })
                            </script>
                        </x-hierarchy-card>
                    @endforeach

                    <div class="input-group mt-4">
                        <button class="btn btn-dark cancel" onclick="cancel('{{route('renstra.sasaran.penanggung_jawab.index')}}')" type="button">Cancel</button>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        @endif
    </div>
@endsection
