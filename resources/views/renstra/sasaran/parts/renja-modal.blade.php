<div class="modal fade" id="{{$modalId}}" tabindex="-1" aria-labelledby="{{$modalId}}_label" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form
                action="{{ route('renstra.sasaran.indikator.renja', [
                    'sasaran' => $sasaran->id,
                    'indikator' => $indikator->id,
                ]) }}"
                method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="{{$modalId}}_label">Target Renja Indikator Sasaran</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2">Indikator Sasaran</th>
                                    <th colspan="{{$sasaran->getPeriode()->periodePerTahun->count()}}">Target Renja</th>
                                </tr>
                                <tr>
                                    @foreach ($sasaran->getPeriode()->periodePerTahun as $periode)
                                        <th>{{$periode->tahun}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$indikator->indikator}}</td>
                                    @foreach ($sasaran->getPeriode()->periodePerTahun as $periode)
                                        @php $targetRenja = $indikator->targetRenja->firstWhere('id_periode_per_tahun', $periode->id); @endphp
                                        <input type="hidden"
                                            name="renja[{{ $loop->index }}][id_tahun]"
                                            value="{{ $periode->id }}">
                                        <td class="text-center">
                                            <div class="input-group input-group-outline">
                                                <input
                                                type="text"
                                                class="form-control"
                                                name="renja[{{ $loop->index }}][value]"
                                                {{ $targetRenja != null ? "value=$targetRenja->nilai" : '' }}
                                                >
                                            </div>
                                        </td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                        data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
