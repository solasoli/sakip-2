div@extends('layouts.app')
@section('title')
    Form Edit Renstra Sasaran
@endsection
@section('content')
<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Renstra Sasaran</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Renstra</a>
        <span class="breadcrumb-item">Sasaran</span>
        <span class="breadcrumb-item" style="color: #000;">Tambah</span>
    </nav>
</div>
<div class="container-fluid pb-2 mt-4">
    <div class="card">
        <div class="card-header">
            <span>Form Edit Renstra Sasaran</span>
        </div>
        <form action="{{ url('') }}/renstra/sasaran/update/{{ hashID($data->id) }}" method="POST" class="edit_renstra_sasaran">
            @csrf
            @method('patch')
            <div class="card-body">
                <div class="input-group ">
                    <div class="col-4">
                        <label for="tujuan">Perangkat Daerah</label>
                        <select name="tujuan" id="tujuan" class="tujuan form-control">
                            <option value=""></option>
                            @foreach ($tujuan as $item)
                            <option value="{{ $item->id }}" {{ $data->renstraTujuan->id == $item->id ? "selected" : "" }}>{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="input-group ">
                    <div class="col-4">
                        <label for="">Sasaran Perangkat Daerah</label>
                        <div class="input-group input-group-outline">
                            <input type="text" class="form-control" value="{{ $data->name }}" name="sasaran">
                        </div>
                    </div>
                </div>
                <div class="place-duplicate">

                    @foreach ($data->indikator as $item)
                    {{--  @dd($item)  --}}
                    <div class="duplicate">

                        <div style="display: flex; align-items: center;">
                            <div class="input-group ml-3">
                                <label for="indikator">Indikator sasaran * : </label>
                                <div class="input-group input-group-outline"> 
                                    <input type="text" id="indikator" class="form-control" name="indikator[]" value="{{ $item->indikator }}">
                                </div>
                                @error('indikator.*')
                                <div class="text-danger">
                                    <small>Indikator sasaran tidak boleh kosong!</small>
                                </div>
                                @enderror
                            </div>

                            <div class="form-group ml-2">
                                <label for="satuan">Satuan * : </label>
                                <select name="satuan[]" id="satuan" class="form-control satuan">
                                    <option value=""></option>
                                    @foreach ($satuan as $val)
                                    <option value="{{ $val->id }}" {{ $item->satuan->id == $val->id ? "selected" : "" }}>{{ $val->name }}</option>
                                    @endforeach
                                </select>
                                @error('satuan.*')
                                <div class="text-danger">
                                    <small>Tidak boleh kosong!</small>
                                </div>
                                @enderror
                            </div>

                            <div class="form-group ml-2">
                                <label for="pk" class="check pk">PK</label>
                                <div class="switch d-flex w-7">
                                    <input type="checkbox" class="check form-control" id="pk" {{ $item->is_pk > 0 ? "checked" : "" }}>
                                    <input type="text" name="pk[]" class="pk check" hidden >
                                </div>
                            </div>
                            <div class="form-group ml-2">
                                <label for="pk" class="check pk">IKU</label>
                                <div class="switch d-flex w-7">
                                    <input type="checkbox" class="check form-control" id="iku" {{ $item->is_iku > 0 ? "checked" : "" }}>
                                    <input type="text" name="iku[]" class="pk check" hidden >
                                </div>
                            </div>
                            <div class="form-group ml-2">
                                <label for="ir" class="check ir">IR</label>
                                <div class="switch d-flex w-7">
                                    <input type="checkbox" class="check form-control" id="ir" {{ $item->is_ir > 0 ? "checked" : "" }}>
                                    <input type="text" name="ir[]" class="ir check" hidden>
                                </div>
                            </div>
                        </div>
                        <div style="display: flex; align-items: center;">
                            <div class="input-group px-3">
                                <label for="pengukuran">Cara Pengukuran * : </label><br>
                                <textarea name="cara_pengukuran[]" id="pengukuran" style="height: 80px" cols="80" rows="30">{{ $item->cara_pengukuran }}</textarea>
                                @error('pengukuran.*')
                                <div class="text-danger">
                                    <small>Cara pengukuran tidak boleh kosong!</small>
                                </div>
                                @enderror
                            </div>
                        </div>
                        <div style="display: flex; align-items: center;">
                            <div class="input-group ml-3" style="width: 120px">
                                <label for="awal">Kondisi Awal * : </label>
                                <div class="input-group input-group-outline">
                                    <div class="input-group input-group-outline">
                                        <input type="text" id="awal" class="form-control" name="awal[]" value="{{ $item->awal }}">
                                    </div>
                                </div>
                            </div>
                            @foreach($periode_per_tahun as $idx => $val)
                            <div class="input-group ml-3" style="width: 120px">
                                <label for="target">Target {{ $val->tahun }} * : </label>
                                <div class="input-group input-group-outline">
                                    <input type="text" id="target" class="form-control" value="{{ $item->target[$idx]->target }}" name="target[]">
                                </div>
                            </div>
                            @endforeach
                            <div class="input-group ml-3" style="width: 120px">
                                <label for="akhir">Kondisi Akhir * : </label>
                                <div class="input-group input-group-outline">
                                    <input type="text" id="akhir" class="form-control" value="{{ $item->akhir }}" name="akhir[]">
                                </div>
                            </div>
                        </div>
                        <div class="input-group close-wrapper" hidden>
                            <button class="close-duplicate btn btn-danger mt-4 ml-3 p-2">Hapus indikator</button>
                        </div>
                        <hr>
                    </div>
                    @endforeach

                </div>
                <div class="input-group row">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-success btn-sm duplicate-btn">
                            <i class="fa fa-plus"></i> Tambah Indikator
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-dark cancel" onclick="return window.history.back()" type="button">Cancel</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        checkboxValue();

        show_remove_btn('.close-wrapper', 'hidden');
        $('.duplicate-btn').click(() => {
            clone_el();
            show_remove_btn('.close-wrapper', 'hidden');
        })

        closeElementClone('close-duplicate', '.duplicate');

        // function show remove button after click duplicate-btn
        function show_remove_btn(el, attr) {
            const close_btn = document.querySelectorAll(el);
            close_btn.forEach((res, idx) => {
                if (idx > 0) {
                    res.removeAttribute(attr)
                }
            })
        }
        // end //

        // function clone element
        const clone_el = function() {
            const el_duplicate = document.querySelector('.duplicate');
            const select_clone = el_duplicate.querySelectorAll('select.satuan');
            let span_select = el_duplicate.querySelectorAll('span.select2');
            span_select.forEach(res => res.remove());

            select_clone.forEach(res => {
                res.classList.remove('satuan');
                res.classList.add('satuan-cloned');
            })

            const clone = el_duplicate.cloneNode(true);
            document.querySelector('.place-duplicate').appendChild(clone);
            clean_el_clone(clone);
            selectPlaceholder('.satuan-cloned', 'Satuan');
        }
        // end //

        // clear value after clone
        const clean_el_clone = function(el) {
            let check = el.querySelectorAll('input[type=checkbox]');
            let input = el.querySelectorAll('input[type=text]');

            let jml_clone = document.querySelectorAll('.duplicate').length;

            correspondingLabelInput(el, 'label.pk', 'input#pk_1', `pk_${jml_clone}`);
            correspondingLabelInput(el, 'label.ir', 'input#ir_1', `ir_${jml_clone}`);

            check.forEach(res => res.checked = false)
            input.forEach(res => (!res.classList.contains('check')) ? res.value = '' : checkboxValue())
        }
        // end //

        // create for label and id input (matching)
        const correspondingLabelInput = function(base_el, label_el, input_el, str) {
            const _label = base_el.querySelector(label_el);
            const _input = base_el.querySelector(input_el);

            _label.removeAttribute('for');
            _input.removeAttribute('id');

            _label.setAttribute('for', str);
            _input.setAttribute('id', str);
        }
        // end //

        // remove element clone
        function closeElementClone(c_btn, c_parent) {
            document.addEventListener('click', function(e) {
                if (e.target.classList.contains(c_btn) || e.target.classList.contains('fa-close')) {
                    e.preventDefault();
                    this.activeElement.closest(c_parent).remove();
                }
            })
        }
        // end //

        // validate input
        const formClass = '.edit_renstra_sasaran';
        inputValidate(formClass, ['input', 'textarea'], 'target');
        // end validate

        // select2 using placeholder
        selectPlaceholder('.tujuan', 'Pilih Tujuan')
        selectPlaceholder('.satuan', "Satuan")
        // end //
    });
</script>
@endsection
