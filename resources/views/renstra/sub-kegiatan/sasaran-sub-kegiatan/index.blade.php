@extends('layouts.app')
@section('title')
    Sasaran Sub Kegiatan
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Sub Kegiatan Renstra",
        "items" => [
                [ "label" => "Dashboard", "href" => "/"],
                [ "label" => "Renstra", "href" => "#"],
                [ "label" => "Program", "href" => "#"],
                [ "label" => "Sub Kegiatan Renstra", "href" => "#"],
        ]
    ])
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection', [
            'redirectUrl' => route('renstra.sub_kegiatan.sasaran_sub_kegiatan.index')
        ])
    </div>
    <div class="card mb-4">
        @if ($errors->any())
            <div class="alert alert-danger">
                <p><strong>Opps Something went wrong</strong></p>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card-body">
            <div class="card-header">
                <h6 class="card-title">Data Sasaran Sub Kegiatan PD</h6>
                <a href="{{ route('renstra.sub_kegiatan.sasaran_sub_kegiatan.create') }}" class="btn btn-primary btn-sm" style="background-color: #4caf50;">
                    <i class="fa fa-plus"></i>
                    <span>Tambah</span>
                </a>
            </div>
            <hr>
            @foreach ($sasaranSubKegiatan as $sasaran)
                @php
                    $hierarchy = [
                        '/ Misi' => $sasaran->getMisi()->misi,
                        'RPJMD / Tujuan' => $sasaran->getRpjmdTujuan()->name,
                        'RPJMN / Sasaran' => $sasaran->getRpjmdSasaran()->name,
                        'RPJMD / Prioritas Pembangunan' => $sasaran->getPrioritasPembangunan()->name,
                        '/ Perangkat Daerah' => $sasaran->getPerangkatDaerah()->name,
                        'Renstra / Tujuan' => $sasaran->getRenstraTujuan()->name,
                        'Renstra / Sasaran' => $sasaran->getRenstraSasaran()->name,
                    ];
                    $title = 'Sasaran Sub Kegiatan ' . $sasaran->getPerangkatDaerah()->name . ' : ' . $sasaran->name;
                    $itemId = "sasaran$sasaran->id";
                    $periodePerTahun = $periodeWalkot->periodePerTahun;
                @endphp
                <x-hierarchy-card :title="$title" :hierarchy="$hierarchy">
                    <x-slot name="action">
                        <div class="aksi">
                            <!-- button delete -->
                            <form
                                action="{{ route('renstra.sub_kegiatan.sasaran_sub_kegiatan.destroy', ["sasaranSubKegiatan" => $sasaran->id]) }}"
                                method="POST" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-sm" type="submit"
                                    onclick="return confirm('Lanjukan menghapus data?')" title="Hapus">
                                    Hapus
                                </button>
                            </form>

                            <!-- button update -->
                            <form action="{{route('renstra.sub_kegiatan.sasaran_sub_kegiatan.edit', [ 'sasaranSubKegiatan' => $sasaran->id ])}}" method="GET"
                                class="d-inline">
                                <button class="btn btn-warning btn-sm" title="Edit">
                                 Ubah
                                </button>
                            </form>
                        </div>
                    </x-slot>

                    <div class="card-body text-center">
                        <div class="table-responsive">
                             <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th>Sub Kegiatan</th>
                                    <th>Satuan</th>
                                    <th>PK</th>
                                    <th>IKU</th>
                                    <th>Cara pengukuran</th>
                                    <th>Kondisi Awal</th>
                                    @foreach ($periodePerTahun as $periode)
                                        <th>Target {{ $periode->tahun }}</th>
                                    @endforeach
                                    <th>Kondisi Akhir</th>
                                    <th>Target Renja</th>
                                </tr>
                                @foreach ($sasaran->output as $output)
                                    <tr>
                                        <td style="text-align: left;">{{ $output->name }}</td>
                                        <td>{{ $output->satuan->name }}</td>
                                        <td>{{ $output->is_pk ? '✓' : '-' }}</td>
                                        <td>{{ $output->is_iku ? '✓' : '-' }}</td>
                                        <td>
                                            <a href="#" data-bs-toggle="modal"
                                                data-bs-target="#caraPengukuran{{ $output->id }}"
                                                style="text-decoration: none">
                                                Selengkapnya
                                            </a>
                                        </td>
                                        <td>{{ $output->target_awal }}</td>
                                        @foreach ($periodePerTahun as $periode)
                                            @php $target = $output->target->firstWhere('id_periode_per_tahun', $periode->id); @endphp
                                            <th>{{ $target ? $target->target : '-' }}</th>
                                        @endforeach
                                        <td>{{ $output->target_akhir }}</td>
                                        <td><button class="btn btn-primary btn-sm" data-bs-toggle="modal"
                                                data-bs-target="#targetRenja{{ $output->id }}"><i
                                                    class="fa fa-plus"></i> Tambah Target</button></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                       
                        @foreach ($sasaran->output as $output)
                            <div class="modal fade" id="caraPengukuran{{ $output->id }}" tabindex="-1"
                                role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Detail
                                                Cara Pengukuran</h5>
                                            <button type="button" class="close" data-bs-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div>
                                                Indikator Sasaran :
                                            </div>
                                            <div>
                                                {{ $output->name }}
                                            </div>
                                            <br>
                                            <br>
                                            <span>
                                                Cara Pengukuran : <br />
                                                {{ $output->cara_pengukuran }}
                                            </span>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-bs-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal" tabindex="-1" role="dialog"
                                id="targetRenja{{ $output->id }}">
                                <div class="modal-dialog" role="document" style="max-width:800px">
                                    <div class="modal-content">
                                        <form
                                            action="{{ route('renstra.sub_kegiatan.sasaran_sub_kegiatan.output.renja', [
                                                'sasaranSubKegiatan' => $sasaran->id,
                                                'output' => $output->id,
                                            ]) }}"
                                            method="POST">
                                            @csrf
                                            <div class="modal-header">
                                                <h5 class="modal-title">Target Renja Output Sasaran Sub Kegiatan</h5>
                                                <button type="button" class="close" data-bs-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <table class="table table-bordered table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th rowspan="2">Output Subkegiatan</th>
                                                            <th colspan="{{ $periodePerTahun->count() }}">Target Renja
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            @foreach ($periodePerTahun as $periode)
                                                                <th>{{ $periode->tahun }}</th>
                                                            @endforeach
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>{{ $output->name }}</td>
                                                            @foreach ($periodePerTahun as $periode)
                                                                @php $targetRenja = $output->targetRenja->firstWhere('id_periode_per_tahun', $periode->id); @endphp
                                                                <input type="hidden"
                                                                    name="renja[{{ $loop->index }}][id_tahun]"
                                                                    value="{{ $periode->id }}">
                                                                <td><input type="text" style="max-width:70px"
                                                                        name="renja[{{ $loop->index }}][value]"
                                                                        {{ $targetRenja != null ? "value=$targetRenja->nilai" : '' }} />
                                                                </td>
                                                            @endforeach
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="modal-footer justify-content-start">
                                                <button type="button" class="btn btn-secondary"
                                                    data-bs-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </x-hierarchy-card>
            @endforeach
        </div>
    </div>
@endsection
