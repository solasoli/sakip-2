<div class="card">
    <div class="card-header">
        <span>{{$headline}}</span>
    </div>

    @php
        $isEdit = isset($sasaranSubKegiatan);
        $url = $isEdit
            ? route('renstra.sub_kegiatan.sasaran_sub_kegiatan.update', ["sasaranSubKegiatan" => $sasaranSubKegiatan->id])
            : route('renstra.sub_kegiatan.sasaran_sub_kegiatan.store');
    @endphp
    <form action="{{$url}}" method="POST" x-data="form">
        @csrf
        @if ($isEdit)
            @method('PUT')
        @endif

        <div class="card-body">
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="opd">Perangkat Daerah</label>
                    <select name="opd" id="opd" class="opd form-control" x-model="selectedOpd">
                        @foreach ($opds as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="opd">Sub Kegiatan OPD</label>
                    <select name="sub_kegiatan" class="form-control" id="subKegiatan"></select>
                </div>
            </div>
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="sasaran_sub_kegiatan">Sasaran Sub Kegiatan</label>
                    <input
                        x-model="sasaranSubKegiatan"
                        type="text"
                        id="sasaran_sub_kegiatan"
                        class="form-control"
                        name="sasaran_sub_kegiatan">
                </div>
            </div>
            <template x-for="(outputItem, index) in output">
                <div>
                    <input type="hidden" :name="`output[${index}][id]`" :value="outputItem.id">
                    <template x-if="index != 0">
                        <div class="input-group input-group-outline close-wrapper">
                            <button style="background-color: #4caf50; " @click.prevent="hapusOutput(index)" class="close-duplicate badge badge-danger mt-4 ml-3 p-2">
                                <i class="fa fa-times"></i> Hapus Output
                            </button>
                        </div>
                    </template>
                    <div style="display: flex; align-items: center;">
                                <div class="input-group ml-3">
                                    <label :for="`indikator_${index}`">Sub Kegiatan : </label>
                                    <div class="input-group input-group-outline">
                                        <input type="text" :id="`output_${index}`" :name="`output[${index}][nama]`" x-model="outputItem.nama" class="form-control">
                                        @error('output.*')
                                        <div class="text-danger">
                                            <small>Output Sub-Kegiatan tidak boleh kosong!</small>
                                        </div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="input-group ml-2">
                                    <label :for="`satuan_${index}`" class="block">Satuan * : </label>
                                        <select :id="`satuan_${index}`" class="form-control" :name="`output[${index}][satuan]`">
                                            <template x-for="satuanItem in satuan">
                                                <option :value="satuanItem.id"><span x-text="satuanItem.name"></span></option>
                                            </template>
                                        </select>
                                    <!-- @error('satuan.*')
                                    <div class="text-danger">
                                        <small>Tidak boleh kosong!</small>
                                    </div>
                                    @enderror -->
                                </div>
                                <div class="input-group input-group-outline form-check ml-3" style="width: 100px">
                                    <label class="check pk ml-2" for="pk">PK</label>
                                    <div class="switch d-flex ml-2">
                                        <input type="checkbox" x-model="outputItem.is_pk" class="check form-control form-check-input" :id="`pk_${index}`" :name="`output[${index}][is_pk]`"/>
                                        
                                    </div>
                                </div>
                                <div class="input-group input-group-outline form-check ml-3" style="width: 120px">
                                    <label class="check iku ml-2" for="iku">IKU</label>
                                    <div class="switch d-flex ml-2">
                                        <input type="checkbox" x-model="outputItem.is_iku" class="check form-control form-check-input" :id="`iku_${index}`" :name="`output[${index}][is_iku]`"/>
                                        <input type="text"  class="iku check" hidden>
                                    </div>
                                </div>
                            </div>
                    <!-- <div style="display: flex; align-items: center;">
                        <div class="input-group ml-3">
                            <label :for="`indikator_${index}`">Sub Kegiatan : </label>
                            <div class="input-group input-group-outline">
                                <input type="text" :id="`output_${index}`" :name="`output[${index}][nama]`" x-model="outputItem.nama" class="form-control">
                            </div>
                            
                        </div>

                        <div class="input-group  ml-2">
                            <label :for="`satuan_${index}`" class="block">Satuan * : </label>
                            <select :id="`satuan_${index}`" class="form-control" :name="`output[${index}][satuan]`">
                                <template x-for="satuanItem in satuan">
                                    <option :value="satuanItem.id"><span x-text="satuanItem.name"></span></option>
                                </template>
                            </select>
                        </div>

                        <div class="input-group input-group-outline ml-3 form-check">
                            <label :for="`pk_${index}`" class="check pk">PK</label>
                            <div class="switch d-flex w-7">
                                <input type="checkbox" x-model="outputItem.is_pk" class="check form-control" :id="`pk_${index}`" :name="`output[${index}][is_pk]`"/>
                                <input type="text" class="pk check" hidden>
                            </div>
                        </div>
                        <div class="input-group input-group-outline ml-3 form-check">
                            <label :for="`iku_${index}`" class="check iku">IKU</label>
                            <div class="switch d-flex w-7">
                                <input type="checkbox" x-model="outputItem.is_iku" class="check form-control" :id="`iku_${index}`" :name="`output[${index}][is_iku]`"/>
                                <input type="text" class="iku check" hidden>
                            </div>
                        </div>
                    </div> -->
                    <div style="display: flex; align-items: center;">
                        <div class="input-group input-group-outline px-3">
                            <label for="pengukuran">Cara Pengukuran : </label><br>
                            <textarea x-model="outputItem.cara_pengukuran" style="height: 80px" cols="80" rows="30" :name="`output[${index}][cara_pengukuran]`"></textarea>
                        </div>
                    </div>
                    <div style="display: flex; align-items: center;">
                        <div class="input-group  ml-3" style="width: 150px">
                            <label for="awal">Kondisi Awal * : </label>
                            <div class="input-group input-group-outline">
                                <input type="text" class="form-control" x-model="outputItem.target_awal":name="`output[${index}][target_awal]`" >
                            </div>
                            
                        </div>
                        <template x-for="(tahun, indexTahun) in outputItem.target_pertahun">
                            <div class="input-group ml-3" style="width: 120px">
                                <label for="target"><span x-text="`Target ${tahun.label} * :`"></span></label>
                                <input type="hidden" :name="`output[${index}][target][${indexTahun}][id]`" :value="tahun.id">
                                <input type="hidden" :name="`output[${index}][target][${indexTahun}][id_tahun]`" :value="tahun.id_tahun">
                                <div class="input-group input-group-outline">
                                <input type="text" id="target" class="form-control" x-model="tahun.value" :name="`output[${index}][target][${indexTahun}][value]`"/>
                                </div>
                            </div>
                        </template>
                        <div class="input-group ml-3" style="width: 150px">
                            <label for="akhir">Kondisi Akhir * : </label>
                                <div class="input-group input-group-outline">

                            <input type="text" class="form-control" x-model="outputItem.target_akhir" :name="`output[${index}][target_akhir]`"/>
                            </div>
                        </div>
                    </div>
                </div>
            </template>
            <template x-if="selectedSubKegiatan != null">
                <div class="input-group input-group-outline close-wrapper">
                    <button @click.prevent="tambahOutput()" class="close-duplicate badge badge-success mt-4 ml-3 p-2" style="background-color: #4caf50;">
                        + Tambah output
                    </button>
                </div>
            </template>
        </div>
        <div class="card-footer">
            <button class="btn btn-dark cancel" onclick="return window.history.back()" type="button">Cancel</button>
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
</div>

<script>
    document.addEventListener('alpine:init', () => {
        Alpine.data('form',() => {
            return {
                selectedOpd: null,
                selectedSubKegiatan: null,
                selectedSubKegiatanPeriodeTahun: [],
                sasaranSubKegiatan: '{{$isEdit ? $sasaranSubKegiatan->name : ''}}',
                output:[],
                fetchedSubKegiatan: [],
                satuan: @json($satuan),
                init() {
                    this.initializeOpd()
                    this.initializeSubKegiatan()
                },
                tambahOutput(prefilledOutput){
                    this.output.push( prefilledOutput || {
                        id: null,
                        cara_pengukuran: '',
                        nama: '',
                        satuan: 1,
                        is_pk: false,
                        is_iku: false,
                        target_awal: null,
                        target_akhir: null,
                        target_pertahun: this.selectedSubKegiatanPeriodeTahun.map(tahun => {
                            return {
                                id: null,
                                id_tahun: tahun.id,
                                label: tahun.tahun,
                                value: null
                            }
                        })
                    });

                    const latestIndex = this.output.length - 1

                    this.$nextTick(() => {
                        const satuanSelect = $(`#satuan_${latestIndex}`).select2()

                        satuanSelect.val(this.output[latestIndex].satuan).trigger("change");

                        satuanSelect.on('select2:select', (event) => {
                            this.output[latestIndex].satuan = event.target.value
                        })

                        // this watcher make error
                        // this.$watch(`output[${latestIndex}].satuan`, (value) => {
                        //     satuanSelect.val(value).trigger("change");
                        // });
                    })
                },
                hapusOutput(index) {
                    this.output.splice(index, 1)
                },
                initializeOpd() {
                    const opdSelect = $('#opd').select2()

                    opdSelect.on('select2:select', (event) => {
                        this.selectedOpd = event.target.value
                        this.selectedSubKegiatan = null;
                        this.output = [];
                        this.selectedSubKegiatanPeriodeTahun = [];
                    })

                    this.$watch('selectedOpd', (value) => {
                        opdSelect.val(value).trigger("change");
                    });

                    @if($isEdit)
                        this.selectedOpd = {{$sasaranSubKegiatan->getPerangkatDaerah()->id}}
                    @else
                        this.selectedOpd = {{$opds->first()->id}}
                    @endIf
                },
                initializeSubKegiatan() {
                    const subKegiatanSelect = $('#subKegiatan').select2({
                        ajax:{
                            url: (params) => {
                                return `/api/opds/${this.selectedOpd}/renstra/sub-kegiatan`;
                            },
                            dataType: 'json',
                            processResults: (data) => {
                                this.fetchedSubKegiatan = data.data.sub_kegiatan
                                return {
                                    results: data.data.sub_kegiatan.map(data => {
                                        return {
                                            text: data.mst_sub_kegiatan.name,
                                            id: data.id
                                        }
                                    })
                                }
                            }
                        },
                    })

                    subKegiatanDropdownSelected = (value, tambahOutput = false) => {
                        this.selectedSubKegiatan = value
                        const selectedSubKegiatan = this.fetchedSubKegiatan.find(item => item.id == value)
                        this.selectedSubKegiatanPeriodeTahun = selectedSubKegiatan.tahun_periode
                        this.output = [];
                        if(tambahOutput) this.tambahOutput();
                    }

                    subKegiatanSelect.on('select2:select', (event) => {
                        subKegiatanDropdownSelected(event.target.value)
                    })

                    this.$watch('selectedSubKegiatan', (value) => {
                        subKegiatanSelect.val(value).trigger("change");
                    });

                    @if($isEdit)
                        const option = new Option('{{ preg_replace('/\r/', '', $sasaranSubKegiatan->subKegiatan->mstSubKegiatan->name)}}', {{$sasaranSubKegiatan->subKegiatan->id}}, true, true);
                        subKegiatanSelect.append(option).trigger('change');
                        subKegiatanSelect.val({{$sasaranSubKegiatan->subKegiatan->mstSubKegiatan->id}}).trigger("change");
                        this.fetchedSubKegiatan = [@json($transformedSubKegiatan)]
                        subKegiatanDropdownSelected({{$sasaranSubKegiatan->subKegiatan->id}})
                        @foreach ($sasaranSubKegiatan->output as $output)
                            @php
                                $targetPertahun = $output->target->map(function($target) {
                                    return [
                                        "id" => $target->id,
                                        "id_tahun" => $target->tahun->id,
                                        "label" => $target->tahun->tahun,
                                        "value" => $target->target
                                    ];
                                });
                            @endphp
                            this.tambahOutput({
                                id: {{$output->id}},
                                cara_pengukuran: '{{$output->cara_pengukuran}}',
                                nama: '{{$output->name}}',
                                satuan: {{$output->id_satuan}},
                                is_pk: {{$output->is_pk ? 'true' : 'false'}},
                                is_iku: {{$output->is_iku ? 'true' :  'false'}},
                                target_awal: '{{$output->target_awal}}',
                                target_akhir: '{{$output->target_akhir}}',
                                target_pertahun: @json($targetPertahun)
                            })
                        @endforeach
                    @endif
                }
            }
        })
    })
</script>
