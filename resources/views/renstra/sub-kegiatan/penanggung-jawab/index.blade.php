@extends('layouts.app')
@section('title')
    Penanggung Jawab Sub Kegiatan
@endsection
@section('content')

    @include('components.content-header', [
        "title" => "Penanggung Jawab Sub Kegiatan",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "Renstra", "href" => "#"],
            [ "label" => "Program", "href" => "#"],
            [ "label" => "Penanggung Jawab Sub Kegiatan", "href" => "#"],
        ]
    ])
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection', [
            'redirectUrl' => route('renstra.sub_kegiatan.penanggung_jawab.index')
        ])
    </div>
    @include('partials.error-message')
    @include('partials.message')
    <div class="card mb-4">
        @if ($sasaranSubKegiatanOutput->count() > 0)
            <form action="{{ route('renstra.sub_kegiatan.penanggung_jawab.mass_update') }}" method="POST">
                @csrf
                <div class="card-body">
                    <div class="card-header">
                        <h6 class="card-title">Data Penanggung Jawab OPD</h6>
                    </div>
                    <hr>
                    @foreach ($sasaranSubKegiatanOutput as $output)
                        @php
                            $hierarchy = [
                                '/ Misi' => $output->getMisi()->misi,
                                'RPJMD / Tujuan' => $output->getRpjmdTujuan()->name,
                                'RPJMN / Sasaran' => $output->getRpjmdSasaran()->name,
                                'RPJMD / Prioritas Pembangunan' => $output->getPrioritasPembangunan()->name,
                                '/ Perangkat Daerah' => $output->getPerangkatDaerah()->name,
                                'Renstra / Tujuan' => $output->getRenstraTujuan()->name,
                                'Renstra / Sasaran' => $output->getRenstraSasaran()->name,
                            ];
                            $title = 'Output ' . $output->getPerangkatDaerah()->name . ' : ' . $output->name;
                            $itemId = "output$output->id";
                            $outputIndex = $loop->index;
                        @endphp
                        <x-hierarchy-card :title="$title" :hierarchy="$hierarchy">
                            <div x-data="{{ $itemId }}">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Penanggungjawab</th>
                                            @foreach ($periode->periodePerTahun as $tahun)
                                                @php $selected = $output->target->firstWhere('id_periode_per_tahun', $tahun->id) @endphp
                                                <th>Target {{ $tahun->tahun }} ( {{ $selected ? $selected->target : '-' }}  )</th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <input type="hidden" name="output[{{ $outputIndex }}][id]" value="{{$output->id}}"/>
                                        <template x-for="(pegawai, index) in penanggungJawab" :key="index">
                                            <tr>
                                                <td class="flex">
                                                    <input
                                                        type="hidden"
                                                        :value="pegawai.id"
                                                        :name="'output[{{ $outputIndex }}][penanggung_jawab]['+index+'][id]'"/>
                                                    <select
                                                        :id="'{{ $itemId }}_'+index"
                                                        :name="'output[{{ $outputIndex }}][penanggung_jawab]['+index+'][pegawai_id]'"
                                                        class="flex-grow mb-2 w-full">
                                                    </select>
                                                    <button
                                                        @click="hapusPenanggungJawab(index)"
                                                        type="button"
                                                        class="flex-none btn btn-danger btn-sm btn-close ml-2">
                                                        <i class="fa fa-close"></i>
                                                    </button>
                                                </td>
                                                @foreach ($periode->periodePerTahun as $key => $tahun)
                                                    <td>
                                                        <input
                                                            type="hidden"
                                                            :name="'output[{{ $outputIndex }}][penanggung_jawab]['+index+'][target][{{ $key }}][periode_id]'"
                                                            value="{{ $tahun->id }}"/>
                                                        <input
                                                            :name="'output[{{ $outputIndex }}][penanggung_jawab]['+index+'][target][{{ $key }}][target]'"
                                                            type="text"
                                                            class="form-control"
                                                            :value="getTarget(index, {{ $tahun->id }})"
                                                            @keyup="event => ubahTarget(index, {{ $tahun->id }}, event.target.value)"/>
                                                    </td>
                                                @endforeach
                                            </tr>
                                        </template>
                                    </tbody>
                                </table>
                                <button type="button" class="btn btn-primary btn-sm btn_clone" @click="tambahPenanggungJawab()">
                                    <i class="fa fa-plus"></i> Tambah Penanggung Jawab
                                </button>
                            </div>
                            <script>
                                document.addEventListener('alpine:init', () => {
                                    Alpine.data("{{ $itemId }}", () => {
                                        return {
                                            init() {
                                                const initialPenanggungJawab = (@json($output->sasaranSubKegiatanOutputPenanggungJawab))
                                                        .map(penanggungJawab => ({
                                                            id: penanggungJawab.id,
                                                            pegawai_id: penanggungJawab.pegawai.id,
                                                            target: penanggungJawab.target.map(target => ({
                                                                periode_id: target.id_periode_per_tahun,
                                                                target: target.target
                                                            }))
                                                        }));

                                                if(initialPenanggungJawab.length == 0) {
                                                    this.tambahPenanggungJawab()
                                                } else {
                                                    initialPenanggungJawab.forEach((initial) => {
                                                        this.tambahPenanggungJawab(initial)
                                                    })
                                                }
                                            },
                                            baseComponentId: '{{ $itemId }}',
                                            penanggungJawab: [],
                                            tambahPenanggungJawab(penanggungJawabObject) {
                                                this.penanggungJawab.push(penanggungJawabObject || {
                                                    id: null,
                                                    pegawai_id: null,
                                                    target: []
                                                })
                                                const latestIndex = this.penanggungJawab.length - 1

                                                this.$nextTick(() => {
                                                    const select2 = $(`#${this.baseComponentId}_${latestIndex}`).select2({
                                                        width: '100%',
                                                        ajax: {
                                                            url: "{{ route('api.opds.pegawai', [ 'opd' => $selectedOpd, 'sasaran' => 'sub-kegiatan' ]) }}",
                                                            dataType: 'json',
                                                            data: function(params) {
                                                                return {
                                                                    search: params.term
                                                                }
                                                            },
                                                            processResults: function(data) {
                                                                return {
                                                                    results: data.data.pegawai.map(data => {
                                                                        return {
                                                                            text: data.nama,
                                                                            id: data.id
                                                                        }
                                                                    })
                                                                }
                                                            }
                                                        }
                                                    })

                                                    // prepopulate initial options so that user name available in dropdown options
                                                    if(penanggungJawabObject != null) {
                                                        const initialData = @json($output->sasaranSubKegiatanOutputPenanggungJawab);
                                                        initialData.forEach(item => {
                                                            const option = new Option(item.pegawai.nama, item.pegawai.id, true, true)
                                                            select2.append(option).trigger('change')
                                                        });
                                                        select2.val(penanggungJawabObject.pegawai_id).trigger("change");
                                                    }

                                                    // AlpineJS and Select2 is entirely different library
                                                    // so there is no way for this two library to communicate each other
                                                    // because of this, i create a listener to listen for select2 value change
                                                    // if select2 changed then modify value on alpinejs
                                                    // or alpinejs value changed, listen it from watcher, then modify select2 value

                                                    select2.on("select2:select", (event) => {
                                                        this.penanggungJawab[latestIndex].pegawai_id = event.target.value
                                                    });

                                                    this.$watch(`penanggungJawab[${latestIndex}]`, (value) => {
                                                        select2.val(value.pegawai_id).trigger("change");
                                                    });
                                                })
                                            },
                                            hapusPenanggungJawab(index) {
                                                this.penanggungJawab.splice(index, 1)
                                            },
                                            ubahTarget(penanggungJawabIndex, periodeTahunId, target) {
                                                const existingIndex = this.penanggungJawab[penanggungJawabIndex]
                                                    .target.findIndex(target => target.periode_id == periodeTahunId)

                                                if(existingIndex != -1) {
                                                    // change value first
                                                    this.penanggungJawab[penanggungJawabIndex].target[existingIndex].target = target
                                                    // then rebuild the array
                                                    this.penanggungJawab[penanggungJawabIndex].target = [ ...this.penanggungJawab[penanggungJawabIndex].target ]
                                                } else {
                                                    this.penanggungJawab[penanggungJawabIndex].target.push({
                                                        periode_id: periodeTahunId,
                                                        target
                                                    })
                                                }
                                            },
                                            getTarget(penanggungJawabIndex, periodeTahunId) {
                                                const target = this.penanggungJawab[penanggungJawabIndex]
                                                    .target.find(target => target.periode_id == periodeTahunId)

                                                return target ? target.target : ''
                                            }
                                        }
                                    })
                                })
                            </script>
                        </x-hierarchy-card>
                    @endforeach

                    <div class="input-group input-group-outline mt-4">
                        <button class="btn btn-dark cancel" onclick="cancel('{{route('renstra.sub_kegiatan.penanggung_jawab.index')}}')" type="button">Cancel</button>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        @endif
    </div>
@endsection
