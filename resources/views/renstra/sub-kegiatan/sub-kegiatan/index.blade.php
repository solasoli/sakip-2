@extends('layouts.app')
@section('title')
    Sub Kegiatan
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Sub Kegiatan Renstra",
        "items" => [
                [ "label" => "Dashboard", "href" => "/"],
                [ "label" => "Renstra", "href" => "#"],
                [ "label" => "Sub Kegiatan", "href" => "#"],
                [ "label" => "Sub Kegiatan Renstra", "href" => "#"],
        ]
    ])
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection', [
          'redirectUrl' => route('renstra.sub_kegiatan.sub_kegiatan.index')
        ])
    </div>
    <div class="card mb-4">
        @include('partials.error-message')
        @include('partials.message')
        <div class="card-body">
            <div class="card-header">
                <h6 class="card-title">Data Sub Kegiatan OPD</h6>
                <a href="{{ route('renstra.sub_kegiatan.sub_kegiatan.create') }}" class="btn btn-primary btn-sm">
                    <i class="fa fa-plus"></i>
                    <span>Tambah</span>
                </a>
            </div>
            <hr>
            @foreach ($kegiatan as $kegiatanItem)
                @php
                    $hierarchy = [
                        '/ Misi' => $kegiatanItem->getMisi()->misi,
                        'RPJMD / Tujuan' => $kegiatanItem->getRpjmdTujuan()->name,
                        'RPJMN / Sasaran' => $kegiatanItem->getRpjmdSasaran()->name,
                        'RPJMD / Prioritas Pembangunan' => $kegiatanItem->getPrioritasPembangunan()->name,
                        '/ Perangkat Daerah' => $kegiatanItem->getPerangkatDaerah()->name,
                        'Renstra / Tujuan' => $kegiatanItem->getRenstraTujuan()->name,
                        'Renstra / Sasaran' => $kegiatanItem->getRenstraSasaran()->name,
                        'Renstra / Program' => $kegiatanItem->getMstProgram()->name,
                        'Renstra / Kegiatan' => $kegiatanItem->mstKegiatan->name,
                    ];
                    $title = 'Kegiatan : ' . $kegiatanItem->mstKegiatan->name;
                    $itemId = "sasaran$kegiatanItem->id";
                @endphp
                <x-hierarchy-card :title="$title" :hierarchy="$hierarchy">
                    <div class="table-responsive">
                        <table class="table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th>Sub Kegiatan</th>
                                @foreach ($periode->periodePerTahun as $tahun)
                                    <th>Anggaran {{$tahun->tahun}}</th>
                                @endforeach
                                <th>Sumber Anggaran</th>
                                <th>Anggaran Renja</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($kegiatanItem->subKegiatan as $subKegiatan)
                                <tr>
                                    <td>{{$subKegiatan->mstSubKegiatan->name}}</td>
                                    @foreach ($periode->periodePerTahun as $tahun)
                                    @php $anggaran = $subKegiatan->anggaranRenstra->firstWhere('id_periode_per_tahun', $tahun->id); @endphp
                                        <th>{{$anggaran->nilai ?? null}}</th>
                                    @endforeach
                                    <td><a href="#" data-bs-toggle="modal" data-bs-target="#{{$subKegiatan->id}}_anggaran">Selengkapnya</a></td>
                                    <td>
                                        <button data-bs-toggle="modal" data-bs-target="#{{$subKegiatan->id}}_renja" class="btn btn-success btn-sm duplicate-btn">+ Tambah Anggaran</button>
                                    </td>
                                    <td>
                                        <form class="inline-block" action="{{route('renstra.sub_kegiatan.sub_kegiatan.destroy', [ "subKegiatan" => $subKegiatan->id])}}" method="POST">
                                            @csrf
                                            @method('DELETE')

                                            <button class="btn btn-danger btn-sm ml-2" type="submit" onclick="return confirm('Lanjukan menghapus data?')">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </form>
                                        <a class="btn btn-warning btn-sm ml-2"href="{{route('renstra.sub_kegiatan.sub_kegiatan.edit', ['subKegiatan' => $subKegiatan->id])}}"> <i class="fa fa-edit"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                    
                    @foreach ($kegiatanItem->subKegiatan as $subKegiatan)
                        @include('renstra.sub-kegiatan.sub-kegiatan.parts.sumber-anggara-modal', [
                            "subKegiatan" => $subKegiatan,
                        ])
                        @include('renstra.sub-kegiatan.sub-kegiatan.parts.renja-modal', [
                            "subKegiatan" => $subKegiatan,
                            "periode" => $periode,
                        ])
                    @endforeach
                </x-hierarchy-card>
            @endforeach
        </div>
    </div>
@endsection
