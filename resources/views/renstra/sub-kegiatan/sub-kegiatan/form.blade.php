@php
    $isEdit = isset($subKegiatan);
    $url = $isEdit
        ? route('renstra.sub_kegiatan.sub_kegiatan.update', [ "subKegiatan" => $subKegiatan->id ])
        : route('renstra.sub_kegiatan.sub_kegiatan.store');
@endphp

<div class="card">
    <div class="card-header">
        <span>{{$headline}}</span>
    </div>
    <form action="{{$url}}" method="POST" x-data="form">
        @csrf
        @if ($isEdit)
            @method('PUT')
        @endif
        <div class="card-body">
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="opd">Perangkat Daerah</label>
                    <select name="opd" id="opd" class="opd form-control" x-model="selectedOpd">
                        @foreach ($opds as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="kegiatan">Kegiatan PD</label>
                    <select name="kegiatan" class="form-control" id="kegiatan"></select>
                </div>
            </div>
            <div class="input-group input-group-outline">
                <div class="col-4">
                    <label for="sub_kegiatan">Sub Kegiatan</label>
                    <select name="sub_kegiatan" class="form-control" id="sub_kegiatan"></select>
                </div>
            </div>
            <template x-for="(anggaranItem, index) in anggaran">
                <div class="input-group input-group-outline" >
                    <div class="col-4">
                        <label x-text="anggaranItem.label"></label>
                        <input type="hidden" :name="`anggaran[${index}][id_tahun]`" :value="anggaranItem.id_tahun">
                        <input
                            x-model="anggaranItem.value"
                            type="text"
                            class="form-control"
                            :name="`anggaran[${index}][value]`">
                    </div>
                </div>
            </template>
            <template x-for="(sumber, index) in sumberAnggaran">
                <div class="input-group input-group-outline">
                    <div class="col-4">
                        <template x-if="index == 0">
                            <label :for="`sumber${index}`">Sumber Anggaran</label>
                        </template>
                        <div class="flex">
                            <select :name="`sumber[${index}]`" class="form-control" :id="`sumber${index}`" x-model="sumber.value">
                                @foreach ($sumberAnggaran as $sumber)
                                    <option value="{{$sumber->id}}">{{$sumber->name}}</option>
                                @endforeach
                            </select>
                            <template x-if="index != 0">
                                <button class="btn btn-danger btn-sm ml-2" @click.prevent="sumberAnggaran.splice(index, 1)" title="Hapus">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </template>
                        </div>
                    </div>
                </div>
            </template>
            <button @click.prevent="tambahSumberAnggaran" class="close-duplicate badge badge-success ml-3 p-2">
                <i class="fa fa-times"></i> Tambah Sumber Anggaran
            </button>
        </div>
        <div class="card-footer">
            <button class="btn btn-dark cancel" onclick="return window.history.back()" type="button">Cancel</button>
            <button class="btn btn-primary" type="submit">Submit</button>
        </div>
    </form>
</div>

<script>
    document.addEventListener('alpine:init', () => {
        Alpine.data('form', () => {
            return {
                selectedOpd: null,
                selectedKegiatan: null,
                selectedSubKegiatan: null,
                fetchedKegiatan: [],
                anggaran: [],
                selectedKegiatanPeriodeTahun : [],
                sumberAnggaran: [],
                init() {
                    this.initializeOpd();
                    this.initializeKegiatan();
                    this.initializeSubKegiatan();

                    @if ($isEdit)
                        @foreach ($subKegiatan->sumberAnggaran as $sumberAnggaran)
                            this.tambahSumberAnggaran({{$sumberAnggaran->id}})
                        @endforeach
                        @php
                            $anggaran = $subKegiatan->anggaranRenstra->map(function($anggaran) {
                                return [
                                    "id_tahun" => $anggaran->id_periode_per_tahun,
                                    "label" => "Anggaran Tahun " . $anggaran->tahun->tahun,
                                    "value" => $anggaran->nilai
                                ];
                            });
                        @endphp
                        this.anggaran = @json($anggaran)
                    @else
                        this.tambahSumberAnggaran();
                    @endif
                },
                initializeOpd() {
                    const opdSelect = $('#opd').select2()

                    opdSelect.on('select2:select', (event) => {
                        this.selectedOpd = event.target.value
                        this.selectedKegiatan = null
                        this.selectedSubKegiatan = null
                    })

                    this.$watch('selectedOpd', (value) => {
                        opdSelect.val(value).trigger("change");
                    });


                    @if ($isEdit)
                        this.selectedOpd = {{$subKegiatan->getPerangkatDaerah()->id}}
                    @else
                        this.selectedOpd = {{$opds->first()->id}}
                    @endif
                },
                initializeKegiatan() {
                    const kegiatanSelect = $('#kegiatan').select2({
                        ajax:{
                            url: (params) => {
                                return `/api/opds/${this.selectedOpd}/renstra/kegiatan`;
                            },
                            dataType: 'json',
                            processResults: (data) => {
                                this.fetchedKegiatan = data.data.kegiatan
                                return {
                                    results: data.data.kegiatan.map(data => {
                                        return {
                                            text: data.mst_kegiatan.name,
                                            id: data.id
                                        }
                                    })
                                }
                            }
                        },
                    })

                    kegiatanDropdownSelected = (value) => {
                        this.selectedKegiatan = value
                        this.selectedSubKegiatan = null
                        const selectedKegiatan = this.fetchedKegiatan.find(item => item.id == value)
                        this.anggaran = selectedKegiatan.tahun_periode.map((tahun) => {
                            return {
                                id_tahun: tahun.id,
                                label: `Anggaran Tahun ${tahun.tahun}`,
                                value: null
                            }
                        });
                    }

                    kegiatanSelect.on('select2:select', (event) => {
                        kegiatanDropdownSelected(event.target.value)
                    })

                    this.$watch('selectedKegiatan', (value) => {
                        kegiatanSelect.val(value).trigger("change");
                    });

                    @if ($isEdit)
                        const option = new Option('{{preg_replace('/\r/', '', $subKegiatan->kegiatan->mstKegiatan->name)}}', {{$subKegiatan->id_renstra_kegiatan}}, true , true)
                        kegiatanSelect.append(option).trigger('change');
                        this.selectedKegiatan = {{$subKegiatan->id_renstra_kegiatan}}
                    @endif
                },
                initializeSubKegiatan() {
                    const subKegiatanSelect = $('#sub_kegiatan').select2({
                        ajax:{
                            url: (params) => {
                                return `/api/renstra/kegiatan/${this.selectedKegiatan}/mst-sub-kegiatan`;
                            },
                            dataType: 'json',
                            processResults: (data) => {
                                return {
                                    results: data.data.sub_kegiatan.map(data => {
                                        return {
                                            text: data.name,
                                            id: data.id
                                        }
                                    })
                                }
                            }
                        },
                    })

                    subKegiatanSelect.on('select2:select', (event) => this.selectedSubKegiatan = event.target.value)

                    this.$watch('selectedSubKegiatan', (value) => {
                        subKegiatanSelect.val(value).trigger("change");
                    });

                    @if ($isEdit)
                        const option = new Option('{{preg_replace('/\r/', '', $subKegiatan->mstSubKegiatan->name)}}', {{$subKegiatan->id}}, true , true)
                        subKegiatanSelect.append(option).trigger('change');
                        this.selectedSubKegiatan = {{$subKegiatan->id}}
                    @endif
                },
                tambahSumberAnggaran(selected) {
                    this.sumberAnggaran.push({value: selected});
                    const index = this.sumberAnggaran.length - 1;

                    this.$nextTick(() => {
                        const select2 = $(`#sumber${index}`).select2();

                        select2.on('select2:select', (event) => {
                            this.sumberAnggaran[index] = event.target.value
                        })

                        this.$watch(`sumberAnggaran[${index}]`, (value) => {
                            select2.val(value).trigger("change");
                        });
                    })
                },
            }
        })
    })
</script>
