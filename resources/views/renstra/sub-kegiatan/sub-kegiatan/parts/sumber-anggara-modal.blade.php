<div class="modal fade" id="{{$subKegiatan->id}}_anggaran" tabindex="-1"
    role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Sumber Anggaran</h5>
                <button type="button" class="close" data-bs-dismiss="modal"
                    aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    Kegiatan :
                </div>
                <div>
                    {{ $subKegiatan->mstSubKegiatan->name }}
                </div>
                <br>
                <br>
                <span>
                    Sumber Anggaran : <br />
                    <ul>
                        @foreach ($subKegiatan->sumberAnggaran as $sumberAnggaran)
                            <li>{{$sumberAnggaran->name}}</li>
                        @endforeach
                    </ul>
                </span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                    data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
