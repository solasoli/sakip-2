@php $modalId = $subKegiatan->id."_renja" ;@endphp
<div class="modal fade" id="{{$modalId}}" tabindex="-1" aria-labelledby="{{$modalId}}_label" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form
                action="{{ route('renstra.sub_kegiatan.sub_kegiatan.renja.submit', [
                    'subKegiatan' => $subKegiatan->id,
                ]) }}"
                method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="{{$modalId}}_label">Anggaran Renja Sub Kegiatan</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2">Sub Kegiatan</th>
                                    <th colspan="{{$periode->periodePerTahun->count()}}">Target Renja</th>
                                </tr>
                                <tr>
                                    @foreach ($periode->periodePerTahun as $tahun)
                                        <th>{{$tahun->tahun}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-overflow: ellipsis;" class="break-all max-w-md overflow-hidden">{{$subKegiatan->mstSubKegiatan->name}}</td>
                                    @foreach ($periode->periodePerTahun as $periode)
                                        @php $targetRenja = $subKegiatan->anggaranRenja->firstWhere('id_periode_per_tahun', $periode->id); @endphp
                                        <input type="hidden"
                                            name="renja[{{ $loop->index }}][id_tahun]"
                                            value="{{ $periode->id }}">
                                        <td class="text-center">
                                            <input
                                                type="text"
                                                class="form-control"
                                                name="renja[{{ $loop->index }}][value]"
                                                value="{{ $targetRenja != null ? $targetRenja->nilai : null }}"
                                                >
                                        </td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                        data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
