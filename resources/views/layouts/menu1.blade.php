@php use App\Constants\AuthorizationModule; @endphp

<div class="sidenav-header sticky-top bg-white">
    <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
        aria-hidden="true" id="iconSidenav"></i>
    <a class="navbar-brand m-0 d-flex align-items-center justify-content-between" href="{{ url('/') }}" target="_self">
        <div class="d-flex align-items-center">
            <img src="{{ asset('v1/img/logo-kabupaten-bogor.png') }}" class="navbar-brand-img h-100 mr-2"
                alt="main_logo">
            <span class="ms-1 font-weight-bold text-black">E-SAKIP</span>
        </div>
        <i class="fas fa-times text-white sakip-close-nav d-flex d-lg-none"></i>
    </a>
</div>
<hr class="horizontal dark mt-0 mb-2">
<nav class="sidebar py-2 mb-4">
    <ul class="nav flex-column" id="nav_accordion">
        <li class="nav-item">
            <a class="nav-link text-white" href="{{ url('/') }}">
                <div class="text-black text-center  me-2  d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10"></i>
                </div>
                <span class="nav-link-text text-black ms-1 text-uppercase font-weight-bolder">Dashboard</span>
            </a>
        </li>
        @can('access-module', AuthorizationModule::MASTER)
        <li class="nav-item has-submenu mt-3">
            <a class="nav-link ps-4 ms-2 text-uppercase text-xs text-black font-weight-bolder opacity-8" href="#">
                <div class="text-black text-center me-2 d-flex align-items-center ">

                    <span class="nav-link-text text-black ms-1">Master</span>
                </div>
            </a>
            <ul class="submenu collapse">
                <li><a class="nav-link text-black" href="{{ url('') }}/periode"><span
                            class="nav-link-text ms-1">Periode</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/opd"><span class="nav-link-text ms-1">Perangkat
                            Daerah</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/satuan"><span
                            class="nav-link-text ms-1">Satuan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/urusan"><span
                            class="nav-link-text ms-1">Urusan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/bidang-urusan"><span
                            class="nav-link-text ms-1">Bidang Urusan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/program"><span
                            class="nav-link-text ms-1">Program</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/kegiatan"><span
                            class="nav-link-text ms-1">Kegiatan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/sub-kegiatan"><span
                            class="nav-link-text ms-1">Sub-Kegiatan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/sumber-anggaran"><span
                            class="nav-link-text ms-1">Sumber Anggaran</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/prioritas-pembangunan"><span
                            class="nav-link-text ms-1">Prioritas Pembangunan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/pegawai"><span
                            class="nav-link-text ms-1">Pegawai</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/eselon"><span
                            class="nav-link-text ms-1">Eselon</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/pangkat"><span
                            class="nav-link-text ms-1">Pangkat</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/pangkat-golongan"><span
                            class="nav-link-text ms-1">Pangkat Golongan</span></a></li>
            </ul>
        </li>
        @endcan
        @can('access-module', AuthorizationModule::RPJMD)
        <li class="nav-item has-submenu mt-3">
            <a class="nav-link ps-4 ms-2 text-uppercase text-xs text-black font-weight-bolder opacity-8" href="#">
                <div class="text-black text-center me-2 d-flex align-items-center ">

                    <span class="nav-link-text text-black ms-1">RPJMD</span>
                </div>
            </a>
            <ul class="submenu collapse">
                <li><a class="nav-link text-black" href="{{ url('') }}/visi"><span
                            class="nav-link-text ms-1">Visi</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/misi"><span
                            class="nav-link-text ms-1">Misi</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/tujuan"><span
                            class="nav-link-text ms-1">Tujuan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/sasaran"><span class="nav-link-text ms-1">Sasaran
                            Strategis</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/prioritas-pembangunan"><span
                            class="nav-link-text ms-1">Prioritas Pembangunan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/rpjmd/strategi"><span
                            class="nav-link-text ms-1">Strategi</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/rpjmd/kebijakan"><span
                            class="nav-link-text ms-1">Kebijakan</span></a></li>

            </ul>
        </li>
        @endcan
        @can('access-module', AuthorizationModule::RENSTRA)
        <li class="nav-item has-submenu mt-3">
            <a class="nav-link ps-4 ms-1 text-uppercase text-xs text-black font-weight-bolder opacity-8" href="#">
                <div class="text-black  me-2 d-flex align-items-center ">
                    <span class="nav-link-text text-black ms-1">Renstra Tujuan & Sasaran</span>
                </div>
            </a>
            <ul class="submenu collapse">
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/tujuan"><span
                            class="nav-link-text ms-1">Tujuan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/sasaran"><span
                            class="nav-link-text ms-1">Sasaran</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/sasaran/penanggung-jawab"><span
                            class="nav-link-text ms-1">Pengampu</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/strategi"><span
                            class="nav-link-text ms-1">Strategi</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/kebijakan"><span
                            class="nav-link-text ms-1">Kebijakan</span></a></li>
            </ul>
        </li>


    <li class="nav-item has-submenu mt-3">
		<a class="nav-link ps-4 ms-1 text-uppercase text-xs text-black font-weight-bolder opacity-8" href="#">
             <div class="text-black text-center me-2 d-flex align-items-center ">
                <span class="nav-link-text text-black ms-1">Renstra Program</span>
            </div>  
        </a>
		<ul class="submenu collapse">
			<li><a class="nav-link text-black" href="{{ url('') }}/renstra/program/program"><span class="nav-link-text ms-1">Program</span></a></li>
			<li><a class="nav-link text-black" href="{{ url('') }}/renstra/program/sasaran-program"><span class="nav-link-text ms-1">Sasaran Program</span></a></li>
            <li><a class="nav-link text-black" href="{{ url('') }}/renstra/program/penanggung-jawab"><span class="nav-link-text ms-1">Pengampu</span></a></li>
		</ul>
	</li>

        <!-- <li class="nav-item has-submenu mt-3">
            <a class="nav-link ps-4 ms-1 text-uppercase text-xs text-black font-weight-bolder opacity-8" href="#">
                <div class="text-black text-center me-2 d-flex align-items-center ">
                    <span class="nav-link-text text-black ms-1">Renstra Program</span>
                </div>
            </a>
            <ul class="submenu collapse">
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/program/program"><span
                            class="nav-link-text ms-1">Tujuan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/program/sasaran-program"><span
                            class="nav-link-text ms-1">Sasaran Program</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/program/penanggung-jawab"><span
                            class="nav-link-text ms-1">Pengampu</span></a></li>
            </ul>
        </li> -->

        <li class="nav-item has-submenu mt-3">
            <a class="nav-link ps-4 ms-1 text-uppercase text-xs text-black font-weight-bolder opacity-8" href="#">
                <div class="text-black text-center me-2 d-flex align-items-center ">
                    <span class="nav-link-text text-black ms-1">Renstra Kegiatan</span>
                </div>
            </a>
            <ul class="submenu collapse">
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/kegiatan/kegiatan"><span
                            class="nav-link-text ms-1">Kegiatan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/kegiatan/sasaran-kegiatan"><span
                            class="nav-link-text ms-1">Sasaran Kegiatan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/kegiatan/penanggung-jawab-kegiatan"><span
                            class="nav-link-text ms-1">Pengampu</span></a></li>
            </ul>
        </li>

        <li class="nav-item has-submenu mt-3">
            <a class="nav-link ps-4 ms-1 text-uppercase text-xs text-black font-weight-bolder opacity-8" href="#">
                <div class="text-black  me-2 d-flex align-items-center ">
                    <span class="nav-link-text text-black ms-1">Renstra Sub Kegiatan</span>
                </div>
            </a>
            <ul class="submenu collapse">
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/sub-kegiatan/sub-kegiatan"><span
                            class="nav-link-text ms-1">Sub-Kegiatan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/sub-kegiatan/sasaran-sub-kegiatan"><span
                            class="nav-link-text ms-1">Sasaran & Output Sub-Kegiatan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/renstra/sub-kegiatan/penanggung-jawab"><span
                            class="nav-link-text ms-1">Pengampu</span></a></li>
            </ul>
        </li>
        @endcan
        @can('access-module', AuthorizationModule::PK)
        <li class="nav-item has-submenu mt-3">
            <a class="nav-link ps-4 ms-1 text-uppercase text-xs text-black font-weight-bolder opacity-8" href="#">
                <div class="text-black  me-2 d-flex align-items-center ">
                    <span class="nav-link-text text-black ms-1">Perjanjian Kinerja</span>
                </div>
            </a>
            <ul class="submenu collapse">
                <li><a class="nav-link text-black"
                        href="{{ url('') }}/perjanjian-kinerja/indikator-sasaran/tahunan"><span
                            class="nav-link-text ms-1">Target PK Sasaran Tahunan</span></a></li>
                <li><a class="nav-link text-black"
                        href="{{ url('') }}/perjanjian-kinerja/indikator-program/tahunan"><span
                            class="nav-link-text ms-1">Target PK Program Tahunan</span></a></li>
                <li><a class="nav-link text-black"
                        href="{{ url('') }}/perjanjian-kinerja/indikator-kegiatan/tahunan"><span
                            class="nav-link-text ms-1">Target PK Kegiatan Tahunan</span></a></li>
                <li><a class="nav-link text-black"
                        href="{{ url('') }}/perjanjian-kinerja/indikator-kegiatan/tahunan"><span
                            class="nav-link-text ms-1">Target PK  Sub Kegiatan Tahunan</span></a></li>
            </ul>
        </li>
        @endcan
        @can('access-module', AuthorizationModule::LAPORAN)
        <li class="nav-item has-submenu mt-3">
            <a class="nav-link ps-4 ms-1 text-uppercase text-xs text-black font-weight-bolder opacity-8" href="#">
                <div class="text-black  me-2 d-flex align-items-center ">
                    <span class="nav-link-text text-black ms-1">Laporan</span>
                </div>
            </a>
            <ul class="submenu collapse">
                <li><a class="nav-link text-black" href="{{ url('') }}/laporan/rpjmd"><span
                            class="nav-link-text ms-1">Target PK Sasaran Tahunan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/laporan/renstra"><span
                            class="nav-link-text ms-1">Target PK Program Tahunan</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/laporan/perjanjian-kinerja"><span
                            class="nav-link-text ms-1">Target PK Kegiatan Tahunan</span></a></li>
                <!-- <li><a class="nav-link text-black" href="{{ url('') }}/laporan"><span class="nav-link-text ms-1">Target PK Output Sub-Kegiatan Tahunan</span></a></li> -->
            </ul>
        </li>
        @endcan
        @can('access-module', AuthorizationModule::CONFIGURATION)
        <li class="nav-item has-submenu mt-3">
            <a class="nav-link ps-4 ms-1 text-uppercase text-xs text-black font-weight-bolder opacity-8" href="#">
                <div class="text-black  me-2 d-flex align-items-center ">
                    <span class="nav-link-text text-black ms-1">Konfigurasi</span>
                </div>
            </a>
            <ul class="submenu collapse">
                <li><a class="nav-link text-black" href="{{ url('') }}/konfigurasi"><span
                            class="nav-link-text ms-1">Manajemen Periode</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/sync/pegawai"><span
                            class="nav-link-text ms-1">Sinkronisasi Pegawai</span></a></li>
                <li><a class="nav-link text-black" href="{{ url('') }}/users"><span class="nav-link-text ms-1">Manajemen
                            Pengguna</span></a></li>
                <!-- <li><a class="nav-link text-black" href="{{ url('') }}/laporan"><span class="nav-link-text ms-1">Target PK Output Sub-Kegiatan Tahunan</span></a></li> -->
            </ul>
        </li>
        @endcan

        <div>
            <a href="{{ route('profile.change_password.index') }}">
                <button type="submit" class="btn btn-primary mt-4 w-100" style="background-color:#4caf50;">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                    Ganti Password
                </button>
                <form action="{{ route('logout') }}" method="post">
                    @csrf
                    <button type="submit" class="btn btn-primary mt-4 w-100">Logout</button>
                </form>
            </a>
        </div>
    </ul>
</nav>