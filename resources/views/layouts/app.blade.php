@component('layouts.head')
@endcomponent
<style>
body {
    overflow-x: hidden;
}

table {
    white-space: nowrap;
}

.lowercase {
    text-transform: lowercase;
}

.uppercase {
    text-transform: uppercase;
}

.capitalize {
    text-transform: capitalize;
}

.nav-link:hover {
    background-color:#4caf50;
    border-radius: 0.375rem;
}

.nav-link:hover {
    background-color: #4caf50;
    border-radius: 0.375rem;
}

.nav-link:hover {
    background-color: #4caf50;
    border-radius: 0.375rem;
}




</style>

<body class="g-sidenav-show  bg-white-200">
    <aside
        class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3 bg-white"
        id="sidenav-main">
        @component('layouts.menu1')
        @endcomponent
    </aside>
    <main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">
        <!-- Navbar -->
        <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 d-flex shadow-none border-radius-xl"
            navbar-scroll="true">
            <div id="content-wrapper" class="col-12" style="background-color: #fff; ">
                <nav class="navbar navbar-expand navbar-light bg-white topbar static-top shadow">
                    <div class="hamburger pe-3">
                        <i class="fas fa-bars sakip-open-nav"></i>
                    </div>
                    <div class="name-apps">
                        <span>Sistem Akuntabilitas Kinerja Instansi Pemerintah</span>
                    </div>
                    <ul class="ml-auto d-flex align-items-center">
                        @if (! is_null(periode()))
                        <div class="mt-2">
                            <span>
                                <a href="{{ auth()->user()->id == 1 ? url('/konfigurasi') : '#' }}"
                                    style="text-decoration: none; color: #777;">
                                    <small>Periode : {{ periode()->nama }} ({{ periode()->dari }} -
                                        {{ periode()->sampai }})</small>
                                </a>
                            </span>
                        </div>
                        @endif
                    </ul>
                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                {{-- element where blade appended --}}
                {{-- <div class="content-wrapper" style="overflow-y: auto; background: #FFF;">
                    @yield('content')
                    @stack('content')
                </div> --}}

            </div>
        </nav>

        <!-- End Navbar -->
        <div class="container-fluid py-4">
            <div class="row">
                @yield('content')
                @stack('content')
            </div>
            <footer class="footer py-4  ">
                <div class="container-fluid">
                    <div class="row align-items-center d-flex">
                        <div class="col-12 mb-lg-0 mb-4">
                            <div class="copyright text-center text-sm text-muted text-lg-start">
                                ©E-SAKIP Bagian Organisasi, Sekretariat Daerah Pemerintah Kabupaten Bogor
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </main>

    <!--   Core JS Files   -->
    <script src="{{ asset('/v1/js/core/popper.min.js') }}"></script>
    <script src="{{ asset('/v1/js/core/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/v1/js/plugins/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('/v1/js/plugins/smooth-scrollbar.min.js') }}"></script>
    <script src="{{ asset('/v1/js/plugins/chartjs.min.js') }}"></script>
    <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
    {{-- <script src="{{ asset('/v1/js/material-dashboard.min.js?v=3.0.0') }}"></script> --}}
    <script src="{{ asset('template-admin/lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('template-admin/lib/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/myFunction.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
    $(function() {
        setTimeout(function() {
            $('.flashMessages').slideUp('slow');
        }, 3500)

        $('input').attr('autocomplete', 'off');

        if (typeof CKEDITOR != 'undefined') {
            // initial CKEDITOR
            CKEDITOR.replace('.ckeditor');
            CKEDITOR.config.height = 300;
        }
    });

    $('.sakip-open-nav').click(function() {
        $('body').removeClass("g-sidenav-show");
        $('body').addClass("g-sidenav-hide");
    });

    $('.sakip-close-nav').click(function() {
        $('body').removeClass("g-sidenav-hide");
        $('body').addClass("g-sidenav-show");

    });
    </script>
    <script>
    const link_collapse = document.querySelectorAll('.collapse-item');
    link_collapse.forEach(function(res) {
        res.addEventListener('click', function(e) {
            if (e.target.classList.contains('collapsed-custom')) {
                const i = e.target.querySelector('i');
                i.classList.toggle('active');
                if (i.classList.contains('active')) {
                    i.classList.remove('fa-angle-right');
                    i.classList.add('fa-angle-down');
                } else {
                    i.classList.remove('fa-angle-down');
                    i.classList.add('fa-angle-right');
                }
            }
        });
    });
    </script>
    <script>
        document.addEventListener("DOMContentLoaded", function(){
  document.querySelectorAll('.sidebar .nav-link').forEach(function(element){
    
    element.addEventListener('click', function (e) {

      let nextEl = element.nextElementSibling;
      let parentEl  = element.parentElement;	

        if(nextEl) {
            e.preventDefault();	
            let mycollapse = new bootstrap.Collapse(nextEl);
            
            if(nextEl.classList.contains('show')){
              mycollapse.hide();
            } else {
                mycollapse.show();
                // find other submenus with class=show
                var opened_submenu = parentEl.parentElement.querySelector('.submenu.show');
                // if it exists, then close all of them
                if(opened_submenu){
                  new bootstrap.Collapse(opened_submenu);
                }
            }
        }
    }); // addEventListener
  }) // forEach
}); 
    </script>

    <script>
        $(document).ready(function() {
        $("#navbar-vertical ul li a").click(function() {
            var id = $(this);

            $("#navbar-vertical ul li a").removeClass("active");
            $(id).addClass("active");
            sessionStorage.setItem("mylink", $(id).text());
        });

        var mylink = sessionStorage.getItem('mylink');
        if (mylink !== null) {
            $("li a:contains('" + mylink + "')").addClass("active");
        }
    });
    </script>
    @yield('scripts')
    @stack('scripts')
</body>

</html>