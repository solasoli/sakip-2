<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <title>E-SAKIP |
        @yield('title')
    </title>

    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

    <script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700,900|Montserrat+Slab:400,700" />
        <!-- Nucleo Icons -->
    <link href="{{ asset('v1/css/nucleo-icons.css') }}" rel="stylesheet" />
    <link href="{{ asset('v1/css/nucleo-svg.css') }}" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    {{-- Bootstrap 5 --}}
    <link rel="stylesheet" href="{{ asset('v1/css/bootstrap.min.css') }}">
    <script src="{{ asset('v1/js/bootstrap.bundle.min.js') }}"></script>
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('DataTables/datatables.min.css') }}">
    <link rel="stylesheet" type="text/css"
        href="{{ asset('') }}DataTables/DataTables/css/dataTables.bootstrap4.min.css" />
    <link href="{{ asset('/template-admin/lib/highlightjs/github.css') }}" rel="stylesheet">
    <link href="{{ asset('/template-admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
    
    <script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    </script>
    <!-- CSS Files -->
    <link id="pagestyle" href="{{ asset('v1/css/material-dashboard.css?v=3.0.0') }}" rel="stylesheet" />
    @stack('css')
</head>


{{-- <!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

<title>E-SAKIP |
    @yield('title')
</title>

<!-- Custom fonts for this template-->
<link href="{{ asset('') }}vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

<!-- css custom -->
<link rel="stylesheet" href="{{ asset('') }}css/style.css">

<!-- select2 -->
<link href="{{ asset('/template-admin/lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('/vendor/select2/dist/css/select2.min.css') }}" rel="stylesheet">

<link href="{{ asset('/template-admin/lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
<link href="{{ asset('/template-admin/lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
<link href="{{ asset('/template-admin/lib/jquery-switchbutton/jquery.switchButton.css') }}" rel="stylesheet">
<link href="{{ asset('/template-admin/lib/highlightjs/github.css') }}" rel="stylesheet">
<link href="{{ asset('/template-admin/lib/select2/css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('/css/app.css') }}" rel="stylesheet">

<!-- DataTables -->
<link rel="stylesheet" href="{{ asset('') }}DataTables/datatables.min.css">
<link rel="stylesheet" type="text/css" href="{{ asset('') }}DataTables/DataTables/css/dataTables.bootstrap4.min.css" />

<script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
<script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
<!-- Custom styles for this template-->
<link href="{{ asset('') }}css/sb-admin-2.css" rel="stylesheet">

@stack('css')
</head> --}}