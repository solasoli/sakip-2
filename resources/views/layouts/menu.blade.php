<!-- @php use App\Constants\AuthorizationModule; @endphp

<div class="sidenav-header sticky-top bg-gradient-dark">
    <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none"
        aria-hidden="true" id="iconSidenav"></i>
    <a class="navbar-brand m-0 d-flex align-items-center justify-content-between" href="{{ url('/') }}" target="_self">
        <div class="d-flex align-items-center">
            <img src="{{ asset('v1/img/logo-kabupaten-bogor.png') }}" class="navbar-brand-img h-100 mr-2"
                alt="main_logo">
            <span class="ms-1 font-weight-bold text-white">E-SAKIP</span>
        </div>
        <i class="fas fa-times text-white sakip-close-nav d-flex d-lg-none"></i>
    </a>
</div>
<hr class="horizontal light mt-0 mb-2">
<div class="collapse navbar-collapse  w-auto h-auto" id="sidenav-collapse-main">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link text-white" href="{{ url('/') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">dashboard</i>
                </div>
                <span class="nav-link-text ms-1">Dashboard</span>
            </a>
        </li>
        @can('access-module', AuthorizationModule::MASTER)
        
        <li class="nav-item mt-3">
            <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">MASTER</h6>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="{{ url('') }}/periode">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Periode</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white" href="{{ url('') }}/opd">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Perangkat Daerah</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/satuan">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Satuan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/urusan">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Urusan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/bidang-urusan">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Bidang Urusan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/program">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Program</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/kegiatan">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Kegiatan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/sub-kegiatan">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Sub Kegiatan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/sumber-anggaran">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Sumber Anggaran</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/prioritas-pembangunan">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Prioritas Pembangunan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/pegawai">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Pegawai</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/eselon">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Eselon</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/pangkat">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Pangkat</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/pangkat-golongan">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Pangkat Golongan</span>
            </a>
        </li>
        @endcan
        @can('access-module', AuthorizationModule::RPJMD)
        <li class="nav-item mt-3">
            <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">RPJMD</h6>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/visi">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Visi</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/misi">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Misi</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/tujuan">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Tujuan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/sasaran">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Sasaran Strategis</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/rpjmd/prioritas-pembangunan">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Prioritas Pembangunan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/rpjmd/strategi">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Strategi</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/rpjmd/kebijakan">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Kebijakan</span>
            </a>
        </li>
        <li class="nav-item mt-3">
            <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">RPJMD Program</h6>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/rpjmd/program/program">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Program</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/rpjmd/program/sumber-anggaran">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Sumber Anggaran</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/rpjmd/program/penanggung-jawab">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Penanggung Jawab</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/rpjmd/program/indikator-program">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Indikator Program</span>
            </a>
        </li>
        @endcan
        @can('access-module', AuthorizationModule::RENSTRA)
        <li class="nav-item mt-3">
            <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">RENSTRA</h6>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/renstra/tujuan"">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Tujuan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ route('renstra.sasaran.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Sasaran Strategis</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ route('renstra.sasaran.penanggung_jawab.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Penanggung Jawab</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/renstra/strategi">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Strategi</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/renstra/kebijakan">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Kebijakan</span>
            </a>
        </li>
        <li class="nav-item mt-3">
            <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">RENSTRA Program</h6>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/renstra/program/program">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Program</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/renstra/program/sasaran-program">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Sasaran Program</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/renstra/program/penanggung-jawab">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Penanggung Jawab</span>
            </a>
        </li>
        <li class="nav-item mt-3">
            <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">RENSTRA Kegiatan</h6>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/renstra/kegiatan/kegiatan">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Kegiatan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ route('renstra.kegiatan.sasaran_kegiatan.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Sasaran Kegiatan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ route('renstra.kegiatan.penanggung_jawab_kegiatan.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Penanggung Jawab</span>
            </a>
        </li>
        <li class="nav-item mt-3">
            <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">RENSTRA Sub Kegiatan
            </h6>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ route('renstra.sub_kegiatan.sub_kegiatan.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Sub Kegiatan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ route('renstra.sub_kegiatan.sasaran_sub_kegiatan.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Sasaran Sub Kegiatan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ route('renstra.sub_kegiatan.penanggung_jawab.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Penanggung Jawab</span>
            </a>
        </li>
        @endcan
        @can('access-module', AuthorizationModule::PK)
        <li class="nav-item mt-3">
            <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Perjanjian Kinerja PD
            </h6>
            <a class="ps-4 ms-2 text-xs text-white opacity-8 ">Target PK Indikator Sasaran </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ route('perjanjian_kinerja.indikator_sasaran.tahunan.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Tahunan</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ route('perjanjian_kinerja.indikator_sasaran.triwulan.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Triwulan</span>
            </a>
        </li>
        <li class="nav-item mt-0">
            <a class="ps-4 ms-2 text-xs text-white opacity-8 ">Target PK Indikator Program </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link text-white " href="{{ route('perjanjian_kinerja.indikator_program.tahunan.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Tahunan</span>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link text-white " href="{{ route('perjanjian_kinerja.indikator_program.triwulan.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Triwulan</span>
            </a>
        </li>
        <li class="nav-item mt-0">
            <a class="ps-4 ms-2 text-xs text-white opacity-8 ">Target PK Indikator Kegiatan </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link text-white " href="{{ route('perjanjian_kinerja.indikator_kegiatan.tahunan.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Tahunan</span>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link text-white " href="{{ route('perjanjian_kinerja.indikator_kegiatan.triwulan.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Triwulan</span>
            </a>
        </li>
        <li class="nav-item mt-0">
            <a class="ps-4 ms-2 text-xs text-white opacity-8 ">Target PK Output Sub-Kegiatan</a>
        </li>
        <li class="nav-item ">
            <a class="nav-link text-white " href="{{ route('perjanjian_kinerja.output_sub_kegiatan.tahunan.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Tahunan</span>
            </a>
        </li>
        <li class="nav-item ">
            <a class="nav-link text-white " href="{{ route('perjanjian_kinerja.output_sub_kegiatan.triwulan.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Triwulan</span>
            </a>
        </li>
        @endcan
        @can('access-module', AuthorizationModule::LAPORAN)
        <li class="nav-item mt-3">
            <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Laporan</h6>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/laporan/rpjmd">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">RPJMD</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/laporan/renstra">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Renstra</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ route('laporan.perjanjian_kinerja.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Perjanjian Kinerja</span>
            </a>
        </li>
        @endcan
        @can('access-module', AuthorizationModule::CONFIGURATION)
        <li class="nav-item mt-3">
            <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Konfigurasi</h6>
        </li>
        <li class="nav-item">
            <a class="nav-link text-white " href="{{ url('') }}/konfigurasi">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Set Periode</span>
            </a>
        </li>
        <li class="nav-item mb-2">
            <a class="nav-link text-white " href="{{ url('') }}/sync/pegawai">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Sync Pegawai</span>
            </a>
        </li>
        <li class="nav-item mb-2">
            <a class="nav-link text-white " href="{{ route('users.index') }}">
                <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                    <i class="material-icons opacity-10">arrow_right_alt</i>
                </div>
                <span class="nav-link-text ms-1">Data Admin</span>
            </a>
        </li>
        @endcan
        <div class="mx-3 mb-5">
            <a href="{{ route('profile.change_password.index') }}">
                <button type="submit" class="btn bg-gradient-primary mt-4 w-100">
                    <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                    Ganti Password
                </button>
            </a>
        </div>
        <div class="mx-3 mb-5">
            <form action="{{ route('logout') }}" method="post">
                @csrf
                <button type="submit" class="btn bg-gradient-primary mt-4 w-100">Logout</button>
            </form>
        </div>
    </ul>
</div> -->
