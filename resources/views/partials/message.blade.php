@if(session()->get('message'))
<div class="alert alert-success flashMessages" role="alert">
    <p><strong>{{ session()->get('message') }}</strong></p>
</div>
@endif
