@extends('layouts/app')
@section('title')
    OPD
@endsection
@section('content')

<style media="screen">
  .modal-lg{
    width: 750px !important;
  },
   #exampleModalAdd{
        background-color:#5cf97a;
    }
</style>


<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">OPD</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">PD</span>
    </nav>
</div>
{{-- @dd($data) --}}
<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List OPD</h6>
            <button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalAdd" style="background-color:#4caf50;"><i class="fa fa-plus"></i> Tambah</button>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    @if ($message = Session::get('successAdd'))
                    <div class="alert alert-success flashMessages" role="alert">
                        {{ $message }}
                    </div>
                    @elseif ($message = Session::get('successDel'))
                    <div class="alert alert-danger flashMessages" role="alert">
                        {{ $message }}
                    </div>
                    @elseif ($message = Session::get('successEdit'))
                    <div class="alert alert-success flashMessages" role="alert">
                        {{ $message }}
                    </div>
                    @endif
                    <div class="table-responsive">
                    <table class="table table-bordered table-stripped" id="tableDataOpd">
                        <thead>
                            <tr>
                                <th>Nama OPD</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $val)
                                <tr>
                                    <td>{{ $val->name }}</td>
                                    <td>
                                        <button class="btn-orange" data-bs-toggle="modal" data-bs-target="#exampleModalEdit{{ $val->id }}"><i class="fa fa-pencil"></i> Ubah</button>
                                        <form action="{{ url('') }}/opd/{{ $val->id }}/delete" method="POST" class="d-inline">
                                            @method('delete')
                                            @csrf
                                            <button class="btn-red" onclick="return confirm('Lanjutkan menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                                        </form>
                                    </td>
                                </tr>
                                
                                {{--  modal ubah  --}}
                                <div class="modal fade" id="exampleModalEdit{{ $val->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <form action="{{ url('') }}/opd/{{ $val->id }}/edit" method="POST" id="">
                                                @method('patch')
                                                @csrf
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Form Tambah PD</h5>
                                                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <label for="">Nama OPD</label>
                                                    <div class="input-group input-group-outline">
                                                        <input type="text" name="nama" value="{{ $val->name }}" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal modal-add fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/opd/add" method="POST" id="addWalkot">
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah PD</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="" class="m-0 ">Nama PD</label>
            <div class="input-group input-group-outline">
                <input type="text" name="nama" class="form-control">
            </div>
            @error('nama')
                <span class="text-danger">
                    <small>Nama OPD tidak boleh kosong!</small>
                </span>
            @enderror
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
    $('#tableDataOpd').DataTable({});

    @if(Session::has('errors'))
    $('.modal').modal({show: true});
    @endif
</script>

@endsection
