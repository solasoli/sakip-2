@extends('layouts/app')
@section('title')
    Satuan
@endsection
@section('content')

<style media="screen">
  .modal-lg{
    width: 750px !important;
  }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Satuan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">Satuan</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Satuan</h6>
            <button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalAdd" style="background-color:#4caf50;"><i class="fa fa-plus"></i> Tambah</button>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
<<<<<<< HEAD
                @if ($message = Session::get('successAdd'))
                <div class="alert alert-success flashMessages" role="alert">
                    {{ $message }}
                </div>
                @elseif ($message = Session::get('successDel'))
                <div class="alert alert-danger flashMessages" role="alert">
                    {{ $message }}
                </div>
                @elseif ($message = Session::get('successEdit'))
                <div class="alert alert-success flashMessages" role="alert">
                    {{ $message }}
                </div>
                @elseif ($message = Session::get('matchData'))
                <div class="alert alert-danger flashMessages" role="alert">
                    {{ $message }}
                </div>
                @endif
                <div class="table-responsive">
                <table class="table table-bordered text-center" id="tableDataSatuan">
=======
                @include('partials.error-message')
                @include('partials.message')
<<<<<<< HEAD
                <div class="table-responsive">
                    <table class="table table-bordered text-center" id="tableDataSatuan">
                        <thead>
                            <tr>
                                <th>Nama Satuan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                            <tr>
                                <td>{{ $item->satuan }}</td>
                                <td id="action">
                                    <button class="btn-orange" data-toggle="modal" data-target="#exampleModalEdit{{ $item->id_satuan }}"><i class="fa fa-pencil"></i> Ubah</button>
                                    <form action="{{ url('') }}/satuan/{{ $item->id_satuan }}/delete" method="POST" class="d-inline">
                                        @method('delete')
                                        @csrf
                                        <button type="submit" class="btn-red" onclick="return confirm('Lanjukan menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
=======
                <table class="table table-bordered table-responsive text-center" id="tableDataSatuan">
>>>>>>> 67fbfff5282632fa2e2da0d4c76d33e3ec1343a9
                    <thead>
                        <tr>
                            <th>Nama Satuan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $item)
                        <tr>
                            <td>{{ $item->satuan }}</td>
                            <td id="action">
                                <button class="btn-orange" data-toggle="modal" data-target="#exampleModalEdit{{ $item->id_satuan }}"><i class="fa fa-pencil"></i> Ubah</button>
                                <form action="{{ url('') }}/satuan/{{ $item->id_satuan }}/delete" method="POST" class="d-inline">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn-red" onclick="return confirm('Lanjukan menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                </div>
                
>>>>>>> remotes/origin/master
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
@foreach ($data as $item)
<div class="modal fade" id="exampleModalEdit{{ $item->id_satuan }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form action="{{ url('') }}/satuan/{{ $item->id_satuan }}/edit" method="POST" id="addWalkot">
            @method('patch')
            @csrf
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Form Update Satuan</h5>
                <button type="button" class="close" data-data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="">Satuan :*</label>
                <div class="input-group input-group-outline">
                    <input type="text" name="satuan" value="{{ $item->satuan }}" class="form-control">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
        </div>
    </div>
</div>
@endforeach

<!-- Modal -->
<div class="modal modal-add fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/satuan/add" method="POST" id="addWalkot">
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Satuan</h5>
            <button type="button" class="close" data-bs-data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="">Satuan :*</label>
            <div class="input-group input-group-outline">
                <input type="text" name="satuan" class="form-control">
            </div>
                @error('satuan')
                    <span class="text-danger">
                        <small>Satuan tidak boleh kosong!</small>
                    </span>
                @enderror
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('scripts')
    <script>
        $(function() {
            $('#tableDataSatuan').DataTable({
                scrollX: true
            });

            @if(Session::has('errors'))
                $('.modal-add').modal({show: true});
            @endif
        });
    </script>
@endsection
