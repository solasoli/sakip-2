@extends('layouts/app')
@section('title')
    Pangkat
@endsection
@section('content')

<style media="screen">
  .modal-lg{
    width: 750px !important;
  }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Pangkat</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">Pangkat</span>
    </nav>
</div>
<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Pangkat</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12 table-responsive">
                    <table class="table table-bordered  table-stripped text-center" id="tableDataPangkat" style="width: 100%" >
                        <thead>
                            <tr>    
                                <th>No</th>
                                <th>Nama</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pangkat as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->nama }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $('#tableDataPangkat').DataTable({
        "scrollX": true,
    });
</script>

@endsection