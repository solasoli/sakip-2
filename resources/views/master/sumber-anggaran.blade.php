@extends('layouts/app')
@section('title')
    Sumber Anggaran
@endsection
@section('content')

<style media="screen">
  .modal-lg{
    width: 750px !important;
  }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Sumber Anggaran</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">Sumber Anggaran</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Sumber Anggaran</h6>
            <button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalAdd" style="background-color:#4caf50;><i class="fa fa-plus"></i> Tambah</button>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
            @include('partials.error-message')
            @include('partials.message')
            <div class="table-responsive">
            <table id="tableDataSumberAnggaran" class="table table-bordered table-stripped">
                <thead>
                    <tr>
                        <th>Nama Sumber Anggaran</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>
                            <button class="btn-orange" data-toggle="modal" data-target="#exampleModalEdit{{ $item->id }}"><i class="fa fa-pencil"></i> Ubah</button>
                            <form action="{{ url('') }}/sumber-anggaran/{{ $item->id }}/delete" method="POST" class="d-inline">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn-red" onclick="return confirm('Lanjutkan menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
            
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/sumber-anggaran/add" method="POST" id="addWalkot">
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Sumber Anggaran</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="">Nama Sumber Anggaran</label>
            <div class="input-group input-group-outline">
                <input type="text" name="sumber_anggaran" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal Edit -->
@foreach ($data as $item)
<div class="modal fade" id="exampleModalEdit{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/sumber-anggaran/{{ $item->id }}/edit" method="POST" id="addWalkot">
        @method('patch')
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Sumber Anggaran</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="">Nama Sumber Anggaran</label>
            <div class="input-group input-group-outline">
                <input type="text" name="sumber_anggaran" class="form-control" value="{{ $item->name }}">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endforeach
@endsection

@section('scripts')
    <script>
        $('#tableDataSumberAnggaran').DataTable({})
    </script>
@endsection
