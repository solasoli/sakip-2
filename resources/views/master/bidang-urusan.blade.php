@extends('layouts/app')
@section('title')
    Bidang Urusan
@endsection
@section('content')


<style type="text/css">
    #exampleModalAdd{
        background-color:#5cf97a;
    }
</style>
<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Bidang Urusan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">Bidang Urusan</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Urusan</h6>
            <button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalAdd" style="background-color:#5cf97a"><i class="fa fa-plus"></i> Tambah</button>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                @include('partials.error-message')
                @include('partials.message')
                <div class="table-responsive">
                    <table class="table table-bordered table-stripped table-hovered" id="tableDataUrusan">
                        <thead>
                            <tr>
                                <th>Kode Bidang Urusan</th>
                                <th>Nama Bidang Urusan</th>
                                <th>Urusan</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                            <tr>
                                <td>{{ $item->kode }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->urusan->name }}</td>
                                <td>
                                    <button class="btn-orange" data-toggle="modal" data-target="#exampleModalEdit{{ $item->id }}"><i class="fa fa-pencil"></i> Ubah</button>
                                    <form action="{{ url('') }}/bidang-urusan/{{ $item->id }}/delete" class="d-inline" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn-red" type="submit" onclick="return confirm('Lanjutkan menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
@foreach ($data as $val)
<div class="modal fade" id="exampleModalEdit{{ $val->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/bidang-urusan/{{ $val->id }}/edit" method="POST" id="addWalkot">
        @method('patch')
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah PD</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="">Urusan</label>
            <div class="urusan-edit-wrapper input-group input-group-outline">
                <select name="urusan" class="select2" id="urusan">
                    @foreach ($urusan as $item)
                        <option value="{{ $item->id }}" {{ $val->id_urusan == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <label for="">Kode Urusan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="kode" class="form-control" value="{{ $val->kode }}">
            </div>
            <label for="">Nama Urusan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="bidang_urusan" value="{{ $val->name }}" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endforeach


<!-- Modal -->
<div class="modal modal-add fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/bidang-urusan/add" method="POST" id="addWalkot">
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Bidang Urusan</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="input-group input-group-outline urusan-add-wrapper">
                <label for="">Urusan</label>
                <select name="urusan" class="select2" id="urusan">
                    <option value="">- Pilih Urusan -</option>
                    @foreach ($urusan as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
                @error('urusan')
                    <span class="text-danger">
                        <small>Urusan tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
            <label for="">Kode Bidang Urusan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="kode" class="form-control">
                @error('kode')
                    <span class="text-danger">
                        <small>Kode bidang urusan tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
            <label for="">Nama Bidang Urusan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="bidang_urusan" class="form-control">
                @error('bidang_urusan')
                    <span class="text-danger">
                        <small>Nama bidang urusan tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
    <script>
        $(function() {
            $('#tableDataUrusan').DataTable({
                scrollX: true
            })
            selectPlaceholder('.select2#urusan', 'Pilih Urusan');

            @if(Session::has('errors'))
                $('.modal-add').modal({show: true});
            @endif
        });
    </script>
@endsection
