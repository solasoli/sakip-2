@extends('layouts/app')
@section('title')
    Periode
@endsection
@section('content')

<style media="screen">
  .modal-lg{
    width: 750px !important;
  }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Periode</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">Periode</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Periode</h6>
            <button class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalAdd" style="background-color:#4caf50;"><i class="fa fa-plus"></i> Tambah</button>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                        @include('partials.error-message')
                        @include('partials.message')

                        <div class="table-responsive">
                            <table class="table table-bordered table-sm" style="width:100%" id="tableData">
                                <thead>
                                    <tr>
                                        <th>Periode</th>
                                        <th>Kepala Daerah</th>
                                        <th class="text-center">PLT</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $idx => $val)    
                                    <tr>
                                        <td>{{ $val->dari }} - {{ $val->sampai }}</td>
                                        <td>{{ $val->nama }}</td>
                                        @if($val->is_plt > 0) 
                                            <td class="text-center">&check;</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td>
                                            <button class="btn-orange" data-bs-toggle="modal" data-bs-target="#exampleModalEdit{{ $val->id }}"><i class="fa fa-pencil"></i> Ubah</button>
                                            <form action="{{ url('') }}/periode/{{ $val->id }}/delete" method="post" class="d-inline">
                                                @csrf
                                                @method('delete')
                                                <button type="submit" class="btn-red" onclick="return confirm('Lanjut menghapus?')">
                                                <i class="fa fa-close"></i>
                                                hapus</button>
                                            </form>
                                        </td>
                                    </tr>

                                    {{--  Modal Edit  --}}
                                    <div class="modal modal-edit fade" id="exampleModalEdit{{ $val->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <form action="{{ url('') }}/periode/{{ $val->id }}/edit" method="POST" id="editWalkot">
                                                @method('patch')
                                                @csrf
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Form Edit Periode</h5>
                                                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="">Tahun Periode</label>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <input type="number" name="dari" class="form-control border" value="{{ $val->dari }}">
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <input type="number" name="sampai" class="form-control border" value="{{ $val->sampai }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Kepala Daerah</label>
                                                        <input type="text" name="nama" class="form-control border" value="{{ $val->nama }}">
                                                    </div>
                                                    <div class="form-group mt-4">
                                                        <label class="container">&emsp;PLT
                                                            <input type="checkbox" {{ $val->is_plt != 0 ? "checked" : "" }} name="plt">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" datdata-bs-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                @endforeach
                            </tbody>
                        </table>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal modal-add fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="" method="POST" id="addWalkot">
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Periode</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="">Tahun Periode</label>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="input-group input-group-outline">
                            <input type="number" name="dari" class="form-control" placeholder="Dari" value="{{ old('dari') }}">
                        </div>
                            @error('dari')
                        <span>
                            <small class="text-danger">tidak boleh kosong!</small>
                        </span>
                        @enderror
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group input-group-outline">
                            <input type="number" name="sampai" class="form-control" placeholder="Sampai" value="{{ old('sampai') }}">
                        </div>
                        @error('sampai')
                        <span>
                            <small class="text-danger">tidak boleh kosong!</small>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="col-sm-12 mt-2">
                <label for="">Kepala Daerah</label>
                <div class="input-group input-group-outline">
                    <input type="text" name="nama" class="form-control" value="{{ old('nama') }}">
                </div>
                @error('nama')
                    <span>
                        <small class="text-danger">Nama Kepala Daerah tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" datdata-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

@section('scripts')
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script>
    $(function() {

        var table = $('#tableData').DataTable();

        $('#addWalkot').submit((e) => {
            $('#addWalkot').attr('action', '{{ url("") }}/periode/add')
        })

        setTimeout(function() {
            $('.flashMessages').slideUp('slow');
        }, 3000)

        @if (Session::has('errors'))
        $('.modal-add').modal({show: true});
        @endif

    })
</script>
@endsection

@endsection
