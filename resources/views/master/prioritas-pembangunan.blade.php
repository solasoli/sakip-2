@extends('layouts/app')
@section('title')
    Prioritas Pembangunan
@endsection
@section('content')
<style media="screen">
  .modal-lg{
    width: 750px !important;
  }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Prioritas Pembangunan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">Prioritas Pembangunan</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Prioritas Pembangunan</h6>
            <button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalAdd" style="background-color:#4caf50;"><i class="fa fa-plus"></i> Tambah</button>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                @if ($message = Session::get('successAdd'))
                <div class="alert alert-success flashMessages" role="alert">
                    {{ $message }}
                </div>
                @elseif ($message = Session::get('successDel'))
                <div class="alert alert-danger flashMessages" role="alert">
                    {{ $message }}
                </div>
                @elseif ($message = Session::get('successEdit'))
                <div class="alert alert-success flashMessages" role="alert">
                    {{ $message }}
                </div>
                @elseif ($message = Session::get('matchData'))
                <div class="alert alert-danger flashMessages" role="alert">
                    {{ $message }}
                </div>
                @endif
                <div class="table-responsive">
                <table class="table table-bordered  " id="tableDataPrioritasPembangunan">
                    <thead>
                        <tr>
                            <th>Nama Prioritas Pembangunan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>
                                <button class="btn-orange" data-bs-toggle="modal" data-bs-target="#exampleModalEdit{{ $item->id }}"><i class="fa fa-pencil"></i> Ubah</button>
                                <form action="{{ url('') }}/prioritas-pembangunan/{{ $item->id }}/delete" class="d-inline" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button class="btn-red" type="submit" onclick="return confirm('Lanjutkan menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
@foreach ($data as $val)
<div class="modal fade" id="exampleModalEdit{{ $val->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/prioritas-pembangunan/{{ $val->id }}/edit" method="POST" id="addWalkot">
        @method('patch')
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah PD</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="">Nama Prioritas Pembangunan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="prioritas_pembangunan" value="{{ $val->name }}" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endforeach

<!-- Modal -->
<div class="modal fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/prioritas-pembangunan/add" method="POST">
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah PD</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="">Nama Prioritas Pembangunan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="prioritas_pembangunan" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#tableDataPrioritasPembangunan').DataTable()
    })
</script>
@endsection