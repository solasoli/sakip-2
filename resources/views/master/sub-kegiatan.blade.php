@extends('layouts/app')
@section('title')
    Sub Kegiatan
@endsection
@section('content')

<style media="screen">
  .modal-lg{
    width: 750px !important;
  }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Sub Kegiatan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">Sub Kegiatan</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Sub Kegiatan</h6>
            <button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalAdd" style="background-color:#4caf50;"><i class="fa fa-plus"></i> Tambah</button>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                @if ($message = Session::get('successAdd'))
                <div class="alert alert-success flashMessages" role="alert">
                    {{ $message }}
                </div>
                @elseif ($message = Session::get('successDel'))
                <div class="alert alert-danger flashMessages" role="alert">
                    {{ $message }}
                </div>
                @elseif ($message = Session::get('successEdit'))
                <div class="alert alert-success flashMessages" role="alert">
                    {{ $message }}
                </div>
                @elseif ($message = Session::get('matchData'))
                <div class="alert alert-danger flashMessages" role="alert">
                    {{ $message }}
                </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered table-hovered" id="tableDataSubKegiatan" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Kode Sub Kegiatan</th>
                                <th>Nama Sub Kegiatan</th>
                                <th>Nama Kegiatan</th>
                            </tr>
                        </thead>
                        {{-- <tbody>
                            @foreach ($data as $item)
                            <tr>
                                <td>{{ $item->kode_sub_kegiatan }}</td>
                                <td>{{ $item->sub_kegiatan }}</td>
                                <td>{{ $item->kegiatan }}</td>
                                <td>
                                    <button class="btn-orange" type="button" data-toggle="modal" data-target="#exampleModalEdit{{ $item->id_sub_kegiatan }}"><i class="fa fa-pencil"></i> Ubah</button>
                                    <form action="{{ url('') }}/sub-kegiatan/{{ $item->id_sub_kegiatan }}/delete" method="POST" class="d-inline">
                                        @method('delete')
                                        @csrf
                                        <button class="btn-red" type="submit" onclick="return confirm('Lanjukan menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody> --}}
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/sub-kegiatan/add" method="POST" id="addWalkot">
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Sub Kegiatan</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="kegiatan">Kegiatan :*</label>
            <div class="input-group input-group-outline">
                <select name="kegiatan" id="kegiatan" class="form-control select2">
                    <option value=""></option>
                    @foreach ($kegiatan as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <label for="kode_sub_kegiatan">Kode Sub Kegiatan :*</label>
            <div class="input-group input-group-outline">
                <input type="text" name="kode_sub_kegiatan" class="form-control">
            </div>
            <label for="sub_kegiatan">Nama Sub Kegiatan :*</label>
            <div class="input-group input-group-outline">
                <input type="text" name="sub_kegiatan" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

{{-- <!-- Modal -->
@foreach ($data as $val)
<div class="modal fade" id="exampleModalEdit{{ $val->id_sub_kegiatan }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/sub-kegiatan/{{ $val->id_sub_kegiatan }}/edit" method="POST" id="addWalkot">
        @method('patch')
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Edit Sub-Kegiatan</h5>
            <button type="button" class="close" data-data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="kegiatan">Kegiatan :*</label>
            <div class="input-group input-group-outline">
                <select name="kegiatan" id="kegiatan" class="form-control select2">
                    @foreach ($kegiatan as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <label for="kode_sub_kegiatan">Kode Sub Kegiatan :*</label>
            <div class="input-group input-group-outline">
                <input type="text" name="kode_sub_kegiatan" value="{{ $val->kode_sub_kegiatan }}" class="form-control">
            </div>
            <label for="sub_kegiatan">Nama Sub Kegiatan :*</label>
            <div class="input-group input-group-outline">
                <input type="text" name="sub_kegiatan" value="{{ $val->sub_kegiatan }}" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endforeach --}}
@endsection

@section('scripts')
    <script>
        $(function() {
            selectPlaceholder('.select2#kegiatan', 'Pilih Kegiatan');

            $('#tableDataSubKegiatan').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                scrollX: true, 
                ajax:'{{url()->current()}}/datatable-sub-kegiatan',
                columns: [
                    { data: 'kode_sub_kegiatan', name: 'kode_sub_kegiatan', className: 'text-center'},
                    { data: 'sub_kegiatan', name: 'nama_sub_ kegiatan', className: 'text-left'},
                    { data: 'kegiatan', name: 'nama_kegiatan', className: 'text-left'},
                ]
            });
        });
    </script>
@endsection