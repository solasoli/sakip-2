@extends('layouts/app')
@section('title')
    Program
@endsection
@section('content')

<style media="screen">
  .modal-lg{
    width: 750px !important;
  }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Program</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">Program</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Program</h6>
            <button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalAdd" style="background-color:#4caf50;"><i class="fa fa-plus"></i> Tambah</button>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                @include('partials.error-message')
                @include('partials.message')
                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-stripped table-hovered" id="tableDataProgram">
                        <thead>
                            <tr>
                                <th>Kode Program</th>
                                <th>Nama Program</th>
                                <th>Bidang Urusan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                            <tr>
                                <td>{{ $item->kode_program }}</td>
                                <td>{{ $item->name }}</td>
                                <td>{{ $item->bidangUrusan->name }}</td>
                                <td>
                                    <button class="btn-orange" data-toggle="modal" data-target="#exampleModalEdit{{ $item->id }}"><i class="fa fa-pencil"></i> Ubah</button>
                                    <form action="{{ url('') }}/program/{{ $item->id }}/delete" method="post" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn-red" onclick="return confirm('Lanjut menghapus?')"><i class="fa fa-trash"></i> hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
@foreach ($data as $item)
<div class="modal fade" id="exampleModalEdit{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/program/{{ $item->id }}/edit" method="POST" id="addWalkot">
        @csrf
        @method('patch')
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Program</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="">Bidang Urusan :*</label>
            <div class="input-group input-group-outline bidang-urusan-add-wrapper">
                <select name="bidang_urusan" class="select2" id="bidang_urusan">
                    @foreach ($bidang_urusan as $val)
                        <option value="{{ $val->id }}" {{ $item->id_bidang_urusan == $val->id ? 'selected' : '' }}>{{ $val->name }}</option>
                    @endforeach
                </select>
            </div>
            <label for="">Kode Program :*</label>
            <div class="input-group input-group-outline">
                <input type="text" name="kode_program" value="{{ $item->kode_program }}" class="form-control">
            </div>
            <label for="">Nama Program :*</label>
            <div class="input-group input-group-outline">
                <input type="text" name="name" value="{{ $item->name }}" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endforeach

<!-- Modal -->
<div class="modal modal-add fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/program/add" method="POST" id="addWalkot">
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Periode</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="bidang_urusan">Bidang Urusan</label>
            <div class="input-group input-group-outline bidang_urusan_wrapper">
                <select name="bidang_urusan" class="form-control select2" id="bidang_urusan">
                    <option value=""></option>
                    @foreach ($bidang_urusan as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
                @error('bidang_urusan')
                    <span class="text-danger">
                        <small>Bidang urusan tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
            <label for="">Kode Program :*</label>
            <div class="input-group input-group-outline">
                <input type="text" name="kode_program" class="form-control">
                @error('kode_program')
                    <span class="text-danger">
                        <small>Kode program tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
            <label for="">Nama Program :*</label>
            <div class="input-group input-group-outline">
                <input type="text" name="name" class="form-control">
                @error('name')
                    <span class="text-danger">
                        <small>Nama program tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        $('#tableDataProgram').DataTable({
            scrollX:true
        })

        // select2 with placeholder
        // selectPlaceholder(el, str, parent)
        selectPlaceholder('.select2#bidang_urusan', 'Pilih Bidang Urusan')

        @if(Session::has('errors'))
            $('.modal-add').modal({show: true});
        @endif
    });
</script>
@endsection
