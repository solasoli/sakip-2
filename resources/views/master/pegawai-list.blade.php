@extends('layouts/app')
@section('content')

<style media="screen">
  .modal-lg{
    width: 750px !important;
  }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Pegawai</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">Pegawai</span>
    </nav>
</div>
<style>
    table{
        white-space: nowrap;
   }
</style>
<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Pegawai</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-bordered table-responsive table-stripped" id="tableDataPegawai" style="width:100%">
                        <thead>
                            <tr>    
                                <th>NIP</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($pegawai as $item)
                                <tr>
                                    <td>{{ $item->nip }}</td>
                                    <td>{{ $item->nama }}</td>
                                    <td>{{ $item->jabatan }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script>
    $('#tableDataPegawai').DataTable({
        "scrollX": true
    });
</script>

@endsection