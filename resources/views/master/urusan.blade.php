@extends('layouts/app')
@section('title')
    Urusan
@endsection
@section('content')

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Urusan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">Urusan</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Urusan</h6>
            <button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalAdd" style="background-color:#4caf50;"><i class="fa fa-plus"></i> Tambah</button>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                @include('partials.error-message')
                @include('partials.message')
                <style>
                    td {
                         text-transform:uppercase
                    }
                </style>
                <div class="table-responsive">
                    <table class="table table-bordered table-stripped table-hovered" id="tableDataUrusan" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Kode Urusan</th>
                                <th>Nama Urusan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $item)
                            <tr>
                                <td>{{ $item->kode }}</td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    <button class="btn-orange" data-bs-toggle="modal" data-bs-target="#exampleModalEdit{{ $item->id }}"><i class="fa fa-pencil"></i> Ubah</button>
                                    <form action="{{ url('') }}/urusan/{{ $item->id }}/delete" class="d-inline" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn-red" type="submit" onclick="return confirm('Lanjutkan menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
@foreach ($data as $val)
<div class="modal fade" id="exampleModalEdit{{ $val->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/urusan/{{ $val->id }}/edit" method="POST" id="addWalkot">
        @method('patch')
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Edit Urusan</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="">Kode Urusan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="kode" class="form-control input-group input-group-outline" value="{{ $val->kode }}">
            </div>
            <label for="">Nama Urusan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="urusan" value="{{ $val->name }}" class="form-control input-group input-group-outline">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endforeach


<!-- Modal -->
<div class="modal modal-add fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/urusan/add" method="POST" id="addWalkot">
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Urusan</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="">Kode Urusan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="kode" class="form-control">
                @error('kode')
                    <span class="text-danger">
                        <small>Kode urusan tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
            <label for="">Nama Urusan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="urusan" class="form-control">
                @error('urusan')
                    <span class="text-danger">
                        <small>Nama urusan tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
    <script>
        $('#tableDataUrusan').DataTable({
            scrollX: true
        });

        @if(Session::has('errors'))
            $('.modal-add').modal({show: true});
        @endif
    </script>
@endsection
