@extends('layouts/app')
@section('title')
    Pegawai
@endsection
@section('content')

<style media="screen">
  .modal-lg{
    width: 750px !important;
  }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Pegawai</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">Pegawai</span>
    </nav>
</div>
<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Pegawai</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">  
                        <table class="table table-bordered table-stripped" id="tableDataPegawai" style="width:100%">
                            <thead>
                                <tr>    
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Gelar Depan</th>
                                    <th>Gelar Belakang</th>
                                    <th>Eselon</th>
                                    <th>Jabatan</th>
                                    <th>PD</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $('#tableDataPegawai').DataTable({
        processing: true,
        serverSide: true,
        ordering: true,
        responsive: true,
        scrollX: true,
        ajax: '{{url()->current()}}/datatable-pegawai',
        columns: [
            {data: 'nip', name: 'nip'},
            {data: 'nama', name: 'nama'},
            {data: 'gelar_depan', name: 'gelar_depan'},
            {data: 'gelar_belakang', name: 'gelar_belakang'},
            {data: 'eselon.nama', name: 'eselon'},
            {data: 'jabatan', name: 'jabatan'},
            {data: 'opd.name', name: 'unor'},
        ]
    });
</script>

@endsection
