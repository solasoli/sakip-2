@extends('layouts/app')
@section('title')
    Kegiatan
@endsection
@section('content')

<style type="text/css">
    #exampleModalAdd{
        background-color:#5cf97a;
    }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Kegiatan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Master</a>
        <span class="breadcrumb-item" style="color: #000;">Kegiatan</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>List Kegiatan</h6>
            <button class="btn btn-success btn-sm" data-bs-toggle="modal" data-bs-target="#exampleModalAdd" style="background-color:#4caf50;"><i class="fa fa-plus"></i> Tambah</button>
        </div>
        <div class="card-body">
            <div class="row"> 
                <div class="col-sm-12">
                @if ($message = Session::get('successAdd'))
                <div class="alert alert-success flashMessages" role="alert">
                    {{ $message }}
                </div>
                @elseif ($message = Session::get('successDel'))
                <div class="alert alert-danger flashMessages" role="alert">
                    {{ $message }}
                </div>
                @elseif ($message = Session::get('successEdit'))
                <div class="alert alert-success flashMessages" role="alert">
                    {{ $message }}
                </div>
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered table-stripped table-hovered " id="tableDataKegiatan" style="width: 100%">
                        <thead>
                            <tr>
                                <th>Kode Kegiatan</th>
                                <th>Nama Kegiatan</th>
                                <th>Nama Program</th>
                            </tr>
                        </thead>
                        {{-- <tbody>
                            @foreach ($data as $item)
                            <tr>
                                <td>{{ $item->kode_kegiatan }}</td>
                                <td>{{ $item->kegiatan }}</td>
                                <td>{{ $item->program }}</td>
                                <td>
                                    <button class="btn-orange" data-toggle="modal" data-target="#exampleModalEdit{{ $item->id_kegiatan }}"><i class="fa fa-pencil"></i> Ubah</button>
                                    <form action="{{ url('') }}/kegiatan/{{ $item->id_kegiatan }}/delete" class="d-inline" method="POST">
                                        @csrf
                                        @method('delete')
                                        <button class="btn-red" type="submit" onclick="return confirm('Lanjutkan menghapus?')"><i class="fa fa-trash"></i> Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody> --}}
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Edit -->
@foreach ($data as $val)
<div class="modal fade" id="exampleModalEdit{{ $val->id_kegiatan }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/kegiatan/{{ $val->id_kegiatan }}/edit" method="POST" id="addWalkot">
        @method('patch')
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Edit Kegiatan</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="input-group input-group-outline">
            <label for="">Nama Program</label>
                <select name="program" id="program" class="form-control select2">
                    <option value=""></option>
                    @foreach ($program as $item)
                        <option value="{{ $item->id }}" {{ $val->id_program == $item->id ? 'selected' : '' }}>{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <label for="">Kode Kegiatan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="kode_kegiatan" class="form-control" value="{{ $val->kode_kegiatan }}">
            </div>
            <label for="">Nama Kegiatan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="kegiatan" value="{{ $val->kegiatan }}" class="form-control">
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endforeach


<!-- Modal -->
<div class="modal modal-add fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form action="{{ url('') }}/kegiatan/add" method="POST" id="addWalkot">
        @csrf
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Form Tambah Kegiatan</h5>
            <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <label for="">Nama Program</label>
            <div class="input-group input-group-outline">
                <select name="program" id="program" class="form-control select2">
                    <option value=""></option>
                    @foreach ($program as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
                @error('program')
                    <span class="text-danger">
                        <small>Program tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
            <label for="">Kode Kegiatan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="kode_kegiatan" class="form-control">
                @error('kode_kegiatan')
                    <span class="text-danger">
                        <small>Kode kegiatan tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
            <label for="">Nama Kegiatan</label>
            <div class="input-group input-group-outline">
                <input type="text" name="kegiatan" class="form-control">
                @error('kegiatan')
                    <span class="text-danger">
                        <small>Kegiatan tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('scripts')
    <script>
        selectPlaceholder('.select2#program', 'Pilih Program');

        $('#tableDataKegiatan').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            ajax: '{{url()->current()}}/datatable-kegiatan',
            columns: [
                { data: 'kode_kegiatan', name: 'kode_kegiatan'},
                { data: 'kegiatan', name: 'kegiatan'},
                { data: 'program', name: 'program'},
            ]
        });
        @if(Session::has('errors'))
            $('.modal-add').modal({show: true});
        @endif
    </script>
@endsection