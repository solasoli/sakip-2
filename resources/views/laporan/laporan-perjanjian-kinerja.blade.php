@extends('layouts.app')
@section('title')
    Laporan Perjanjian Kinerja
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Laporan Perjanjian Kinerja",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "Laporan", "href" => "#"],
            [ "label" => "Perjanjian Kinerja", "href" => "#"],
        ]
    ])

    <div class="card mt-2">
        <div class="card-body">
            <div class="container-fluid pb-2 mt-4" x-data="laporan">
                <div class="input-group input-group-outline">
                    <div class="col-4">
                        <label for="opd">Perangkat Daerah</label>
                        <select name="opd" class="form-control" id="opd">
                        @foreach ($opds as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="input-group input-group-outline">
                    <div class="col-4">
                        <label for="tahun">Tahun</label>
                        <select name="tahun" class="form-control" id="tahun">
                            @foreach (periode()->periodePerTahun as $tahun)
                                <option value="{{ $tahun->id }}">{{ $tahun->tahun }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="input-group input-group-outline">
                    <div class="col-4">
                        <label for="jenis">Jenis Perjanjian Kinerja</label>
                        <select name="jenis" class="form-control" id="jenis">
                            <option value="App\Models\Renstra\SasaranRenstraIndikator">Sasaran Renstra</option>
                            <option value="App\Models\Renstra\Program\SasaranProgramIndikatorRenstra">Sasaran Program Renstra</option>
                            <option value="App\Models\Renstra\Kegiatan\SasaranKegiatanIndikatorRenstra">Sasaran Kegiatan Renstra</option>
                            <option value="App\Models\Renstra\SubKegiatan\SasaranSubKegiatanOutputRenstra">Sasaran Sub Kegiatan Renstra</option>
                        </select>
                    </div>
                </div>
                <div class="input-group input-group-outline">
                    <div class="col-4">
                        <label for="atasan">Atasan</label>
                        <select name="atasan" class="form-control" id="atasan">
                        </select>
                    </div>
                </div>
                <template x-if="showPenanggungJawab">
                    <div class="input-group input-group-outline">
                        <div class="col-4">
                            <label for="pegawai">Penanggung Jawab</label>
                            <select name="pegawai" class="form-control" id="pegawai">
                            </select>
                        </div>
                    </div>
                </template>
                <button type="button" class="mt-2 clone-program btn btn-primary btn-sm"
                    @click="showReport">
                    Tampilkan</button>
                <template x-if="showPrint">
                    <button type="button" class="mt-2 clone-program btn btn-success btn-sm"
                        @click="print">
                        Print</button>
                </template>

                <template x-if="showPrint">
                    <iframe
                        id="iframe"
                        :src="`/laporan/perjanjian-kinerja/tahun/${selectedTahun}/jenis/${parsedSelectedJenis}/atasan/${selectedAtasan}/pegawai/${selectedPenanggungJawab}/surat`"
                        class="w-full h-full"
                        onLoad="calcHeight(this);"
                    ></iframe>
                </template>
            </div>
        </div>
    </div>

    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data("laporan", () => {
                return {
                    showPrint: false,
                    showPenanggungJawab: false,
                    opds: @json($opds),

                    selectedOpd: {{ $opds->first()->id }},
                    selectedTahun: {{ periode()->periodePerTahun->first()->id }},
                    selectedAtasan: null,
                    selectedPenanggungJawab: null,
                    selectedJenis: "App\\Models\\Renstra\\SasaranRenstraIndikator",
                    get parsedSelectedJenis() {
                        return encodeURIComponent(this.selectedJenis);
                    },
                    init() {
                        this.initializeOpd()
                        this.initializeTahun()
                        this.initializeJenis()
                        this.initializeAtasan()
                    },
                    initializeOpd(){
                        const opdSelect = $('#opd').select2()

                        opdSelect.on('select2:select', (event) => {
                            this.showPrint = false;
                            this.reset();
                            this.selectedOpd = event.target.value
                        })

                        this.$watch('selectedOpd', (value) => {
                            opdSelect.val(value).trigger("change");
                        });
                    },
                    initializeTahun(){
                        const tahunSelected = $('#tahun').select2()

                        tahunSelected.on('select2:select', (event) => {
                            this.showPrint = false;
                            this.reset();
                            this.selectedTahun = event.target.value
                        })

                        this.$watch('selectedTahun', (value) => {
                            tahunSelected.val(value).trigger("change");
                        });
                    },
                    initializeJenis(){
                        const jenisSelect = $('#jenis').select2()

                        jenisSelect.on('select2:select', (event) => {
                            this.showPrint = false;
                            this.reset();
                            this.selectedJenis = event.target.value
                        })

                        this.$watch('selectedJenis', (value) => {
                            jenisSelect.val(value).trigger("change");
                        });
                    },
                    initializeAtasan(){
                        const atasanSelect = $('#atasan').select2({
                            ajax: {
                                type: 'POST',
                                url: () => "{{ route('laporan.perjanjian_kinerja.atasan') }}",
                                dataType: 'json',
                                data: (params) => {
                                    return {
                                        opd: this.selectedOpd,
                                        keyword: params.term,
                                        tahun: this.selectedTahun,
                                        jenis: this.selectedJenis
                                    }
                                },
                                processResults: (data) => {
                                    const pegawais = data.data.pegawai
                                    return {
                                        results: pegawais.map(pegawai => {
                                            return {
                                                text: pegawai.jabatan,
                                                id: pegawai.id
                                            }
                                        })
                                    }
                                }
                            }
                        })

                        atasanSelect.on('select2:select', (event) => {
                            this.reset();
                            this.showPenanggungJawab = true;
                            this.selectedAtasan = event.target.value
                            this.$nextTick(() => this.initializePenanggungJawab())
                        })

                        this.$watch('selectedAtasan', (value) => {
                            atasanSelect.val(value).trigger("change");
                        });
                    },
                    initializePenanggungJawab(){
                        const pegawaiSelect = $('#pegawai').select2({
                            ajax: {
                                type: 'POST',
                                url: () => "{{ route('laporan.perjanjian_kinerja.penanggung_jawab') }}",
                                dataType: 'json',
                                data: (params) => {
                                    return {
                                        opd: this.selectedOpd,
                                        keyword: params.term,
                                        tahun: this.selectedTahun,
                                        atasan: this.selectedAtasan,
                                        jenis: this.selectedJenis
                                    }
                                },
                                processResults: (data) => {
                                    const pegawais = data.data.pegawai
                                    return {
                                        results: pegawais.map(pegawai => {
                                            return {
                                                text: pegawai.jabatan,
                                                id: pegawai.id
                                            }
                                        })
                                    }
                                }
                            }
                        })

                        pegawaiSelect.on('select2:select', (event) => {
                            this.showPrint = true
                            this.selectedPenanggungJawab = event.target.value
                        })

                        this.$watch('selectedPenanggungJawab', (value) => {
                            pegawaiSelect.val(value).trigger("change");
                        });
                    },
                    showReport(){
                        this.showPrint = true;
                    },
                    reset() {
                        this.showPrint = false
                        this.showPenanggungJawab = false
                        this.selectedPenanggungJawab = null
                        this.selectedAtasan = null
                    },
                    print() {
                        document.getElementById("iframe").contentWindow.print();
                    }
                }
            })
        })
        function calcHeight(iframeElement) {
            var the_height = iframeElement.contentWindow.document.body.scrollHeight;
            iframeElement.height = the_height;
        }

        
        

    </script>
    
@endsection
