<style>
  .table-data {
    border-collapse: collapse;
    width: 100%;
  }
  .table-data th, .table-data td {
    padding: 10px 10px;
  }
</style>
<div class="modal-body" style="border-bottom: 15px solid #e9ecef">
  <div class="container-fluid px-5">
    <table style="width: 100%;color: #555;">
        <tr>
            <td width="100px" align="right"><img src="{{ public_path('logo-kabupaten-bogor.png') }}"
                    width="100px" height="120px">
            </td>
            <td align="center">
                <div style="margin-left: 0px;">
                    <h4 style="color:#000000; line-height: 1.2; font-family: arial, sans-serif;"><strong>PEMERINTAH DAERAH KABUPATEN BOGOR</strong></h5>
                    <h3 style="color:#000000; line-height: 0.3;"><strong>PEMERINTAH DAERAH</strong></h3>
                    <p style="font-family: times, sans-serif; font-size:16px; color:#000000; line-height:1.2;">Jl. Tegar Beriman, Tengah, Cibinong, Kabupaten Bogor, Jawa Barat 16914<br>
                        Telp. (0251) 8754733<br>
                        Website: https://bogorkab.go.id
                    </p>
                </div>
            </td>
            <td width="100px"></td>
        </tr>
        <tr>
            <td colspan="3">
                <hr style="margin-top: 0; color:#000000; border-top: 3px solid #000000; margin-bottom: 0px;">
                <hr style="margin-top: 0; color:#000000; border-bottom: 1px solid #000000;">
            </td>
        </tr>
    </table>
    <table class="table-data" border="1">
      <thead>
        <tr>
          <th>Misi</th>
          <th>Tujuan</th>
          <th>Sasaran Strategis</th>
          <th>Program</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($data as $item)
        <tr>
          <td>{{ $item->misi }}</td>
          <td>{{ $item->tujuan }}</td>
          <td>{{ $item->sasaran }}</td>
          <td>{{ $item->program }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
