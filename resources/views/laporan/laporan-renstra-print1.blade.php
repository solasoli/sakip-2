<!DOCTYPE html>
<html>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap.min.css') }}">
    <style>
        .table-header{
            font-size: 10px;
        },
        .table-content{
            font-size: 9px;
        }
    </style>
    <body>
        <div style="border-bottom: 15px solid #e9ecef">
            <div class="container-fluid px-5 table-responsive" style="width: 900px">
                <!-- Konten Header -->
                <table style="width: 100%;color: #555;">
                    <tr>
                        <td align="center">
                            <div style="margin-left: 0px;">
                                <h4 style="color:#000000; line-height: 1.2; font-family: arial, sans-serif;">
                                    <strong>RENCANA PROGRAM, KEGIATAN DAN SUB KEGIATAN, SERTA PENDANAAN {{ strtoupper($opd->name) }} </strong>
                                </h4>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <hr style="margin-top: 0; color:#000000; border-top: 3px solid #000000; margin-bottom: 0px;">
                            <hr style="margin-top: 0; color:#000000; border-bottom: 1px solid #000000;">
                        </td>
                    </tr>
                </table>
                <!-- end of Konten Header -->
                <!-- Konten Renstra -->
                <table  class="table-data" border="1">
                    <thead class="table-header">
                        <tr>
                            <th rowspan="3">No</th>
                            <th rowspan="3">Tujuan</th>
                            <th rowspan="3">Sasaran</th>
                            <th rowspan="3">Program</th>
                            <th rowspan="3">Kegiatan</th>
                            <th rowspan="3">Sub Kegiatan</th>
                            <th rowspan="3">Indikator</th>
                            <th rowspan="3">Satuan</th>
                            <th>Capaian Awal</th>
                            <th colspan="{{ (count($listPeriode) - 2) * 2}}">Target</th>
                            <th rowspan="2" colspan="2">Target Kondisi Akhir Periode
                                ({{ $listPeriode[count($listPeriode) - 1]->tahun }})
                            </th>
                            <th rowspan="2">Pengampu</th>
                        </tr>
                        <tr>
                            @foreach ($listPeriode as $idx => $row)
                                @if ($idx < count($listPeriode) - 1)
                                    <th {{ $idx > 0 ? 'colspan=2' : '' }}>{{ $row->tahun }}</th>
                                @endif
                            @endforeach
                        </tr>
                        <tr>
                            @foreach ($listPeriode as $idx => $row)
                                @if ($idx < count($listPeriode))
                                    <th>Volume</th>
                                    @if ($idx > 0)
                                        <th>Rp</th>
                                    @endif
                                @endif
                            @endforeach
                        </tr>
                    </thead>

                    <tbody class="table-content">
                         @foreach ($data as $idx => $tr)
                         <!-- start of tujuan -->
                            @foreach ($tr->indikator as $ii => $tri)
                                <tr>
                                    <td>{{ $idx + 1 }}</td>
                                    <td>{{ $tr->name }}</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>{{ $tri->indikator }}</td>
                                    <td>{{ $tri->satuan->name }}</td>

                                    @foreach ($listPeriode as $idx => $row)
                                        @if ($idx < count($listPeriode))
                                            <td>{{ $tri->target->where('id_periode_per_tahun', $row->id)->first()->target }} </td>
                                            @if ($idx > 0)
                                                {{-- <th>Rp</th> PASANG RP DISINI --}}
                                                <td></td>
                                            @endif
                                        @endif
                                    @endforeach

                                </tr>
                            @endforeach
                            <!-- end of tujuan -->
                            
                            <!-- start of sasaran -->
                            @foreach ($tr->renstraSasaran as $is => $ss)
                                @foreach ($ss->indikator as $isi => $si)
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        @if ($isi == 0)
                                            <td>{{ $isi + 1 }}. {{ $ss->name }}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{ $si->indikator }}</td>
                                        <td>{{ $si->satuan->name }}</td>

                                        @foreach ($listPeriode as $idx => $row)
                                            @if ($idx < count($listPeriode))
                                                <td>{{ $si->target->where('id_periode_per_tahun', $row->id)->first()->target }}
                                                </td>
                                                @if ($idx > 0)
                                                    {{-- <th border=1>Rp</th> PASANG RP DISINI --}}
                                                    <td></td>
                                                @endif
                                            @endif
                                        @endforeach
                                        <td>

                                            @php
                                                $list_penanggung_jawab = [];
                                                foreach ($si->sasaranIndikatorPenanggungJawab as $isip => $rsip) {
                                                    $list_penanggung_jawab[] = $rsip->pegawai->jabatan;
                                                }
                                            @endphp

                                            {{ implode($list_penanggung_jawab, ',') }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                            <!-- end of sasaran -->
                            
                            <!-- start of program -->
                            @foreach ($ss->programRenstra as $ipr => $pr)
                                @foreach ($pr->programRpjmd->indikatorProgram as $iprj => $prj)
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        @if ($iprj == 0)
                                            <td>{{ $ipr + 1 }}. {{ $pr->programRpjmd->mstProgram->name }}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        <td></td>
                                        <td></td>
                                        <td>{{ $prj->indikator }}</td>
                                        <td>{{ $prj->satuan->name }}</td>

                                        @foreach ($listPeriode as $idx => $row)
                                            @if ($idx < count($listPeriode))
                                                <td>{{ $prj->targetProgram->where('id_periode_per_tahun', $row->id)->first()->target }}
                                                </td>
                                                @if ($idx > 0)
                                                    {{-- <th>Rp</th> PASANG RP DISINI --}}
                                                    <td></td>
                                                @endif
                                            @endif
                                        @endforeach
                                        <td>
                                            @php
                                                
                                                $list_penanggung_jawab = [];
                                                foreach ($pr->programRpjmd->Opd as $isip => $rsip) {
                                                    $list_penanggung_jawab[] = $rsip->name;
                                                }
                                            @endphp

                                            {{ implode($list_penanggung_jawab, ',') }}
                                        </td>
                                    </tr>
                                @endforeach
                            @endforeach
                            <!-- end of program -->
                         @endforeach
                    </tbody>
                </table>
                <!-- end of Konten Renstra -->
            </div>
        </div>
    </body>
</html>