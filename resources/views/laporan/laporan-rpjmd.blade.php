@extends('layouts.app')
@section('title')
   {{ $title }}
@endsection
@section('content')

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Laporan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">Laporan</a>
        <span class="breadcrumb-item" style="color: #000;">RPJMD</span>
    </nav>
</div>
<div class="container-fluid pb-2 mt-4">
    <div class="card mb-4">
        <div class="card-header">
            <span>Laporan RPJMD Kabupaten Bogor</span>
        </div>
        <div class="card-body">
            <div class="input-group input-group-outline col-4">
                <label for="periode">Periode</label>
                <select name="periode" class="periode" id="periode">
                    <option value=""></option>
                    @foreach ($periode as $item)
                        <option value="{{ $item->id }}">{{ $item->nama }} | {{ $item->dari }} - {{ $item->sampai }}</option>
                    @endforeach
                </select>
            </div>
            <div class="input-group input-group-outline col-12">
                {{-- <a href="#" class="btn btn-dark btn-sm">HTML</a>
                <a href="#" class="btn btn-primary btn-sm">Print</a> --}}
                <form method="POST" class="d-inline pdf">
                    @csrf
                    <input type="text" name="periode" id="pdf" hidden>
                    <button type="submit" class="btn btn-danger btn-sm">
                        <i class="fa fa-file-pdf"></i> PDF
                    </button>
                </form>
                <form method="POST" class="d-inline excel">
                    @csrf
                    <input type="text" name="periode" id="excel" hidden>
                    <button type="submit" class="btn btn-success btn-sm">
                        <i class="fa fa-file-excel"></i> Excel
                    </button>
                </form>
                <form method="POST" class="d-inline print">
                    @csrf
                    <input type="text" name="periode" id="print" hidden>
                    <button type="submit" class="btn btn-danger btn-sm">
                        <i class="fa fa-file-pdf"></i> Print
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    selectPlaceholder('#periode', 'Pilih Periode');

    laporan('excel', `{{ url('') }}/laporan/rpjmd/excel`);
    laporan('pdf', `{{ url('') }}/laporan/rpjmd/pdf`);
    laporan('print', `{{ url('') }}/laporan/rpjmd/print`);

    function laporan(type, action_url) {
        const periode = document.querySelector('.periode');
        const input = document.querySelector(`#${type}`);
        const form = document.querySelector(`.${type}`);
    
        form.addEventListener('submit', function(e) {
            const parent = periode.parentElement;
            let checkValidation = parent.querySelector('small');

            if(checkValidation != null) {
                checkValidation.remove();
            }
            if(periode.value == '' || periode.value == undefined || periode.value == null) {
                e.preventDefault();
                const validation = document.createElement('small');
                const validation_text = document.createTextNode('Periode tidak boleh kosong!');
                validation.append(validation_text);
                validation.classList.add('text-danger');

                parent.append(validation);
            }
            input.setAttribute('value', periode.value);
            form.setAttribute('action', action_url);
        });
    }
</script>
@endsection