<!DOCTYPE html>
<html>

<head>
    <title></title>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap.min.css') }}">
    <style>
        .surat {
            font-family: "Arial", "Verdana";
            font-size: 24px;
            width: 900px;
            margin: auto;
            ;
        }

        .table_data th {
            padding: 5px;
        }

        .table_data td {
            padding: 5px;
        }

    </style>
</head>

<body>
    <table border="0" class="surat">

        <tr>
            <td class="text-center" style="padding-top: 100px;"><img
                    src="https://bogorkab.go.id/uploads/config/085993041728c96df0b3e40ecdba8200.png" width="120px"
                    height="auto"></td>
        </tr>

        <tr>
            <td class="text-center"><strong>PERJANJIAN KINERJA TAHUN {{ $tahun->tahun }}</strong></td>
        </tr>

        <tr>
            <td class="text-center"><strong>{{ strtoupper($perangkatDaerah->name) }}</strong></td>
        </tr>
        <tr>
            <td class="text-center"><strong>KABUPATEN BOGOR</strong></td>
        </tr>

        <!-- <tr>
            <td class="text-center"><strong>PERJANJIAN KINERJA TAHUN {{ $tahun->tahun }}</strong></td>
        </tr> -->


        <td>&nbsp;</td>

        <tr>
            <td class="text-justify">Dalam rangka mewujudkan manajemen pemerintahan yang efektif, transparan, dan
                akuntabel serta berorientasi pada hasil, kami yang bertanda tangan di bawah ini:</td>
        </tr>
        <tr>
            <td class="text-justify">
                <table width="100%" border="0">

                    <td>&nbsp;</td>

                    <tr>
                        <td width="125px">Nama</td>
                        <td>: {{ $pegawai->gelar_depan . '.' ?: ''}} {{ $pegawai->nama }} {{ $pegawai->gelar_belakang }}</td>
                    </tr>
                    <tr>
                        <td>Jabatan</td>
                        <td>: {{ strtoupper($pegawai->jabatan) }}</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td class="text-justify">selanjutnya disebut PIHAK PERTAMA</td>
        </tr>

        <td>&nbsp;</td>

        <tr>
            <td class="text-justify">
                <table width="100%" border="0">
                    <tr>
                        <td width="125px">Nama</td>
                        <td>: {{ $atasan->gelar_depan}} {{ $atasan->nama }} {{ $atasan->gelar_belakang }}</td>
                    </tr>
                    <tr>
                        <td>Jabatan</td>
                        <td id="jabatan">: {{ strtoupper($atasan->jabatan) }}</td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td class="text-justify">selaku atasan pihak pertama, selanjutnya disebut sebagai PIHAK KEDUA</td>
        </tr>
        <td>&nbsp;</td>
        <tr>
            <td class="text-justify">Pihak pertama berjanji akan mewujudkan target kinerja yang seharusnya sesuai
                lampiran perjanjian ini, dalam rangka mencapai target kinerja jangka menengah seperti yang telah
                ditetapkan dalam dokumen perencanaan. Keberhasilan dan kegagalan pencapaian target kinerja tersebut
                menjadi tanggung jawab kami.</td>
        </tr>
        <td>&nbsp;</td>
        <tr>
            <td class="text-justify">Pihak kedua akan melakukan supervisi yang diperlukan serta akan melakukan
                evaluasi
                terhadap capaian kinerja dari perjanjian ini dan mengambil tindakan yang diperlukan dalam rangka
                pemberian penghargaan dan sanksi.</td>
        </tr>
        <td>&nbsp;</td>
        <tr>
            <td>
                <table width="100%" border="0">
                    <tr>
                        <p style="margin-left: 469px;">Bogor,</p>
                    </tr>
                    <tr>
                        <td width="50%" class="text-center">PIHAK KEDUA</td>
                        <td class="text-center">PIHAK PERTAMA</td>
                    </tr>
                    <tr>
                        <td colspan="2" height="70px"></td>
                    </tr>
                    <tr>
                        <td width="50%" class="text-center">{{ $atasan->gelar_depan}} {{ $atasan->nama }}
                            {{ $atasan->gelar_belakang }}</td>
                        <td class="text-center">{{ $pegawai->gelar_depan }} {{ $pegawai->nama }}
                            {{ $pegawai->gelar_belakang }}</td>
                    </tr>
                    <tr>
                            <td class="text-center" id="nip-atasan">NIP. {{ $atasan->nip }}</td>
                             <td class="text-center">NIP. {{ $pegawai->nip }}</td>
                            
                           
                    </tr>
                    <tr>
                        <td width="50%">&nbsp;</td>
                        <td class="text-center"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <p style="page-break-before: always;"></p>

    <table border="0" class="surat">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <td class="text-center"><strong>PERJANJIAN KINERJA TAHUN {{ $tahun->tahun }}</strong></td>
        </tr>
        <tr>
            <td class="text-center"><strong>{{ strtoupper($pegawai->jabatan) }}</strong></td>
        </tr>
        <tr>
            <td class="text-center"><strong>{{ strtoupper($perangkatDaerah->name) }}</strong></td>
        </tr>

        <tr>
            <td class="text-center"><strong>KABUPATEN BOGOR</strong></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>

        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table class="table table-bordered table_data">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">Sasaran</th>
                            <th class="text-center">Indikator Kinerja</th>
                            <th class="text-center">Satuan</th>
                            <th>Target</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($groupedTargetPkRenja as $parent)
                            @php $parentIndex = $loop->index @endphp
                            @foreach ($parent['children'] as $child)
                                @php $childIndex = $loop->index @endphp
                                <tr>
                                    @if ($childIndex == 0)
                                        <td rowspan="{{ $parent['count'] }}">
                                            {{ $parentIndex + 1 }}
                                        </td>
                                        <td rowspan="{{ $parent['count'] }}">
                                            {{ $parent['name'] }}
                                        </td>
                                    @endif
                                    <td>{{ $child->pkRenjaTahunan->pk->name }}</td>
                                    <td>{{ $child->satuan->name }}</td>
                                    <td>{{ $child->target }}</td>
                                </tr>
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table class="table table-bordered table_data">
                    <thead>
                        <tr>
                            <th class="text-center">No</th>
                            <th class="text-center">{{ $anggaranLevel }}</th>
                            <th class="text-center">Anggaran (Rp)</th>
                            <th class="text-center">Sumber</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($groupedAnggaran as $anggaran)
                            <tr>
                                <td>{{ $loop->index + 1 }}</td>
                                <td>{{ $anggaran['nama'] }}</td>
                                <td>{{ $anggaran['anggaran'] }}</td>
                                <td>{{ $anggaran['sumber'] }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <table width="100%" border="0" style="margin-top: -95px;">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="50%">&nbsp;</td>
                        <p style="margin-left: 469px;">Bogor,</p>
                    </tr>

                    <tr>
                        <td width="50%" class="text-center">{{ strtoupper($atasan->jabatan) }}</td>
                        <td class="text-center">{{ strtoupper($pegawai->jabatan) }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" height="70px"></td>
                    </tr>
                    <tr>
                        <td width="50%" class="text-center">{{ $atasan->gelar_depan }} {{ $atasan->nama }}
                            {{ $atasan->gelar_belakang }}</td>
                        <td class="text-center">{{ $pegawai->gelar_depan }}. {{ $pegawai->nama }}
                            {{ $pegawai->gelar_belakang }}</td>
                    </tr>
                    <tr>
                        
                        <td class="text-center" id="nip-atasan1">NIP. {{ $atasan->nip ?: '' }}</td>
                        <td class="text-center">NIP. {{ $pegawai->nip ?: '' }}</td>
                    </tr>

                </table>
            </td>
        </tr>
    </table>

</body>
<script>
        const jabatan = document.getElementById('jabatan');
        const nip = document.getElementById('nip-atasan');
        const nip1 = document.getElementById('nip-atasan1');

        if (jabatan.textContent.includes('BUPATI')) {
            nip.style.visibility = "hidden";
            nip1.style.visibility = "hidden";
        }
    </script>
</html>
