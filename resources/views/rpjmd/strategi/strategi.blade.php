@extends('layouts.app')
@section('title')
    Strategi
@endsection
@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
        <span class="brand ml-4">Strategi</span>
        <nav class="mr-4">
            <a class="breadcrumb-item" href="/">Dashboard</a>
            <a class="breadcrumb-item" href="#">RPJMD</a>
            <span class="breadcrumb-item" style="color: #000;">Strategi</span>
        </nav>
    </div>
    <style media="screen">
        .info_bar {
            position: absolute;
            top: 50px;
            z-index: 998;
            border-radius: 0;
            width: 100%;
            background-image: linear-gradient(195deg, #42424a 0%, #191919 100%);
        }

        #card-body-info {
            transition: .3s;
        }

        .relative-info {
            transition: .3s;
        }

    </style>

    <div class="container-fluid pb-2 mt-4">
        <div class="card">
            @if (!is_null(periode()))
                <div class="card-body">
                    <div class="card-header">
                        <h6 class="card-title">Data Strategi Tingkat Kabupaten Periode {{ periode()->dari }} -
                            {{ periode()->sampai }}</h6>
                    </div>
                    @include('partials.error-message')
                    @include('partials.message')
                    <hr>
                    @if(!is_null($sasaran->first()))
                    <form method="POST" class="form_strategi">
                    @csrf

                    @foreach ($data as $idx => $item)
                        <div class="card justify-content-center mt-2">
                            <div class="card-header bg-primary text-light"
                                style="position: relative; z-index: 999;">
                                <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                                    <a href="#" class="info_btn"><i
                                            class="fa fa-info-circle"></i></a>&emsp;
                                    <i class="fa fa-angle-double-right"></i> &nbsp; Sasaran :
                                    {{ $item->name }} &nbsp;
                                </span>
                            </div>
                            <div class="alert show-hidden info_bar mt-4"
                                style="display: flex; flex-direction: column" role="alert">
                                <span class="hierarki_m">~ / Misi &nbsp; <i class="fa fa-caret-right"></i>&emsp;
                                        {{ $item->misi }}</span>
                                <span class="hierarki_m">~ / Tujuan &nbsp; <i
                                        class="fa fa-caret-right"></i>&emsp;
                                        {{ $item->tujuan }}</span>
                                <span class="hierarki_m">~ / Sasaran &nbsp; <i
                                        class="fa fa-caret-right"></i>&emsp;
                                        {{ $item->name }}</span>
                            </div>
                            <div class="card-body">
                                <input type="number" value="{{ $item->id }}" name="id_sasaran{{ $idx }}[]" hidden>
                                <div class="finput-group input-group-outline">
                                    @if(!is_null($strategi->where('id_sasaran', $item->id)->first()))
                                    @php $strategi_sasaran = $strategi->where('id_sasaran', $item->id); @endphp
                                    <div class="duplicate">
                                        <div class="input-group input-group-outline input-wrapper d-flex">
                                            <input type="text" class="form-control strategi" name="strategi{{ $idx }}[]" value="{{ $strategi_sasaran->first()->name }}">
                                        </div>
                                    </div>
                                    @foreach ($strategi_sasaran->skip(1) as $i => $val)
                                    <div class="duplicate mt-2">
                                        <div class="input-group input-group-outline d-flex input-wrapper align-items-center" style="width: 100%">
                                            <input type="text" class="form-control strategi " name="strategi{{ $idx }}[]" value="{{ $val->name }}">
                                            <button class="btn btn-danger btn-sm ml-2 close-duplicate ">
                                                X 
                                            </button>
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div class="duplicate mt-2">
                                        <div class="d-flex input-wrapper" style="width: 100%">
                                            <input type="text" class="form-control strategi" name="strategi{{ $idx }}[]">
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <button class="clone_strategi btn btn-primary btn-sm mt-2">
                                    <i class="fa fa-plus"></i> Tambah poin
                                </button>
                            </div>
                        </div>
                    @endforeach
                    {{--  paginate  --}}
                    <div class="finput-group input-group-outline mt-2">
                        <span class="d-flex justify-content-center">
                            {!! $data->links('pagination::bootstrap-4') !!}
                        </span>
                    </div>

                    <div class="finput-group input-group-outline mt-4">
                        <a href="" class="btn btn-dark">Cancel</a>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
                @else
                <span class="text-danger">Isi data sasaran terlebih dahulu!</span><br>
                <span>Klik <a href="{{ url('') }}/sasaran/add">disini</a> untuk mengisi sasaran</span>
                @endif
                </div>
            @else
            <div class="card-body">
                <span class="text-danger">Tetapkan periode terlebih dahulu!</span><br>
            </div>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        getInfoHierarki('.info_btn'); // create and get hierarki
        function getInfoHierarki(btn) {
            let info = document.querySelectorAll(btn);
            info.forEach(res => {
                res.addEventListener('click', () => {
                    const card = res.closest('.card')
                    const card_header = card.querySelector('.card-header')
                    const info_bar = card_header.nextElementSibling
                    const card_body = card.querySelector('.card-body')
                    const height_info_bar = info_bar.offsetHeight

                    info_bar.classList.toggle('show-hidden')
                    card_body.style.transition = '.3s';
                    if (!info_bar.classList.contains('show-hidden')) {
                        card_body.style.marginTop = `${height_info_bar - 10}px`;
                    } else {
                        card_body.style.marginTop = `0`;
                    }
                })
            })
        }

        cloneStrategi(); // clone form
        function cloneStrategi() {
            let btn = document.querySelectorAll('.clone_strategi');
            btn.forEach(res => {
                res.addEventListener('click', function(e) {
                    e.preventDefault();
                    let parent = this.previousElementSibling;
                    let el_clone = parent.querySelector('.duplicate');
                    let clone = el_clone.cloneNode(true);

                    clone.classList.add('mt-2');
                    const fix_el = cleanNodeClone(clone); // callback
                    fix_el.querySelector('input.strategi').classList.add('input-cloned');

                    parent.append(fix_el);
                });
            });
        }

        // clean form clone
        function cleanNodeClone(el) {
            const btn = document.createElement('button');
            const i = document.createElement('i');
            el.querySelector('input.strategi').value = "";

            btn.setAttribute('class', 'btn btn-danger btn-sm ml-2 close-duplicate')
            i.setAttribute('class', 'fa fa-close');
            btn.append(i);

            const wrapper = el.querySelector('.input-wrapper');
            wrapper.append(btn);
            el.append(wrapper);

            return el;
        }

        closeNodeClone(); // close form clone
        function closeNodeClone() {
            document.addEventListener('click', function(e) {
                let target_class = e.target.classList;
                if (target_class.contains('close-duplicate') || target_class.contains('fa-close')) {
                    e.target.closest('.duplicate')
                        .remove();
                }
            })
        }

        // submit form
        const submitForm = function() {
            const form = document.querySelector('.form_strategi');
            form.addEventListener('submit', function(e) {
                const nodeLs = document.querySelectorAll('.input-cloned');
                this.setAttribute('action', '{{ url('') }}/rpjmd/strategi/create');
                validate(nodeLs, e); // calback
            });
        }
        submitForm();

        // validation form from null value
        const validate = function(arr, event) {
            document.querySelectorAll('.warning').forEach(res => {
                res.remove();
            });

            arr.forEach(res => {
                if(res.value == "") {
                    event.preventDefault();

                    const span = document.createElement('span');
                    const small = document.createElement('small');
                    const text = document.createTextNode('Form tidak boleh kosong / tutup form jika tidak diperlukan!');

                    small.classList.add('text-danger');
                    span.classList.add('warning');

                    small.appendChild(text);
                    span.appendChild(small);

                    res.closest('.duplicate').appendChild(span);
                }
            });
        }
    </script>
@endsection
