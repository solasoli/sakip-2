@extends('layouts.app')
@section('title')
    Misi
@endsection
@section('content')
<style media="screen">
    .modal-lg{
      width: 750px !important;
    }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Misi</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">RPJMD</a>
        <span class="breadcrumb-item" style="color: #000;">Misi</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    @include('partials.error-message')
    @include('partials.message')
    <div class="card">
        <div class="card-body">
            <h6 class="card-title">Form Tambah Misi</h6>
            <hr>
            <div class="row justify-content-center">
                <div class="col-sm-12">
                    <div class="row justify-content-center">
                        <form action="{{ url('') }}/misi/add" method="POST" class="col-sm-10 col-md-10 col-lg-10 col-xs-12">
                            @csrf
                            <table class="table table-bordered" id="misi">
                                <thead>
                                    <tr class="text-center">
                                        <th width="70">No.</th>
                                        <th colspan="2">Misi</th>
                                    </tr>
                                </thead>
                                <tbody id="table_body">
                                    @if ($misi->count() > 0)
                                    @php
                                        $no = 1
                                    @endphp
                                    <tr class="text-center">
                                        <td class="no">{{ $no++ }}</td>
                                        <td colspan="2">
                                            <textarea name="misi[]" id="misi">{{ $misi->first()->misi }}</textarea>
                                        </td>
                                    </tr>
                                    @foreach ($misi->skip(1) as $item)
                                    <tr class="text-center">
                                        <td class="no">{{ $no++ }}</td>
                                        <td>
                                            <textarea name="misi[]" id="misi">{{ $item->misi }}</textarea>
                                        </td>
                                        <td width="50" class="text-center">
                                            <a href="{{ url('') }}/misi/delete/{{ hashID($item->id) }}" onclick="return confirm('Lanjutkan menghapus misi?')" class="btn btn-danger btn-sm">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr class="text-center">
                                        <td class="no">1</td>
                                        <td colspan="2">
                                            <textarea name="misi[]" id="misi"></textarea>
                                        </td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            <div class="input-group input-group-outline float-right">
                                <button type="button" class="btn btn-success btn-sm" id="add_misi">
                                    <i class="fa fa-plus"></i>
                                    Tambah Misi
                                </button>
                            </div><br><br><hr>
                            <div class="input-group input-group-outline">
                                <a href="" class="btn btn-secondary">Cancel</a>
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')

<script>
$(function() {
    $('#add_misi').click(() => {
        var el_misi = '';
        var trLength = $('#table_body tr td.no').length

        var no = $('#table_body tr td.no')[trLength - 1].textContent
        no = parseInt(no) + 1

        el_misi += `<tr class="text-center">`;
        el_misi += `<td class="no">${no}</td>`;
        el_misi += `<td><textarea name="misi[]" id="misi"></textarea></td>`;
        el_misi += `<td width="50" class="text-center"><button type="button" class="btn btn-danger btn-sm" id="close">`;
        el_misi += `<i class="fa fa-close"></i></button></td></tr>`;

        $('#table_body').append(el_misi)
    })

    $(document).on('click', '#close', function() {
        $(this).parents('tr').remove()
    })
})
</script>
@endsection
