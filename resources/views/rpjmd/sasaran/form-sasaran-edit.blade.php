@extends('layouts/app')
@section('title')
    Form Edit Sasaran
@endsection
@section('content')
    <style media="screen">
        .modal-lg {
            width: 750px !important;
        }
        button.badge {
            border: none;
            padding: 3px;
        }
    </style>
    <div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
        <span class="brand ml-4">sasaran</span>
        <nav class="mr-4">
            <a class="breadcrumb-item" href="/">Dashboard</a>
            <a class="breadcrumb-item" href="#">RPJMD</a>
            <span class="breadcrumb-item" style="color: #000;">sasaran</span>
        </nav>
    </div>

    <div class="container-fluid pb-2 mt-4">
        <div class="card ">
            <div class="card-header">
                <h6 class="card-title">Form Tambah sasaran Tingkat Kabupaten Periode {{ periode()->dari }} -
                    {{ periode()->sampai }}
                </h6>
            </div>
            <div class="card-body scrollmenu">
                @foreach ($sasaran as $val)
                <form action="{{ url('') }}/sasaran/store" method="POST" class="add_rpjmd_sasaran">
                    @method('patch')
                    @csrf
                    <div class="input-group row">
                        <div class="col-sm-4">
                            <div class="input-group">
                                <label for="tujuan">Tujuan Kabupaten * : </label> <br>
                                <select name="tujuan" id="tujuan" class="select2 tujuan">
                                    <option value=""></option>
                                    @foreach ($tujuan as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select><br>
                                @error('tujuan')
                                <div class="text-danger">
                                    <small>Tujuan tidak boleh kosong!</small>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="input-group row py-2">
                        <div class="col-sm-4">
                            <label for="sasaran">Sasaran Kabupaten * : </label>
                            <div class="input-group input-group-outline">
                                <input type="text" id="sasaran" class="form-control" name="sasaran" value="{{ !is_null(old('sasaran')) ? old('sasaran') : $val->name }}">
                                @error('sasaran')
                                <div class="text-danger">
                                    <small>Sasaran tidak boleh kosong!</small>
                                </div>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <div class="row place-duplicate">
                            @foreach ($sasaran_detail->where('id_sasaran', $val->id) as $i => $itemDetail)
                            <div class="duplicate">
                                <div class="d-flex align-items-center">
                                    <div class="input-group px-3">
                                        <label for="indikator">Indikator Sasaran * : </label>
                                        <div class="input-group input-group-outline">
                                            <input type="text" id="indikator" value="{{ !is_null(old('indikator.0')) ? old('indikator.0') : $itemDetail->indikator }}" class="form-control" name="indikator[]">
                                        </div>
                                    </div>
                                    <div class="input-group input-group-outline ml-3">
                                        <label for="satuan">Satuan * : </label><br>
                                        <div class="input-group input-group-outline ml-3">
                                            <select name="satuan[]" id="satuan" class="form-control satuan">
                                                @foreach ($satuan as $item)
                                                <option value="{{ $item->id }}" {{ $itemDetail->satuan->id == $item->id ? "selected" : "" }}>{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- kondisi awal -->
                                    <div class="input-group input-group-outline form-check ml-3" style="width: 100px">
                                        <label class="check ml-2 pk" for="pk">PK</label>
                                        <div class="switch d-flex ml-2">
                                            <input type="checkbox" class="check form-control form-check-input"{{ $itemDetail->is_pk == 1 ? "checked" : '' }} id="pk"/>
                                            <input type="text" value="{{ $itemDetail->is_pk == 1 ? 1 : 0 }}" name="pk[]" class="pk check" hidden>
                                        </div>
                                    </div>
                                    <div class="input-group input-group-outline form-check ml-3" style="width: 120px">
                                        <label class="check ml-2 iku" for="iku">IKU</label>
                                        <div class="switch d-flex ml-2">
                                            <input type="checkbox" {{ $itemDetail->is_iku == 1 ? "checked" : '' }} class="check form-control form-check-input" id="iku"/>
                                            <input type="text" name="iku[]" {{ $itemDetail->is_iku == 1 ? 1 : 0 }} class="iku check" hidden>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center " style="">
                                    <div class="input-group m-3" style="">
                                        <label for="pengukuran">Cara Pengukuran * : </label>
                                        <div class="input-group input-group-outline">
                                            <textarea name="pengukuran[]" id="pengukuran" style="height: 100px" cols="80" rows="5">{!! !is_null(old('pengukuran.0')) ? old('pengukuran.0') : $itemDetail->cara_pengukuran !!}</textarea>
                                        </div>
                                        @error('pengukuran.*')
                                            <div class="text-danger">
                                                <small>Cara pengukuran tidak boleh kosong!</small>
                                            </div>
                                        @enderror
                                    </div>
                                </div>
                                <div style="display: flex; align-items: center;">
                                    <div class="input-group ml-3" style="width: 120px">
                                        <label for="awal">Kondisi Awal * : </label>
                                        <div class="input-group input-group-outline">
                                            <input type="text" id="awal" class="form-control" name="awal[]" value="{{ $itemDetail->awal }}">
                                        </div>
                                    </div>
                                    @foreach ($years as $idx => $item)
                                    <div class="input-group m-3" style="width: 120px">
                                        <label for="target">Target {{ $item->tahun }} * : </label>
                                        <div class="input-group input-group-outline">
                                            <input type="text" class="form-control" id="target{{ $loop->iteration }}" value="{{ !is_null(old('target.' . $idx)) ? old('target.' . $idx) : $itemDetail->sasaranTarget->where('id_periode_per_tahun', $item->id)->first()->target }}">
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="input-group ml-3" style="width: 120px">
                                        <label for="akhir">Kondisi Akhir * : </label>
                                        <div class="input-group input-group-outline">
                                            <input type="text" id="akhir" class="form-control" name="akhir[]" value="{{ $itemDetail->akhir }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="input-group ml-3 ">
                                    <div class="col-sm-12">
                                        <button type="button" class="close-duplicate btn btn-danger btn-sm close-wrapper" hidden>
                                            Hapus Indikator
                                        </button>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            @endforeach
                        </div>
                        <div class="input-group row">
                            <div class="col-sm-12">
                                <button type="button" class="btn btn-success btn-sm duplicate-btn"><i class="fa fa-plus"></i> Tambah Indikator</button>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="input-group row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <button type="button" onclick="return window.history.back()" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </form>
                @endforeach
            </div>
        </div>
    </div>
    @include('rpjmd.partial.rpjmd_js')
@endsection

@section('scripts')
    <script>
        const form = '.edit_rpjmd_sasaran';
        inputValidate(form, ['input', 'select', 'textarea']);
    </script>
@endsection
