@extends('layouts/app')
@section('title')
    Form Tambah Sasaran
@endsection
@section('content')
<style media="screen">
    .modal-lg{
      width: 750px !important;
    }
    button.badge {
        border: none;
        padding: 3px;
    }
</style>
<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Tambah Sasaran</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">RPJMD</a>
        <span class="breadcrumb-item" style="color: #000;">sasaran</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card ">
        <div class="card-header">
            <h6 class="card-title">Form Tambah sasaran Tingkat Kabupaten Periode {{ periode()->dari }} - {{ periode()->sampai }}</h6>
        </div>
        <div class="card-body scrollmenu">
            <form action="{{ url('') }}/sasaran/store" method="POST" class="add_rpjmd_sasaran">
                @csrf
                <div class="input-group row">
                    <div class="col-sm-4">
                        <div class="input-group">
                            <label for="tujuan">Tujuan Kabupaten * : </label> <br>
                            <select name="tujuan" id="tujuan" class="select2 tujuan">
                                <option value=""></option>
                                @foreach ($tujuan as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select><br>
                            @error('tujuan')
                            <div class="text-danger">
                                <small>Tujuan tidak boleh kosong!</small>
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="input-group row py-2">
                    <div class="col-sm-4">
                        <label for="sasaran">Sasaran Kabupaten * : </label>
                        <div class="input-group input-group-outline">
                            <input type="text" id="sasaran" value="{{ old('sasaran') }}" class="form-control" name="sasaran">
                            @error('sasaran')
                            <div class="text-danger">
                                <small>Sasaran tidak boleh kosong!</small>
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                <div class="row place-duplicate">
                        <div class="duplicate">
                            <div class="d-flex align-items-center">
                                <div class="input-group px-3">
                                    <label for="indikator">Indikator Sasaran * : </label>
                                    <div class="input-group input-group-outline">
                                        <input type="text" id="indikator" value="{{ old('indikator.0') }}" class="form-control" name="indikator[]">
                                    </div>
                                </div>
                                <div class="input-group input-group-outline ml-3">
                                    <label for="satuan">Satuan * : </label><br>
                                    <div class="input-group input-group-outline ml-3">
                                        <select name="satuan[]" id="satuan" class="form-control satuan">
                                            <option value=""></option>
                                            @foreach ($satuan as $item)
                                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="input-group input-group-outline form-check ml-3" style="width: 100px">
                                    <label class="check ml-2 pk" for="pk">PK</label>
                                    <div class="switch d-flex ml-2">
                                        <input type="checkbox" class="check form-control form-check-input" id="pk"/>
                                        <input type="text" name="pk[]" class="pk check" hidden>
                                    </div>
                                </div>
                                <div class="input-group input-group-outline form-check ml-3" style="width: 120px">
                                    <label class="check ml-2 iku" for="iku">IKU</label>
                                    <div class="switch d-flex ml-2">
                                        <input type="checkbox" class="check form-control form-check-input" id="iku"/>
                                        <input type="text" name="iku[]" class="iku check" hidden>
                                    </div>
                                </div>
                            </div>

                            <div class="d-flex align-items-center " style="">
                                <div class="input-group m-3" style="">
                                    <label for="pengukuran">Cara Pengukuran * : </label>
                                    <div class="input-group input-group-outline">
                                        <textarea name="pengukuran[]" id="pengukuran" style="height: 100px" cols="80" rows="5">{{old('pengukuran.0')}}</textarea>                                    
                                    </div>
                                    @error('pengukuran.*')
                                        <div class="text-danger">
                                            <small>Cara pengukuran tidak boleh kosong!</small>
                                        </div>
                                    @enderror
                                </div>
                            </div>
                            <div style="display: flex; align-items: center;">
                                <div class="input-group ml-3" style="width: 120px">
                                    <label for="awal">Kondisi Awal * : </label>
                                    <div class="input-group input-group-outline">
                                        <input type="text" id="awal" class="form-control" name="awal[]">
                                    </div>
                                </div>
                                @foreach($years as $idx => $item)
                                <div class="input-group m-3" style="width: 120px">
                                    <label for="target">Target {{ $item->tahun }} * : </label>
                                    <div class="input-group input-group-outline">
                                        <input type="text" class="form-control" id="target{{ $loop->iteration }}" value="{{ old('target.'.$idx) }}" name="target[]">
                                    </div>
                                </div>
                                @endforeach
                                <div class="input-group ml-3" style="width: 120px">
                                    <label for="akhir">Kondisi Akhir * : </label>
                                    <div class="input-group input-group-outline">
                                        <input type="text" id="akhir" class="form-control" name="akhir[]">
                                    </div>
                                </div>
                            </div>
                            <div class="input-group ml-3 ">
                                <div class="col-sm-12">
                                    <button type="button" class="close-duplicate btn btn-danger btn-sm close-wrapper" hidden>
                                        Hapus Indikator
                                    </button>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <div class="input-group row">
                        <div class="col-sm-12">
                            <button type="button" class="btn btn-success btn-sm duplicate-btn"><i class="fa fa-plus"></i> Tambah Indikator</button>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="input-group row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="button" onclick="return window.history.back()" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('rpjmd.partial.rpjmd_js')
@endsection

@section('scripts')
    <script>
        inputValidate('.add_rpjmd_sasaran', ['input', 'select', 'textarea'], ['pk', 'iku']);
    </script>
@endsection