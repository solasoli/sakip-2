@extends('layouts.app')
@section('title')
    Sasaran
@endsection
@section('content')

<style media="screen">
  .modal-lg{
    width: 750px !important;
  }
  .info_bar {
    position: absolute;
    top: 50px;
    z-index: 998;
    border-radius: 0;
    width: 100%;
    background-image: linear-gradient(195deg, #42424a 0%, #191919 100%);
  }
  #card-body-info {
    transition: .3s;
  }
  .relative-info {
    transition: .3s;
  }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
  <span class="brand ml-4">Sasaran</span>
  <nav class="mr-4">
    <a class="breadcrumb-item" href="/">Dashboard</a>
    <a class="breadcrumb-item" href="#">RPJMD</a>
    <span class="breadcrumb-item" style="color: #000;">Sasaran</span>
  </nav>
</div>

<hr/>

<div class="container-fluid pb-2 mt-4">
  <div class="card">
    @if(!is_null(periode()))
    <div class="card-body">
      <div class="card-header">
        <h6 class="card-title">Data Sasaran Tingkat Kabupaten Periode {{ periode()->dari }} - {{ periode()->sampai }}</h6>
        <a href="{{ url('') }}/sasaran/add" class="btn btn-primary btn-sm" style="background-color: #4caf50;"><i class="fa fa-plus"></i> Tambah</a>
      </div>

      @include('partials.error-message')
      @include('partials.message')
      <hr>
      @foreach ($data as $idx => $item)
        <div class="card justify-content-center mt-2">
          <div class="card-header bg-primary text-light" style="position: relative; z-index: 999;">
            <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
              <a href="#" class="info_btn"><i class="fa fa-info-circle"></i></a>&emsp;
              <i class="fa fa-angle-double-right"></i> &nbsp; Sasaran : {{ $item->name }} &nbsp;
            </span>
            <div class="action">
              <form action="{{ url('') }}/sasaran/edit/{{ hashID($item->id) }}" method="POST" class="d-inline">
                @csrf
                <button class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i>Edit</button>
              </form>
              <form action="{{ url('') }}/sasaran/{{ $item->id }}/delete" method="POST" class="d-inline">
                @method('delete')
                @csrf
                <button class="btn btn-danger btn-sm" onclick="return confirm('Lanjutkan menghapus data?')">Hapus</button>
              </form>
            </div>
          </div>
          <div class="alert show-hidden info_bar mt-5" style="display: flex; flex-direction: column" role="alert">
            <span class="hierarki_m">~ / Misi &nbsp; <i class="fa fa-caret-right"></i>&emsp; {{ $item->tujuan->misi->misi }}</span>
            <span class="hierarki_m">~ / Tujuan &nbsp; <i class="fa fa-caret-right"></i>&emsp; {{ $item->tujuan->name }}</span>
            <span class="hierarki_m">~ / Sasaran &nbsp; <i class="fa fa-caret-right"></i>&emsp; {{ $item->name }}</span>
          </div>
          <style>
            span.checklist {
                content: "asd";
                color: #000;
            }
          </style>
          <div class="card-body text-center">
              <style>
                span.checklist {
                    content: "asd";
                    color: #000;
                }
              </style>
              <div class="card-body text-center">
                <div class="table-responsive">

                  <table class="table table-bordered">
                    <tr>
                      <th>Indikator Sasaran</th>
                      <th>Satuan</th>
                      <th>PK</th>
                      <th>IKU</th>
                      <th>Cara pengukuran</th>
                      <th>Kondisi Awal</th>
                      @foreach($periode_per_tahun as $t)
                      <th>Target {{ $t->tahun }}</th>
                      @endforeach
                      <th>Kondisi Akhir</th>
                      <th>Target RKPD</th>
                    </tr>
                    
                    @foreach($item->sasaranDetail as $idx => $itemDetail)
                    @php
                      $targetId = "target$itemDetail->id";
                    @endphp
                    <tr>
                      <td>{{ $itemDetail->indikator }}</td>
                      <td>{{ $itemDetail->satuan->name }}</td>
                      <td>{!! $itemDetail->is_pk > 0 ? "&check;" : '&times;' !!}</td>
                      <td>{!! $itemDetail->is_iku > 0 ? "&check;" : '&times;' !!}</td>
                      <td>
                        <a href="#" data-bs-toggle="modal" data-bs-target="#caraPengukuran{{ $idx }}" style="text-decoration: none">
                          Selengkapnya
                        </a>
                      </td>
                      <td>{{ $itemDetail->awal }}</td>
                      @foreach ($itemDetail->sasaranTarget as $itemTarget)
                        <td>{{ !is_null($itemTarget->target) ? $itemTarget->target : '-' }}</td>
                      @endforeach
                      <td>{{ $itemDetail->akhir }}</td>
                      <td>
                          <button
                            data-bs-toggle="modal"
                            data-bs-target="#{{$targetId}}"
                            class="btn btn-success btn-sm duplicate-btn">
                                + Tambah Target
                            </button>
                        </td>
                    </tr>
                  @endforeach
                </table>
              </div>
            </div>
          </div>
        </div>

          <!-- Modal -->
          @foreach($item->sasaranDetail as $idx => $itemDetail)
            @php
                $targetId = "target$itemDetail->id";
            @endphp
          <div class="modal fade" id="caraPengukuran{{ $idx }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLongTitle">Detail Cara Pengukuran</h5>
                  <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <span>
                    Indikator Sasaran : <br/>
                    {{ $itemDetail->indikator }}
                  </span>
                  <br>
                  <br>
                  <span>
                    Cara Pengukuran : <br/>
                    {{ $itemDetail->cara_pengukuran }}
                  </span>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
          @include('rpjmd.partial.modal-target-rkpd', [
            'modalId' => $targetId,
            'headline' => 'Anggaran Renstra Program PD',
            'namaIndikator' => $itemDetail->indikator,
            'tahunPeriode' => $periode->periodePerTahun,
            'targetRkpd' => $itemDetail->targetRkpd,
            'submitUrl' => route('rpjmd.sasaran.sasaran_detail', ['sasaranDetail' => $itemDetail->id]),
          ])
          @endforeach

      @endforeach

      <div class="input-group input-group-outline mt-4">
        <span class="d-flex justify-content-center">
          {{ $data->links('pagination::bootstrap-4') }}
        </span>
      </div>
    </div>
    @else
    <div class="card-body">
        <h6 class="text-danger">Periode belum ditetapkan!</h6>
        @if(auth()->user()->id == 1)
            <a href="{{ url('/konfigurasi') }}" class="btn btn-primary btn-sm mt-3">Tetapkan Periode</a>
        @endif
    </div>
    @endif
  </div>
</div>
@endsection

@section('scripts')
<script>

  // create and get hierarki
  function getInfoHierarki(btn) {
    let info = document.querySelectorAll(btn);
    info.forEach(res => {
      res.addEventListener('click', () => {
        const card = res.closest('.card')
        const card_header = card.querySelector('.card-header')
        const info_bar = card_header.nextElementSibling
        const card_body = card.querySelector('.card-body')
        const height_info_bar = info_bar.offsetHeight

        info_bar.classList.toggle('show-hidden')
        card_body.style.transition = '.3s';
        if(!info_bar.classList.contains('show-hidden')) {
          card_body.style.marginTop = `${height_info_bar - 10}px`;
        }else{
          card_body.style.marginTop = `0`;
        }
      })
    })
  }

  // function if form submited
  function submitForm(el) {
    let form = document.querySelector(el);
    form.addEventListener('submit', function(e) {
      let url = `{{ url('') }}/sasaran/`;

      this.setAttribute('action', url)
    })
  }

  getInfoHierarki('.info_btn');
  submitForm('.get_sasaran');

</script>
@endsection
