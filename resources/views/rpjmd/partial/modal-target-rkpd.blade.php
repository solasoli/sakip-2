<div class="modal fade" id="{{$modalId}}" tabindex="-1" aria-labelledby="{{$modalId}}_label" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <form action="{{$submitUrl}}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="{{$modalId}}_label">Sasaran Rencana Kerja Pembangunan Daerah</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Indikator Sasaran</th>
                                    <th>Satuan</th>
                                    @foreach ($tahunPeriode as $tahun)
                                    <th>Tahun {{$tahun->tahun}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="p-3">{{$namaIndikator}}</td>
                                    <td>
                                        <div class="input-group input-group-outline">
                                            <select class="form-control" name="satuan" id="{{ $modalId."satuan" }}">
                                                @foreach ($satuan as $item)
                                                <option value="{{$item->id}}" {{$item->id == ($targetRkpd->id_satuan ?? null) ? 'selected' : null}}>
                                                    {{$item->name}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                    @foreach ($tahunPeriode as $tahun)
                                        @php
                                            $target = $targetRkpd ? $targetRkpd->target->where('id_periode_per_tahun', $tahun->id)->first() : null;
                                        @endphp
                                        <input type="hidden"
                                            name="target[{{ $loop->index }}][id_tahun]"
                                            value="{{ $tahun->id }}">
                                        <td>
                                            <div class="input-group input-group-outline">
                                                <input
                                                type="text"
                                                value="{{$target != null ? $target->nilai : null}}"
                                                name="target[{{ $loop->index }}][value]"
                                                class="form-control input-group input-group-outline"/>
                                            </div>
                                        </td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
