<script>
    $(document).ready(function() {
        checkboxValue();

        show_remove_btn('.close-wrapper', 'hidden');
        $('.duplicate-btn').click(() => {
            clone_el();
            show_remove_btn('.close-wrapper', 'hidden');
        })

        closeElementClone('close-duplicate', '.duplicate');

        // function show remove button after click duplicate-btn
        function show_remove_btn(el, attr) {
            const close_btn = document.querySelectorAll(el);
            close_btn.forEach((res, idx) => {
                if (idx > 0) {
                    res.removeAttribute(attr)
                }
            })
        }
        // end //

        // function clone element 
        const clone_el = function() {
            const el_duplicate = document.querySelector('.duplicate');
            const select_clone = el_duplicate.querySelectorAll('select.satuan');
            let span_select = el_duplicate.querySelectorAll('span.selection');
            span_select.forEach(res => res.remove());

            select_clone.forEach(res => {
                res.classList.remove('satuan');
                res.classList.add('satuan-cloned');
            })

            const clone = el_duplicate.cloneNode(true);
            document.querySelector('.place-duplicate').appendChild(clone);
            clean_el_clone(clone);
            selectPlaceholder('.satuan-cloned', 'Satuan');
        }
        // end //

        // clear value after clone
        const clean_el_clone = function(el) {
            let check = el.querySelectorAll('input[type=checkbox]');
            let input = el.querySelectorAll('input[type=text]');

            let jml_clone = document.querySelectorAll('.duplicate').length;

            correspondingLabelInput(el, 'label.pk', 'input#pk', `pk_${jml_clone}`);
            correspondingLabelInput(el, 'label.iku', 'input#iku', `iku_${jml_clone}`);

            check.forEach(res => res.checked = false)
            input.forEach(res => (!res.classList.contains('check')) ? res.value = '' : checkboxValue())
        }
        // end //

        // create for label and id input (matching)
        const correspondingLabelInput = function(base_el, label_el, input_el, str) {
            const _label = base_el.querySelector(label_el);
            const _input = base_el.querySelector(input_el);

            _label.removeAttribute('for');
            _input.removeAttribute('id');

            _label.setAttribute('for', str);
            _input.setAttribute('id', str);
        }
        // end //

        // remove element clone
        function closeElementClone(c_btn, c_parent) {
            document.addEventListener('click', function(e) {
                if (e.target.classList.contains(c_btn) || e.target.classList.contains('fa-close')) {
                    e.preventDefault();
                    this.activeElement.closest(c_parent).remove();
                }
            })
        }
        // end //

        // select2 using placeholder
        selectPlaceholder('.select2.tujuan', 'Pilih Tujuan')
        selectPlaceholder('select.satuan', "Satuan")
        // end //
    });
</script>
