@extends('layouts.app')
@section('title')
    Kebijakan
@endsection
@section('content')
    <div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
        <span class="brand ml-4">Kebijakan</span>
        <nav class="mr-4">
            <a class="breadcrumb-item" href="/">Dashboard</a>
            <a class="breadcrumb-item" href="#">RPJMD</a>
            <span class="breadcrumb-item" style="color: #000;">Kebijakan</span>
        </nav>
    </div>
    <style media="screen">
        .info_bar {
            position: absolute;
            top: 50px;
            z-index: 998;
            border-radius: 0;
            width: 100%;
            background-image: linear-gradient(195deg, #42424a 0%, #191919 100%);
        }

        #card-body-info {
            transition: .3s;
        }

        .relative-info {
            transition: .3s;
        }
    </style>
    <div class="container-fluid pb-2 mt-4">
        <div class="card">
            @if (!is_null(periode()))
                <div class="card-body">
                    <div class="card-header">
                        <h6 class="card-title">Data Kebijakan Tingkat Kabupaten Periode {{ periode()->dari }} -
                            {{ periode()->sampai }}</h6>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success flashMessages" role="alert">
                            {{ $message }}
                        </div>
                    @endif
                    <hr>
                    @if(!is_null($strategi->first()))
                    <form action="{{ url('') }}/rpjmd/kebijakan/create" method="POST" class="form_rpjmd_kebijakan">
                        @csrf
                        @foreach ($hierarki as $idx => $item)    
                            <div class="card mt-2">
                                <div class="card-header bg-primary text-light" style="position: relative; z-index: 999;">
                                    <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                                        <a href="#" class="info_btn">
                                            <i class="fa fa-info-circle"></i>
                                        </a>&emsp;
                                        <i class="fa fa-angle-double-right"></i> &nbsp; Strategi : &nbsp; {{ $item->name }}
                                    </span>
                                </div>
                                <div class="alert show-hidden info_bar mt-5"
                                    style="display: flex; flex-direction: column" role="alert">
                                    <span class="hierarki_m">~ / Misi &nbsp; 
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->misi }}
                                    </span>
                                    <span class="hierarki_m">~ / Tujuan &nbsp; 
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->tujuan }}
                                    </span>
                                    <span class="hierarki_m">~ / Sasaran &nbsp; 
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->sasaran }}
                                    </span>
                                    <span class="hierarki_m">~ / Strategi &nbsp; 
                                        <i class="fa fa-caret-right">&emsp;</i>{{ $item->name }}
                                    </span>
                                </div>
                                <div class="card-body">
                                    <input type="number" name="id_strategi{{ $idx }}" value="{{ $item->id }}" hidden>
                                    <div class="row">
                                        @if(!is_null($kebijakan->where('id_strategi', $item->id)->first()))
                                        @php $kebijakan_ = $kebijakan->where('id_strategi', $item->id); @endphp
                                        <div class="duplicate">
                                            <div class="input-group input-group-outline">
                                                <input type="text" class="form-control kebijakan" name="kebijakan{{ $idx }}[]" value="{{ $kebijakan_->first()->name }}">
                                            </div>
                                        </div>
                                        @foreach ($kebijakan_->skip(1) as $val)
                                        <div class="duplicate mt-2 d-flex">
                                            <div class="input-group input-group-outline my-2"> 
                                                <input type="text" class="form-control kebijakan" name="kebijakan{{ $idx }}[]" value="{{ $val->name }}">
                                            </div>
                                            <button class="btn btn-danger btn-sm ml-2 close-duplicate">
                                                X
                                            </button>
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="duplicate">
                                            <div class="input-group input-group-outline my-2"> 
                                               <input type="text" class="form-control kebijakan" name="kebijakan{{ $idx }}[]">
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    <button class="clone_kebijakan btn btn-primary btn-sm mt-1" type="button">
                                        <i class="fa fa-plus"></i> Tambah poin
                                    </button>
                                </div>
                            </div>
                        @endforeach
                        {{--  Pagination  --}}
                        <div class="input-group mt-2">
                            <span class="d-flex justify-content-center">
                                {{ $hierarki->links('pagination::bootstrap-4') }}
                            </span>
                        </div>
                        <div class="input-group ">
                            <a href="" class="btn btn-dark">Cancel</a>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                    @else
                    <span class="text-danger">Isi data strategi terlebih dahulu!</span><br>
                    <span>Klik <a href="{{ url('') }}/rpjmd/strategi">disini</a> untuk mengisi strategi</span>
                    @endif
                </div>
            @else
            <div class="card-body">
                <span class="text-danger">Tetapkan periode terlebih dahulu!</span><br>
            </div>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        // create and get hierarki
        function getInfoHierarki(btn) {
            let info = document.querySelectorAll(btn);
            info.forEach(res => {
                res.addEventListener('click', () => {
                    const card = res.closest('.card')
                    const card_header = card.querySelector('.card-header')
                    const info_bar = card_header.nextElementSibling
                    const card_body = card.querySelector('.card-body')
                    const height_info_bar = info_bar.offsetHeight

                    info_bar.classList.toggle('show-hidden')
                    card_body.style.transition = '.3s';
                    if (!info_bar.classList.contains('show-hidden')) {
                        card_body.style.marginTop = `${height_info_bar - 10}px`;
                    } else {
                        card_body.style.marginTop = `0`;
                    }
                })
            })
        }

        function cloneKebijakan() {
            let btn = document.querySelectorAll('.clone_kebijakan');
            btn.forEach(res => {
                res.addEventListener('click', function(e) {
                    e.preventDefault();
                    let parent = this.previousElementSibling;
                    let el_clone = parent.querySelector('.duplicate');
                    let clone = el_clone.cloneNode(true);

                    clone.classList.add('mt-2', 'd-flex');
                    const fix_el = cleanNodeClone(clone);

                    parent.append(fix_el);
                });
            });
        }

        function cleanNodeClone(el) {
            const btn = document.createElement('button');
            const i = document.createElement('i');
            el.querySelector('input.kebijakan').value = "";

            btn.setAttribute('class', 'btn btn-danger btn-sm ml-2 my-2 close-duplicate')
            i.setAttribute('class', 'fa fa-close');
            btn.append(i);
            el.append(btn);

            return el;
        }

        function closeNodeClone() {
            document.addEventListener('click', function(e) {
                let target_class = e.target.classList;
                if (target_class.contains('close-duplicate') || target_class.contains('fa-close')) {
                    e.target.closest('.duplicate')
                        .remove();
                }
            })
        }

        const form = '.form_rpjmd_kebijakan';

        closeNodeClone();
        cloneKebijakan();
        getInfoHierarki('.info_btn');
    </script>
@endsection