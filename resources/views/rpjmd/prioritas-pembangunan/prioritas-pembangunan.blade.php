@extends('layouts.app')
@section('title')
    Prioritas Pembangunan
@endsection
@section('content')

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
  <span class="brand ml-4">Prioritas Pembangunan</span>
  <nav class="mr-4">
    <a class="breadcrumb-item" href="/">Dashboard</a>
    <a class="breadcrumb-item" href="#">RPJMD</a>
    <span class="breadcrumb-item" style="color: #000;">Prioritas Pembangunan</span>
  </nav>
</div>

<style>
  .info_bar {
    position: absolute; 
    top: 50px; 
    z-index: 998;
    border-radius: 0; 
    width: 100%;
    background-image: linear-gradient(195deg, #42424a 0%, #191919 100%);
  }
  #card-body-info {
    transition: .3s;
  }
</style>

<div class="container-fluid pb-2 mt-4">
    <div class="card">
      @if(!is_null(periode()))
      <div class="card-body">
        <div class="card-header">
          <h6 class="card-title">Data Prioritas Pembangunan Periode {{ periode()->dari }} - {{ periode()->sampai }}</h6>
        </div>
        @if ($message = Session::get('success'))
        <div class="alert alert-success flashMessages" role="alert">
          {{ $message }}
        </div>
        @endif
        <div class="card-body">
          @if(!is_null($sasaran->first()))
          <form action="{{ url('') }}/rpjmd/prioritas-pembangunan/create" method="POST">
            @csrf
            @foreach ($misi as $idxMisi => $itemMisi)

              @if(count($tujuan->where('id_misi', $itemMisi->id)) > 0)
                  
                @foreach ($tujuan->where('id_misi', $itemMisi->id) as $itemTujuan)
      
                  @if($sasaran->where('id_tujuan', $itemTujuan->id)->first() != null)
                    
                    @if(!is_null($sasaran->where('id_tujuan', $itemTujuan->id)->first()))

                      @foreach($sasaran->where('id_tujuan', $itemTujuan->id) as $idx => $itemSasaran)
                      
                        <div class="card justify-content-center mt-2">
                          <div class="card-header bg-primary text-light" style="position: relative; z-index: 999;">
                            <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                              <a href="#" class="info_btn"><i class="fa fa-info-circle"></i></a>&emsp;
                              <h6 class="text-white">Sasaran : {{ $itemSasaran->name }}</h6>
                            </span>
                          </div>
                          <div class="alert show-hidden info_bar mt-4" style="display: flex; flex-direction: column" role="alert">
                            <span class="hierarki_m">~ / Misi &nbsp; <i class="fa fa-caret-right"></i>&emsp; {{ $itemMisi->misi }}</span>
                            <span class="hierarki_m">~ / Tujuan &nbsp; <i class="fa fa-caret-right"></i>&emsp; {{ $itemTujuan->name }}</span>
                            <span class="hierarki_m">~ / Sasaran &nbsp; <i class="fa fa-caret-right"></i>&emsp; {{ $itemSasaran->name }}</span>
                          </div>
                          <div class="card-body">
                            <div class="input-group input-group-outline d-flex prioritas_pembangunan_wrapper">
                              <select name="prioritas_pembangunan{{ $idx }}" class="form-control col-11 prioritas_pembangunan" id="prioritas_pembangunan">
                                <option value=""></option>
                                @foreach ($mst_prioritas_pembangunan as $item)
                                  @if (!is_null($itemSasaran->prioritasPembangunan)) 
                                  <option {{ $itemSasaran->prioritasPembangunan->id_mst_prioritas_pembangunan == $item->id ? "selected" : "" }} value="{{ $item->id }}">{{ $item->name }}</option>
                                  @else
                                  <option value="{{ $item->id }}">{{ $item->name }}</option>
                                  @endif
                                @endforeach
                              </select>
                              <input type="number" name="id_sasaran{{ $idx }}" value="{{ $itemSasaran->id }}" hidden>
                            </div>
                          </div>
                        </div>
                      @endforeach

                    @endif

                  @endif

                @endforeach

              @endif

            @endforeach
            <div class="input-group input-group-outline mt-4">
              <a href="" class="btn btn-dark">Cancel</a>
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
          @else
          <span class="text-danger">Isi data sasaran terlebih dahulu!</span><br>
          <span>Klik <a href="{{ url('') }}/sasaran/add">disini</a> untuk mengisi sasaran</span>
          @endif
        </div>
      </div>
      @else
      <div class="card-body">
        <span class="text-danger">Tetapkan periode terlebih dahulu!</span><br>
      </div>
      @endif    
    </div>
</div>

@endsection

@section('scripts')
    <script>
      /* function create and get hierarki */
      function getInfoHierarki(btn) {
        let info = document.querySelectorAll(btn);
        info.forEach(res => {
          res.addEventListener('click', () => {
            const card = res.closest('.card')
            const card_header = card.querySelector('.card-header')
            const info_bar = card_header.nextElementSibling
            const card_body = card.querySelector('.card-body')
            const height_info_bar = info_bar.offsetHeight

            info_bar.classList.toggle('show-hidden')
            card_body.style.transition = '.5s';
            if(!info_bar.classList.contains('show-hidden')) {
              card_body.style.marginTop = `${height_info_bar - 10}px`;
            }else{
              card_body.style.marginTop = `0`;
            }
          })
        })
      }
      /* end hierarki */

      /* call method */
      selectPlaceholder('.prioritas_pembangunan', 'Pilih prioritas pembangunan')
      getInfoHierarki('.info_btn');
    </script>
@endsection 