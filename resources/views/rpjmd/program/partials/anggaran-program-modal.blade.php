<div class="modal fade" id="{{ $modalId }}" tabindex="-1" aria-labelledby="{{ $modalId }}_label"
    aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form action="{{ route('rpjmd.program.program.submit_anggaran_rkpd', ['program' => $program->id]) }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="{{ $modalId }}_label">Anggaran Program RKPD</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2">Program</th>
                                    <th colspan="{{ $periodePerTahun->count() }}">Anggaran RKPD</th>
                                </tr>
                                <tr>
                                    @foreach ($periodePerTahun as $tahun)
                                        <th>{{ $tahun->tahun }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-overflow: ellipsis;" class="break-all max-w-md overflow-hidden">
                                        {{ $program->mstProgram->name }}</td>
                                    @foreach ($periodePerTahun as $periode)
                                        @php $anggaran = $program->anggaran->firstWhere('id_periode_per_tahun', $periode->id); @endphp
                                        <input type="hidden" name="anggaran[{{ $loop->index }}][id_tahun]"
                                            value="{{ $periode->id }}">
                                        <td class="text-center">
                                            <div class="input-group input-group-outline">
                                                <input type="text" class="form-control"
                                                name="anggaran[{{ $loop->index }}][value]"
                                                value="{{ $anggaran != null ? $anggaran->nilai : null }}">
                                            </div>
                                        </td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
