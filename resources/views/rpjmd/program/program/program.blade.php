@extends('layouts.app')
@section('title')
    Program
@endsection
@section('content')
<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Program</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">RPJMD</a>
        <a class="breadcrumb-item" href="#">Program</a>
        <span class="breadcrumb-item" style="color: #000;">Program</span>
    </nav>
</div>

<style media="screen">
    .info_bar {
        position: absolute;
        top: 50px;
        z-index: 998;
        border-radius: 0;
        width: 100%;
        background-color: rgb(41, 77, 100);
    }

    #card-body-info {
        transition: .3s;
    }

    .relative-info {
        transition: .3s;
    }

</style>

<div class="container-fluid pb-2 mt-4">
    @include('partials.error-message')
    @include('partials.message')
    <div class="card">
        @if (!is_null(periode()))
            <div class="card-body">
                <div class="card-header">
                    <h6 class="card-title">Data Program Tingkat Kabupaten Periode {{ periode()->dari }} -
                        {{ periode()->sampai }}</h6>
                    <button type="button" class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#addProgram" style="background-color: #4caf50;">
                        <i class="fa fa-plus"> Tambah Program</i>
                    </button>
                </div>
                <hr>

                @foreach ($data as $idx => $item)
                    @if (!is_null($item->program->first()))
                    <div class="card justify-content-center mt-2">
                        <div class="card-header bg-primary text-light"
                            style="position: relative; z-index: 999;">
                            <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                                <a href="#" class="info_btn"><i
                                        class="fa fa-info-circle"></i></a>&emsp;
                                <i class="fa fa-angle-double-right"></i> &nbsp; Sasaran :
                                {{ $item->name }} &nbsp;
                            </span>
                        </div>
                        <!-- hierarki -->
                        <div class="alert show-hidden info_bar mt-4"
                            style="display: flex; flex-direction: column" role="alert">
                            <span class="hierarki_m">~ / Misi &nbsp; <i class="fa fa-caret-right"></i>&emsp;{{ $item->tujuan->misi->misi }}</span>
                            <span class="hierarki_m">~ / Tujuan &nbsp; <i class="fa fa-caret-right"></i>&emsp;{{ $item->tujuan->name }}</span>
                            <span class="hierarki_m">~ / Sasaran &nbsp; <i class="fa fa-caret-right"></i>&emsp;{{ $item->name }}</span>
                            <span class="hierarki_m">~ / Prioritas Pembangunan &nbsp;
                                <i class="fa fa-caret-right"></i>&emsp;
                                @if(!is_null($item->prioritasPembangunan))

                                    @if (!is_null($item->prioritasPembangunan->mstPrioritasPembangunan))
                                        {{ $item->prioritasPembangunan->mstPrioritasPembangunan->name }}
                                    @else
                                        <span class="text-warning">Prioritas Pembangunan belum ditetapkan! &nbsp; klik
                                        <a href="{{ url('') }}/rpjmd/prioritas-pembangunan" class="text-info">disini</a> untuk menetapkan</span>
                                    @endif

                                @else
                                    <span class="text-warning">Prioritas Pembangunan belum ditetapkan! &nbsp; klik
                                    <a href="{{ url('') }}/rpjmd/prioritas-pembangunan" class="text-info">disini</a> untuk menetapkan</span>
                                @endif
                            </span>
                        </div>

                        <div class="card-body">
                            <div class="card">
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Program</th>
                                                    @foreach ($periode_per_tahun as $val)
                                                        <th width="150" class="text-center">Anggaran Program {{ $val->tahun }}</th>
                                                    @endforeach
                                                    <th>Anggaran RKPD</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($item->program as $val)
                                                <tr>
                                                    <td>{{ $val->mstProgram->name }}</td>
                                                    @foreach ($periode_per_tahun as $res)
                                                        <td class="text-center">0</td>
                                                    @endforeach
                                                    <td>
                                                        <button
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#anggaran{{$val->id}}"
                                                        class="btn btn-success btn-sm duplicate-btn">
                                                            + Tambah Anggaran
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button type="button" class="btn btn-warning btn-sm" data-bs-toggle="modal" data-bs-target="#editProgram{{ $val->id }}">ubah</button>
                                                        <a href="{{ url('') }}/rpjmd/program/program/delete/{{ hashID($val->id) }}" class="btn btn-danger btn-sm" onclick="return confirm('Lanjutkan menghapus data?')">hapus</a>
                                                    </td>
                                                </tr>

                                                <!-- Modal edit -->
                                                <div class="modal fade" id="editProgram{{ $val->id }}" tabindex="-1" role="dialog" aria-labelledby="addProgramTitle" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Form tambah program</h5>
                                                        </div>
                                                        <form action="{{ url('') }}/rpjmd/program/program/edit/{{ $val->id }}" method="POST">
                                                            @method('patch')
                                                            @csrf
                                                            <div class="modal-body">
                                                                <div class="input-group input-group-outline my-2">
                                                                    <select name="sasaran" class="sasaran">
                                                                        @foreach ($sasaran as $sasaranItem)
                                                                            <option value="{{ $sasaranItem->id }}" {{ $val->id_rpjmd_sasaran == $sasaranItem->id ? "selected" : "" }}>{{ $sasaranItem->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                                <div class="input-group input-group-outline my-2">
                                                                    <select name="program" class="select2" id="program">
                                                                        @foreach ([$val->mstProgram, ...$mst_program] as $program)
                                                                            <option value="{{ $program->id }}" {{ $val->id_mst_program == $program->id ? "selected" : "" }}>{{ $program->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary btn-sm" data-bs-dismiss="modal">Close</button>
                                                                <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                                                            </div>
                                                        </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
                @foreach ($data as $idx => $item)
                    @foreach ($item->program as $val)
                        @include('rpjmd.program.partials.anggaran-program-modal', [
                            'modalId' => "anggaran".$val->id,
                            'periodePerTahun' => $periode_per_tahun,
                            'program' => $val
                        ])
                    @endforeach
                @endforeach
            </div>
        @else
        <div class="card-body">
            <span class="text-danger">Tetapkan periode terlebih dahulu!</span><br>
        </div>
        @endif
    </div>
</div>

<!-- Modal add -->
<div class="modal modal-add fade" id="addProgram" tabindex="-1" role="dialog" aria-labelledby="addProgramTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Form tambah program</h5>
      </div>
      <form action="{{ url('') }}/rpjmd/program/program/add" method="POST">
        @csrf
        <div class="modal-body">
            <div class="form-group" id="sasaranSelect">
                <select name="sasaran" class="sasaran">
                    <option value=""></option>
                    @foreach ($sasaran as $sasaranItem)
                        <option value="{{ $sasaranItem->id }}" {{ old('sasaran') == $sasaranItem->id ? "selected" : "" }}>{{ $sasaranItem->name }}</option>
                    @endforeach
                </select>
                @error ('sasaran')
                    <span class="text-danger">
                        <small>sasaran tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
            <div class="form-group" id="programSelect">
                <select name="program" class="select2" id="program">
                    <option value=""></option>
                    @foreach ($mst_program as $program)
                        <option value="{{ $program->id }}" {{ old('program') == $program->id ? "selected" : "" }}>{{ $program->name }}</option>
                    @endforeach
                </select>
                @error ('program')
                    <span class="text-danger">
                        <small>program tidak boleh kosong!</small>
                    </span>
                @enderror
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary btn-sm">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            selectUsingParent('.sasaran', '#sasaranSelect'/*parent*/, 'Pilih sasaran');
            selectUsingParent('#program.select2', '#programSelect', 'Pilih program');

            getInfoHierarki('.info_btn');
            // create and get hierarki
            function getInfoHierarki(btn) {
                let info = document.querySelectorAll(btn);
                info.forEach(res => {
                    res.addEventListener('click', () => {
                        const card = res.closest('.card')
                        const card_header = card.querySelector('.card-header')
                        const info_bar = card_header.nextElementSibling
                        const card_body = card.querySelector('.card-body')
                        const height_info_bar = info_bar.offsetHeight

                        info_bar.classList.toggle('show-hidden')
                        card_body.style.transition = '.3s';
                        if (!info_bar.classList.contains('show-hidden')) {
                            card_body.style.marginTop = `${height_info_bar - 10}px`;
                        } else {
                            card_body.style.marginTop = `0`;
                        }
                    })
                })
            }

            // modal handle error
            @if(Session::has('errors'))
            $('.modal-add').modal({show: true});
            @endif

        });
    </script>
@endsection
