    @extends('layouts.app')
@section('title')
    Indikator Program
@endsection
@section('content')
<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Indikator Program</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">RPJMD</a>
        <a class="breadcrumb-item" href="#">Program</a>
        <span class="breadcrumb-item" style="color: #000;">Indikator Program</span>
    </nav>
</div>

<style media="screen">
    .info_bar {
        position: absolute;
        top: 50px;
        z-index: 998;
        border-radius: 0;
        width: 100%;
        background-color: rgb(41, 77, 100);
    }

    #card-body-info {
        transition: .3s;
    }

    .relative-info {
        transition: .3s;
    }
</style>

<div class="container-fluid pb-2 mt-4">
    <div class="card">
        @include('partials.error-message')
        @include('partials.message')
        @if (!is_null(periode()))
            <div class="card-body">
                <div class="card-header">
                    <h6 class="card-title">Indikator Program Tingkat Kabupaten Periode {{ periode()->dari }} -
                        {{ periode()->sampai }}</h6>
                </div>
                <hr>
                <form action="{{ url('') }}/rpjmd/program/indikator-program/create" method="POST">
                    @csrf
                    @foreach ($program as $programItem)
                        @php
                            $hierarchy = [
                                '/ Misi' => $programItem->getMisi()->misi,
                                '/ Tujuan' => $programItem->getRpjmdTujuan()->name,
                                '/ Sasaran' => $programItem->getRpjmdSasaran()->name,
                                '/ Prioritas Pembangunan' => $programItem->getPrioritasPembangunan()->name,
                                '/ Program' => $programItem->mstProgram->name,
                            ];
                            $programIndex = $loop->index;
                            $title = 'Program : ' . $programItem->mstProgram->name;
                            $elementId = "program_$programItem->id";
                        @endphp
                        <input type="hidden" name="program[{{$programIndex}}][id]" value="{{$programItem->id}}">
                        <div x-data="{{$elementId}}">
                            <x-hierarchy-card :title="$title" :hierarchy="$hierarchy">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Indikator Program</th>
                                                <th>Satuan</th>
                                                <th>Kondisi Awal</th>
                                                <template x-for="tahun in periode">
                                                    <th width="150" class="text-center"><span x-text="`Program ${tahun.tahun}`"></span></th>
                                                </template>
                                                <th>Kondisi Akhir</th>
                                                <th>Target RKPD</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <template x-for="(indikator, indikatorIndex) in indikators">
                                                <tr>
                                                    <input
                                                        type="hidden"
                                                        :name="`program[{{$programIndex}}][indikator][${indikatorIndex}][id]`"
                                                        :value="indikator.id"/>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input
                                                            type="text"
                                                            class="input-group input-group-outline form-control"
                                                            x-model="indikator.indikator"
                                                            :name="`program[{{$programIndex}}][indikator][${indikatorIndex}][name]`"
                                                            autocomplete="off"/>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <select
                                                            :name="`program[{{$programIndex}}][indikator][${indikatorIndex}][satuan_id]`"
                                                            class="satuan">
                                                            <template x-for="satuanItem in satuan">
                                                                <option :value="satuanItem.id" x-text="satuanItem.name" :selected="indikator.id_satuan == satuanItem.id"></option>
                                                            </template>
                                                        </select>
                                                    </td>
                                                <td>
                                                    <div class="input-group input-group-outline">
                                                        <input
                                                            type="text"
                                                            class="form-control"
                                                            :value="indikator.awal"
                                                            :name="`program[{{$programIndex}}][indikator][${indikatorIndex}][kondisi_awal]`"
                                                            autocomplete="off"/>
                                                    </div>
                                                </td>
                                                    <template x-for="(tahun, tahunIndex) in periode">
                                                        <td>
                                                            <div class="input-group input-group-outline">
                                                                <input
                                                                type="hidden"
                                                                :value="tahun.id"
                                                                :name="`program[{{$programIndex}}][indikator][${indikatorIndex}][target][${tahunIndex}][id_tahun]`"/>
                                                                <input
                                                                type="text"
                                                                class=" form-control"
                                                                :value="getIndikatorsTargetForSpecificYear(indikator, tahun)"
                                                                :name="`program[{{$programIndex}}][indikator][${indikatorIndex}][target][${tahunIndex}][value]`"
                                                                autocomplete="off"/>
                                                            </div>
                                                        </td>
                                                    </template>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input
                                                            type="text"
                                                            class="form-control"
                                                            :value="indikator.akhir"
                                                            :name="`program[{{$programIndex}}][indikator][${indikatorIndex}][kondisi_akhir]`"
                                                            autocomplete="off"/>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <button
                                                            type="button"
                                                            data-toggle="modal"
                                                            :data-target="'#' + generateTargetId(indikator)"
                                                            class="btn btn-success btn-sm"
                                                            :disabled="indikator.id == null">
                                                            + Tambah Target
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <template x-if="indikatorIndex != 0">
                                                            <button class="btn btn-danger btn-sm" type="button"
                                                                    @click="hapusIndikator(indikatorIndex)"
                                                                    title="Hapus">
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                        </template>
                                                    </td>
                                                </tr>
                                            </template>
                                        </tbody>
                                    </table>
                                </div>
                                <button @click="tambahIndikator()" type="button" class="btn btn-primary btn-sm btn_clone">
                                    <i class="fa fa-plus"></i> Tambah poin
                                </button>
                            </x-hierarchy-card>
                            <template x-for="(indikator, indikatorIndex) in indikators">
                                <div class="modal fade" :id="generateTargetId(indikator)" tabindex="-1" :aria-labelledby="generateTargetId(indikator)+'_label'" aria-hidden="true">
                                    <div class="modal-dialog modal-xl">
                                        <form :action="generateRkpdSubmitUrl(indikator)" method="POST">
                                            @csrf
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" :id="generateTargetId(indikator)+'_label'"> Target RKPD Indikator Program</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>Indikator Sasaran</th>
                                                                    <th>Satuan</th>
                                                                    <template x-for="tahun in periode">
                                                                        <th width="150" class="text-center"><span x-text="`Tahun ${tahun.tahun}`"></span></th>
                                                                    </template>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td x-text="indikator.indikator"></td>
                                                                    <td>
                                                                        <select class="form-control satuan" name="satuan" :id="generateTargetId(indikator)+'_satuan'">
                                                                            <template x-for="satuanItem in satuan">
                                                                                <option :value="satuanItem.id" x-text="satuanItem.name" :selected="indikator.target_rkpd != null && indikator.target_rkpd.id_satuan == satuanItem.id"></option>
                                                                            </template>
                                                                        </select>
                                                                    </td>
                                                                    <template x-for="(tahun, tahunIndex) in periode">
                                                                        <td>
                                                                            <input type="hidden"
                                                                                :name="`target[${tahunIndex}][id_tahun]`"
                                                                                :value="tahun.id"/>
                                                                            <input
                                                                                type="text"
                                                                                :value="getRkpdTargetForSpecificYear(indikator, tahun)"
                                                                                :name="`target[${tahunIndex}][value]`"
                                                                                class="form-control"/>
                                                                        </td>
                                                                    </template>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </template>
                        </div>
                        <script>
                            document.addEventListener('alpine:init', () => {
                                Alpine.data("{{$elementId}}", () => {
                                    return {
                                        indikators: @json($programItem->indikatorProgram),
                                        satuan: @json($satuan),
                                        periode: @json(periode()->periodePerTahun),
                                        generateTargetId(indikator) {
                                            return `target_${indikator.id}`;
                                        },
                                        getIndikatorsTargetForSpecificYear(indikator, tahun) {
                                            const data = indikator.target_program.find(target => target.id_periode_per_tahun == tahun.id)
                                            return data ? data.target : null
                                        },
                                        getRkpdTargetForSpecificYear(indikator, tahun) {
                                            const data = indikator.target_rkpd
                                                ? indikator.target_rkpd.target.find(target => target.id_periode_per_tahun == tahun.id)
                                                : null

                                            return data ? data.nilai : null
                                        },
                                        generateRkpdSubmitUrl(indikator) {
                                            return `{{url('/rpjmd/program/indikator-program/')}}/${indikator.id}/target-rpjmd`;
                                        },
                                        tambahIndikator() {
                                            this.indikators.push({
                                                id: null,
                                                indikator: '',
                                                id_satuan: null,
                                                awal: null,
                                                akhir: null,
                                                target_rkpd: null,
                                                target_program: []
                                            });
                                            this.$nextTick(() => {
                                                selectPlaceholder('.satuan', 'Satuan')
                                            })
                                        },
                                        hapusIndikator(index) {
                                            const confirmed = confirm('Lanjukan menghapus data?');

                                            if(!confirmed) return;

                                            this.indikators.splice(index, 1)
                                        }
                                    }
                                })
                            })
                        </script>
                    @endforeach
                    <div class="form-group mt-4">
                        <button class="btn btn-dark cancel" onclick="cancel('{{ url('') }}/rpjmd/program/penanggung-jawab')" type="button">Cancel</button>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
                {!! $program->links() !!}
            </div>
        @else
        <div class="card-body">
            <span class="text-danger">Tetapkan periode terlebih dahulu!</span><br>
        </div>
        @endif
    </div>
</div>
@endsection

@push('scripts')
    <script>
        selectPlaceholder('.satuan', 'Satuan')
    </script>
@endpush
