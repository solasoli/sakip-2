@extends('layouts.app')
@section('title')
    Penanggung Jawab
@endsection
@section('content')
<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Penanggung Jawab</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">RPJMD</a>
        <a class="breadcrumb-item" href="#">Program</a>
        <span class="breadcrumb-item" style="color: #000;">Penanggung Jawab</span>
    </nav>
</div>

<style media="screen">
    .info_bar {
        position: absolute;
        top: 50px;
        z-index: 998;
        border-radius: 0;
        width: 100%;
        background-color: rgb(41, 77, 100);
    }

    #card-body-info {
        transition: .3s;
    }

    .relative-info {
        transition: .3s;
    }

</style>

<div class="container-fluid pb-2 mt-4">
    <div class="card">
        @if (!is_null(periode()))
            <div class="card-body">
                <div class="card-header">
                    <h6 class="card-title">Penanggung Jawab Tingkat Kabupaten Periode {{ periode()->dari }} -
                        {{ periode()->sampai }}</h6>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success flashMessages" role="alert">
                        {{ $message }}
                    </div>
                @endif
                <hr>
                <form action="{{ url('') }}/rpjmd/program/penanggung-jawab/create" method="POST">
                    @csrf
                    @foreach ($data as $idx => $item)
                        @if (!is_null($item))
                        <div class="card justify-content-center mt-2">
                            <div class="card-header bg-primary text-light"
                                style="position: relative; z-index: 999;">
                                <span class="location-sasaran d-flex font-weight-bold" style="align-items:center">
                                    <a href="#" class="info_btn"><i
                                            class="fa fa-info-circle"></i></a>&emsp;
                                    <i class="fa fa-angle-double-right"></i> &nbsp; Program :
                                    {{ $item->mstProgram->name }} &nbsp;
                                </span>
                            </div>

                            <!-- hierarki -->
                            <div class="alert show-hidden info_bar"
                                style="display: flex; flex-direction: column" role="alert">
                                <span class="hierarki_m">~ / Misi &nbsp; <i class="fa fa-caret-right"></i>&emsp;{{ $item->sasaran->tujuan->misi->misi }}</span>
                                <span class="hierarki_m">~ / Tujuan &nbsp; <i class="fa fa-caret-right"></i>&emsp;{{ $item->sasaran->tujuan->name }}</span>
                                <span class="hierarki_m">~ / Sasaran &nbsp; <i class="fa fa-caret-right"></i>&emsp;{{ $item->sasaran->name }}</span>
                                <span class="hierarki_m">~ / Prioritas Pembangunan &nbsp;
                                    <i class="fa fa-caret-right"></i>&emsp;
                                    @if(!is_null($item->sasaran->prioritasPembangunan->mstPrioritasPembangunan ?? null))
                                        {{ $item->sasaran->prioritasPembangunan->mstPrioritasPembangunan->name }}
                                    @else
                                        <span class="text-warning">Prioritas Pembangunan belum ditetapkan! &nbsp; klik
                                        <a href="{{ url('') }}/rpjmd/prioritas-pembangunan" class="text-info">disini</a> untuk menetapkan</span>
                                    @endif
                                </span>
                                <span class="hierarki_m">~ / Program &nbsp; <i class="fa fa-caret-right"></i>&emsp;{{ $item->mstProgram->name }}</span>
                            </div>

                            <div class="card-body">
                                <div class="form-group clone-wrapper">
                                    <input type="number" name="program{{ $idx }}[]" value="{{ $item->id }}" hidden>

                                    @php $penanggung_jawab = $rpjmd_penanggung_jawab->where('id_rpjmd_program', $item->id) @endphp
                                    @if(!is_null($penanggung_jawab->first()))
                                    <div class="duplicate">
                                        <div class="input-wrapper d-flex">
                                            <select name="opd{{ $idx }}[]" id="opd" class="form-control opd">
                                                <option value=""></option>
                                                @foreach ($opd as $item)
                                                <option value="{{ $item->id }}" {{ $item->id == $penanggung_jawab->first()->id_mst_opd ? "selected" : "" }}>{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    @foreach ($penanggung_jawab->skip(1) as $val)
                                    <div class="duplicate mt-2">
                                        <div class="input-wrapper d-flex">
                                            <select name="opd{{ $idx }}[]" id="opd" class="form-control opd">
                                                <option value=""></option>
                                                @foreach ($opd as $item)
                                                    <option value="{{ $item->id }}" {{ $item->id == $val->id_mst_opd ? "selected" : "" }}>{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                            <button type="button" class="btn btn-danger btn-sm btn-close ml-2"><i class="fa fa-close"></i></button>
                                        </div>
                                    </div>
                                    @endforeach

                                    @else
                                    <div class="duplicate">
                                        <div class="input-wrapper d-flex">
                                            <select name="opd{{ $idx }}[]" id="opd" class="form-control opd">
                                                <option value=""></option>
                                                @foreach ($opd as $item)
                                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endif

                                </div>
                                <button type="button" class="btn btn-primary btn-sm btn_clone mt-2">
                                    <i class="fa fa-plus"></i> Tambah poin
                                </button>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    <div class="form-group mt-4">
                        <button class="btn btn-dark cancel" onclick="cancel('{{ url('') }}/rpjmd/program/penanggung-jawab')" type="button">Cancel</button>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        @else
        <div class="card-body">
            <span class="text-danger">Tetapkan periode terlebih dahulu!</span><br>
        </div>
        @endif
    </div>
</div>
@endsection

@section('scripts')
<script>
    selectPlaceholder('.opd', 'Pilih Penanggung Jawab');

    // create and get hierarki
    function getInfoHierarki(btn) {
        let info = document.querySelectorAll(btn);
        info.forEach(res => {
            res.addEventListener('click', () => {
                const card = res.closest('.card')
                const card_header = card.querySelector('.card-header')
                const info_bar = card_header.nextElementSibling
                const card_body = card.querySelector('.card-body')
                const height_info_bar = info_bar.offsetHeight

                info_bar.classList.toggle('show-hidden')
                card_body.style.transition = '.3s';
                if (!info_bar.classList.contains('show-hidden')) {
                    card_body.style.marginTop = `${height_info_bar - 10}px`;
                } else {
                    card_body.style.marginTop = `0`;
                }
            });
        });
    }
    getInfoHierarki('.info_btn');

    function clone() {
        const btn = document.querySelectorAll('.btn_clone');
        btn.forEach(res => {
            res.addEventListener('click', function() {
                const parent = this.closest('.card-body');
                const el_clone = parent.querySelector('.duplicate');
                const clone = el_clone.cloneNode(true);
                const place_clone = parent.querySelector('.clone-wrapper');
                const fix_el = cleanElClone(clone);

                place_clone.append(fix_el);
                selectPlaceholder('.sumber-anggaran-cloned', 'Pilih Penanggung Jawab');
            });
        })
    }
    clone();

    const cleanElClone = function(el) {
        // pembentukan variable
        const wrapper = el.querySelector('.input-wrapper');
        const select = el.querySelector('#opd');
        const btn = document.createElement('btn');
        const i = document.createElement('i');
        const span = el.querySelectorAll('span.select2');
        // remove class and element
        select.classList.remove('.sumber-anggaran');
        span.forEach(res => res.remove());
        // add class in any element
        el.classList.add('mt-2');
        i.classList.add('fa', 'fa-close');
        btn.classList.add('btn', 'btn-danger', 'btn-sm', 'ml-2', 'btn_close');
        select.classList.add('sumber-anggaran-cloned');
        // add style in btn
        btn.style.display = 'flex';
        btn.style.alignItems = 'center';
        //append element
        btn.append(i);
        wrapper.append(btn);

        return el;
    }

    const closeElClone = function() {
        document.addEventListener('click', function(e) {
            let classes = e.target.classList;
            if(classes.contains('btn_close') || classes.contains('fa-close')) {
                e.target.closest('.duplicate').remove();
            }
        });
    }
    closeElClone();
</script>
@endsection
