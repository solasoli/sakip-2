@extends('layouts.app')
@section('title')
    Form Edit Tujuan
@endsection
@section('content')
<style media="screen">
    .modal-lg{
      width: 750px !important;
    }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Tujuan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">RPJMD</a>
        <span class="breadcrumb-item" style="color: #000;">Tujuan</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card ">
        <div class="card-header">
            <h6 class="card-title">Form Tambah Tujuan Tingkat Kabupaten Periode {{ periode()->dari }} - {{ periode()->sampai }}</h6>
        </div>
        <div class="card-body scrollmenu">
            <form action="{{ url('') }}/tujuan/{{ $tujuan->first()->id }}/update" method="POST" class="edit_rpjmd_tujuan">
                @csrf
                @method('patch')
                <div class="input-group row py-2">
                    <div class="col-sm-4">
                        <label for="misi">Misi Kabupaten * : </label> <br>
                        <select name="misi" id="misi" class="select2">
                            @foreach ($misi as $item)
                            <option value="{{ $item->id }}" {{ $item->id == $tujuan->first()->id_misi ? "selected" : "" }}>{{ $item->misi }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="input-group row py-2">
                    <div class="col-sm-4">
                        <label for="tujuan">Tujuan Kabupaten * : </label>
                        <div class="input-group input-group-outline">
                            <input type="text" id="tujuan" class="form-control" name="tujuan" value="{{ $tujuan->first()->name }}">
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="row place-duplicate">
                        @foreach ($indikator as $key => $itemIndikator)
                        <div class="duplicate" style="display: flex; align-items: center;">
                            <div class="input-group p-2">
                                <label for="indikator">Indikator Tujuan * : </label>
                                <div class="input-group input-group-outline">
                                    <textarea cols="50" id="indikator" class="form-control" name="indikator[]" value="{{ $itemIndikator->name }}">{{ $itemIndikator->name }}</textarea>
                                </div>
                            </div>
                            <div class="input-group ml-3">
                                <label for="indikator">Satuan * : </label><br>
                                <div class="input-group input-group-outline">
                                    <select name="satuan[]" id="satuan" class="form-control satuan"> 
                                        @foreach ($satuan as $item)
                                        <option value="{{ $item->id }}" {{ $item->id == $itemIndikator->id_satuan ? "selected" : "" }}>{{ $item->name }}</option>
                                        @endforeach
                                    </select><br>
                                </div>
                            </div>
                            <div class="input-group px-3">
                                <label for="awal">Kondisi Awal * : </label>
                                <div class="input-group input-group-outline">
                                    <input type="text" id="awal" class="form-control" name="awal[]" value="{{ $itemIndikator->awal }}">
                                </div>
                            </div>
                            @foreach($itemIndikator->target as $i => $target)
                            <div class="input-group ml-3" style="width: 120px">
                                <label for="indikator">Target {{ $years[$i]->tahun }}* : </label>
                                <div class="input-group input-group-outline">
                                    <input type="text" id="target{{ $loop->iteration }}" class="form-control" name="target[]" value="{{ $target->target }}">
                                </div>
                            </div>
                            @endforeach
                            <div class="input-group px-3">
                                <label for="akhir">Kondisi Akhir * : </label>
                                <div class="input-group input-group-outline">
                                    <input type="text" id="akhir" class="form-control" name="akhir[]" value="{{ $itemIndikator->akhir }}">
                                </div>
                            </div>
                            @if ($key > 0)
                            <div class="input-group">
                                <button class="close-duplicate btn btn-danger btn-sm mt-4 ml-2"><i class="fa fa-close"></i></button>
                            </div>
                            @endif
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="input-group row">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-success btn-sm duplicate-btn"><i class="fa fa-plus"></i> Tambah Indikator</button>
                    </div>
                </div>
                <hr>
                <div class="input-group row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary btn">Simpan</button>
                        <button type="button" onclick="return window.history.back()" class="btn btn-secondary ">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    selectPlaceholder('#misi', 'Pilih misi');
    selectPlaceholder('.satuan', 'Satuan');

    $('.duplicate-btn').click(() => {
        $('.place-duplicate').append(
            `<div class="duplicate" style="display: flex; align-items: center;">
                <div class="input-group p-2">
                    <label for="indikator">Indikator Tujuan * : </label>
                    <div class="input-group input-group-outline">
                        <textarea cols="50" id="indikator" class="form-control" name="indikator[]" value="">{{ old('indikator[]') }}</textarea>
                    </div>
                </div>
                <div class="input-group ml-3">
                    <label for="indikator">Satuan * : </label><br>
                    <div class="input-group input-group-outline">
                        <select name="satuan[]" id="satuan" class="form-control satuan"> 
                            @foreach ($satuan as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select><br>
                    </div>
                </div>
                <div class="input-group px-3">
                    <label for="awal">Kondisi Awal * : </label>
                    <div class="input-group input-group-outline">
                        <input type="text" id="awal" class="form-control" name="awal[]">
                    </div>
                </div>
                @foreach($itemIndikator->target as $i => $target)
                <div class="input-group ml-3" style="width: 120px">
                    <label for="indikator">Target {{ $years[$i]->tahun }}* : </label>
                    <div class="input-group input-group-outline">
                        <input type="text" id="target[]" class="form-control" name="target[]">
                    </div>
                </div>
                @endforeach
                <div class="input-group px-3">
                    <label for="akhir">Kondisi Akhir * : </label>
                    <div class="input-group input-group-outline">
                        <input type="text" id="akhir" class="form-control" name="akhir[]">
                    </div>
                </div>
                <div class="input-group">
                    <button class="close-duplicate btn btn-danger btn-sm mt-4 ml-2"><i class="fa fa-close"></i></button>
                </div>
            </div>`
        )
        selectPlaceholder('.satuan', 'Satuan');
    })
    $(document).on('click', '.close-duplicate', function() {
        $(this).parent().closest('.duplicate').remove();
    })

    const form = '.edit_rpjmd_tujuan';
    inputValidate(form, ['input', 'select']);
</script>
@endsection
