@extends('layouts.app')
@section('title')
    Form Tambah Tujuan
@endsection
@section('content')
<style media="screen">
    .modal-lg{
      width: 750px !important;
    }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Tujuan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">RPJMD</a>
        <span class="breadcrumb-item" style="color: #000;">Tujuan</span>
    </nav>
</div>

<div class="container-fluid pb-2 mt-4">
    <div class="card ">
        <div class="card-header">
            <h6 class="card-title">Form Tambah Tujuan Tingkat Kabupaten Periode {{ $periode }}</h6>
        </div>
        <div class="card-body scrollmenu">
            <form action="{{ url('') }}/tujuan/store" method="POST" class="add_rpjmd_tujuan">
                @csrf
                <div class="row">
                    <div class="col-4">
                        <label for="misi">Misi Kabupaten * : </label> <br>
                        <div class="input-group input-group-outline">
                            <select name="misi" id="misi" class="form-control">
                                <option value=""></option>
                                @foreach ($misi as $item)
                                <option value="{{ $item->id }}" {{ old('misi') == $item->id ? "selected" : "" }}>{{ $item->misi }}</option>
                                @endforeach
                            </select><br>
                        </div>
                        <label for="tujuan">Tujuan Kabupaten * : </label>
                        <div class="input-group input-group-outline">
                            <input type="text" id="tujuan" class="form-control" value="{{ old('tujuan') }}" name="tujuan">
                            @error('tujuan')
                            <span class="text-danger">
                                <small>Tujuan tidak boleh kosong!</small> 
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <div class="row place-duplicate">
                        <div class="duplicate p-3" style="display: flex; align-items: center;">
                            <div class="input-group p-3">
                                <label for="indikator">Indikator Tujuan * : </label>
                                <div class="input-group input-group-outline">    
                                    <textarea id="indikator" value="{{ old('indikator[]') }}" class="form-control" name="indikator[]"></textarea>
                                </div>
                                @error('indikator.*')
                                <span class="text-danger">
                                    <small>Indikator tidak boleh kosong!</small> 
                                </span>
                                @enderror
                            </div>
                            <div class="input-group input-group-outline ml-3">
                                <label for="indikator">Satuan * : </label><br>
                                <select name="satuan[]" id="satuan" class="form-control">
                                    <option value=""></option>
                                    @foreach ($satuan as $item)
                                    <option value="{{ $item->id }}" {{ old('satuan.0') == $item->id ? "selected" : "" }}>{{ $item->name }}</option>
                                    @endforeach
                                </select><br>
                                @error('satuan.*')
                                <span class="text-danger">
                                    <small>tidak boleh kosong!</small> 
                                </span>
                                @enderror
                            </div>

                            <!-- kondisi awal -->
                            <div class="input-group input-group-outline ml-3" style="width: 120px">
                                <label for="awal">Kondisi Awal * : </label>
                                <input type="text" class="form-control" name="awal[]" id="awal">
                            </div>

                            @foreach($years as $item)
                            <div class="input-group input-group-outline ml-3" style="width: 120px">
                                <label for="target">Target {{ $item->tahun }} * : </label>
                                <input type="text" class="form-control" name="target[]" id="target{{ $loop->iteration }}">
                            </div>
                            @endforeach

                            <!-- kondisi akhir -->
                            <div class="input-group input-group-outline ml-3" style="width: 120px">
                                <label for="akhir">Kondisi Akhir * : </label>
                                <input type="text" class="form-control" name="akhir[]" id="akhir">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="input-group input-group-outline row">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-success btn-sm duplicate-btn"><i class="fa fa-plus"></i> Tambah Indikator</button>
                    </div>
                </div>
                <hr>
                <div class="input-group input-group-outline row">
                    <div class="col-sm-12">
                        <button type="submit" class="btn btn-primary btn-sm ">Submit</button>
                        <button type="button" onclick="return window.history.back()" class="btn btn-secondary btn-sm ">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function() {
        selectPlaceholder('#misi', 'Pilih misi');
        selectPlaceholder('#satuan', 'Satuan')
        $('.duplicate-btn').click(() => {
            $('.place-duplicate').append(
                `<div class="duplicate" style="display: flex; align-items: center;">
                    <div class="input-group px-3">
                        <label for="indikator">Indikator Tujuan * : </label>
                        <div class="input-group input-group-outline">
                            <textarea cols="50" id="indikator" class="form-control" name="indikator[]"></textarea>
                        </div>
                    </div>
                    <div class="input-group input-group-outline ml-3">
                        <label for="indikator">Satuan * : </label><br>
                        <select name="satuan[]" id="satuan" class="form-control satuan">
                            <option value=""></option>
                            @foreach ($satuan as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <!-- kondisi awal -->
                    <div class="input-group input-group-outline ml-3" style="width: 120px">
                        <label for="indikator">Kondisi Awal * : </label>
                        <input type="text" class="form-control" name="awal[]">
                    </div>
                    @foreach($years as $item)
                    <div class="input-group input-group-outline ml-3" style="width: 120px">
                        <label for="indikator">Target {{ $item->tahun }} * : </label>
                        <input type="text" name="target[]" class="form-control">
                    </div>
                    @endforeach
                    <!-- kondisi akhir -->
                    <div class="input-group input-group-outline ml-3" style="width: 120px">
                        <label for="indikator">Kondisi Akhir * : </label>
                        <input type="text" class="form-control" name="akhir[]">
                    </div>
                    <div class="input-group input-group-outline">
                        <button class="close-duplicate btn btn-danger btn-sm mt-4 ml-2"><i class="fa fa-close"></i></button>
                    </div>
                </div>`
            );
            selectPlaceholder('.satuan', 'Satuan');
        })
        $(document).on('click', '.close-duplicate', function() {
            $(this).parent().closest('.duplicate').remove();
        })
    });

    const form = '.add_rpjmd_tujuan';
    inputValidate(form, ['input', 'select']);
</script>
@endsection