@extends('layouts.app')
@section('title')
    Tujuan
@endsection
@section('content')
<style media="screen">
    .modal-lg{
      width: 750px !important;
    }
</style>

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Tujuan</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Dashboard</a>
        <a class="breadcrumb-item" href="#">RPJMD</a>
        <span class="breadcrumb-item" style="color: #000;">Tujuan</span>
    </nav>
</div>


<div class="container-fluid pb-2 mt-4">
    <div class="card">
        @include('partials.error-message')
        @include('partials.message')

        @if(!is_null(periode()))
        <div class="card-body">
            <div class="card-header">
                <h6 class="card-title">Data Tujuan Tingkat Kabupaten Periode {{ periode()->dari }} - {{ periode()->sampai }}</h6>
                <a href="{{ url('') }}/tujuan/add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
            </div>
            <hr>
            {{--  card misi  --}}
            @foreach ($misi as $idxMisi => $itemMisi)
            @if(count($tujuan->where('id_misi', $itemMisi->id)) > 0)
            <div class="card justify-content-center mt-2">
                <div class="card-header bg-primary text-light">
                    <span>Misi {{ $idxMisi + 1 }} * : {{ $itemMisi->misi }}</span>
                </div>
                <div class="card-body text-center">

                    {{--  card tujuan  --}}
                    @php $sub = 1 @endphp
                    @foreach ($tujuan->where('id_misi', $itemMisi->id) as $itemTujuan)

                    <div class="card justify-content-center mt-3">
                        <div class="card-header bg-light">
                            <span>Tujuan {{ $idxMisi + 1 }}.{{ $sub++ }} * : {{ $itemTujuan->name }}</span>
                            <div class="action">
                                <a href="{{ url('') }}/tujuan/edit/{{ hashID($itemTujuan->id) }}" class="btn btn-warning btn-sm">Ubah</a>
                                <form action="{{ url('') }}/tujuan/{{ $itemTujuan->id }}/delete" method="post" class="d-inline">
                                    @csrf
                                    @method('delete')
                                    <button onclick="return confirm('Lanjutkan menghapus?')" type="submit" class="btn btn-danger btn-sm">Hapus</button>
                                </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-responsive tableDataTujuan" width="100%" id="dtHorizontalExample">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Indikator Tujuan</th>
                                            <th>Satuan</th>
                                            <th>Kondisi Awal</th>

                                            @foreach ($periodePerTahun as $itemPeriodePerTahun)
                                                @php $tahun = $itemPeriodePerTahun->periodePerTahun @endphp
                                                @foreach ($tahun as $valTahun)
                                                <th>Target {{ $valTahun->tahun }}</th>
                                                @endforeach
                                            @endforeach

                                            <th>Kondisi Akhir</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    @php $no = 1 @endphp
                                    @foreach ($indikator->where('id_tujuan', $itemTujuan->id) as $itemIndikator)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $itemIndikator->name }}</td>
                                            <td>{{ $itemIndikator->satuan->name }}</td>
                                            <td>{{ $itemIndikator->awal }}</td>
                                            @php $target = $itemIndikator->target; @endphp

                                            @foreach ($target as $itemTarget)
                                                <td class="text-center">{{ $itemTarget->target }}</td>
                                            @endforeach
                                            <td>{{ $itemIndikator->akhir }}</td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>  
                        </div>
                    </div>

                    @endforeach
                    {{--  end card tujuan  --}}

                </div>
            </div>
            @endif
            @endforeach
            {{--  end card misi  --}}

            <div class="input-group input-group-outline mt-4">
                <span class="d-flex justify-content-center">
                    {{ $misi->links('pagination::bootstrap-4') }}
                </span>
            </div>
        </div>
        @else
        <div class="card-body">
            <h6 class="text-danger">Periode belum ditetapkan!</h6>
            @if(auth()->user()->id == 1)
                <a href="{{ url('/konfigurasi') }}" class="btn btn-primary btn-sm mt-3">Tetapkan Periode</a>
            @endif
        </div>
        @endif
    </div>
</div>
@endsection

@section('scripts')
@endsection
