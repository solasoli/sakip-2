@extends('layouts.app')
@section('title')
    Visi
@endsection
@section('content')
<style media="screen">
    .modal-lg{
      width: 750px !important;
    }
  </style>
  
  <div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
      <span class="brand ml-4">Visi</span>
      <nav class="mr-4">
          <a class="breadcrumb-item" href="/">Dashboard</a>
          <a class="breadcrumb-item" href="#">RPJMD</a>
          <span class="breadcrumb-item" style="color: #000;">Visi</span>
      </nav>
  </div>
  
  <div class="container-fluid pb-2 mt-4">
    @if ($message = Session::get('successAdd'))
        <div class="alert alert-success flashMessages" role="alert">
            {{ $message }}
        </div>
    @elseif ($message = Session::get('not_periode'))
        <div class="alert alert-danger flashMessages" role="alert">
            {{ $message }}
        </div>
    @endif
    <style>
        .ck-editor__editable_inline {
            min-height: 400px;
        }
    </style>
        
    <div class="card">
        <div class="card-body">
            <h6 class="card-title">Form Tambah Visi</h6>
            <hr>
            <div class="row justify-content-center">
                <div class="col-sm-12">
                    <form action="{{ url('') }}/visi/add" method="POST">
                        @csrf
                        <div class="input-group input-group-outline row">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                Visi <span class="required">*</span> :
                                </label>
                                <div class="col-sm-7 col-md-7 col-lg-7 col-xs-12">
                                    <textarea name="visi" id="visi" class="ckeditor" cols="30" rows="30">
                                        @foreach ($data as $item)
                                        {{ !is_null($item->visi) ? $item->visi : '' }}
                                        @endforeach
                                    </textarea>
                                </div>
                        </div>
                        <div class="input-group input-group-outline row">
                            <label for="penjabaran" class="control-label col-md-3 col-sm-3 col-xs-12">
                                Penjabaran Visi <span class="required">*</span> :
                            </label>
                            <div class="col-sm-7 col-md-7 col-lg-7 col-xs-12">
                                <textarea name="penjabaran" id="penjabaran" style="height: 50px" class="ckeditor" cols="30" rows="20">
                                    @foreach($data as $item)
                                    {{ !is_null($item->penjabaran) ? $item->penjabaran : '' }}
                                    @endforeach
                                </textarea>
                            </div>
                        </div>
                        <hr>
                        <div class="input-group input-group-outline row justify-content-end">
                            <div class="col-sm-12">
                                <a href="" class="btn btn-secondary btn-sm">Cancel</a>
                                <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection

@section('scripts')
    <script src="{{ asset('/vendor/ckeditor/ckeditor.js') }}"></script>
    <script>
        $(function() {
            $('#cancel').click(() => {
                window.history.back();
            })
        })
    </script>
@endsection