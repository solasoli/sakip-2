@extends('layouts.app')
@section('title')
    Ganti Password
@endsection
@section('content')

<div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
    <span class="brand ml-4">Administrator</span>
    <nav class="mr-4">
        <a class="breadcrumb-item" href="/">Profile</a>
        <span class="breadcrumb-item" style="color: #000;">Ganti Password</span>
    </nav>
</div>
<div class="container-fluid pb-2 mt-4">
    <div class="card shadow-base pb-3">
        <div class="card-header">
            <h6>Ganti Password</h6>
            <a href="{{ route('users.create') }}">
                <button class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Tambah</button>
            </a>
        </div>
        <form action="{{ route('profile.change_password.update') }}" method="POST">
            <div class="card-body">
                @include('partials.error-message')
                @include('partials.message')
                @csrf
                <div class="input-group input-group-outline">
                    <div class="col-4">
                        <label for="old_password">Password Lama</label>
                        <input
                            type="password"
                            id="old_password"
                            class="form-control"
                            name="old_password">
                    </div>
                </div>
                <div class="input-group input-group-outline">
                    <div class="col-4">
                        <label for="password">Password baru</label>
                        <input
                            type="password"
                            id="password"
                            class="form-control"
                            name="password">
                    </div>
                </div>
                <div class="input-group input-group-outline">
                    <div class="col-4">
                        <label for="password_confirmation">Konfirmasi Password baru</label>
                        <input
                            type="password"
                            id="password_confirmation"
                            class="form-control"
                            name="password_confirmation">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button class="btn btn-dark cancel" onclick="return window.history.back()" type="button">Cancel</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>
        </form>

    </div>
</div>
@endsection
