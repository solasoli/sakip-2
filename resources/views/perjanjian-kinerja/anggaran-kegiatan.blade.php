@extends('layouts.app')
@section('title')
    Penanggung Jawab
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Anggaran Kegiatan PK",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "PK PD", "href" => "#"],
            [ "label" => "Anggaran Kegiatan PK", "href" => "#"],
        ]
    ])
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection', [
            'redirectUrl' => route('perjanjian_kinerja.anggaran_kegiatan.index')
        ])
        @include('partials.error-message')
        @include('partials.message')
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-header">
                    <h6 class="card-title">Data Anggaran PK Kegiatan PD</h6>
                </div>
                @foreach ($programs as $program)
                    <x-hierarchy-card :title='"Program PD : $program->name"' :hierarchy="[
                        'Misi Kota' => $program->getMisi()->misi,
                        'Tujuan Kota' => $program->getRpjmdTujuan()->name,
                        'Sasaran Kota' => $program->getRpjmdSasaran()->name,
                        'Prioritas Pembangunan' => $program->getPrioritasPembangunan()->name,
                        'Perangkat Daerah' => $program->getRenstraTujuan()->name,
                    ]">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Kegiatan</th>
                                    <th>Anggaran Renstra</th>
                                    <th>Anggaran Renja</th>
                                    <th>Anggaran PK</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($program->kegiatanRenstra as $key => $kegiatan)
                                    @php
                                        $renstraKey = "renstra_$kegiatan->id";
                                        $renjaKey = "renja_$kegiatan->id";
                                        $anggaranKey = "anggaran_$kegiatan->id";
                                    @endphp
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$kegiatan->name}}</td>
                                        <td>
                                            <a
                                                href="#"
                                                data-toggle="modal"
                                                data-target="#{{$renstraKey}}">Selengkapnya
                                            </a>
                                        </td>
                                        <td>
                                            <a
                                                href="#"
                                                data-toggle="modal"
                                                data-target="#{{$renjaKey}}">Selengkapnya
                                            </a>
                                        </td>
                                        <td>
                                            <button
                                                data-toggle="modal"
                                                data-target="#{{$anggaranKey}}"
                                                class="btn btn-success btn-sm duplicate-btn">
                                                + Tambah Target
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @foreach ($program->kegiatanRenstra as $key => $kegiatan)
                            @php
                                $renstraKey = "renstra_$kegiatan->id";
                                $renjaKey = "renja_$kegiatan->id";
                                $anggaranKey = "anggaran_$kegiatan->id";
                                $tahunHeads = $periode->periodePerTahun->map(function($tahun) {
                                    return 'Tahun '.$tahun->tahun;
                                });
                                $renstraBody = $periode->periodePerTahun->map(function($tahun) use($kegiatan){
                                    return $kegiatan->anggaranRenstra->where('id_periode_per_tahun', $tahun->id)->first()->nilai ?? '-';
                                });
                                $renjaBody = $periode->periodePerTahun->map(function($tahun) use($kegiatan){
                                    return $kegiatan->anggaranRenja->where('id_periode_per_tahun', $tahun->id)->first()->nilai ?? '-';
                                });
                            @endphp
                            @include('partials.modal-table', [
                                "title" => "Anggaran Renstra Kegiatan PD",
                                "modalId" => $renstraKey,
                                "tableHeads" => [
                                    "Program",
                                    ...$tahunHeads,
                                ],
                                "tableData" => [
                                    [
                                        $kegiatan->name,
                                        ...$renstraBody
                                    ]
                                ]
                            ])
                            @include('partials.modal-table', [
                                "title" => "Anggaran Renja Kegiatan PD",
                                "modalId" => $renjaKey,
                                "tableHeads" => [
                                    "Kegiatan",
                                    ...$tahunHeads,
                                ],
                                "tableData" => [
                                    [
                                        $kegiatan->name,
                                        ...$renjaBody
                                    ]
                                ]
                            ])
                            @include('perjanjian-kinerja.partials.modal-anggaran-pk', [
                                'modalId' => $anggaranKey,
                                'periode' => $periode,
                                'submitUrl' => route('perjanjian_kinerja.anggaran_kegiatan.store', ['kegiatan' => $kegiatan->id]),
                                'label' => 'Anggaran PK Kegiatan PD',
                                'dataName' => $kegiatan->name,
                                'dataAnggaran' => $kegiatan->anggaranPk
                            ])
                        @endforeach
                    </x-hierarchy-card>
                @endforeach
            </div>
            {!! $programs->links() !!}
        </div>
    </div>
@endsection
