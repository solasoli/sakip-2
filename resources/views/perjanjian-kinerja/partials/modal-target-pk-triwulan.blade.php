<div class="modal fade" id="{{$modalId}}" tabindex="-1" aria-labelledby="{{$modalId}}_label" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <form action="{{$submitUrl}}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="{{$modalId}}_label">{{$headline}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Indikator Sasaran</th>
                                    <th>Satuan</th>
                                    @foreach ($tahunPeriode as $tahun)
                                        <th>Tahun {{$tahun->tahun}}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$namaIndikator}}</td>
                                    <td>
                                        <select class="form-control" name="satuan">
                                            @foreach ($satuan as $item)
                                                <option value="{{$item->id}}" {{$item->id == ($pkRenja->id_satuan ?? null) ? 'selected' : null}}>
                                                    {{$item->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </td>
                                    @foreach ($tahunPeriode as $tahun)
                                        @php
                                            $targetRenja = $pkRenja ? $pkRenja->targetRenja->where('id_periode_per_tahun', $tahun->id)->first() : null;
                                        @endphp
                                        <input type="hidden"
                                            name="renja[{{ $loop->index }}][id_tahun]"
                                            value="{{ $tahun->id }}">
                                        <td>
                                            <input
                                                type="text"
                                                value="{{$targetRenja != null ? $targetRenja->nilai : null}}"
                                                name="renja[{{ $loop->index }}][value]"
                                                class="form-control"/>
                                        </td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
