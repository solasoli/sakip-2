<div class="modal fade" id="{{ $modalId }}" tabindex="-1" aria-labelledby="{{ $modalId }}_label"
    aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <form action="{{ $submitUrl }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="{{ $modalId }}_label">{{$label}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th rowspan="2">Program</th>
                                    <th colspan="{{ $periode->periodePerTahun->count() }}">Anggaran PK</th>
                                </tr>
                                <tr>
                                    @foreach ($periode->periodePerTahun as $tahun)
                                        <th>{{ $tahun->tahun }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="text-overflow: ellipsis;" class="break-all max-w-md overflow-hidden">
                                        {{ $dataName }}
                                    </td>
                                    @foreach ($periode->periodePerTahun as $tahun)
                                        @php $anggaran = $dataAnggaran->firstWhere('id_periode_per_tahun', $tahun->id); @endphp
                                        <input type="hidden" name="anggaran[{{ $loop->index }}][id_tahun]"
                                            value="{{ $tahun->id }}">
                                        <td class="text-center">
                                            <input type="text" class="form-control"
                                                name="anggaran[{{ $loop->index }}][value]"
                                                value="{{ $anggaran != null ? $anggaran->nilai : null }}">
                                        </td>
                                    @endforeach
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
