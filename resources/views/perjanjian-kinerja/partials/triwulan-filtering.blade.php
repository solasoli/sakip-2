<div class="row justify-content-center mb-4">
    <div class="col-4">
        <form action="{{$redirectUrl}}" method="GET">
            <div class="form-group">
                <label for="opd" class="d-block">Perangkat Daerah</label>
                <select name="opd" id="opd" class="form-control" required>
                    @foreach ($opds as $opd)
                        <option value="{{ $opd->id }}" {{ $selectedOpd == $opd->id ? "selected" : "" }}>{{ $opd->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="opd" class="d-block">Tahun</label>
                <select name="tahun" id="tahun" class="form-control" required>
                    @foreach ($periode as $tahun)
                        <option {{$selectedTahun == $tahun->id ? 'selected' : null}} value="{{$tahun->id}}">{{$tahun->tahun}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="triwulan" class="d-block">Triwulan</label>
                <select name="triwulan" id="triwulan-select" class="form-control" required>
                    <option {{$selectedTriwulan == "1" ? 'selected' : null}} value="1">Triwulan 1</option>
                    <option {{$selectedTriwulan == "2" ? 'selected' : null}} value="2">Triwulan 2</option>
                    <option {{$selectedTriwulan == "3" ? 'selected' : null}} value="3">Triwulan 3</option>
                    <option {{$selectedTriwulan == "4" ? 'selected' : null}} value="4">Triwulan 4</option>
                </select>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-sm">Submit</button>
            </div>
        </form>
    </div>
</div>

@push('scripts')
<script>
    selectPlaceholder('#opd', 'Pilih PD');
    selectPlaceholder('#triwulan-select', 'Pilih Triwulan');
    selectPlaceholder('#tahun', 'Pilih Tahun');
</script>
@endpush
