@php
    $pkRenjaTahunan = $indikator->pkRenjaTahunan;
    $targetRenja = $pkRenjaTahunan != null
        ? $pkRenjaTahunan->targetPk->where('tahun_id', $tahun->id)->first()
        : null;
@endphp
<div class="modal fade" id="{{$modalId}}" tabindex="-1" aria-labelledby="{{$modalId}}_label" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <form action="{{route('perjanjian_kinerja.pk_renja_tahunan.submit')}}" method="POST">
            @csrf
            <input
                type="hidden"
                value="{{ $tahun->id }}"
                name="tahun_id">
            <input
                type="hidden"
                name="id"
                value="{{ $pkRenjaTahunan ? $pkRenjaTahunan->id : null }}">
            <input
                type="hidden"
                value="{{ get_class($indikator) }}"
                name="pk_type">
            <input
                type="hidden"
                value="{{ $indikator->id }}"
                name="pk_id">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="{{$modalId}}_label">{{$headline}}</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Indikator Sasaran</th>
                                    <th>Satuan</th>
                                    <th>Target Tahun {{$tahun->tahun}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$indikator->name}}</td>
                                    <td>
                                        <div class="input-group input-group-outline">
                                            <select class="form-control" name="satuan_id">
                                                @foreach ($satuan as $item)
                                                    <option
                                                        value="{{$item->id}}"
                                                        {{$item->id == ($targetRenja->satuan_id ?? null) ? 'selected' : null}}>
                                                        {{$item->name}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group">
                                            <input
                                                style="padding-left: 5px"
                                                type="text"
                                                value="{{$targetRenja->target ?? null}}"
                                                name="target"
                                                class="form-control"/>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Penanggung Jawab</th>
                                    <th>Atasan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $indikatorId = "indikator_$indikator->id"."_"."$tahun->id" @endphp
                                <tr x-data="{{ $indikatorId }}">
                                    <td>
                                        <template x-for="(pic, index) in penanggungJawab">
                                            <div class="mb-2 w-100 d-flex">
                                                <select
                                                    class="custom-select"
                                                    :name="`penanggung_jawab[${index}]`"
                                                    x-model="pic"
                                                    @change="(e) => penanggungJawab[index] = e.target.value">>
                                                    <template x-for="user in pegawai">
                                                        <option :selected="user . id == pic" :value="user . id"><span
                                                                x-text="user.jabatan"></span> | <span x-text="user.nama"></span></option>
                                                    </template>
                                                </select>
                                                <button
                                                    type="button"
                                                    class="btn btn-danger btn-sm h-100 ml-1"
                                                    @click="penanggungJawab.splice(index, 1)">
                                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                                </button>
                                            </div>
                                        </template>
                                        <button type="button" class="clone-program btn btn-primary btn-sm"
                                            @click="penanggungJawab.push(null)"><i class="fa fa-plus"></i>
                                            Tambah Penanggung Jawab</button>
                                    </td>
                                    <td colspan="3">
                                        <select
                                            class="custom-select"
                                            name="atasan_id"
                                            x-model="selectedAtasan">
                                            <template x-for="user in atasanOption">
                                                <option :selected="user . id == selectedAtasan" :value="user . id"><span
                                                        x-text="user.jabatan"></span> | <span x-text="user.nama"></span><option>
                                            </template>
                                        </select>
                                    </td>
                                </tr>
                                <script>
                                    @php
                                    $penanggungJawab = $targetRenja ? $targetRenja->penanggungJawab : collect([]);
                                    $selectedPenanggungJawab = $penanggungJawab->count() == 0
                                        ? [null]
                                        : $penanggungJawab->map(fn($pgw) => $pgw->id);
                                    @endphp
                                    document.addEventListener('alpine:init', () => {
                                        Alpine.data("{{ $indikatorId }}", () => {
                                            return {
                                                pegawai: @json($pegawai),
                                                penanggungJawab: @json($selectedPenanggungJawab),
                                                atasanOption: @json($atasanOption),
                                                selectedAtasan: {{ $targetRenja ? $targetRenja->atasan_id : 'null' }}
                                            }
                                        })
                                    })
                                </script>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

<style>
    .form-control {
        border: 1px solid #d2d6da;
    }
</style>
