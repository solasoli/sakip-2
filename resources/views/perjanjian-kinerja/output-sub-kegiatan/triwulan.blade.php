@extends('layouts.app')
@section('title')
    Penanggung Jawab
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Target PK Indikator Kegiatan Triwulan",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "Renstra", "href" => "#"],
            [ "label" => "Program", "href" => "#"],
            [ "label" => "Target PK Indikator Kegiatan Triwulan", "href" => "#"],
        ]
    ])
    <style media="screen">
        .info_bar {
            position: absolute;
            top: 50px;
            z-index: 998;
            border-radius: 0;
            width: 100%;
            background-image: linear-gradient(195deg, #42424a 0%, #191919 100%);
        }

        #card-body-info {
            transition: .3s;
        }

        .relative-info {
            transition: .3s;
        }
    </style>
    <div class="container-fluid pb-2 mt-4">
        @include('perjanjian-kinerja.partials.triwulan-filtering', [
            'redirectUrl' => route('perjanjian_kinerja.output_sub_kegiatan.triwulan.index'),
            'periode' => periode()->periodePerTahun,
            'selectedTriwulan' => $triwulan,
            'selectedTahun' => $selectedTahun->id
        ])
        @include('partials.error-message')
        @include('partials.message')
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-header">
                    <h6 class="card-title">Target PK Indikator Kegiatan Triwulan {{ periode()->dari }} -
                        {{ periode()->sampai }}</h6>
                </div>
                <form action="{{route('perjanjian_kinerja.output_sub_kegiatan.triwulan.submit')}}" method="POST">
                    <input type="hidden" name="triwulan" value="{{$triwulan}}">
                    <input type="hidden" name="periode" value="{{$selectedTahun->id}}">
                    @csrf
                    @foreach ($sasaranSubKegiatan as $sasaran)
                        @php $sasaranIndex = $loop->index @endphp
                        <x-hierarchy-card :title='"Kegiatan PD : $sasaran->name"' :hierarchy="[
                            'Misi Kota' => $sasaran->getMisi()->misi,
                            'Tujuan Kota' => $sasaran->getRpjmdTujuan()->name,
                            'Sasaran Kota' => $sasaran->getRpjmdSasaran()->name,
                            'Prioritas Pembangunan' => $sasaran->getPrioritasPembangunan()->name,
                            'Perangkat Daerah' => $sasaran->getRenstraTujuan()->name,
                        ]">
                            <input type="hidden" name="data[{{$sasaranIndex}}][sasaran_id]" value="{{$sasaran->id}}">
                            <div class="table-responsive">
                                <table class="table table-bordered table-responsive">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Output Sub-Kegiatan</th>
                                            <th>Satuan</th>
                                            <th>IKU</th>
                                            <th>Target PK tahun {{$selectedTahun->tahun}}</th>
                                            <th>Target PK Tahun {{$selectedTahun->tahun}} Triwulan {{$triwulan}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($sasaran->output as $output)
                                            <input type="hidden" name="data[{{$sasaranIndex}}][output][{{$loop->index}}][output_id]" value="{{$output->id}}">
                                            @php
                                                $outputId = "output$output->id";
                                            @endphp
                                            <tr>
                                                <td>{{$loop->index + 1}}</td>
                                                <td>{{$output->name}}</td>
                                                <td>{{$output->satuan->name}}</td>
                                                <td>{!!$output->is_iku ? '<i class="fa fa-check" aria-hidden="true"></i>' : '-'!!}</td>
                                                <td>{{$output->pkRenjaTahunan->targetPk->first()->target}}</td>
                                                <td><input type="text" name="data[{{$sasaranIndex}}][output][{{$loop->index}}][target_triwulan]" class="form-control" value="{{$output->pkRenjaTriwulan->first()->target ?? null}}"></td>
                                            </tr>
                                            <tr x-data="{{$outputId}}">
                                                <td colspan="5">
                                                    <template x-for="(pic, index) in penanggungJawab">
                                                        <div class="mb-2 w-100 d-flex">
                                                            <select class="custom-select" :name="`data[{{$sasaranIndex}}][output][{{$loop->index}}][penanggung_jawab][${index}]`" x-model="pic">
                                                                <template x-for="user in pegawai">
                                                                    <option :selected="user.id == pic" :value="user.id"><span x-text="user.nama"></span></option>
                                                                </template>
                                                            </select>
                                                            <button class="btn btn-danger h-100 ml-1" @click="penanggungJawab.splice(index, 1)"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                        </div>
                                                    </template>
                                                </td>
                                                <td>
                                                    <button type="button"
                                                        class="clone-program btn btn-primary btn-sm" @click="penanggungJawab.push(null)"><i class="fa fa-plus"></i> Tambah Penanggung Jawab</button>
                                                </td>
                                            </tr>
                                            <script>
                                                @php
                                                    $pkRenjaTriwulan = $output->pkRenjaTriwulan->first();
                                                    $selectedPenanggungJawab = $pkRenjaTriwulan ? $pkRenjaTriwulan->penanggungJawab->map(function($pgw) {return $pgw->id;}) : [null];
                                                @endphp
                                                document.addEventListener('alpine:init', () => {
                                                    Alpine.data("{{$outputId}}", () => {
                                                        return {
                                                            pegawai: @json($pegawai),
                                                            penanggungJawab: @json($selectedPenanggungJawab),
                                                        }
                                                    })
                                                })
                                            </script>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </x-hierarchy-card>
                    @endforeach
                    <div class="input-group input-group-outline mt-4">
                        <button class="btn btn-dark cancel" onclick="cancel('{{route('perjanjian_kinerja.output_sub_kegiatan.triwulan.index')}}')" type="button">Cancel</button>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
