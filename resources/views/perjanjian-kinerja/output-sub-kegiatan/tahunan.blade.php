@extends('layouts.app')
@section('title')
     Sub Kegiatan
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Target PK  Sub Kegiatan Tahunan",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "Renstra", "href" => "#"],
            [ "label" => "Program", "href" => "#"],
            [ "label" => "Target PK  Sub Kegiatan Tahunan", "href" => "#"],
        ]
    ])
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection', [
            'redirectUrl' => route('perjanjian_kinerja.output_sub_kegiatan.tahunan.index')
        ])
        <div class="card mb-4">
            <div class="card-body">
                @include('partials.error-message')
                @include('partials.message')
                <div class="card-header">
                    <h6 class="card-title">Target PK Sub Kegiatan Tahunan {{ periode()->dari }} -
                        {{ periode()->sampai }}</h6>
                </div>
                @foreach ($sasaranSubKegiatan as $sasaran)
                    <x-hierarchy-card :title='"Kegiatan PD : $sasaran->name"' :hierarchy="[
                        'Misi Kota' => $sasaran->getMisi()->misi,
                        'Tujuan Kota' => $sasaran->getRpjmdTujuan()->name,
                        'Sasaran Kota' => $sasaran->getRpjmdSasaran()->name,
                        'Prioritas Pembangunan' => $sasaran->getPrioritasPembangunan()->name,
                        'Perangkat Daerah' => $sasaran->getRenstraTujuan()->name,
                    ]">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Sub Kegiatan</th>
                                        <th>IKU</th>
                                        <th>Target Renstra</th>
                                        <th>Target Renja</th>
                                        @foreach (periode()->periodePerTahun as $tahun)
                                            <th>Target PK {{ $tahun->tahun }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sasaran->output as $output)
                                        @php
                                            $targetOutputId = "targetOutput$output->id";
                                            $targetOutputRenja = "targetRenja$output->id";
                                        @endphp
                                        <tr>
                                            <td>{{$loop->index + 1}}</td>
                                            <td>{{$output->name}}</td>
                                            <td>{!!$output->is_iku ? '<i class="fa fa-check" aria-hidden="true"></i>' : '-'!!}</td>
                                            <td><a href="#" data-bs-toggle="modal" data-bs-target="#{{$targetOutputId}}">Selengkapnya</a></td>
                                            <td><a href="#" data-bs-toggle="modal" data-bs-target="#{{$targetOutputRenja}}">Selengkapnya</a></td>
                                            @foreach (periode()->periodePerTahun as $tahun)
                                                <td>
                                                    <button
                                                        type="button"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#targetPk_{{ $output->id }}_{{$tahun->tahun}}"
                                                        class="btn btn-success btn-sm duplicate-btn">+ Tambah Target
                                                    </button>
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </x-hierarchy-card>
                @endforeach
            </div>
            @foreach ($sasaranSubKegiatan as $sasaran)
                @foreach ($sasaran->output as $output)
                    @php
                        $targetOutputId = "targetOutput$output->id";
                        $targetOutputRenja = "targetRenja$output->id";
                        $targetPkId = "targetPK$output->id";
                        $tahun = $periode->periodePerTahun->map(function($periode) {return $periode->tahun;});
                        $tahunHeads = $tahun->map(function($tahun){return "Tahun $tahun";});
                    @endphp
                    @include('partials.modal-table', [
                        "title" => "Target Renstra Sub Kegiatan",
                        "modalId" => $targetOutputId,
                        "tableHeads" => [
                            "Output Program",
                            "Satuan",
                            "Kondisi Awal",
                            ...$tahunHeads,
                            "Kondisi Akhir"
                        ],
                        "tableData" => [[
                                $output->name,
                                $output->satuan->name,
                                $output->target_awal,
                                ...$periode->periodePerTahun->map(function($periode) use($output) {
                                    return $output->target->where('id_periode_per_tahun', $periode->id)->first()->target ?? null;
                            }),
                            $output->target_akhir
                        ]]
                    ])
                    @include('partials.modal-table', [
                        "title" => "Target Renstra Sub Kegiatan Renja",
                        "modalId" => $targetOutputRenja,
                        "tableHeads" => [
                            "Output Program",
                            ...$tahunHeads,
                        ],
                        "tableData" => [[
                                $output->name,
                                ...$periode->periodePerTahun->map(function($periode) use($output) {
                                    return $output->targetRenja->where('id_periode_per_tahun', $periode->id)->first()->nilai ?? null;
                            }),
                        ]]
                    ])
                    @foreach (periode()->periodePerTahun as $tahun)
                        @include('perjanjian-kinerja.partials.modal-target-pk-tahunan', [
                            "modalId" => "targetPk_".$output->id."_".$tahun->tahun,
                            "headline" => "Target PK Sub Kegiatan tahun $tahun->tahun",
                            "indikator" => $output,
                            "tahun" => $tahun,
                        ])
                    @endforeach
                @endforeach
            @endforeach
        </div>
        {{$sasaranSubKegiatan->links()}}
    </div>
@endsection
