@extends('layouts.app')
@section('title')
    Penanggung Jawab
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Anggaran Sub Kegiatan PK",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "PK PD", "href" => "#"],
            [ "label" => "Anggaran Sub Kegiatan PK", "href" => "#"],
        ]
    ])
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection', [
            'redirectUrl' => route('perjanjian_kinerja.anggaran_sub_kegiatan.index')
        ])
        @include('partials.error-message')
        @include('partials.message')
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-header">
                    <h6 class="card-title">Data Anggaran PK Sub Kegiatan PD</h6>
                </div>
                @foreach ($kegiatans as $kegiatan)
                    <x-hierarchy-card :title='"Kegiatan PD : $kegiatan->name"' :hierarchy="[
                        'Misi Kota' => $kegiatan->getMisi()->misi,
                        'Tujuan Kota' => $kegiatan->getRpjmdTujuan()->name,
                        'Sasaran Kota' => $kegiatan->getRpjmdSasaran()->name,
                        'Prioritas Pembangunan' => $kegiatan->getPrioritasPembangunan()->name,
                        'Perangkat Daerah' => $kegiatan->getRenstraTujuan()->name,
                    ]">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Sub Kegiatan</th>
                                    <th>Anggaran Renstra</th>
                                    <th>Anggaran Renja</th>
                                    <th>Anggaran PK</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($kegiatan->subKegiatan as $key => $subKegiatan)
                                    @php
                                        $renstraKey = "renstra_$subKegiatan->id";
                                        $renjaKey = "renja_$subKegiatan->id";
                                        $anggaranKey = "anggaran_$subKegiatan->id";
                                    @endphp
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$subKegiatan->name}}</td>
                                        <td>
                                            <a
                                                href="#"
                                                data-toggle="modal"
                                                data-target="#{{$renstraKey}}">Selengkapnya
                                            </a>
                                        </td>
                                        <td>
                                            <a
                                                href="#"
                                                data-toggle="modal"
                                                data-target="#{{$renjaKey}}">Selengkapnya
                                            </a>
                                        </td>
                                        <td>
                                            <button
                                                data-toggle="modal"
                                                data-target="#{{$anggaranKey}}"
                                                class="btn btn-success btn-sm duplicate-btn">
                                                + Tambah Target
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @foreach ($kegiatan->subKegiatan as $key => $subKegiatan)
                            @php
                                $renstraKey = "renstra_$subKegiatan->id";
                                $renjaKey = "renja_$subKegiatan->id";
                                $anggaranKey = "anggaran_$subKegiatan->id";
                                $tahunHeads = $periode->periodePerTahun->map(function($tahun) {
                                    return 'Tahun '.$tahun->tahun;
                                });
                                $renstraBody = $periode->periodePerTahun->map(function($tahun) use($subKegiatan){
                                    return $subKegiatan->anggaranRenstra->where('id_periode_per_tahun', $tahun->id)->first()->nilai ?? '-';
                                });
                                $renjaBody = $periode->periodePerTahun->map(function($tahun) use($subKegiatan){
                                    return $subKegiatan->anggaranRenja->where('id_periode_per_tahun', $tahun->id)->first()->nilai ?? '-';
                                });
                            @endphp
                            @include('partials.modal-table', [
                                "title" => "Anggaran Renstra Kegiatan PD",
                                "modalId" => $renstraKey,
                                "tableHeads" => [
                                    "Program",
                                    ...$tahunHeads,
                                ],
                                "tableData" => [
                                    [
                                        $subKegiatan->name,
                                        ...$renstraBody
                                    ]
                                ]
                            ])
                            @include('partials.modal-table', [
                                "title" => "Anggaran Renja Kegiatan PD",
                                "modalId" => $renjaKey,
                                "tableHeads" => [
                                    "Kegiatan",
                                    ...$tahunHeads,
                                ],
                                "tableData" => [
                                    [
                                        $subKegiatan->name,
                                        ...$renjaBody
                                    ]
                                ]
                            ])
                            @include('perjanjian-kinerja.partials.modal-anggaran-pk', [
                                'modalId' => $anggaranKey,
                                'periode' => $periode,
                                'submitUrl' => route('perjanjian_kinerja.anggaran_sub_kegiatan.store', ['subKegiatan' => $subKegiatan->id]),
                                'label' => 'Anggaran PK Kegiatan PD',
                                'dataName' => $subKegiatan->name,
                                'dataAnggaran' => $subKegiatan->anggaranPk
                            ])
                        @endforeach
                    </x-hierarchy-card>
                @endforeach
            </div>
            {!! $kegiatans->links() !!}
        </div>
    </div>
@endsection
