@extends('layouts.app')
@section('title')
    Penanggung Jawab
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Target PK Indikator Kegiatan Tahunan",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "Renstra", "href" => "#"],
            [ "label" => "Program", "href" => "#"],
            [ "label" => "Target PK Indikator Kegiatan Tahunan", "href" => "#"],
        ]
    ])
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection', [
            'redirectUrl' => route('perjanjian_kinerja.indikator_kegiatan.tahunan.index')
        ])
        <div class="card mb-4">
            <div class="card-body">
                @include('partials.error-message')
                @include('partials.message')
                <div class="card-header">
                    <h6 class="card-title">Target PK Indikator Kegiatan Tahunan {{ periode()->dari }} -
                        {{ periode()->sampai }}</h6>
                </div>
                @foreach ($sasaranKegiatan as $kegiatan)
                    <x-hierarchy-card :title='"Kegiatan PD : $kegiatan->name"' :hierarchy="[
                        'Misi Kota' => $kegiatan->getMisi()->misi,
                        'Tujuan Kota' => $kegiatan->getRpjmdTujuan()->name,
                        'Sasaran Kota' => $kegiatan->getRpjmdSasaran()->name,
                        'Prioritas Pembangunan' => $kegiatan->getPrioritasPembangunan()->name,
                        'Perangkat Daerah' => $kegiatan->getRenstraTujuan()->name,
                    ]">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Indikator Sasaran</th>
                                        <th>IKU</th>
                                        <th>Target Renstra</th>
                                        <th>Target Renja</th>
                                        @foreach (periode()->periodePerTahun as $tahun)
                                            <th>Target PK {{ $tahun->tahun }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($kegiatan->indikator as $indikator)
                                        @php
                                            $targetIndikatorId = "targetIndikator$indikator->id";
                                            $targetIndikatorRenja = "targetRenja$indikator->id";
                                        @endphp
                                        <tr>
                                            <td>{{$loop->index + 1}}</td>
                                            <td>{{$indikator->name}}</td>
                                            <td>{!!$indikator->is_iku ? '<i class="fa fa-check" aria-hidden="true"></i>' : '-'!!}</td>
                                            <td><a href="#" data-bs-toggle="modal" data-bs-target="#{{$targetIndikatorId}}">Selengkapnya</a></td>
                                            <td><a href="#" data-bs-toggle="modal" data-bs-target="#{{$targetIndikatorRenja}}">Selengkapnya</a></td>
                                            @foreach (periode()->periodePerTahun as $tahun)
                                                <td>
                                                    <button
                                                        type="button"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#targetPk_{{ $indikator->id }}_{{$tahun->tahun}}"
                                                        class="btn btn-success btn-sm duplicate-btn">+ Tambah Target
                                                    </button>
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </x-hierarchy-card>
                @endforeach
            </div>
            @foreach ($sasaranKegiatan as $kegiatan)
                @foreach ($kegiatan->indikator as $indikator)
                    @php
                        $targetIndikatorId = "targetIndikator$indikator->id";
                        $targetIndikatorRenja = "targetRenja$indikator->id";
                        $targetPkId = "targetPK$indikator->id";
                        $tahun = $kegiatan->getPeriode()->periodePerTahun->map(function($periode) {return $periode->tahun;});
                        $tahunHeads = $tahun->map(function($tahun){return "Tahun $tahun";});
                    @endphp
                    @include('partials.modal-table', [
                        "title" => "Target Renstra Indikator Program",
                        "modalId" => $targetIndikatorId,
                        "tableHeads" => [
                            "Indikator Program",
                            "Satuan",
                            "Kondisi Awal",
                            ...$tahunHeads,
                            "Kondisi Akhir"
                        ],
                        "tableData" => [[
                                $indikator->name,
                                $indikator->satuan->name,
                                $indikator->target_awal,
                                ...$kegiatan->getPeriode()->periodePerTahun->map(function($periode) use($indikator) {
                                    return $indikator->target->where('id_periode_per_tahun', $periode->id)->first()->target ?? null;
                            }),
                            $indikator->target_akhir
                        ]]
                    ])
                    @include('partials.modal-table', [
                        "title" => "Target Renstra Indikator Kegiatan Renja",
                        "modalId" => $targetIndikatorRenja,
                        "tableHeads" => [
                            "Indikator Program",
                            ...$tahunHeads,
                        ],
                        "tableData" => [[
                                $indikator->name,
                                ...$kegiatan->getPeriode()->periodePerTahun->map(function($periode) use($indikator) {
                                    return $indikator->targetRenja->where('id_periode_per_tahun', $periode->id)->first()->nilai ?? null;
                            }),
                        ]]
                    ])
                    @foreach (periode()->periodePerTahun as $tahun)
                        @include('perjanjian-kinerja.partials.modal-target-pk-tahunan', [
                            "modalId" => "targetPk_".$indikator->id."_".$tahun->tahun,
                            "headline" => "Target PK Indikator Kegiatan tahun $tahun->tahun",
                            "indikator" => $indikator,
                            "tahun" => $tahun,
                        ])
                    @endforeach
                @endforeach
            @endforeach
        </div>
        {{$sasaranKegiatan->links()}}
    </div>
@endsection
