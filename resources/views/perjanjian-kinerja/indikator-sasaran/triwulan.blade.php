@extends('layouts.app')
@section('title')
    Penanggung Jawab
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Target PK Indikator Sasaran Triwulan",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "PK PD", "href" => "#"],
            [ "label" => "Target PK Indikator Sasaran Triwulan", "href" => "#"],
        ]
    ])
    <style media="screen">
        .info_bar {
            position: absolute;
            top: 50px;
            z-index: 998;
            border-radius: 0;
            width: 100%;
            background-image: linear-gradient(195deg, #42424a 0%, #191919 100%);
        }

        #card-body-info {
            transition: .3s;
        }

        .relative-info {
            transition: .3s;
        }

    </style>
    <div class="container-fluid pb-2 mt-4">
        @include('perjanjian-kinerja.partials.triwulan-filtering', [
            'redirectUrl' => route('perjanjian_kinerja.indikator_sasaran.triwulan.index'),
            'periode' => periode()->periodePerTahun,
            'selectedTriwulan' => $triwulan,
            'selectedTahun' => $selectedTahun->id
        ])
        @include('partials.error-message')
        @include('partials.message')
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-header">
                    <h6 class="card-title">Target PK Indikator Sasaran Triwulan {{ periode()->dari }} -
                        {{ periode()->sampai }}</h6>
                </div>
                <form action="{{route('perjanjian_kinerja.indikator_sasaran.triwulan.submit')}}" method="POST">

                <input type="hidden" name="triwulan" value="{{$triwulan}}">
                <input type="hidden" name="periode" value="{{$selectedTahun->id}}">
                @csrf
                @foreach ($sasaranRenstra as $sasaran)
                    @php $sasaranIndex = $loop->index @endphp
                    <x-hierarchy-card :title='"Sasaran Perangkat Daerah : $sasaran->name"' :hierarchy="[
                        'Misi Kota' => $sasaran->getMisi()->misi,
                        'Tujuan Kota' => $sasaran->getRpjmdTujuan()->name,
                        'Sasaran Kota' => $sasaran->getRpjmdSasaran()->name,
                        'Prioritas Pembangunan' => $sasaran->getPrioritasPembangunan()->name,
                        'Perangkat Daerah' => $sasaran->getRenstraTujuan()->name,
                    ]">
                        <input type="hidden" name="data[{{$sasaranIndex}}][sasaran_id]" value="{{$sasaran->id}}">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Indikator Sasaran</th>
                                        <th>Satuan</th>
                                        <th>IKU</th>
                                        <th>Target PK tahun {{$selectedTahun->tahun}}</th>
                                        <th>Target PK Tahun {{$selectedTahun->tahun}} Triwulan {{$triwulan}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sasaran->indikator as $indikator)
                                        <input type="hidden" name="data[{{$sasaranIndex}}][indikator][{{$loop->index}}][indikator_id]" value="{{$indikator->id}}">
                                        @php
                                            $indikatorId = "indikator$indikator->id";
                                        @endphp
                                        <tr>
                                            <td>{{$loop->index + 1}}</td>
                                            <td>{{$indikator->indikator}}</td>
                                            <td>{{$indikator->satuan->name}}</td>
                                            <td>{!!$indikator->is_iku ? '<i class="fa fa-check" aria-hidden="true"></i>' : '-'!!}</td>
                                            <td>{{$indikator->pkRenjaTahunan->targetPk->first()->target}}</td>
                                            <td><input type="text" name="data[{{$sasaranIndex}}][indikator][{{$loop->index}}][target_triwulan]" class="form-control" value="{{$indikator->pkRenjaTriwulan->first()->target ?? null}}"></td>
                                        </tr>
                                        <tr x-data="{{$indikatorId}}">
                                            <td colspan="5">
                                                <template x-for="(pic, index) in penanggungJawab">
                                                    <div class="mb-2 w-100 d-flex">
                                                        <select class="custom-select" :name="`data[{{$sasaranIndex}}][indikator][{{$loop->index}}][penanggung_jawab][${index}]`" x-model="pic">
                                                            <template x-for="user in pegawai">
                                                                <option :selected="user.id == pic" :value="user.id"><span x-text="user.nama"></span></option>
                                                            </template>
                                                        </select>
                                                        <button class="btn btn-danger btn-sm h-100 ml-1" @click="penanggungJawab.splice(index, 1)"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                                    </div>
                                                </template>
                                            </td>
                                            <td>
                                                <button type="button"
                                                    class="clone-program btn btn-primary btn-sm" @click="penanggungJawab.push(null)"><i class="fa fa-plus"></i> Tambah Penanggung Jawab</button>
                                            </td>
                                        </tr>
                                        <script>
                                            @php
                                                $pkRenjaTriwulan = $indikator->pkRenjaTriwulan->first();
                                                $selectedPenanggungJawab = $pkRenjaTriwulan ? $pkRenjaTriwulan->penanggungJawab->map(function($pgw) {return $pgw->id;}) : [null];
                                            @endphp
                                            document.addEventListener('alpine:init', () => {
                                                Alpine.data("{{$indikatorId}}", () => {
                                                    return {
                                                        pegawai: @json($pegawai),
                                                        penanggungJawab: @json($selectedPenanggungJawab),
                                                    }
                                                })
                                            })
                                        </script>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </x-hierarchy-card>
                @endforeach
                <div class="input-group input-group-outline mt-4">
                    <button class="btn btn-dark cancel" onclick="cancel('{{route('perjanjian_kinerja.indikator_sasaran.triwulan.index')}}')" type="button">Cancel</button>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
