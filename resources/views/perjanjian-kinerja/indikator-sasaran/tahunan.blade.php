@extends('layouts.app')
@section('title')
    Penanggung Jawab
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Target PK Indikator Sasaran Tahunan",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "PK PD", "href" => "#"],
            [ "label" => "Target PK Indikator Sasaran Tahunan", "href" => "#"],
        ]
    ])
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection', [
            'redirectUrl' => route('perjanjian_kinerja.indikator_sasaran.tahunan.index')
        ])
        <div class="card mb-4">
            <div class="card-body">
                @include('partials.error-message')
                @include('partials.message');
                <div class="card-header">
                    <h6 class="card-title">Target PK Indikator Sasaran Tahunan {{ periode()->dari }} -
                        {{ periode()->sampai }}</h6>
                </div>
                @foreach ($sasaranRenstra as $sasaran)
                    <x-hierarchy-card :title='"Sasaran Perangkat Daerah : $sasaran->name"' :hierarchy="[
                        'Misi Kota' => $sasaran->getMisi()->misi,
                        'Tujuan Kota' => $sasaran->getRpjmdTujuan()->name,
                        'Sasaran Kota' => $sasaran->getRpjmdSasaran()->name,
                        'Prioritas Pembangunan' => $sasaran->getPrioritasPembangunan()->name,
                        'Perangkat Daerah' => $sasaran->getRenstraTujuan()->name,
                    ]">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Indikator Sasaran</th>
                                        <th>IKU</th>
                                        <th>Target Renstra</th>
                                        <th>Target Renja</th>
                                        @foreach (periode()->periodePerTahun as $tahun)
                                            <th>Target PK {{ $tahun->tahun }}</th>
                                        @endforeach
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($sasaran->indikator as $indikator)
                                        @php
                                            $targetIndikatorId = "targetIndikator$indikator->id";
                                            $targetIndikatorRenja = "targetRenja$indikator->id";
                                            $indikatorIndex = $loop->index;
                                        @endphp
                                        <tr>
                                            <td>{{$indikatorIndex + 1}}</td>
                                            <td>{{$indikator->indikator}}</td>
                                            <td>{!!$indikator->is_iku ? '<i class="fa fa-check" aria-hidden="true"></i>' : '-'!!}</td>
                                            <td><a href="#" data-bs-toggle="modal" data-bs-target="#{{$targetIndikatorId}}">Selengkapnya</a></td>
                                            <td><a href="#" data-bs-toggle="modal" data-bs-target="#{{$targetIndikatorRenja}}">Selengkapnya</a></td>
                                            @foreach (periode()->periodePerTahun as $tahun)
                                                <td>
                                                    <button
                                                        type="button"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#targetPk_{{ $indikator->id }}_{{$tahun->tahun}}"
                                                        class="btn btn-success btn-sm duplicate-btn">+ Tambah Target
                                                    </button>
                                                </td>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </x-hierarchy-card>
                @endforeach
                @foreach ($sasaranRenstra as $sasaran)
                    @foreach ($sasaran->indikator as $indikator)
                            @php
                                $targetIndikatorId = "targetIndikator$indikator->id";
                                $targetIndikatorRenja = "targetRenja$indikator->id";
                                $tahun = $sasaran->getPeriode()->periodePerTahun->map(function($periode) {return $periode->tahun;});
                                $tahunHeads = $tahun->map(function($tahun){return "Tahun $tahun";})
                            @endphp
                            @include('partials.modal-table', [
                                "title" => "Target PK Indikator Sasaran",
                                "modalId" => $targetIndikatorId,
                                "tableHeads" => [
                                    "Indikator Sasaran",
                                    "Satuan",
                                    "Kondisi Awal",
                                    ...$tahunHeads,
                                    "Kondisi Akhir"
                                ],
                                "tableData" => [[
                                     $indikator->indikator,
                                     $indikator->satuan->name,
                                     $indikator->awal,
                                     ...$sasaran->getPeriode()->periodePerTahun->map(function($periode) use($indikator) {
                                         return $indikator->target->where('id_periode_per_tahun', $periode->id)->first()->target ?? null;
                                    }),
                                    $indikator->akhir
                                ]]
                            ])
                            @include('partials.modal-table', [
                                "title" => "Target Renja Indikator Sasaran Renstra",
                                "modalId" => $targetIndikatorRenja,
                                "tableHeads" => [
                                    "Indikator Sasaran",
                                    ...$tahunHeads,
                                ],
                                "tableData" => [[
                                     $indikator->indikator,
                                     ...$sasaran->getPeriode()->periodePerTahun->map(function($periode) use($indikator) {
                                         return $indikator->targetRenja->where('id_periode_per_tahun', $periode->id)->first()->nilai ?? null;
                                    }),
                                ]]
                            ])
                            @foreach (periode()->periodePerTahun as $tahun)
                                @include('perjanjian-kinerja.partials.modal-target-pk-tahunan', [
                                    "modalId" => "targetPk_".$indikator->id."_".$tahun->tahun,
                                    "headline" => "Target PK Indikator Sasaran tahun $tahun->tahun",
                                    "indikator" => $indikator,
                                    "tahun" => $tahun,
                                ])
                            @endforeach
                        @endforeach
                @endforeach
            </div>
        </div>
    </div>
@endsection
