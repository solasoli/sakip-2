<div class="modal fade" id="{{$modalId}}" tabindex="-1" aria-labelledby="target-renstraLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="target-renstraLabel">Target Renstra Indikator Sasaran Renja</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Indikator Sasaran</th>
                                <th>Satuan</th>
                                <th>Kondisi Awal</th>
                                @foreach ($sasaran->getPeriode()->periodePerTahun as $periode)
                                    <th>Target {{$periode->tahun}}</th>
                                @endforeach
                                <th>Kondisi Akhir</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{$indikator->indikator}}</td>
                                <td>{{$indikator->satuan->name}}</td>
                                <td>{{$indikator->awal}}</td>
                                @foreach ($sasaran->getPeriode()->periodePerTahun as $periode)
                                    <th>{{$indikator->targetRenja->where('id_periode_per_tahun', $periode->id)->first()->nilai ?? null}}</th>
                                @endforeach
                                <td>{{$indikator->akhir}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
               
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
