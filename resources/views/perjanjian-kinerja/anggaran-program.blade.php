@extends('layouts.app')
@section('title')
    Penanggung Jawab
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Anggaran Program PK",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "PK OPD", "href" => "#"],
            [ "label" => "Anggaran Program PK", "href" => "#"],
        ]
    ])
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection', [
            'redirectUrl' => route('perjanjian_kinerja.anggaran_program.index')
        ])
        @include('partials.error-message')
        @include('partials.message')
        <div class="card mb-4">
            <div class="card-body">
                <div class="card-header">
                    <h6 class="card-title">Data Anggaran PK Program OPD</h6>
                </div>
                @foreach ($sasarans as $sasaran)
                    <x-hierarchy-card :title='"Sasaran Perangkat Daerah : $sasaran->name"' :hierarchy="[
                        'Misi Kota' => $sasaran->getMisi()->misi,
                        'Tujuan Kota' => $sasaran->getRpjmdTujuan()->name,
                        'Sasaran Kota' => $sasaran->getRpjmdSasaran()->name,
                        'Prioritas Pembangunan' => $sasaran->getPrioritasPembangunan()->name,
                        'Tujuan Perangkat Daerah' => $sasaran->getRenstraTujuan()->name,
                    ]">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Program</th>
                                    <th>Anggaran Renstra</th>
                                    <th>Anggaran Renja</th>
                                    <th>Anggaran PK</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sasaran->programRenstra as $key => $program)
                                    @php
                                        $renstraKey = "renstra_$program->id";
                                        $renjaKey = "renja_$program->id";
                                        $anggaranKey = "anggaran_$program->id";
                                    @endphp
                                    <tr>
                                        <td>{{$key + 1}}</td>
                                        <td>{{$program->name}}</td>
                                        <td>
                                            <a
                                                href="#"
                                                data-toggle="modal"
                                                data-target="#{{$renstraKey}}">Selengkapnya
                                            </a>
                                        </td>
                                        <td>
                                            <a
                                                href="#"
                                                data-toggle="modal"
                                                data-target="#{{$renjaKey}}">Selengkapnya
                                            </a>
                                        </td>
                                        <td>
                                            <button
                                                data-toggle="modal"
                                                data-target="#{{$anggaranKey}}"
                                                class="btn btn-success btn-sm duplicate-btn">
                                                + Tambah Target
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @foreach ($sasaran->programRenstra as $key => $program)
                            @php
                                $renstraKey = "renstra_$program->id";
                                $renjaKey = "renja_$program->id";
                                $anggaranKey = "anggaran_$program->id";
                                $tahunHeads = $periode->periodePerTahun->map(function($tahun) {
                                    return 'Tahun '.$tahun->tahun;
                                });
                                $renstraBody = $periode->periodePerTahun->map(function($tahun) use($program){
                                    return $program->anggaranRenstra->where('id_periode_per_tahun', $tahun->id)->first()->nilai ?? '-';
                                });
                                $renjaBody = $periode->periodePerTahun->map(function($tahun) use($program){
                                    return $program->anggaranRenja->where('id_periode_per_tahun', $tahun->id)->first()->nilai ?? '-';
                                });
                            @endphp
                            @include('partials.modal-table', [
                                "title" => "Anggaran Renstra Program OPD",
                                "modalId" => $renstraKey,
                                "tableHeads" => [
                                    "Program",
                                    ...$tahunHeads,
                                ],
                                "tableData" => [
                                    [
                                        $program->name,
                                        ...$renstraBody
                                    ]
                                ]
                            ])
                            @include('partials.modal-table', [
                                "title" => "Anggaran Renja Program OPD",
                                "modalId" => $renjaKey,
                                "tableHeads" => [
                                    "Program",
                                    ...$tahunHeads,
                                ],
                                "tableData" => [
                                    [
                                        $program->name,
                                        ...$renjaBody
                                    ]
                                ]
                            ])
                            @include('perjanjian-kinerja.partials.modal-anggaran-pk', [
                                'modalId' => $anggaranKey,
                                'periode' => $periode,
                                'submitUrl' => route('perjanjian_kinerja.anggaran_program.store', ['program' => $program->id]),
                                'label' => 'Anggaran PK Program OPD',
                                'dataName' => $program->name,
                                'dataAnggaran' => $program->anggaranPk
                            ])
                        @endforeach
                    </x-hierarchy-card>
                @endforeach
            </div>
            {!! $sasarans->links() !!}
        </div>
    </div>
@endsection
