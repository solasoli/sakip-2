@extends('layouts.app')
@section('title')
    Penanggung Jawab
@endsection
@section('content')
    @include('components.content-header', [
        "title" => "Target PK Indikator Program Tahunan",
        "items" => [
            [ "label" => "Dashboard", "href" => "/"],
            [ "label" => "PK PD", "href" => "#"],
            [ "label" => "Target PK Indikator Program Tahunan", "href" => "#"],
        ]
    ])
    <div class="container-fluid pb-2 mt-4">
        @include('components.opd-selection', [
            'redirectUrl' => route('perjanjian_kinerja.indikator_program.tahunan.index')
        ])
        <div class="card mb-4">
            <div class="card-body">
                @include('partials.error-message')
                @include('partials.message')
                <div class="card-header">
                    <h6 class="card-title">Target PK Indikator Program Tahunan {{ periode()->dari }} -
                        {{ periode()->sampai }}</h6>
                </div>
                @foreach ($sasaranPrograms as $sasaranProgram)
                    <x-hierarchy-card :title='"Sasaran Program PD : $sasaranProgram->name"' :hierarchy="[
                        'Misi Kota' => $sasaranProgram->getMisi()->misi,
                        'Tujuan Kota' => $sasaranProgram->getRpjmdTujuan()->name,
                        'Sasaran Kota' => $sasaranProgram->getRpjmdSasaran()->name,
                        'Prioritas Pembangunan' => $sasaranProgram->getPrioritasPembangunan()->name,
                        'Perangkat Daerah' => $sasaranProgram->getRenstraTujuan()->name,
                    ]">
                    <div class="table-responsive">
                            <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Indikator Program</th>
                                    <th>IKU</th>
                                    <th>Target Renstra</th>
                                    <th>Target Renja</th>
                                    @foreach (periode()->periodePerTahun as $tahun)
                                        <th>Target PK {{ $tahun->tahun }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($sasaranProgram->indikator as $indikator)
                                    @php
                                        $targetIndikatorId = "targetIndikator$indikator->id";
                                        $targetIndikatorRenja = "targetRenja$indikator->id";
                                    @endphp
                                    <tr>
                                        <td>{{$loop->index + 1}}</td>
                                        <td>{{$indikator->indikator}}</td>
                                        <td>{!!$indikator->is_iku ? '<i class="fa fa-check" aria-hidden="true"></i>' : '-'!!}</td>
                                        <td><a href="#" data-bs-toggle="modal" data-bs-target="#{{$targetIndikatorId}}">Selengkapnya</a></td>
                                        <td><a href="#" data-bs-toggle="modal" data-bs-target="#{{$targetIndikatorRenja}}">Selengkapnya</a></td>
                                        @foreach (periode()->periodePerTahun as $tahun)
                                            <td>
                                                <button
                                                    type="button"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#targetPk_{{ $indikator->id }}_{{$tahun->tahun}}"
                                                    class="btn btn-success btn-sm duplicate-btn">+ Tambah Target
                                                </button>
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    </x-hierarchy-card>
                @endforeach
            </div>
            @foreach ($sasaranPrograms as $sasaranProgram)
                @foreach ($sasaranProgram->indikator as $indikator)
                    @php
                        $targetIndikatorId = "targetIndikator$indikator->id";
                        $targetIndikatorRenja = "targetRenja$indikator->id";
                        $targetPkId = "targetPK$indikator->id";
                        $tahun = $sasaranProgram->getPeriode()->periodePerTahun->map(function($periode) {return $periode->tahun;});
                        $tahunHeads = $tahun->map(function($tahun){return "Tahun $tahun";});
                    @endphp
                    @include('partials.modal-table', [
                        "title" => "Target PK Indikator Program",
                        "modalId" => $targetIndikatorId,
                        "tableHeads" => [
                            "Indikator Program",
                            "Satuan",
                            "Kondisi Awal",
                            ...$tahunHeads,
                            "Kondisi Akhir"
                        ],
                        "tableData" => [[
                                $indikator->indikator,
                                $indikator->satuan->name,
                                $indikator->awal,
                                ...$sasaranProgram->getPeriode()->periodePerTahun->map(function($periode) use($indikator) {
                                    return $indikator->target->where('id_periode_per_tahun', $periode->id)->first()->target ?? null;
                            }),
                            $indikator->akhir
                        ]]
                    ])
                    @include('partials.modal-table', [
                        "title" => "Target Renja Indikator Program",
                        "modalId" => $targetIndikatorRenja,
                        "tableHeads" => [
                            "Indikator Program",
                            ...$tahunHeads,
                        ],
                        "tableData" => [[
                                $indikator->indikator,
                                ...$sasaranProgram->getPeriode()->periodePerTahun->map(function($periode) use($indikator) {
                                    return $indikator->targetRenja->where('id_periode_per_tahun', $periode->id)->first()->nilai ?? null;
                            }),
                        ]]
                    ])
                    @foreach (periode()->periodePerTahun as $tahun)
                        @include('perjanjian-kinerja.partials.modal-target-pk-tahunan', [
                            "modalId" => "targetPk_".$indikator->id."_".$tahun->tahun,
                            "headline" => "Target PK Indikator Program tahun $tahun->tahun",
                            "indikator" => $indikator,
                            "tahun" => $tahun,
                        ])
                    @endforeach
                @endforeach
            @endforeach
        </div>
    </div>
@endsection
