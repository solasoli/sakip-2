@extends('layouts/app')
@section('title')
    Konfigurasi
@endsection
@section('content')

<style media="screen">
    .modal-lg{
      width: 750px !important;
    }
  </style>
  
  <div class="br-pageheader pd-y-15 pd-l-20" style="background-color: #fff; box-shadow: 1px 1px 1px rgba(0,0,0,.1)">
      <span class="brand ml-4">Set Periode</span>
      <nav class="mr-4">
          <a class="breadcrumb-item" href="/">Dashboard</a>
          <a class="breadcrumb-item" href="#">Konfigurasi</a>
          <span class="breadcrumb-item" style="color: #000;">Set Periode</span>
      </nav>
  </div>
  
  <div class="container-fluid pb-2 mt-4">
    <div class="card">
        @if ($message = Session::get('selected'))
        <div class="alert alert-success flashMessages" role="alert">
            {{ $message }}
        </div>
        @endif

        @if(!is_null($periode->first()))
        <div class="card-header">
            <table>
                <tr>
                    <td><h6>Periode saat ini</td>
                    <td><h6>&emsp;:&emsp;</h6></td>
                    @if(!is_null(periode()))
                    <td><h6>{{ periode()->nama }} ({{ periode()->dari }} - {{ periode()->sampai }})</h6></td>
                    @else
                    <td><h6 class="text-danger">Periode Belum di set!</h6></td>
                    @endif
                </tr>
            </table>
        </div>
        <div class="card-body">
            <div class="col-sm-4">
                <form action="{{ url('') }}/konfigurasi/set" method="POST">
                    @csrf
                    <div class="input-group input-group-outline">
                        <label for="periode">Periode * :</label>
                        <select name="periode" class="periode">
                            <option value=""></option>
                            @foreach ($periode as $item)
                            <option value="{{ $item->id }}">{{ $item->nama }} ({{ $item->dari }} - {{ $item->sampai }})</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="input-group input-group-outline">
                        <button type="submit" class="btn btn-primary btn-sm">Set Periode</button>
                    </div>
                </form>
            </div>
        </div>
        @else
        <h6 class="text-danger">Isi data periode terlebih dahulu!</h6>
        <a href="{{ url('/periode') }}" class="btn btn-primary btn-sm mt-3">Tambah periode</a>
        @endif
    </div>
  </div>
@endsection

@section('scripts')
  <script>
      selectPlaceholder('.periode', 'Pilih Periode');
  </script>
@endsection