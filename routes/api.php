<?php

use App\Http\Controllers\Api\OpdsController;
use App\Http\Controllers\Api\SkpIntegrasiController;
use App\Http\Controllers\Api\Renstra\KegiatanController;
use App\Http\Controllers\Api\Renstra\SubKegiatanController;
use App\Http\Controllers\Renstra\SasaranRenstraController;
use App\Http\Controllers\Renstra\TujuanRenstraController;
use App\Http\Controllers\Renstra\Program\SasaranProgramController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('/sakip')->group(function () {
    Route::get('/tujuan', [TujuanRenstraController::class, 'api']);
    Route::get('/tujuan/{id}', [TujuanRenstraController::class, 'show']);
    Route::get('/sasaran', [SasaranRenstraController::class, 'api']);
    Route::get('/sasaran/{id}', [SasaranRenstraController::class, 'show']);
    Route::get('/program', [SasaranProgramController::class, 'api']);
});

Route::middleware('auth')->group(function () {

    Route::prefix('opds')->name('opds.')->group(function () {
        Route::get('/')->uses([OpdsController::class, 'index'])->name('index');

        Route::get('/{opd}/renstra/kegiatan')
            ->uses([KegiatanController::class, 'index'])
            ->name('renstra.kegiatan.index');

        Route::get('/{opd}/renstra/kegiatan')
            ->uses([KegiatanController::class, 'kegiatanDenganOpd'])
            ->name('renstra.kegiatan.kegiatan_dengan_opd');

        Route::get('/{opd}/renstra/sub-kegiatan')
            ->uses([SubKegiatanController::class, 'getSubKegiatanDenganOpd'])
            ->name('renstra.sub_kegiatan.sub_kegiatan_dengan_opd');

        Route::get('{opd}/pegawai')->uses([OpdsController::class, 'getPegawai'])->name('pegawai');
    });

    Route::get('renstra/kegiatan/{kegiatan}/mst-sub-kegiatan')
        ->uses([KegiatanController::class, 'getSubKegiatan'])
        ->name('renstra.kegiatan.mst-sub-kegiatan.index');
});

Route::prefix('integrasi')->name('integrasi.')->group(function () {
    Route::get('pegawai')->name('pegawai.index')->uses([SkpIntegrasiController::class, 'getPegawai']);
    Route::get('perjanjian-kinerja/pegawai/{nip}')->name('pegawai.data')->uses([SkpIntegrasiController::class, 'getPerjanjianKinerja']);
    Route::get('sasaran-renstra')->name('sasaran_renstra.index')->uses([SkpIntegrasiController::class, 'getSasaranRenstra']);
    Route::get('sasaran-program-renstra')->name('sasaran_program_renstra.index')->uses([SkpIntegrasiController::class, 'getSasaranProgramRenstra']);
    Route::get('sasaran-kegiatan-renstra')->name('sasaran_kegiatan_renstra.index')->uses([SkpIntegrasiController::class, 'getSasaranKegiatanRenstra']);
    Route::get('sasaran-sub-kegiatan-renstra')->name('sasaran_sub_kegiatan_renstra.index')->uses([SkpIntegrasiController::class, 'getSasaranSubKegiatanRenstra']);
});
