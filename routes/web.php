<?php

use App\Constants\AuthorizationModule;
use App\Http\Controllers\KonfigurasiController;
use App\Http\Controllers\Laporan\LaporanRpjmdController;
use App\Http\Controllers\Laporan\LaporanRenstraController;
use App\Http\Controllers\Laporan\PerjanjianKinerjaController;
use App\Http\Controllers\Mst\BidangUrusanController;
use App\Http\Controllers\Mst\OpdController;
use App\Http\Controllers\Mst\PeriodeController;
use App\Http\Controllers\Mst\UrusanController;
use App\Http\Controllers\Mst\SatuanController;
use App\Http\Controllers\Mst\ProgramController;
use App\Http\Controllers\Mst\KegiatanController;
use App\Http\Controllers\Mst\SubKegiatanController;
use App\Http\Controllers\Mst\SumberAnggaranController;
use App\Http\Controllers\Mst\PrioritasPembangunanController;
use App\Http\Controllers\Mst\PegawaiController;
use App\Http\Controllers\Mst\EselonController;
use App\Http\Controllers\Mst\PangkatController;
use App\Http\Controllers\Mst\PangkatGolonganController;
use App\Http\Controllers\PerjanjianKinerja\AnggaranKegiatanController;
use App\Http\Controllers\PerjanjianKinerja\AnggaranProgramController;
use App\Http\Controllers\PerjanjianKinerja\AnggaranSubKegiatanController;
use App\Http\Controllers\PerjanjianKinerja\IndikatorKegiatanController;
use App\Http\Controllers\PerjanjianKinerja\IndikatorProgramController;
use App\Http\Controllers\PerjanjianKinerja\IndikatorSasaranController;
use App\Http\Controllers\PerjanjianKinerja\OutputSubKegiatanController;
use App\Http\Controllers\PerjanjianKinerja\PkRenjaTahunanController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Renstra\TujuanRenstraController;
use App\Http\Controllers\Renstra\SasaranRenstraController;
use App\Http\Controllers\Renstra\StrategiRenstraController;
use App\Http\Controllers\Renstra\KebijakanRenstraController;
use App\Http\Controllers\Renstra\Kegiatan\KegiatanRenstraController;
use App\Http\Controllers\Renstra\Kegiatan\PenanggungJawabKegiatanController;
use App\Http\Controllers\Renstra\Kegiatan\SasaranKegiatanController;
use App\Http\Controllers\Renstra\Program\PenanggungJawabController;
use App\Http\Controllers\Renstra\Program\SasaranProgramController;
use App\Http\Controllers\Renstra\ProgramRenstraController;
use App\Http\Controllers\Renstra\Sasaran\PenanggungJawabSasaranRenstraController;
use App\Http\Controllers\Renstra\SubKegiatan\PenanggungJawabSubKegiatanController;
use App\Http\Controllers\Renstra\SubKegiatan\SasaranSubKegiatanController;
use App\Http\Controllers\Renstra\SubKegiatan\SubKegiatanController as SubKegiatanSubKegiatanController;
use App\Http\Controllers\Rpjmd\KebijakanRpjmdController;
use App\Http\Controllers\Rpjmd\MisiController;
use App\Http\Controllers\Rpjmd\PrioritasPembangunanRpjmdController;
use App\Http\Controllers\Rpjmd\Program\IndikatorProgramRpjmdController;
use App\Http\Controllers\Rpjmd\Program\ProgramRpjmdController;
use App\Http\Controllers\Rpjmd\Program\SumberAnggaranRpjmdController;
use App\Http\Controllers\Rpjmd\Program\PenanggungJawabRpjmdController;
use App\Http\Controllers\Rpjmd\SasaranController;
use App\Http\Controllers\Rpjmd\StrategiRpjmdController;
use App\Http\Controllers\Rpjmd\VisiController;
use App\Http\Controllers\Rpjmd\TujuanController;
use App\Http\Controllers\SyncRestfulApiController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

require __DIR__ . '/auth.php';
Route::middleware(['auth'])->group(function () {
    Route::get('/', function () {
        return view('dashboard');
    });

    Route::prefix('profile/change-password')->name('profile.change_password.')->group(function () {
        Route::get('/')->name('index')->uses([ProfileController::class, 'changePasswordIndex']);
        Route::post('/')->name('update')->uses([ProfileController::class, 'changePasswordUpdate']);
    });

    Route::get('/dashboard', function () {
        return view('dashboard');
    });

    Route::middleware('access_module:' . AuthorizationModule::MASTER)->group(function () {
        Route::prefix('periode')->group(function () {
            Route::get('/', [PeriodeController::class, 'index']);
            Route::post('/add', [PeriodeController::class, 'store']);
            Route::delete('/{id}/delete', [PeriodeController::class, 'destroy']);
            Route::patch('/{id}/edit', [PeriodeController::class, 'update']);
        });

        Route::prefix('opd')->group(function () {
            Route::get('/', [OpdController::class, 'index']);
            Route::post('/add', [OpdController::class, 'store']);
            Route::delete('/{id}/delete', [OpdController::class, 'destroy']);
            Route::patch('/{id}/edit', [OpdController::class, 'update']);
        });

        Route::prefix('satuan')->group(function () {
            Route::get('/', [SatuanController::class, 'index']);
            Route::post('/add', [SatuanController::class, 'store']);
            Route::delete('/{id}/delete', [SatuanController::class, 'destroy']);
            Route::patch('/{id}/edit', [SatuanController::class, 'update']);
        });

        Route::prefix('urusan')->group(function () {
            Route::get('/', [UrusanController::class, 'index']);
            Route::post('/add', [UrusanController::class, 'store']);
            Route::delete('/{id}/delete', [UrusanController::class, 'destroy']);
            Route::patch('/{id}/edit', [UrusanController::class, 'update']);
        });

        Route::prefix('bidang-urusan')->group(function () {
            Route::get('/', [BidangUrusanController::class, 'index']);
            Route::post('/add', [BidangUrusanController::class, 'store']);
            Route::delete('/{id}/delete', [BidangUrusanController::class, 'destroy']);
            Route::patch('/{id}/edit', [BidangUrusanController::class, 'update']);
        });

        Route::prefix('program')->group(function () {
            Route::get('/', [ProgramController::class, 'index']);
            Route::post('/add', [ProgramController::class, 'store']);
            Route::delete('/{id}/delete', [ProgramController::class, 'destroy']);
            Route::patch('/{id}/edit', [ProgramController::class, 'update']);
        });

        Route::prefix('kegiatan')->group(function () {
            Route::get('/', [KegiatanController::class, 'index']);
            Route::post('/add', [KegiatanController::class, 'store']);
            Route::delete('/{id}/delete', [KegiatanController::class, 'destroy']);
            Route::patch('/{id}/edit', [KegiatanController::class, 'update']);
            Route::get('/datatable-kegiatan', [KegiatanController::class, 'datatableKegiatan'])->name('kegiatan.index');
        });

        Route::prefix('sub-kegiatan')->group(function () {
            Route::get('/', [SubKegiatanController::class, 'index']);
            Route::post('/add', [SubKegiatanController::class, 'store']);
            Route::delete('/delete/{id}', [SubKegiatanController::class, 'destroy']);
            Route::patch('/edit/{id}', [SubKegiatanController::class, 'update']);
            Route::get('/datatable-sub-kegiatan', [SubKegiatanController::class, 'datatableSubKegiatan']);
        });

        Route::prefix('sumber-anggaran')->group(function () {
            Route::get('/', [SumberAnggaranController::class, 'index']);
            Route::post('/add', [SumberAnggaranController::class, 'store']);
            Route::delete('/{id}/delete', [SumberAnggaranController::class, 'destroy']);
            Route::patch('/{id}/edit', [SumberAnggaranController::class, 'update']);
        });

        Route::prefix('prioritas-pembangunan')->group(function () {
            Route::get('/', [PrioritasPembangunanController::class, 'index']);
            Route::post('/add', [PrioritasPembangunanController::class, 'store']);
            Route::delete('/{id}/delete', [PrioritasPembangunanController::class, 'destroy']);
            Route::patch('/{id}/edit', [PrioritasPembangunanController::class, 'update']);
        });

        Route::prefix('pegawai')->group(function () {
            Route::get('/', [PegawaiController::class, 'index']);
            Route::get('/datatable-pegawai', [PegawaiController::class, 'datatablePegawai']);
        });

        Route::prefix('eselon')->group(function () {
            Route::get('/', [EselonController::class, 'index']);
        });

        Route::prefix('pangkat')->group(function () {
            Route::get('/', [PangkatController::class, 'index']);
        });

        Route::prefix('pangkat-golongan')->group(function () {
            Route::get('/', [PangkatGolonganController::class, 'index']);
        });
    });

    Route::middleware('access_module:' . AuthorizationModule::RPJMD)->name('rpjmd.')->group(function () {

        Route::prefix('visi')->group(function () {
            Route::get('/', [VisiController::class, 'index']);
            Route::post('/add', [VisiController::class, 'store']);
        });

        Route::prefix('misi')->group(function () {
            Route::get('/', [MisiController::class, 'index']);
            Route::post('/add', [MisiController::class, 'store']);
            Route::get('/delete/{id}', [MisiController::class, 'destroy']);
        });

        Route::prefix('tujuan')->group(function () {
            Route::get('/', [TujuanController::class, 'index']);
            Route::get('/add', [TujuanController::class, 'create']);
            Route::post('/store', [TujuanController::class, 'store']);
            Route::get('/edit/{id}', [TujuanController::class, 'edit']);
            Route::patch('/{id}/update', [TujuanController::class, 'update']);
            Route::delete('/{id}/delete', [TujuanController::class, 'destroy']);
            Route::get('/{id}/delete-indikator', [TujuanController::class, 'destroyIndikator']);
        });

        Route::prefix('sasaran')->name('sasaran.')->group(function () {
            Route::get('/', [SasaranController::class, 'index']);
            Route::post('/', [SasaranController::class, 'index']);
            Route::get('/add', [SasaranController::class, 'create']);
            Route::post('/store', [SasaranController::class, 'store']);
            Route::post('/edit/{id}', [SasaranController::class, 'edit']);
            Route::patch('/{id}/update', [SasaranController::class, 'update']);
            Route::delete('/{id}/delete', [SasaranController::class, 'destroy']);
            Route::get('/getTujuan/{misi}', [SasaranController::class, 'getTujuan']);
            Route::post('/sasaran-detail/{sasaranDetail}')->name('sasaran_detail')->uses([SasaranController::class, 'submitTargetRkpd']);
        });

        Route::prefix('rpjmd')->group(function () {
            Route::prefix('prioritas-pembangunan')->group(function () {
                Route::get('/', [PrioritasPembangunanRpjmdController::class, 'index']);
                Route::post('/create', [PrioritasPembangunanRpjmdController::class, 'store']);
            });

            Route::prefix('strategi')->group(function () {
                Route::get('/', [StrategiRpjmdController::class, 'index']);
                Route::post('/create', [StrategiRpjmdController::class, 'store']);
            });

            Route::prefix('kebijakan')->group(function () {
                Route::get('/', [KebijakanRpjmdController::class, 'index']);
                Route::post('/create', [KebijakanRpjmdController::class, 'store']);
            });

            // Program Route
            Route::prefix('program')->name('program.')->group(function () {
                Route::prefix('program')->name('program.')->group(function () {
                    Route::get('/', [ProgramRpjmdController::class, 'index']);
                    Route::post('/add', [ProgramRpjmdController::class, 'store']);
                    Route::patch('/edit/{id}', [ProgramRpjmdController::class, 'update']);
                    Route::get('/delete/{id}', [ProgramRpjmdController::class, 'destroy']);
                    Route::post('/{program}/anggaran-rkpd')->name('submit_anggaran_rkpd')->uses([ProgramRpjmdController::class, 'submitAnggaranRkpd']);
                });

                Route::prefix('sumber-anggaran')->group(function () {
                    Route::get('/', [SumberAnggaranRpjmdController::class, 'index']);
                    Route::post('/create', [SumberAnggaranRpjmdController::class, 'store']);
                });

                Route::prefix('penanggung-jawab')->group(function () {
                    Route::get('/', [PenanggungJawabRpjmdController::class, 'index']);
                    Route::post('/create', [PenanggungJawabRpjmdController::class, 'store']);
                });

                Route::prefix('indikator-program')->name('indikator_program.')->group(function () {
                    Route::get('/', [IndikatorProgramRpjmdController::class, 'index']);
                    Route::post('/create', [IndikatorProgramRpjmdController::class, 'store']);
                    Route::get('/delete/{id}', [IndikatorProgramRpjmdController::class, 'destroy']);
                    Route::post('/{indikator}/target-rpjmd')->name('target_rpjmd.submit')->uses([IndikatorProgramRpjmdController::class, 'submitTargetRkpd']);
                });
            });
        });
    });

    Route::prefix('renstra')->name('renstra.')->middleware('access_module:' . AuthorizationModule::RENSTRA)->group(function () {
        Route::prefix('tujuan')->name('tujuan.')->group(function () {
            // get method
            Route::name('index')->get('/', [TujuanRenstraController::class, 'index']);
            Route::get('/add', [TujuanRenstraController::class, 'create']);
            // post method
            Route::post('/store', [TujuanRenstraController::class, 'store']);
            Route::post('/edit/{id}', [TujuanRenstraController::class, 'edit']);
            // patch method
            Route::patch('/update/{id}', [TujuanRenstraController::class, 'update']);
            // delete method
            Route::delete('/delete/{id}', [TujuanRenstraController::class, 'destroy']);
        });

        Route::prefix('sasaran')->name('sasaran.')->group(function () {
            // get method
            Route::get('/', [SasaranRenstraController::class, 'index'])->name('index');
            Route::get('/add', [SasaranRenstraController::class, 'create']);
            // post method
            Route::post('/store', [SasaranRenstraController::class, 'store']);
            Route::post('/edit/{id}', [SasaranRenstraController::class, 'edit']);
            // patch method
            Route::patch('/update/{id}', [SasaranRenstraController::class, 'update']);
            // delete method
            Route::delete('/delete/{id}', [SasaranRenstraController::class, 'destroy']);
            Route::get('/api', [SasaranRenstraController::class, 'api']);

            Route::post('/{sasaran}/indikator/{indikator}/renja')
                ->uses([SasaranRenstraController::class, 'submitRenja'])
                ->name('indikator.renja');

            Route::prefix('penanggung-jawab')->name('penanggung_jawab.')->group(function () {
                Route::get('/')->name('index')->uses([PenanggungJawabSasaranRenstraController::class, 'index']);
                Route::post('/mass-update')->name('mass_update')->uses([PenanggungJawabSasaranRenstraController::class, 'massUpdate']);
            });
        });

        Route::prefix('strategi')->name('strategi.')->group(function () {
            Route::get('/', [StrategiRenstraController::class, 'index'])->name('index');
            Route::post('/create', [StrategiRenstraController::class, 'store']);
        });

        Route::prefix('kebijakan')->name('kebijakan.')->group(function () {
            Route::get('/', [KebijakanRenstraController::class, 'index'])->name('index');
            Route::post('/create', [KebijakanRenstraController::class, 'store']);
        });

        Route::prefix('program')->name('program.')->group(function () {
            Route::get('program', [ProgramRenstraController::class, 'index'])->name('program.index');
            Route::post('add', [ProgramRenstraController::class, 'store']);
            Route::patch('edit/{id}', [ProgramRenstraController::class, 'update']);
            Route::delete('delete/{id}', [ProgramRenstraController::class, 'destroy']);

            Route::prefix('sasaran-program')->name('sasaran_program.')->group(function () {
                // method get
                Route::get('/', [SasaranProgramController::class, 'index'])->name('index');
                Route::get('/add', [SasaranProgramController::class, 'create']);
                // method post
                Route::post('/add', [SasaranProgramController::class, 'store']);
                Route::post('/edit/{id}', [SasaranProgramController::class, 'edit']);
                // method delete
                Route::delete('/delete/{id}', [SasaranProgramController::class, 'destroy']);
                // method patch
                Route::patch('/edit/{id}', [SasaranProgramController::class, 'update']);

                Route::post('/{sasaranProgram}/indikator/{indikator}/renja')
                    ->uses([SasaranProgramController::class, 'submitRenja'])
                    ->name('indikator.renja');
            });

            Route::prefix('penanggung-jawab')->name('penanggung_jawab.')->group(function () {
                Route::get('/')->name('index')->uses([PenanggungJawabController::class, 'index']);
                Route::post('/mass-update')->name('mass_update')->uses([PenanggungJawabController::class, 'massUpdate']);
            });
        });

        Route::prefix('kegiatan')->name('kegiatan.')->group(function () {
            Route::prefix('kegiatan')->group(function () {
                // method get
                Route::get('/', [KegiatanRenstraController::class, 'index'])->name('index');
                Route::get('/add', [KegiatanRenstraController::class, 'create']);
                Route::get('/edit/{id}', [KegiatanRenstraController::class, 'edit']);
                // method post
                Route::post('/add', [KegiatanRenstraController::class, 'store']);
                // method delete
                Route::delete('/delete/{id}', [KegiatanRenstraController::class, 'destroy']);
                // method patch
                Route::patch('/edit/{id}', [KegiatanRenstraController::class, 'update']);
            });
            Route::prefix('sasaran-kegiatan')->name('sasaran_kegiatan.')->group(function () {
                Route::get('/')->uses([SasaranKegiatanController::class, 'index'])->name('index');
                Route::get('/create')->uses([SasaranKegiatanController::class, 'create'])->name('create');
                Route::post('')->uses([SasaranKegiatanController::class, 'store'])->name('store');
                Route::post('/{sasaranKegiatan}/indikator/{indikator}/renja')
                    ->uses([SasaranKegiatanController::class, 'submitRenja'])
                    ->name('indikator.renja');
                Route::delete('/{kegiatan}', [SasaranKegiatanController::class, 'destroy'])->name('destroy');
                Route::get('/{kegiatan}/edit')->uses([SasaranKegiatanController::class, 'edit'])->name('edit');
                Route::put('/{sasaranKegiatanRenstra}')->uses([SasaranKegiatanController::class, 'update'])->name('update');
            });

            Route::prefix('penanggung-jawab-kegiatan')->name('penanggung_jawab_kegiatan.')->group(function () {
                Route::get('/')->uses([PenanggungJawabKegiatanController::class, 'index'])->name('index');
                Route::post('/mass-update')->name('mass_update')->uses([PenanggungJawabKegiatanController::class, 'massUpdate']);
            });
        });

        Route::prefix('sub-kegiatan')->name('sub_kegiatan.')->group(function () {
            Route::prefix('sub-kegiatan')->name('sub_kegiatan.')->group(function () {
                Route::get('/')->name('index')->uses([SubKegiatanSubKegiatanController::class, 'index']);
                Route::get('/create')->name('create')->uses([SubKegiatanSubKegiatanController::class, 'create']);
                Route::post('/')->name('store')->uses([SubKegiatanSubKegiatanController::class, 'store']);
                Route::post('/{subKegiatan}/renja')
                    ->uses([SubKegiatanSubKegiatanController::class, 'submitRenja'])
                    ->name('renja.submit');
                Route::delete('/{subKegiatan}')
                    ->name('destroy')
                    ->uses([SubKegiatanSubKegiatanController::class, 'destroy']);

                Route::get('/{subKegiatan}')
                    ->name('edit')
                    ->uses([SubKegiatanSubKegiatanController::class, 'edit']);

                Route::put('/{subKegiatan}')
                    ->name('update')
                    ->uses([SubKegiatanSubKegiatanController::class, 'update']);
            });

            Route::prefix('sasaran-sub-kegiatan')->name('sasaran_sub_kegiatan.')->group(function () {
                Route::get('/')->name('index')->uses([SasaranSubKegiatanController::class, 'index']);
                Route::get('/create')->name('create')->uses([SasaranSubKegiatanController::class, 'create']);
                Route::post('/')->name('store')->uses([SasaranSubKegiatanController::class, 'store']);
                Route::put('/{sasaranSubKegiatan}')->name('update')->uses([SasaranSubKegiatanController::class, 'update']);
                Route::get('/{sasaranSubKegiatan}/edit')->name('edit')->uses([SasaranSubKegiatanController::class, 'edit']);

                Route::post('/{sasaranSubKegiatan}/output/{output}/renja')
                    ->uses([SasaranSubKegiatanController::class, 'submitRenja'])
                    ->name('output.renja');

                Route::delete('/{sasaranSubKegiatan}', [SasaranSubKegiatanController::class, 'destroy'])->name('destroy');
            });

            Route::prefix('penanggung-jawab')->name('penanggung_jawab.')->group(function () {
                Route::get('/')->name('index')->uses([PenanggungJawabSubKegiatanController::class, 'index']);
                Route::post('/mass-update')->name('mass_update')->uses([PenanggungJawabSubKegiatanController::class, 'massUpdate']);
            });
        });
    });

    Route::prefix('perjanjian-kinerja')->name('perjanjian_kinerja.')->middleware('access_module:' . AuthorizationModule::PK)->group(function () {

        Route::prefix('indikator-sasaran')->name('indikator_sasaran.')->group(function () {

            Route::get('tahunan')->name('tahunan.index')->uses([IndikatorSasaranController::class, 'indexTahunan']);
            Route::post('tahunan')->name('tahunan.submit')->uses([IndikatorSasaranController::class, 'submitTahunan']);
            Route::post('tahunan/{indikator}/pk')->name('tahunan.pk.submit')->uses([IndikatorSasaranController::class, 'submitPkTahunan']);

            Route::get('triwulan')->name('triwulan.index')->uses([IndikatorSasaranController::class, 'indexTriwulan']);
            Route::post('triwulan')->name('triwulan.submit')->uses([IndikatorSasaranController::class, 'submitTriwulan']);
        });

        Route::prefix('indikator-program')->name('indikator_program.')->group(function () {
            Route::get('tahunan')->name('tahunan.index')->uses([IndikatorProgramController::class, 'indexTahunan']);
            Route::post('tahunan')->name('tahunan.submit')->uses([IndikatorProgramController::class, 'submitTahunan']);
            Route::post('tahunan/{indikator}/pk')->name('tahunan.pk.submit')->uses([IndikatorProgramController::class, 'submitPkTahunan']);

            Route::get('triwulan')->name('triwulan.index')->uses([IndikatorProgramController::class, 'indexTriwulan']);
            Route::post('triwulan')->name('triwulan.submit')->uses([IndikatorProgramController::class, 'submitTriwulan']);
        });

        Route::prefix('indikator-kegiatan')->name('indikator_kegiatan.')->group(function () {
            Route::get('tahunan')->name('tahunan.index')->uses([IndikatorKegiatanController::class, 'indexTahunan']);
            Route::post('tahunan')->name('tahunan.submit')->uses([IndikatorKegiatanController::class, 'submitTahunan']);
            Route::post('tahunan/{indikator}/pk')->name('tahunan.pk.submit')->uses([IndikatorKegiatanController::class, 'submitPkTahunan']);

            Route::get('triwulan')->name('triwulan.index')->uses([IndikatorKegiatanController::class, 'indexTriwulan']);
            Route::post('triwulan')->name('triwulan.submit')->uses([IndikatorKegiatanController::class, 'submitTriwulan']);
        });

        Route::prefix('output-sub-kegiatan')->name('output_sub_kegiatan.')->group(function () {
            Route::get('tahunan')->name('tahunan.index')->uses([OutputSubKegiatanController::class, 'indexTahunan']);
            Route::post('tahunan')->name('tahunan.submit')->uses([OutputSubKegiatanController::class, 'submitTahunan']);
            Route::post('tahunan/{output}/pk')->name('tahunan.pk.submit')->uses([OutputSubKegiatanController::class, 'submitPkTahunan']);

            Route::get('triwulan')->name('triwulan.index')->uses([OutputSubKegiatanController::class, 'indexTriwulan']);
            Route::post('triwulan')->name('triwulan.submit')->uses([OutputSubKegiatanController::class, 'submitTriwulan']);
        });

        Route::post('pk-renja-tahunan')
            ->name('pk_renja_tahunan.submit')
            ->uses([PkRenjaTahunanController::class, 'store']);

        Route::prefix('anggaran-program')->name('anggaran_program.')->group(function () {
            Route::get('/')->name('index')->uses([AnggaranProgramController::class, 'index']);
            Route::post('/{program}')->name('store')->uses([AnggaranProgramController::class, 'store']);
        });

        Route::prefix('anggaran-kegiatan')->name('anggaran_kegiatan.')->group(function () {
            Route::get('/')->name('index')->uses([AnggaranKegiatanController::class, 'index']);
            Route::post('/{kegiatan}')->name('store')->uses([AnggaranKegiatanController::class, 'store']);
        });

        Route::prefix('anggaran-sub-kegiatan')->name('anggaran_sub_kegiatan.')->group(function () {
            Route::get('/')->name('index')->uses([AnggaranSubKegiatanController::class, 'index']);
            Route::post('/{subKegiatan}')->name('store')->uses([AnggaranSubKegiatanController::class, 'store']);
        });
    });

    Route::prefix('laporan')->name('laporan.')->middleware('access_module:' . AuthorizationModule::LAPORAN)->group(function () {
        //rpjmd
        Route::get('rpjmd', [LaporanRpjmdController::class, 'index']);
        Route::post('rpjmd/excel', [LaporanRpjmdController::class, 'excel']);
        Route::post('rpjmd/pdf', [LaporanRpjmdController::class, 'pdf']);
        Route::post('rpjmd/print', [LaporanRpjmdController::class, 'print']);

        //renstra
        Route::get('renstra', [LaporanRenstraController::class, 'index']);
        Route::post('renstra/print', [LaporanRenstraController::class, 'print']);
        Route::post('renstra/excel', [LaporanRenstraController::class, 'exportToExcel']);
        Route::post('renstra/pdf', [LaporanRenstraController::class, 'pdf']);

        //Perjanjian Kinerja
        Route::get('perjanjian-kinerja')->name('perjanjian_kinerja.index')->uses([PerjanjianKinerjaController::class, 'index']);
        Route::post('perjanjian-kinerja/atasan')->name('perjanjian_kinerja.atasan')->uses([PerjanjianKinerjaController::class, 'getAtasan']);
        Route::post('perjanjian-kinerja/penanggung-jawab')->name('perjanjian_kinerja.penanggung_jawab')->uses([PerjanjianKinerjaController::class, 'getPenanggungJawab']);
        Route::get('perjanjian-kinerja/tahun/{tahun}/jenis/{jenis}/atasan/{atasan}/pegawai/{pegawai}/surat')->name('perjanjian_kinerja.surat')->uses([PerjanjianKinerjaController::class, 'generateSurat']);
    });

    Route::middleware('access_module:' . AuthorizationModule::CONFIGURATION)->group(function () {
        Route::prefix('konfigurasi')->group(function () {
            Route::get('/', [KonfigurasiController::class, 'index']);
            Route::post('/set', [KonfigurasiController::class, 'update']);
            Route::get('/get-tahun-periode/{id}', [KonfigurasiController::class, 'getTahunPeriode']);
        });

        // Sync route
        Route::prefix('sync')->group(function () {
            Route::get('/pegawai', [SyncRestfulApiController::class, 'syncPegawai']);
        });

        Route::prefix('users')->name('users.')->group(function () {
            Route::get('/')->name('index')->uses([UsersController::class, 'index']);
            Route::get('/data')->name('data')->uses([UsersController::class, 'data']);
            Route::get('/create')->name('create')->uses([UsersController::class, 'create']);
            Route::post('/')->name('store')->uses([UsersController::class, 'store']);
            Route::get('/{user}/edit')->name('edit')->uses([UsersController::class, 'edit']);
            Route::put('/{user}')->name('update')->uses([UsersController::class, 'update']);
            Route::get('/{user}/delete')->name('destroy')->uses([UsersController::class, 'destroy']);
            Route::get('/{user}/reset-password')->name('reset_password')->uses([UsersController::class, 'resetPassword']);
        });
    });
});


Route::get('/test', [KonfigurasiController::class, 'test']);
