<?php

use App\Constants\Role;
use App\Models\Opd;
use App\Models\PeriodeWalkot;
use Illuminate\Database\Eloquent\Collection;

if (!function_exists('periode')) {
    function periode()
    {
        return app('PeriodeAktif');
    }
}

if (!function_exists('hashID')) {
    function hashID($str)
    {
        $res = base64_encode($str);
        return $res;
    }
}

function jsonResponse($data = null, $message = "success", int $status = 200){
    return response()->json([
        "message" => $message,
        "data" => $data
    ], $status);
}

function getAllowedOpds() : Collection {
    /** @var \App\Models\User */
    $user = auth()->user();

    if(!$user) throw new Error('Log in first');

    if($user->role == Role::SUPER_ADMIN) {
        return Opd::all();
    }

    return Opd::where('id', $user->id_opd)->get();
}

function flashSuccess($message = 'Success') {
    session()->flash('message', $message);
}
