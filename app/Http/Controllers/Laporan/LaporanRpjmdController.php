<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use App\Models\PeriodeWalkot;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class LaporanRpjmdController extends Controller
{
    function index()
    {
        $periode = PeriodeWalkot::all();

        return view('laporan.laporan-rpjmd', [
            'periode' => $periode,
            'title' => 'Laporan RPMD'
        ]);
    }

    function excel(Request $request)
    {
        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=rpjmd.csv',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        $list = DB::table('rpjmd_misi as m')
            ->join('rpjmd_tujuan as rt', 'rt.id_misi', '=', 'm.id')
            ->join('rpjmd_sasaran as rs', 'rs.id_tujuan', '=', 'rt.id')
            ->join('rpjmd_program as rp', 'rp.id_rpjmd_sasaran', '=', 'rs.id')
            ->join('mst_program as mp', 'rp.id_mst_program', '=', 'mp.id')
            ->select('m.misi', 'rt.name as tujuan', 'rs.name as sasaran', 'mp.name as program')
            ->where('m.id_periode', $request->periode)
            ->get();

        $list = array_map(function ($item) {
            return (array)$item;
        }, $list->toArray());
        // $list = ProgramRenstra::with('programRpjmd')->toArray();

        # add headers for each column in the CSV download
        array_unshift($list, array_keys($list[0]));

        $callback = function () use ($list) {
            $FH = fopen('php://output', 'w');
            foreach ($list as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return response()->stream($callback, 200, $headers);
    }

    function pdf(Request $request)
    {
        $data = DB::table('rpjmd_misi as m')
            ->join('rpjmd_tujuan as rt', 'rt.id_misi', '=', 'm.id')
            ->join('rpjmd_sasaran as rs', 'rs.id_tujuan', '=', 'rt.id')
            ->join('rpjmd_program as rp', 'rp.id_rpjmd_sasaran', '=', 'rs.id')
            ->join('mst_program as mp', 'rp.id_mst_program', '=', 'mp.id')
            ->select('m.misi', 'rt.name as tujuan', 'rs.name as sasaran', 'mp.name as program')
            ->where('m.id_periode', $request->periode)
            ->get();

        $time = Carbon::today();

        $pdf = PDF::loadView('laporan.laporan-rpjmd-pdf', [
            'data' => $data
        ])->setPaper('a4', 'landscape');

        return $pdf->download($time . '-' . 'laporan-rpjmd');
    }

    function print(Request $request)
    {
        $data = DB::table('rpjmd_misi as m')
            ->join('rpjmd_tujuan as rt', 'rt.id_misi', '=', 'm.id')
            ->join('rpjmd_sasaran as rs', 'rs.id_tujuan', '=', 'rt.id')
            ->join('rpjmd_program as rp', 'rp.id_rpjmd_sasaran', '=', 'rs.id')
            ->join('mst_program as mp', 'rp.id_mst_program', '=', 'mp.id')
            ->select('m.misi', 'rt.name as tujuan', 'rs.name as sasaran', 'mp.name as program')
            ->where('m.id_periode', $request->periode)
            ->get();
        $time = Carbon::today();

        $pdf = PDF::loadView('laporan.laporan-rpjmd-pdf', [
            'data' => $data
        ])->setPaper('a4', 'landscape');

        return $pdf->stream($time . '-' . 'laporan-rpjmd');
    }
}
