<?php

namespace App\Http\Controllers\Laporan;

use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use App\Models\PeriodePerTahun;
use App\Models\PkRenjaTahunan;
use App\Models\Renstra\Kegiatan\SasaranKegiatanIndikatorRenstra;
use App\Models\Renstra\Program\SasaranProgramIndikatorRenstra;
use App\Models\Renstra\SasaranRenstraIndikator;
use App\Models\Renstra\SubKegiatan\SasaranSubKegiatanOutputRenstra;
use App\Models\TargetPkRenjaTahunan;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class PerjanjianKinerjaController extends Controller
{

    const ALLOWED_TYPES = [
        SasaranRenstraIndikator::class,
        SasaranProgramIndikatorRenstra::class,
        SasaranKegiatanIndikatorRenstra::class,
        SasaranSubKegiatanOutputRenstra::class
    ];

    public function index(Request $request)
    {
        $opds = getAllowedOpds();

        return view('laporan.laporan-perjanjian-kinerja', compact('opds'));
    }

    public function getPenanggungJawab(Request $request)
    {
        $this->validate($request, [
            'opd' => 'required|exists:mst_opd,id',
            'keyword' => 'nullable|string',
            'atasan' => 'required|exists:mst_pegawai,id',
            'tahun' => 'required|exists:mst_periode_per_tahun,id',
            'jenis' => [
                'required',
                Rule::in(self::ALLOWED_TYPES)
            ]
        ]);

        $pegawai = Pegawai::where('id_opd', $request->opd)
            ->limit(10)
            ->whereHas('targetPkRenjaTahunan', function ($query) use ($request) {
                $query->where('tahun_id', $request->tahun)
                    ->where('atasan_id', $request->atasan)
                    ->whereHas('pkRenjaTahunan', fn ($query) => $query->where('pk_type', $request->jenis));
            });

        if ($request->keyword) {
            $pegawai->where('nama', 'like', "%$request->keyword%");
        }

        return jsonResponse([
            "pegawai" => $pegawai->get()
        ]);
    }

    public function getAtasan(Request $request)
    {
        $this->validate($request, [
            'opd' => 'required|exists:mst_opd,id',
            'keyword' => 'nullable|string',
            'tahun' => 'required|exists:mst_periode_per_tahun,id',
            'jenis' => [
                'required',
                Rule::in(self::ALLOWED_TYPES)
            ]
        ]);

        $pegawai = Pegawai::where('id_opd', $request->opd)
            ->limit(10)
            ->whereHas('targetPkRenjaTahunanSebagaiAtasan', function ($query) use ($request) {
                $query->where('tahun_id', $request->tahun)
                    ->whereHas('pkRenjaTahunan', fn ($query) => $query->where('pk_type', $request->jenis));
            });

        if ($request->keyword) {
            $pegawai->where('nama', 'like', "%$request->keyword%");
        }

        return jsonResponse([
            "pegawai" => $pegawai->get()
        ]);
    }

    public function generateSurat(PeriodePerTahun $tahun, string $jenis, Pegawai $atasan, Pegawai $pegawai)
    {

        $anggaranLevel = $this->jenisAnggaranLevel($jenis);

        $relation = $jenis == SasaranRenstraIndikator::class
            ? 'pkRenjaTahunan.pk.sasaran.programRenstra.anggaranRenstra'
            : 'pkRenjaTahunan.pk.sasaran.parent.anggaranRenstra';

        /** @var Collection */
        $targetPkRenja = $pegawai->targetPkRenjaTahunan()
            ->with([
                $relation,
                'satuan'
            ])
            ->whereHas('pkRenjaTahunan', fn ($query) => $query->where('pk_type', $jenis))
            ->where('tahun_id', $tahun->id)->get();

        $perangkatDaerah = $targetPkRenja->first()->pkRenjaTahunan->pk->getPerangkatDaerah();

        $groupedTargetPkRenja = $targetPkRenja->groupBy('pkRenjaTahunan.pk.sasaran.id')
            ->map(function ($data) {
                $referrencedParent = $data->first()->pkRenjaTahunan->pk->sasaran;
                return collect([
                    'id' => $referrencedParent->id,
                    'name' => $referrencedParent->name,
                    'count' => $data->count(),
                    'children' => $data
                ]);
            });

        if ($jenis == SasaranRenstraIndikator::class) {

            $groupedAnggaran = $targetPkRenja->groupBy('pkRenjaTahunan.pk.sasaran.id')
                ->map(function ($data) use ($tahun) {

                    $referrencedParent = $data->first()->pkRenjaTahunan->pk->sasaran->programRenstra;

                    return $referrencedParent
                        ->map(function ($renstraSasaran) use ($tahun) {
                            return [
                                'id' => $renstraSasaran->id,
                                'nama' => $renstraSasaran->name,
                                'anggaran' => $renstraSasaran->anggaranRenstra->where('id_periode_per_tahun', $tahun->id)->first()->nilai,
                                'sumber' => $renstraSasaran->getSumberAnggaran()->implode(', ')
                            ];
                        });
                })->flatten(1);
        } else {

            $groupedAnggaran = $targetPkRenja->groupBy('pkRenjaTahunan.pk.sasaran.parent.id')
                ->map(function ($data) use ($tahun) {
                    $referrencedParent = $data->first()->pkRenjaTahunan->pk->sasaran->parent;
                    return [
                        'id' => $referrencedParent->id,
                        'nama' => $referrencedParent->name,
                        'anggaran' => $referrencedParent->anggaranRenstra->where('id_periode_per_tahun', $tahun->id)->first()->nilai,
                        'sumber' => $referrencedParent->getSumberAnggaran()->implode(', ')
                    ];
                });
        }


        return view('laporan.laporan-perjanjian-kinerja-generate', compact(
            'tahun',
            'atasan',
            'pegawai',
            'targetPkRenja',
            'groupedTargetPkRenja',
            'anggaranLevel',
            'perangkatDaerah',
            'groupedAnggaran'
        ));
    }

    private function jenisAnggaranLevel(string $jenis)
    {
        switch ($jenis) {
            case SasaranRenstraIndikator::class:
            case SasaranProgramIndikatorRenstra::class:
                return 'Program';
            case SasaranKegiatanIndikatorRenstra::class:
                return 'Kegiatan';
            case SasaranSubKegiatanOutputRenstra::class:
                return 'Sub Kegiatan';
            default:
                throw ValidationException::withMessages(['jenis' => 'Jenis salah']);
        }
    }
}
