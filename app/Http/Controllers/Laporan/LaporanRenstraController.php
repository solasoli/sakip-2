<?php

namespace App\Http\Controllers\Laporan;

use App\Exports\Laporan\RenstraExport;
use App\Http\Controllers\Controller;
use App\Models\Opd;
use App\Models\PeriodeWalkot;
use App\Models\Renstra\Program\ProgramRenstra;
use App\Models\Renstra\TujuanRenstra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class LaporanRenstraController extends Controller
{
    public function index()
    {
        $periode_walkot = PeriodeWalkot::all();
        $program = ProgramRenstra::all();
        $opd = Opd::all();

        return view('laporan.laporan-renstra', [
            'title' => 'Laporan Renstra',
            'periode_walkot' => $periode_walkot,
            'program' => $program,
            'opd' => $opd
        ]);
    }

    public function exportToExcel(Request $request)
    {
    }

    public function print(Request $request)
    {
        $periode = $request->input('periode');
        $opd = $request->input('opd');

        // $getOpd = Opd::findOrFail($opd);
        $getOpd = DB::table('mst_opd')->find($opd);

        $getRenstra = TujuanRenstra::where('id_periode', $periode)
            ->where('id_mst_opd', $opd)
            ->get();
        $getPeriode = DB::table("mst_periode_per_tahun")
            ->where("id_periode", $periode)
            ->get();
        $pdf = PDF::LoadView('laporan.laporan-renstra-print1', [
            'title' => 'Laporan Renstra',
            'data' => $getRenstra,
            'listPeriode' => $getPeriode,
            'opd' => $getOpd
        ])->setPaper('a4', 'landscape');

        return $pdf->stream('laporan-renstra-print1');
    }
}
