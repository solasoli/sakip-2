<?php

namespace App\Http\Controllers\PerjanjianKinerja;

use App\Http\Controllers\Controller;
use App\Models\KeuanganTahunan;
use App\Models\Renstra\Program\ProgramRenstra;
use App\Models\Renstra\Program\SasaranProgramRenstra;
use App\Models\Renstra\SasaranRenstra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class AnggaranProgramController extends Controller
{
    public function index(Request $request) {
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        Gate::authorize('access-opd', $selectedOpd);
        $periode = periode();

        $sasarans = SasaranRenstra::with([
            'programRenstra.programRpjmd.mstProgram',
            'renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'renstraTujuan.rpjmdSasaran.tujuan.misi.periode.periodePerTahun',
            'programRenstra.anggaranRenstra',
            'programRenstra.anggaranRenja',
            'programRenstra.anggaranPk',
        ])->whereHas('renstraTujuan', function($query) use ($selectedOpd){
            $query->where('id_mst_opd', $selectedOpd);
        })->paginate(10);

        return view('perjanjian-kinerja.anggaran-program', compact('opds', 'selectedOpd', 'sasarans', 'periode'));
    }

    public function store(ProgramRenstra $program, Request $request) {
        $opdId = $program->sasaranRenstra->renstraTujuan->id_mst_opd;
        Gate::authorize('access-opd', $opdId);

        $this->validate($request, [
            'anggaran' => 'required|array|min:1',
            'anggaran.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'anggaran.*.value' => 'nullable|integer',
        ]);



        $program->anggaranPk()->delete();
        foreach ($request->get('anggaran') as $anggaran) {
            $program->anggaranPk()->create([
                'assignable_kind' => KeuanganTahunan::KIND_ANGGARAN_PK,
                'nilai' => $anggaran['value'] ?? 0,
                'id_periode_per_tahun' => $anggaran['id_tahun']
            ]);
        }



        flashSuccess('Berhasil Mengubah Anggaran PK');

        return redirect()->back();
    }
}
