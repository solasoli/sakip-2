<?php

namespace App\Http\Controllers\PerjanjianKinerja;

use App\Http\Controllers\Controller;
use App\Models\KeuanganTahunan;
use App\Models\Renstra\Kegiatan\KegiatanRenstra;
use App\Models\Renstra\Program\ProgramRenstra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class AnggaranKegiatanController extends Controller
{
    public function index(Request $request) {
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        Gate::authorize('access-opd', $selectedOpd);
        $periode = periode();

        $programs = ProgramRenstra::with([
            'programRpjmd.mstProgram',
            'sasaranRenstra.renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'sasaranRenstra.renstraTujuan.rpjmdSasaran.tujuan.misi.periode.periodePerTahun',
            'kegiatanRenstra.anggaranRenstra',
            'kegiatanRenstra.anggaranRenja',
            'kegiatanRenstra.anggaranPk',
        ])->whereHas('sasaranRenstra.renstraTujuan', function($query) use ($selectedOpd){
            $query->where('id_mst_opd', $selectedOpd);
        })->paginate(10);

        return view('perjanjian-kinerja.anggaran-kegiatan', compact('opds', 'selectedOpd', 'programs', 'periode'));
    }

    public function store(KegiatanRenstra $kegiatan, Request $request) {
        $opdId = $kegiatan->programRenstra->sasaranRenstra->renstraTujuan->id_mst_opd;
        Gate::authorize('access-opd', $opdId);

        $this->validate($request, [
            'anggaran' => 'required|array|min:1',
            'anggaran.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'anggaran.*.value' => 'nullable|integer',
        ]);



        $kegiatan->anggaranPk()->delete();
        foreach ($request->get('anggaran') as $anggaran) {
            $kegiatan->anggaranPk()->create([
                'assignable_kind' => KeuanganTahunan::KIND_ANGGARAN_PK,
                'nilai' => $anggaran['value'] ?? 0,
                'id_periode_per_tahun' => $anggaran['id_tahun']
            ]);
        }



        flashSuccess('Berhasil Mengubah Anggaran PK');

        return redirect()->back();
    }
}
