<?php

namespace App\Http\Controllers\PerjanjianKinerja;

use App\Http\Controllers\Controller;
use App\Models\Eselon;
use App\Models\Pegawai;
use App\Models\PeriodePerTahun;
use App\Models\Renstra\Kegiatan\SasaranKegiatanIndikatorRenstra;
use App\Models\Renstra\Kegiatan\SasaranKegiatanRenstra;
use App\Models\Satuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\PkRenjaTahunan;

class IndikatorKegiatanController extends Controller
{
    public function indexTahunan(Request $request) {
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        Gate::authorize('access-opd', $selectedOpd);
        $satuan = Satuan::all();
        $pegawai = Pegawai::where('id_opd', $selectedOpd)->whereIn('id_eselon', [Eselon::III_A, Eselon::III_B])->get();
        $atasanOption = Pegawai::where('id_opd', $selectedOpd)->whereIn('id_eselon', [Eselon::II_A, Eselon::II_B])->get();

        $sasaranKegiatan = SasaranKegiatanRenstra::whereHas(
            'kegiatan.programRenstra.sasaranRenstra.renstraTujuan.opd',
            function($query) use ($selectedOpd){
                $query->where('id', $selectedOpd);
            }
        )->with([
            'kegiatan.programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.tujuan.misi',
            'kegiatan.programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'indikator.targetRenja',
            'indikator.pkRenjaTahunan',
            'indikator.target',
        ])->paginate(10);

        return view('perjanjian-kinerja.indikator-kegiatan.tahunan', compact('opds', 'selectedOpd', 'sasaranKegiatan', 'satuan', 'pegawai', 'atasanOption'));
    }

    public function submitTahunan(Request $request)
    {
        $this->validate($request, [
            'data' => 'required|array',
            'data.*.sasaran_id' => 'required|exists:renstra_sasaran_kegiatan,id',
            'data.*.indikator' => 'nullable|array',
            'data.*.indikator.*.indikator_id' => 'required|exists:renstra_sasaran_kegiatan_indikator,id',
            'data.*.indikator.*.atasan' => 'nullable|exists:mst_pegawai,id',
            'data.*.indikator.*.penanggung_jawab' => 'nullable|array',
            'data.*.indikator.*.penanggung_jawab.*' => 'required|exists:mst_pegawai,id',
        ]);

        collect($request->get('data'))->each(function ($sasaranData) {
            $sasaran = SasaranKegiatanRenstra::with(['indikator'])->firstWhere('id', $sasaranData['sasaran_id']);

            collect($sasaranData["indikator"] ?? [])->each(function ($indikatorData) use ($sasaran) {

                if (!isset($indikatorData['penanggung_jawab'])) return;

                /** @var  SasaranKegiatanIndikatorRenstra */
                $indikator = $sasaran->indikator->firstWhere('id', $indikatorData['indikator_id']);

                /** @var PkRenjaTahunan */
                $tahunanModel = $indikator->pkRenjaTahunan;

                $tahunanModel->atasan()->associate($indikatorData['atasan']);
                $tahunanModel->save();

                $tahunanModel->penanggungJawab()->sync($indikatorData["penanggung_jawab"] ?? []);
            });
        });

        session()->flash('message', 'data berhasil di simpan');

        return redirect()->back();
    }

    public function submitPkTahunan(SasaranKegiatanIndikatorRenstra $indikator, Request $request) {
        $this->validate($request, [
            'satuan' => 'required|exists:mst_satuan,id',
            'renja' => 'required|array|min:1',
            'renja.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'renja.*.value' => 'required|string',
        ]);

        $pkRenja = $indikator->pkRenjaTahunan()->firstOrCreate([], [
            'id_satuan' => $request->get('satuan'),
        ]);

        $pkRenja->update([
            "id_satuan" => $request->get('satuan')
        ]);

        $pkRenja->targetRenja()->delete();
        collect($request->get('renja'))->each(function($renja) use($pkRenja){
            $pkRenja->targetRenja()->create([
                "id_periode_per_tahun" => $renja["id_tahun"],
                "nilai" => $renja["value"]
            ]);
        });

        session()->flash('message', 'Renja Berhasil di input');

        return redirect()->back();
    }

    public function indexTriwulan(Request $request) {

        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        Gate::authorize('access-opd', $selectedOpd);
        $triwulan = $request->get('triwulan', 1);
        $selectedTahun = PeriodePerTahun::findOrFail($request->get('tahun', periode()->periodePerTahun->first()->id));
        $satuan = Satuan::all();
        $pegawai = Pegawai::where('id_opd', $selectedOpd)->whereIn('id_eselon', [Eselon::IV_A, Eselon::IV_B, Eselon::III_A, Eselon::III_B])->get();

        $sasaranProgramRenstra = SasaranKegiatanRenstra::whereHas(
            'kegiatan.programRenstra.sasaranRenstra.renstraTujuan.opd',
            function($query) use ($selectedOpd){
                $query->where('id', $selectedOpd);
            }
        )->with([
            'kegiatan.programRenstra.sasaranRenstra.renstraTujuan.opd',
            'kegiatan.programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.tujuan.misi.periode',
            'kegiatan.programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'indikator' => function($query) use($selectedTahun, $triwulan){
                $query->with([
                    'satuan',
                    'pkRenjaTahunan.targetPk' => function($query) use($selectedTahun){
                        $query->where('tahun_id', $selectedTahun->id);
                    },
                    'pkRenjaTriwulan' => function($query) use($triwulan, $selectedTahun){
                        $query->where('triwulan', $triwulan)->where('id_periode_per_tahun', $selectedTahun->id);
                    }
                ])->whereHas('pkRenjaTahunan.targetPk', function ($query) use ($selectedTahun) {
                    $query->where('tahun_id', $selectedTahun->id);
                });
            },
        ])->paginate(10);

        return view('perjanjian-kinerja.indikator-kegiatan.triwulan', compact(
            'opds',
            'selectedOpd',
            'sasaranProgramRenstra',
            'satuan',
            'pegawai',
            'selectedTahun',
            'triwulan'
        ));
    }

    public function submitTriwulan(Request $request) {
        $this->validate($request, [
            'triwulan' => 'required|in:1,2,3,4',
            'periode' => 'required|exists:mst_periode_per_tahun,id',
            'data' => 'required|array',
            'data.*.sasaran_id' => 'required|exists:renstra_sasaran_kegiatan,id',
            'data.*.indikator' => 'nullable|array',
            'data.*.indikator.*.indikator_id' => 'required|exists:renstra_sasaran_kegiatan_indikator,id',
            'data.*.indikator.*.target_triwulan' => 'required|string',
            'data.*.indikator.*.penanggung_jawab' => 'nullable|array',
            'data.*.indikator.*.penanggung_jawab.*' => 'required|exists:mst_pegawai,id',
        ]);


        $periode = PeriodePerTahun::find($request->get('periode'));
        $triwulan = $request->get('triwulan');

        collect($request->get('data'))->each(function($sasaranData) use($periode, $triwulan) {
            $sasaran = SasaranKegiatanRenstra::with(['indikator'])->firstWhere('id', $sasaranData['sasaran_id']);

            collect($sasaranData["indikator"] ?? [] )->each(function($indikatorData) use($sasaran, $periode, $triwulan) {

                $indikator = $sasaran->indikator->firstWhere('id', $indikatorData['indikator_id']);

                $triwulanModel = $indikator->pkRenjaTriwulan()->updateOrCreate([
                    'id_periode_per_tahun' => $periode->id,
                    'triwulan' => $triwulan
                ], [
                    "target" => $indikatorData["target_triwulan"]
                ]);

                $triwulanModel->penanggungJawab()->sync($indikatorData["penanggung_jawab"] ?? []);

            });
        });



        session()->flash('message', 'data berhasil di simpan');

        return redirect()->back();
    }


}
