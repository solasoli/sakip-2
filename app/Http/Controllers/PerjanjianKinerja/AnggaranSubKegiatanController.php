<?php

namespace App\Http\Controllers\PerjanjianKinerja;

use App\Http\Controllers\Controller;
use App\Models\KeuanganTahunan;
use App\Models\Renstra\Kegiatan\KegiatanRenstra;
use App\Models\Renstra\Program\ProgramRenstra;
use App\Models\Renstra\SubKegiatan\SubKegiatanRenstra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class AnggaranSubKegiatanController extends Controller
{
    public function index(Request $request) {
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        Gate::authorize('access-opd', $selectedOpd);
        $periode = periode();

        $kegiatans = KegiatanRenstra::with([
            'mstKegiatan',
            'programRenstra.programRpjmd.mstProgram',
            'programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.tujuan.misi.periode.periodePerTahun',
            'subKegiatan.mstSubKegiatan',
            'subKegiatan.anggaranPk',
            'subKegiatan.anggaranRenstra',
            'subKegiatan.anggaranRenja',
        ])->whereHas('programRenstra.sasaranRenstra.renstraTujuan', function($query) use ($selectedOpd){
            $query->where('id_mst_opd', $selectedOpd);
        })->paginate(10);

        return view('perjanjian-kinerja.anggaran-sub-kegiatan', compact('opds', 'selectedOpd', 'kegiatans', 'periode'));
    }

    public function store(SubKegiatanRenstra $subKegiatan, Request $request) {
        $opdId = $subKegiatan->kegiatan->programRenstra->sasaranRenstra->renstraTujuan->id_mst_opd;
        Gate::authorize('access-opd', $opdId);

        $this->validate($request, [
            'anggaran' => 'required|array|min:1',
            'anggaran.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'anggaran.*.value' => 'nullable|integer',
        ]);



        $subKegiatan->anggaranPk()->delete();
        foreach ($request->get('anggaran') as $anggaran) {
            $subKegiatan->anggaranPk()->create([
                'assignable_kind' => KeuanganTahunan::KIND_ANGGARAN_PK,
                'nilai' => $anggaran['value'] ?? 0,
                'id_periode_per_tahun' => $anggaran['id_tahun']
            ]);
        }



        flashSuccess('Berhasil Mengubah Anggaran PK');

        return redirect()->back();
    }
}
