<?php

namespace App\Http\Controllers\PerjanjianKinerja;

use App\Http\Controllers\Controller;
use App\Models\PkRenjaTahunan;
use App\Models\Renstra\Kegiatan\SasaranKegiatanIndikatorRenstra;
use App\Models\Renstra\Program\SasaranProgramIndikatorRenstra;
use App\Models\Renstra\SasaranRenstraIndikator;
use App\Models\Renstra\SubKegiatan\SasaranSubKegiatanOutputRenstra;
use App\Models\TargetPkRenjaTahunan;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PkRenjaTahunanController extends Controller
{
    public function store(Request $request)
    {
        $allowedTypes = [
            SasaranRenstraIndikator::class,
            SasaranProgramIndikatorRenstra::class,
            SasaranKegiatanIndikatorRenstra::class,
            SasaranSubKegiatanOutputRenstra::class
        ];

        $this->validate($request, [
            'id' => 'nullable|exists:pk_renja_tahunan,id',
            'pk_type' => [
                'required',
                Rule::in($allowedTypes)
            ],
            'pk_id' => [
                'required',
                'numeric',
                function ($attribute, $value, $fail) use ($request, $allowedTypes) {
                    if (in_array($request->pk_type, $allowedTypes)) {
                        $class = $request->pk_type;
                        $isExist = $class::where('id', $value)->count();

                        if (!$isExist) {
                            $fail('pk_id fails');
                        }
                        return;
                    }

                    $fail('pk_id fails');
                }
            ],
            'atasan_id' => 'required|exists:mst_pegawai,id',
            'tahun_id' => 'required|exists:mst_periode_per_tahun,id',
            'satuan_id' => 'required|exists:mst_satuan,id',
            'target' => 'required|string',
            'penanggung_jawab' => 'required|array|min:1',
            'penanggung_jawab.*' => 'required|exists:mst_pegawai,id'
        ]);

        /** @var PkRenjaTahunan */
        $pkRenjaTahunan = $request->id == null
            ? new PkRenjaTahunan()
            : PkRenjaTahunan::where('id', $request->id)->firstOrFail();

        $pkRenjaTahunan->pk_type = $request->pk_type;
        $pkRenjaTahunan->pk_id = $request->pk_id;
        $pkRenjaTahunan->save();

        /** @var TargetPkRenjaTahunan */
        $target = $pkRenjaTahunan->targetPk()->where('tahun_id', $request->tahun_id)->first() ?? new TargetPkRenjaTahunan([
            'pk_renja_tahunan_id' => $pkRenjaTahunan->id
        ]);

        $target->atasan_id = $request->atasan_id;
        $target->tahun_id = $request->tahun_id;
        $target->satuan_id = $request->satuan_id;
        $target->target = $request->target;
        $target->save();

        $target->penanggungJawab()->sync($request->penanggung_jawab);

        flashSuccess('Data berhasil di simpan');

        return redirect()->back();
    }
}
