<?php

namespace App\Http\Controllers\PerjanjianKinerja;

use App\Http\Controllers\Controller;
use App\Models\Eselon;
use App\Models\Pegawai;
use App\Models\PeriodePerTahun;
use App\Models\Renstra\SubKegiatan\SasaranSubKegiatanOutputRenstra;
use App\Models\Renstra\SubKegiatan\SasaranSubKegiatanRenstra;
use App\Models\Satuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Models\PkRenjaTahunan;
class OutputSubKegiatanController extends Controller
{
    public function indexTahunan(Request $request) {
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        Gate::authorize('access-opd', $selectedOpd);
        $satuan = Satuan::all();
        $periode = periode();
        $pegawai = Pegawai::where('id_opd', $selectedOpd)->whereIn('id_eselon', [Eselon::IV_A, Eselon::IV_B])->get();
        $atasanOption = Pegawai::where('id_opd', $selectedOpd)->whereIn('id_eselon', [Eselon::IV_A, Eselon::III_A, Eselon::III_B])->get();

        $sasaranSubKegiatan = SasaranSubKegiatanRenstra::whereHas(
            'subKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.opd',
            function($query) use ($selectedOpd){
                $query->where('id', $selectedOpd);
            }
        )->with([
            'subKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.tujuan.misi',
            'subKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
        ])->paginate(10);

        return view('perjanjian-kinerja.output-sub-kegiatan.tahunan', compact('opds', 'selectedOpd', 'sasaranSubKegiatan', 'satuan', 'periode', 'pegawai', 'atasanOption'));
    }

    public function submitTahunan(Request $request)
    {
        $this->validate($request, [
            'data' => 'required|array',
            'data.*.sasaran_id' => 'required|exists:renstra_sasaran_kegiatan,id',
            'data.*.indikator' => 'nullable|array',
            'data.*.indikator.*.indikator_id' => 'required|exists:renstra_sasaran_sub_kegiatan,id',
            'data.*.indikator.*.atasan' => 'nullable|exists:mst_pegawai,id',
            'data.*.indikator.*.penanggung_jawab' => 'nullable|array',
            'data.*.indikator.*.penanggung_jawab.*' => 'required|exists:mst_pegawai,id',
        ]);

        collect($request->get('data'))->each(function ($sasaranData) {
            $sasaran = SasaranSubKegiatanRenstra::with(['output'])->firstWhere('id', $sasaranData['sasaran_id']);

            collect($sasaranData["indikator"] ?? [])->each(function ($indikatorData) use ($sasaran) {

                if (!isset($indikatorData['penanggung_jawab'])) return;

                /** @var  SasaranSubKegiatanOutputRenstra */
                $indikator = $sasaran->output->firstWhere('id', $indikatorData['indikator_id']);

                /** @var PkRenjaTahunan */
                $tahunanModel = $indikator->pkRenjaTahunan;

                $tahunanModel->atasan()->associate($indikatorData['atasan']);
                $tahunanModel->save();

                $tahunanModel->penanggungJawab()->sync($indikatorData["penanggung_jawab"] ?? []);
            });
        });

        session()->flash('message', 'data berhasil di simpan');

        return redirect()->back();
    }

    public function submitPkTahunan(SasaranSubKegiatanOutputRenstra $output, Request $request) {
        $this->validate($request, [
            'satuan' => 'required|exists:mst_satuan,id',
            'renja' => 'required|array|min:1',
            'renja.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'renja.*.value' => 'required|string',
        ]);

        $pkRenja = $output->pkRenjaTahunan()->firstOrCreate([], [
            'id_satuan' => $request->get('satuan'),
        ]);

        $pkRenja->update([
            "id_satuan" => $request->get('satuan')
        ]);

        $pkRenja->targetRenja()->delete();
        collect($request->get('renja'))->each(function($renja) use($pkRenja){
            $pkRenja->targetRenja()->create([
                "id_periode_per_tahun" => $renja["id_tahun"],
                "nilai" => $renja["value"]
            ]);
        });

        session()->flash('message', 'Renja Berhasil di input');

        return redirect()->back();
    }

    public function indexTriwulan(Request $request) {

        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        Gate::authorize('access-opd', $selectedOpd);
        $triwulan = $request->get('triwulan', 1);
        $selectedTahun = PeriodePerTahun::findOrFail($request->get('tahun', periode()->periodePerTahun->first()->id));
        $satuan = Satuan::all();
        $pegawai = Pegawai::where('id_opd', $selectedOpd)->whereIn('id_eselon', [Eselon::IV_A, Eselon::IV_B])->get();

        $sasaranSubKegiatan = SasaranSubKegiatanRenstra::whereHas(
            'subKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.opd',
            function($query) use ($selectedOpd){
                $query->where('id', $selectedOpd);
            }
        )->with([
            'subKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.opd',
            'subKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.tujuan.misi',
            'subKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'output' => function($query) use($selectedTahun, $triwulan){
                $query->with([
                    'satuan',
                    'pkRenjaTahunan.targetPk' => function($query) use($selectedTahun){
                        $query->where('tahun_id', $selectedTahun->id);
                    },
                    'pkRenjaTriwulan' => function($query) use($triwulan, $selectedTahun){
                        $query->where('triwulan', $triwulan)->where('id_periode_per_tahun', $selectedTahun->id);
                    }
                ])->whereHas('pkRenjaTahunan.targetPk', function ($query) use ($selectedTahun) {
                    $query->where('tahun_id', $selectedTahun->id);
                });
            },
        ])->paginate(10);

        return view('perjanjian-kinerja.output-sub-kegiatan.triwulan', compact(
            'opds',
            'selectedOpd',
            'sasaranSubKegiatan',
            'satuan',
            'pegawai',
            'selectedTahun',
            'triwulan'
        ));
    }

    public function submitTriwulan(Request $request) {

        $this->validate($request, [
            'triwulan' => 'required|in:1,2,3,4',
            'periode' => 'required|exists:mst_periode_per_tahun,id',
            'data' => 'required|array',
            'data.*.sasaran_id' => 'required|exists:renstra_sasaran_sub_kegiatan,id',
            'data.*.output' => 'nullable|array',
            'data.*.output.*.output_id' => 'required|exists:renstra_sasaran_sub_kegiatan_output,id',
            'data.*.output.*.target_triwulan' => 'required|string',
            'data.*.output.*.penanggung_jawab' => 'nullable|array',
            'data.*.output.*.penanggung_jawab.*' => 'required|exists:mst_pegawai,id',
        ]);


        $periode = PeriodePerTahun::find($request->get('periode'));
        $triwulan = $request->get('triwulan');

        collect($request->get('data'))->each(function($sasaranData) use($periode, $triwulan) {
            $sasaran = SasaranSubKegiatanRenstra::with(['output'])->firstWhere('id', $sasaranData['sasaran_id']);

            collect($sasaranData["output"] ?? [])->each(function($outputData) use($sasaran, $periode, $triwulan) {

                $output = $sasaran->output->firstWhere('id', $outputData['output_id']);

                $triwulanModel = $output->pkRenjaTriwulan()->updateOrCreate([
                    'id_periode_per_tahun' => $periode->id,
                    'triwulan' => $triwulan
                ], [
                    "target" => $outputData["target_triwulan"]
                ]);

                $triwulanModel->penanggungJawab()->sync($outputData["penanggung_jawab"] ?? []);
            });
        });



        session()->flash('message', 'data berhasil di simpan');

        return redirect()->back();
    }
}
