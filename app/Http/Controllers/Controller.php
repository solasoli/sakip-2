<?php

namespace App\Http\Controllers;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\PeriodePerTahun;
use App\Models\PeriodeWalkot;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\ValidationException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $id_periode;
    protected $id_tahun;

    public function __construct(PeriodeWalkot $periode, PeriodePerTahun $tahun)
    {
        $periode = periode();

        if($periode) $this->id_periode = $periode->id;
    }

    public function getPeriode()
    {
        $validate_periode = PeriodeWalkot::where('is_selected', 1)->get();
        if(!is_null($validate_periode->first())) {
            $getPeriode = PeriodeWalkot::find($this->id_periode);
            $periode = $getPeriode->first()->dari . ' - ' . $getPeriode->first()->sampai;

            return $periode;
        }
    }

    public function validateParentModelDeletion(ChildrenDataCheckerInterface $data, string $dataName, array $dependedData) {
        if($data->hasMandatoryChildren()) {
            // https://stackoverflow.com/questions/8586141/implode-an-array-with-and-add-and-before-the-last-item
            $dependedString = join(' dan ', array_filter(array_merge(array(join(', ', array_slice($dependedData, 0, -1))), array_slice($dependedData, -1)), 'strlen'));

            throw ValidationException::withMessages(["$dataName tidak bisa di hapus, mohon hapus $dependedString terlebih dahulu"]);
        }
    }
}
