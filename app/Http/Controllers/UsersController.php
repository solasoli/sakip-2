<?php

namespace App\Http\Controllers;

use App\Constants\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;
use Yajra\DataTables\Html\Builder;

class UsersController extends Controller
{
    public function index(Builder $builder) {

        $builder->ajax([
            'url' => route('users.data'),
            'type' => 'GET',
        ])->addAction();

        $builder->columns([
            ['data' => 'name', 'name' => 'name', 'title' => 'Nama'],
            ['data' => 'email', 'name' => 'email', 'title' => 'Email'],
            ['data' => 'username', 'name' => 'username', 'title' => 'Username'],
            ['data' => 'role', 'name' => 'role', 'title' => 'Role'],
            [
                'data' => 'opd',
                'name' => 'opd',
                'title' => 'Opd',
                'render' => 'data == null ? "-" : data.name',
                'searchable' => false
            ],
        ])->addAction();

        return view('users.index', ["html" => $builder]);
    }

    public function data() {
        $model = User::with('opd');

        return DataTables::eloquent($model)
            ->addColumn('action', function($row) {
                $editButton = '
                    <a href='.route('users.edit', ['user' => $row->id]).'>
                        <button class="btn-orange"><i class="fa fa-pencil"></i> Ubah</button>
                    </a>';
                $deleteButton = '
                    <a href='.route('users.destroy', ['user' => $row->id]).' onclick="return confirm(`Anda yakin menghapus user '.$row->username.' ?`)">
                        <button class="btn-red"><i class="fa fa-trash"></i> Hapus</button>
                    </a>
                ';
                $resetButton = '
                    <a href='.route('users.reset_password', ['user' => $row->id]).' onclick="return confirm(`Anda yakin mereset password user '.$row->username.' ?`)">
                        <button class="btn-info"><i class="fa fa-key"></i> Reset password</button>
                    </a>
                ';
                return $editButton.$deleteButton.$resetButton;
            })
            ->toJson();
    }

    public function create() {
        $roles = Role::get();
        return view('users.create', [
            'opds' => getAllowedOpds(),
            'roles' => $roles
        ]);
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'username' => 'required|string|unique:users,username',
            'opd' => 'required|exists:mst_opd,id',
            'role' => [
                'required',
                Rule::in([
                    Role::ADMIN_OPD,
                    Role::SUPER_ADMIN,
                ])
            ]
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'id_opd' => $request->opd,
            'role' => $request->role,
            'password' => Hash::make("password"),
        ]);

        flashSuccess('Data user berhasil di buat');

        return redirect()->route('users.index');
    }

    public function edit(User $user) {
        $roles = Role::get();
        return view('users.edit', [
            'opds' => getAllowedOpds(),
            'roles' => $roles,
            'user' => $user
        ]);
    }

    public function update(User $user, Request $request) {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'username' => [
                'required',
                'string',
                Rule::unique('users', 'username')->ignore($user->id)
            ],
            'opd' => 'required|exists:mst_opd,id',
            'role' => [
                'required',
                Rule::in([
                    Role::ADMIN_OPD,
                    Role::SUPER_ADMIN,
                ])
            ]
        ]);

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'username' => $request->username,
            'id_opd' => $request->opd,
            'role' => $request->role,
        ]);

        flashSuccess('Data user berhasil di ubah');

        return redirect()->back();
    }

    public function destroy(User $user) {
        $user->delete();
        flashSuccess("Data user $user->name berhasil di hapus");

        return redirect()->route('users.index');
    }

    public function resetPassword(User $user) {
        $user->password = Hash::make('password');
        $user->save();

        flashSuccess('Password berhasil di reset');

        return redirect()->back();
    }
}
