<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\Kegiatan;
use App\Models\Program;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
 
class KegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('mst_kegiatan as mk')
            ->select(DB::raw('mk.name as kegiatan, mk.id as id_kegiatan, mk.kode_kegiatan, mp.name as program, mp.id as id_program'))
            ->join('mst_program as mp', 'mp.id', '=', 'mk.id_program')
            ->get();

        $program = Program::all();

        return view('master/kegiatan', [
            'data' => $data,
            'program' => $program
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kegiatan' => 'required',
            'program' => 'required',
            'kode_kegiatan' => 'required',
        ]);

        $data = new Kegiatan;
        $data->name = $request->kegiatan;
        $data->id_program = $request->program;
        $data->kode_kegiatan = $request->kode_kegiatan;
        $data->save();

        return redirect()
            ->back()
            ->with([
                'successAdd' => 'Data berhasil di simpan',
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function show(Kegiatan $kegiatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function edit(Kegiatan $kegiatan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Kegiatan::where('id', $id)->update([
            'kode_kegiatan' => $request->kode_kegiatan,
            'name' => $request->kegiatan,
            'id_program' => $request->program
        ]);

        return redirect()->back()->with([
            'successEdit' => 'Data berhasil diubah',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kegiatan  $kegiatan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kegiatan::find($id)->delete();

        return redirect()
            ->back()
            ->with([
                'successDel' => 'Data berhasil dihapus',
            ]);
    }

    public function datatableKegiatan()
    {
        $data = DB::table('mst_kegiatan as mk')
        ->select(DB::raw('mk.name as kegiatan, mk.id as id_kegiatan, mk.kode_kegiatan, mp.name as program, mp.id as id_program'))
        ->join('mst_program as mp', 'mp.id', '=', 'mk.id_program')
        ->get();

        return Datatables::of($data)->make(true); 
    }
}
