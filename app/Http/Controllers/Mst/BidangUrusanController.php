<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\Mst\BidangUrusan;
use App\Models\Mst\Urusan;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class BidangUrusanController extends Controller
{
    function index()
    {
        $data = BidangUrusan::with('urusan')->get();

        $urusan = Urusan::all();
        return view('master.bidang-urusan', [
            'data' => $data,
            'urusan' => $urusan
        ]);
    }

    function store(Request $request)
    {
        $request->validate([
            'kode' => 'required',
            'bidang_urusan' => 'required',
            'urusan' => 'required'
        ]);
        $validate_kode = BidangUrusan::where('kode', $request->kode)->get();

        if (!is_null($validate_kode->first())) {
            throw ValidationException::withMessages(["Kode bidang urusan sudah ada"]);
        }

        $urusan = new BidangUrusan();
        $urusan->kode = $request->kode;
        $urusan->name = $request->bidang_urusan;
        $urusan->id_urusan = $request->urusan;
        $urusan->save();

        flashSuccess('Data berhasil di tambah');

        return redirect('/bidang-urusan');
    }

    function update(Request $request, $id)
    {
        BidangUrusan::find($id)->update([
            'kode' => $request->kode,
            'name' => $request->bidang_urusan,
            'id_urusan' => $request->urusan
        ]);

        flashSuccess('Data berhasil di ubah');

        return redirect('/bidang-urusan');
    }

    function destroy($id)
    {
        $bidangUrusan = BidangUrusan::findOrFail($id);

        $this->validateParentModelDeletion($bidangUrusan, 'Bidang Urusan', ['Master Program']);

        $bidangUrusan->delete();

        flashSuccess('Data berhasil di hapus');

        return redirect('/bidang-urusan');
    }
}
