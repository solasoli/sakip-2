<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\Mst\BidangUrusan;
use App\Models\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Program::with('bidangUrusan')->get();
        $bidang_urusan = BidangUrusan::all();

        return view('master.program', [
            'data' => $data,
            'bidang_urusan' => $bidang_urusan,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kode_program' => 'required',
            'name' => 'required',
            'bidang_urusan' => 'required'
        ]);

        $data = new Program;
        $data->kode_program = $request->kode_program;
        $data->name = $request->name;
        $data->id_bidang_urusan = $request->bidang_urusan;
        $data->save();

        flashSuccess('Data berhasil di simpan');

        return redirect()
            ->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Program::where('id', $id)->update([
            'kode_program' => $request->kode_program,
            'name' => $request->name,
            'id_bidang_urusan' => $request->bidang_urusan
        ]);

        flashSuccess('Data berhasil di ubah');

        return redirect()
            ->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $program = Program::find($id);

        //$this->validateParentModelDeletion($program, 'Program', ['Program RPJMD']);

        $program->delete();

        flashSuccess('Data Berhasil di hapus');

        return redirect()
            ->back();
    }
}
