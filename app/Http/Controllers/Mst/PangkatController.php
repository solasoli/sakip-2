<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pangkat;

class PangkatController extends Controller
{
    public function index()
    {
        $pangkat = Pangkat::all();

        return view('master.pangkat',[
            'pangkat' => $pangkat
        ]);
    }
}
