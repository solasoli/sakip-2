<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\PeriodePerTahun;
use App\Models\PeriodeWalkot;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PeriodeController extends Controller
{
    public function index()
    {
        $data = PeriodeWalkot::all();
        return view('master.periode', [
            'data' => $data
        ]);
    }

    public function store(Request $request)
    {
        $validate_add = $this->validator($request);

        $periode = new PeriodeWalkot;
        $periode->dari = $request->dari;
        $periode->sampai = $request->sampai;
        $periode->nama = $request->nama;
        $periode->save();

        $date1 = $request->dari;
        $date2 = $request->sampai;

        $range = [];
        for ($i = (int)$date1 + 1; $i <= (int)$date2; $i++) {
            $range[] = $i;
        }

        foreach ($range as $item) {
            $periodePerTahun = new PeriodePerTahun;
            $periodePerTahun->tahun = $item;
            $periodePerTahun->id_periode = $periode->id;
            $periodePerTahun->save();
        }

        flashSuccess('Data berhasil di tambah');

        return redirect()->back();
    }

    public function show(PeriodeWalkot $r)
    {
        //
    }

    public function edit(PeriodeWalkot $r)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $this->validator($request);

        $periode = PeriodeWalkot::find($id);
        $periode->dari = $request->dari;
        $periode->sampai = $request->sampai;
        $periode->nama = $request->nama;
        isset($request->plt) ? $periode->is_plt = 1 : $periode->is_plt = 0;
        $periode->save();

        flashSuccess('Data berhasil di ubah');

        return redirect()
            ->back();
    }

    public function destroy($id)
    {
        $periode = PeriodeWalkot::findOrFail($id);

        $this->validateParentModelDeletion($periode, 'Periode', ['Semua data di periode ini']);

        $periode->delete();

        flashSuccess('Data berhasil di hapus');

        return redirect()
            ->back();
    }

    static function validator($request)
    {
        $request->validate([
            'dari' => 'required',
            'sampai' => 'required',
            'nama' => 'required',
        ]);
    }
}
