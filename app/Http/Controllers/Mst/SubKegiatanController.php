<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\Kegiatan;
use App\Models\SubKegiatan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DataTables;
class SubKegiatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = DB::table('mst_sub_kegiatan as msk')
            ->select(DB::raw('msk.id as id_sub_kegiatan, msk.name as sub_kegiatan, msk.kode_sub_kegiatan, mk.id as id_kegiatan, mk.name as kegiatan'))
            ->join('mst_kegiatan as mk', 'mk.id', '=', 'msk.id_kegiatan')
            ->get();
        $kegiatan = Kegiatan::all();
        return view('master/sub-kegiatan', [
            'data' => $data,
            'kegiatan' => $kegiatan,
        ]);
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new SubKegiatan;
        $data->id_kegiatan = $request->kegiatan;
        $data->kode_sub_kegiatan = $request->kode_sub_kegiatan;
        $data->name = $request->sub_kegiatan;
        $data->save();

        return redirect()->back()->with([
            'successAdd' => 'Data berhasil disimpan',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubKegiatan  $subKegiatan
     * @return \Illuminate\Http\Response
     */
    public function show(SubKegiatan $subKegiatan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubKegiatan  $subKegiatan
     * @return \Illuminate\Http\Response
     */
    public function edit(SubKegiatan $subKegiatan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SubKegiatan  $subKegiatan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        SubKegiatan::where('id', $id)->update([
            'kode_sub_kegiatan' => $request->kode_sub_kegiatan,
            'name' => $request->sub_kegiatan,
            'id_kegiatan' => $request->kegiatan
        ]);

        return redirect()->back()->with([
            'successEdit' => 'Data berhasil diubah',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubKegiatan  $subKegiatan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SubKegiatan::find($id)->delete();
        
        return redirect()->back()->with([
            'successDel' => 'Data berhasil dihapus',
        ]);
    }

    public function datatableSubKegiatan()
    {
        $data = DB::table('mst_sub_kegiatan as msk')
                ->select(DB::raw('msk.id as id_sub_kegiatan, msk.kode_sub_kegiatan, msk.name as sub_kegiatan,  mk.id as id_kegiatan, mk.name as kegiatan'))
                ->join('mst_kegiatan as mk', 'mk.id', '=', 'msk.id_kegiatan')
                ->get();

        return Datatables::of($data)->make(true); 
    }
}
