<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables as DataTables;
use DB;

class PegawaiController extends Controller
{
    public function index()
    {

        return view('master.pegawai');
    }

    public function datatablePegawai(Request $request)
    {
        $data = Pegawai::with(['eselon', 'opd', 'pangkat', 'pangkatGolongan']);

        return DataTables::of($data)
            ->filter(function ($query) use ($request) {
                $keyword = $request->search['value'] ?? null;

                if (!$keyword) {
                    return;
                }

                $query->where('nip', 'like', "%$keyword%")
                    ->orWhere('nama', 'like', "%$keyword%")
                    ->orWhere('gelar_depan', 'like', "%$keyword%")
                    ->orWhere('gelar_belakang', 'like', "%$keyword%")
                    ->orWhere('jabatan', 'like', "%$keyword%")
                    ->orWhereHas('opd', function ($query) use ($keyword) {
                        $query->where('name', 'like', "%$keyword%");
                    })
                    ->orWhereHas('eselon', function ($query) use ($keyword) {
                        $query->where('nama', 'like', "%$keyword%");
                    });
            })
            ->make(true);
    }
}
