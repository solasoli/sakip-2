<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Eselon;

class EselonController extends Controller
{
    public function index()
    {
        $eselon = Eselon::all();

        return view('master.eselon',[
            'eselon' => $eselon
        ]);
    }
}
