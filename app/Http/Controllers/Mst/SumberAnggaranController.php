<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\SumberAnggaran;
use Illuminate\Http\Request;

class SumberAnggaranController extends Controller
{
    public function index()
    {
        $data = SumberAnggaran::all();

        return view('master.sumber-anggaran', [
            'data' => $data
        ]);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $data = new SumberAnggaran;
        $data->name = $request->sumber_anggaran;
        $data->save();

        flashSuccess('Data Berhasil di tambahkan');

        return redirect()->back();
    }


    public function show(SumberAnggaran $sumberAnggaran)
    {
        //
    }


    public function edit(SumberAnggaran $sumberAnggaran)
    {
        //
    }

    public function update(Request $request, $id)
    {
        SumberAnggaran::where('id', $id)->update([
            'name' => $request->sumber_anggaran
        ]);

        flashSuccess('Data berhasil di ubah');

        return redirect()->back();
    }


    public function destroy($id)
    {
        $sumberAnggaran = SumberAnggaran::findOrFail($id);

        $this->validateParentModelDeletion($sumberAnggaran, 'Sumber Anggaran', [
            'Program RPJMD',
            'Kegiatan Renstra',
            'Sub Kegiatan Renstra',
        ]);

        $sumberAnggaran->delete();

        flashSuccess('Data berhasil di hapus');

        return redirect()->back();
    }
}
