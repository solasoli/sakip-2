<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\Mst\Urusan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class UrusanController extends Controller
{
    function index()
    {
        $data = Urusan::all();
        return view('master.urusan', [
            'data' => $data,
        ]);
    }

    function store(Request $request)
    {
        $request->validate([
            'kode' => 'required',
            'urusan' => 'required'
        ]);

        $validate_kode = Urusan::where('kode', $request->kode)->get();

        if (!is_null($validate_kode->first())) {
            throw ValidationException::withMessages(["Kode urusan sudah ada"]);
        }

        $urusan = new Urusan;
        $urusan->kode = $request->kode;
        $urusan->name = $request->urusan;
        $urusan->save();

        flashSuccess('Data berhasil di tambah');

        return redirect('/urusan');
    }

    function update(Request $request, $id)
    {
        Urusan::find($id)->update([
            'kode' => $request->kode,
            'name' => $request->urusan,
        ]);

        flashSuccess('Data berhasil di ubah');

        return redirect('/urusan');
    }

    function destroy($id)
    {
        $urusan = Urusan::find($id);

        $this->validateParentModelDeletion($urusan, 'Urusan', ['Bidang Urusan']);

        $urusan->delete();

        flashSuccess('Data berhasil di hapus');

        return redirect('/urusan');
    }
}
