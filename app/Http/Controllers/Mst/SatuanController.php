<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\Satuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class SatuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('mst_satuan as ms')
            ->select(DB::raw('ms.name as satuan, ms.id as id_satuan'))
            ->get();

        return view('master.satuan', [
            'data' => $data,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'satuan' => ['required'],
        ]);

        $validasi = DB::table('mst_satuan as ms')
            ->select(DB::raw('ms.name as satuan'))
            ->where('name', $request->satuan)
            ->get();

        if($validasi->count() < 1) {
            $data = new Satuan;
            $data->name = $request->satuan;
            $data->save();

            flashSuccess('Data berhasil di simpan');

            return redirect()->back();
        }else {
            throw ValidationException::withMessages(["Data gagal di simpan / Data sudah ada !"]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Satuan  $satuan
     * @return \Illuminate\Http\Response
     */
    public function show(Satuan $satuan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Satuan  $satuan
     * @return \Illuminate\Http\Response
     */
    public function edit(Satuan $satuan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Satuan  $satuan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Satuan::where('id', $id)->update([
            'name' => $request->satuan,
        ]);

        flashSuccess('Data Berhasil di simpan');

        return redirect()
            ->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Satuan  $satuan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $satuan = Satuan::findOrFail($id);

        $this->validateParentModelDeletion($satuan, 'Satuan', [
            'Rpjmd Tujuan Indikator',
            'Rpjmd Sasaran Detail',
            'Renstra Sasaran Indikator',
            'Renstra Sasaran Kegiatan Indikator',
            'Renstra Sasaran Sub Kegiatan Indikator',
            'Renstra Tujuan Indikator',
            'Renstra Program Indikator',
        ]);

        $satuan->delete();

        flashSuccess('Data Berhasil di hapus');

        return redirect()
            ->back();
    }
}
