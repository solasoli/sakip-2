<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use App\Models\PrioritasPembangunan;
use Illuminate\Http\Request;

class PrioritasPembangunanController extends Controller
{
    function index()
    {
        $data = PrioritasPembangunan::all();
        return view('master/prioritas-pembangunan', [
            'data' => $data
        ]);
    }

    public function store(Request $request)
    {
        $data = new PrioritasPembangunan;
        $data->name = $request->prioritas_pembangunan;
        $data->save();

        return redirect()->back()->with([
            'successAdd' => 'Data berhasil di simpan',
        ]);
    }

    public function update(Request $request, $id)
    {
        PrioritasPembangunan::where('id', $id)->update([
            'name' => $request->prioritas_pembangunan,
        ]);

        return redirect()->back()->with([
            'successEdit' => 'Data berhasil diubah',
        ]);
    }

    public function destroy($id)
    {
        PrioritasPembangunan::find($id)->delete();
        return redirect()->back()->with([
            'successDel' => 'Data berhasil dihapus',
        ]);
    }
}
