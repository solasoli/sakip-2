<?php

namespace App\Http\Controllers\Mst;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\PangkatGolongan;

class PangkatGolonganController extends Controller
{
    public function index()
    {
        $pangkatGolongan = PangkatGolongan::all();

        return view('master.pangkat-golongan',[
            'pangkatGolongan' => $pangkatGolongan
        ]);
    }
}
