<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function changePasswordIndex(){
        return view('ganti-password', [
            'user' => auth()->user()
        ]);
    }

    public function changePasswordUpdate(Request $request) {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|confirmed'
        ]);

        /** @var \App\Models\User */
        $user = auth()->user();

        if(!Hash::check($request->old_password, $user->password)) {
            return redirect()->back()->withErrors(['old_password' => ['password lama salah']]);
        }

        $user->password = Hash::make($request->password);
        $user->save();

        flashSuccess('Password berhasil di ganti');

        return redirect()->back();
    }
}
