<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', 0); //3 minutes
ini_set("memory_limit", "-1");

use Illuminate\Http\Request;

use App\Models\Eselon;
use App\Models\Opd;
use App\Models\Pangkat;
use App\Models\PangkatGolongan;
use App\Models\Pegawai;
use App\Models\User;
use App\Models\UserRole;
use DB;
use Illuminate\Support\Facades\Hash;

class SyncRestfulApiController extends Controller
{
  public function syncPegawai()
  {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://simpeg.bogorkab.go.id/rest_ci/dt_Eselon');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($ch);
    $result = json_decode($response);
    echo "<pre>";

    //eselon
    $eselon = [];
    $get_eselon = Eselon::all();
    foreach ($get_eselon as $idx => $row) {
      $eselon[$row->nama] = $row->id;
    }


    //opd
    $opd = [];
    $get_opd = Opd::all();
    foreach ($get_opd as $idx => $row) {
      $opd[$row->name] = $row->id;
    }

    $data = $result;

    foreach ($data as $idx => $row) {
      //insert eselon
      if (!isset($eselon[$row->eselon]) && $row->eselon != null && $row->eselon != 'Z') {
        $new_data = new Eselon;
        $new_data->nama = $row->eselon;
        $new_data->save();

        $eselon[$row->eselon] = $new_data->id;
      }

      //insert opd
      if (!isset($opd[$row->unor]) && $row->unor != null) {
        $new_data = new Opd;
        $new_data->name = $row->unor;
        $new_data->save();

        $opd[$row->unor] = $new_data->id;
      }

      if (isset($eselon[$row->eselon]) && isset($opd[$row->unor])) {
        //insert pegawai
        $find_pegawai = Pegawai::where('nip', $row->nip)->first();
        $id_pegawai = isset($find_pegawai->id) ? $find_pegawai->id : 0;
        $new_pegawai = Pegawai::findOrNew($id_pegawai);
        $new_pegawai->nip = $row->nip;
        $new_pegawai->nama = $row->nama;
        $new_pegawai->gelar_depan = $row->glrd ?? '';
        $new_pegawai->gelar_belakang = $row->glrb ?? '';
        $new_pegawai->id_eselon = $row->eselon != 'Z' && isset($eselon[$row->eselon]) ? $eselon[$row->eselon] : null;
        $new_pegawai->jabatan = $row->jabatan ?? '';
        $new_pegawai->id_opd = $row->unor ? $opd[$row->unor] : null;
        $new_pegawai->save();

        // echo "Fetching data pegawai<br>";
        // echo "NIP: ". $row->nip ." - " . $row->nama ."<br>";
        // echo "Eselon: " . $row->eselon."<br>".
        // "Pangkat: " . $row->pangkat. "<br>".
        // "Pangkat Golongan: ". $row->pangkat_gol."<br>".
        // "Jabatan: ". $row->jabatan."<br>".
        // "Unit: ". $row->unit."<br>".
        // "OPD: ". $row->opd."<br>".
        // "====================================================================================================<br>";
      } else {
        echo "Pegawai Tidak Termasukan ! NIP: " . $row->nip . " - " . $row->nama . "<br>";
        echo "Eselon: " . $row->eselon . "<br>" .
          "Gelar Depan : " . $row->glrd . "<br>" .
          "Gelar Belakang : " . $row->glrb . "<br>" .
          "Jabatan: " . $row->jabatan . "<br>" .
          "OPD: " . $row->opd . "<br>" .
          "====================================================================================================<br>";
      }
    }
  }


  public function restApiPegawai()
  {
    $pegawai = DB::table('mst_pegawai')->get();;
    return response()->json([
      'success' => true,
      'message' => 'List Data Pegawai',
      'data'    => $pegawai
    ]);
  }

  public function restApiOpd()
  {
    $opd = DB::table('mst_opd')->get();
    return response()->json([
      'success' => true,
      'message' => 'List Data Opd',
      'data'    => $opd
    ], 200);
  }
}
