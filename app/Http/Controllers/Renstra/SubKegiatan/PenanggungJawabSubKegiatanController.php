<?php

namespace App\Http\Controllers\Renstra\SubKegiatan;

use App\Http\Controllers\Controller;
use App\Models\Opd;
use App\Models\Renstra\SubKegiatan\SasaranSubKegiatanOutputPenanggungJawabRenstra;
use App\Models\Renstra\SubKegiatan\SasaranSubKegiatanOutputRenstra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class PenanggungJawabSubKegiatanController extends Controller
{
    public function index(Request $request) {
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        Gate::authorize('access-opd', $selectedOpd);
        $sasaranSubKegiatanOutput = SasaranSubKegiatanOutputRenstra::with([
            'sasaranSubKegiatan.subKegiatan.kegiatan',
            'sasaranSubKegiatanOutputPenanggungJawab.pegawai',
            'sasaranSubKegiatanOutputPenanggungJawab.target',
            'target'
        ])->whereHas(
            'sasaranSubKegiatan.subKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.opd',
            function ($query) use ($selectedOpd) {
                $query->where('id', $selectedOpd);
            }
        )->get();

        $periode = periode();

        return view('renstra.sub-kegiatan.penanggung-jawab.index', compact(
            'sasaranSubKegiatanOutput',
            'selectedOpd',
            'opds',
            'periode'
        ));
    }

    public function massUpdate(Request $request) {
        $this->validate($request, [
            'output' => 'required|array|min:1',
            'output.*.id' => 'required|exists:renstra_sasaran_sub_kegiatan_output,id',
            'output.*.penanggung_jawab' => 'nullable|array|min:1',
            'output.*.penanggung_jawab.*.id' => 'nullable|exists:renstra_sasaran_sub_kegiatan_output_penanggung_jawab,id',
            'output.*.penanggung_jawab.*.pegawai_id' => 'nullable|exists:mst_pegawai,id',
            'output.*.penanggung_jawab.*.target' => 'array',
            'output.*.penanggung_jawab.*.target.*.periode_id' => 'exists:mst_periode_per_tahun,id',
            'output.*.penanggung_jawab.*.target.*.target' => 'nullable|string',
        ]);



        $parameters = collect($request->get('output'));
        $outputIds = $parameters->map(function($output) {return $output['id'];});
        $outputs = SasaranSubKegiatanOutputRenstra::whereIn('id', $outputIds)->get();

        foreach ($outputs as $output) {
            /** @var SasaranSubKegiatanOutputRenstra $output  */
            $data = $parameters->where('id', $output->id)->first();
            $penanggungJawabCollection = collect($data['penanggung_jawab'] ?? [])
                ->filter(function($penanggungJawab) {
                    return isset($penanggungJawab['pegawai_id']);
                });

            $penanggungJawabToDelete = $output->sasaranSubKegiatanOutputPenanggungJawab()
                ->whereNotIn('id', $penanggungJawabCollection->pluck('id'))->get();

            foreach ($penanggungJawabToDelete as $penanggungJawab) {

                /** @var SasaranSubKegiatanOutputPenanggungJawabRenstra $penanggungJawab */
                $penanggungJawab->target()->delete();
                $penanggungJawab->delete();
            }

            foreach ($penanggungJawabCollection as $penanggungJawab) {

                $targetCollection = $penanggungJawab['target'];
                /** @var SasaranSubKegiatanOutputPenanggungJawabRenstra */
                $pivot = $penanggungJawab['id'] == null
                    ? SasaranSubKegiatanOutputPenanggungJawabRenstra::create([
                        'id_pegawai' => $penanggungJawab['pegawai_id'],
                        'id_renstra_sasaran_sub_kegiatan_output' => $output->id
                    ])
                    : $output->sasaranSubKegiatanOutputPenanggungJawab()->updateOrCreate(
                        ['id' => $penanggungJawab['id']],
                        ['id_pegawai' => $penanggungJawab['pegawai_id']]
                    );

                foreach ($targetCollection as $target) {
                    $pivot->target()->updateOrCreate(
                        ['id_periode_per_tahun' => $target['periode_id']],
                        ['target' => $target['target']]
                    );
                }
            }
        };

        flashSuccess();



        return redirect()->back();
    }
}
