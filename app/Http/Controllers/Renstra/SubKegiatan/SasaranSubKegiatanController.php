<?php

namespace App\Http\Controllers\Renstra\SubKegiatan;

use App\Http\Controllers\Controller;
use App\Models\Opd;
use App\Models\Renstra\SubKegiatan\SasaranSubKegiatanRenstra;
use App\Models\Satuan;
use App\Transformers\SatuanTransformer;
use App\Transformers\SubKegiatanRenstraTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SasaranSubKegiatanController extends Controller
{
    public function index(Request $request) {
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        Gate::authorize('access-opd', $selectedOpd);
        $periodeWalkot = periode();
        $sasaranSubKegiatan = SasaranSubKegiatanRenstra::whereHas(
            'subKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.opd',
            function($query) use ($selectedOpd) {
                $query->where('id', $selectedOpd);
            }
        )->get();

        return view('renstra.sub-kegiatan.sasaran-sub-kegiatan.index', compact(
            'opds',
            'selectedOpd',
            'periodeWalkot',
            'sasaranSubKegiatan'
        ));
    }

    public function create() {
        $opds = getAllowedOpds();
        $satuan = fractal(Satuan::all(), new SatuanTransformer);
        return view('renstra.sub-kegiatan.sasaran-sub-kegiatan.create', compact('opds', 'satuan'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            "sub_kegiatan" => 'required|exists:renstra_sub_kegiatan,id',
            "sasaran_sub_kegiatan" => 'required|string',
            "output" => 'required|array|min:1',
            "output.*.nama" => "required|string",
            "output.*.satuan" => "required|exists:mst_satuan,id",
            "output.*.is_pk" => "in:on",
            "output.*.is_iku" => "in:on",
            "output.*.cara_pengukuran" => "required|string",
            "output.*.target_awal" => "required|string",
            "output.*.target_akhir" => "required|string",
            "output.*.target" => "required|array|min:1",
            "output.*.target.*.id_tahun" => "required|exists:mst_periode_per_tahun,id",
            "output.*.target.*.value" => "required|string",
        ]);


        $sasaranSubKegiatanRenstra = SasaranSubKegiatanRenstra::create([
            "id_renstra_sub_kegiatan" => $request->get('sub_kegiatan'),
            "name" => $request->get('sasaran_sub_kegiatan'),
        ]);


        collect($request->get('output'))->each(function($output) use ($sasaranSubKegiatanRenstra) {
            $outputModel = $sasaranSubKegiatanRenstra->output()->create([
                'name' => $output["nama"],
                'id_satuan' => $output["satuan"],
                'is_pk' => ($output["is_pk"] ?? null) == "on",
                'is_iku' => ($output["is_iku"] ?? null) == "on",
                'cara_pengukuran' => $output["cara_pengukuran"],
                'target_awal' => $output["target_awal"],
                'target_akhir' => $output["target_akhir"],
            ]);


            collect($output["target"])->each(function($target) use ($outputModel){
                $outputModel->target()->create([
                    "id_periode_per_tahun" => $target["id_tahun"],
                    "target" => $target["value"],
                ]);
            });
        });



        return redirect()->route('renstra.sub_kegiatan.sasaran_sub_kegiatan.index');

    }

    public function submitRenja(SasaranSubKegiatanRenstra $sasaranSubKegiatan, $output, Request $request) {
        $this->validate($request, [
            'renja' => 'required|array|min:1',
            'renja.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'renja.*.value' => 'required|string',
        ]);

        $output = $sasaranSubKegiatan->output()->where('id', $output)->firstOrFail();

        $output->targetRenja()->delete();

        collect($request->get('renja'))->each(function($renja) use($output){
            $output->targetRenja()->create([
                "id_periode_per_tahun" => $renja["id_tahun"],
                "nilai" => $renja["value"]
            ]);
        });

        session()->flash('message', 'Renja Berhasil di input');

        return redirect()->back();
    }

    public function destroy(SasaranSubKegiatanRenstra $sasaranSubKegiatan) {

        $this->validateParentModelDeletion($sasaranSubKegiatan, 'sasaran sub kegiatan renstra', []);

        foreach ($sasaranSubKegiatan->output as $output) {
            $output->delete();
        }

        $sasaranSubKegiatan->delete();

        flashSuccess();

        return redirect()->back();
    }

    public function edit(SasaranSubKegiatanRenstra $sasaranSubKegiatan) {
        $opds = Opd::all();
        $satuan = fractal(Satuan::all(), new SatuanTransformer);
        $subKegiatanSelected = fractal($sasaranSubKegiatan->subKegiatan, new SubKegiatanRenstraTransformer)
            ->parseIncludes([
                'mst_sub_kegiatan',
                'tahun_periode'
            ]);
        return view('renstra.sub-kegiatan.sasaran-sub-kegiatan.edit', compact(
            'opds',
            'satuan',
            'sasaranSubKegiatan',
            'subKegiatanSelected'
        ));
    }

    public function update(SasaranSubKegiatanRenstra $sasaranSubKegiatan, Request $request) {
        $this->validate($request, [
            "sub_kegiatan" => 'required|exists:renstra_sub_kegiatan,id',
            "sasaran_sub_kegiatan" => 'required|string',
            "output" => 'required|array|min:1',
            "output.*.id" => "nullable|exists:renstra_sasaran_sub_kegiatan_output,id",
            "output.*.nama" => "required|string",
            "output.*.satuan" => "required|exists:mst_satuan,id",
            "output.*.is_pk" => "in:on",
            "output.*.is_iku" => "in:on",
            "output.*.cara_pengukuran" => "required|string",
            "output.*.target_awal" => "required|string",
            "output.*.target_akhir" => "required|string",
            "output.*.target" => "required|array|min:1",
            "output.*.target.*.id" => "nullable|exists:renstra_sasaran_sub_kegiatan_output_target,id",
            "output.*.target.*.id_tahun" => "required|exists:mst_periode_per_tahun,id",
            "output.*.target.*.value" => "required|string",
        ]);

        $sasaranSubKegiatan->update([
            "id_renstra_sub_kegiatan" => $request->get('sub_kegiatan'),
            "name" => $request->get('sasaran_sub_kegiatan'),
        ]);

        $outputItem = collect($request->get('output'));
        $outputItemids = $outputItem->map(function($output){
            return $output["id"];
        })->filter(function($output){
            return $output != null;
        });

        $sasaranSubKegiatan->output()->whereNotIn('id', $outputItemids)->delete();

        $outputItem->each(function($output) use($sasaranSubKegiatan) {
            if($output["id"] != null) {
                $outputModel = $sasaranSubKegiatan->output()->firstWhere('id', $output["id"]);
                $outputModel->update([
                    'name' => $output["nama"],
                    'id_sasaran_sub_kegiatan' => $sasaranSubKegiatan->id,
                    'id_satuan' => $output["satuan"],
                    'is_pk' => ($output["is_pk"] ?? null) == "on",
                    'is_iku' => ($output["is_iku"] ?? null) == "on",
                    'cara_pengukuran' => $output["cara_pengukuran"],
                    'target_awal' => $output["target_awal"],
                    'target_akhir' => $output["target_akhir"],
                ]);
            } else {
                $outputModel = $sasaranSubKegiatan->output()->create([
                    'name' => $output["nama"],
                    'id_sasaran_sub_kegiatan' => $sasaranSubKegiatan->id,
                    'id_satuan' => $output["satuan"],
                    'is_pk' => ($output["is_pk"] ?? null) == "on",
                    'is_iku' => ($output["is_iku"] ?? null) == "on",
                    'cara_pengukuran' => $output["cara_pengukuran"],
                    'target_awal' => $output["target_awal"],
                    'target_akhir' => $output["target_akhir"],
                ]);
            }

            $targetItems = collect($output["target"]);
            $targetItemIds = $targetItems->map(function($target){
                return $target["id"];
            })->filter(function($target){
                return $target != null;
            });

            $outputToDelete = $outputModel->target()->whereNotIn('id', $targetItemIds)->get();

            foreach($outputToDelete as $output) {
                $output->delete();
            }

            $targetItems->each(function($target) use ($outputModel){
                if($target["id"] != null ) {
                    $outputModel->target()->where('id', $target["id"])->update([
                        "id_periode_per_tahun" => $target["id_tahun"],
                        "target" => $target["value"],
                    ]);
                } else {
                    $outputModel->target()->create([
                        "id_periode_per_tahun" => $target["id_tahun"],
                        "target" => $target["value"],
                    ]);
                }
            });
        });

        session()->flash('message', 'Data berhasil di ubah');



        return redirect()->route('renstra.sub_kegiatan.sasaran_sub_kegiatan.edit', ["sasaranSubKegiatan" => $sasaranSubKegiatan->id]);
    }
}
