<?php

namespace App\Http\Controllers\Renstra\SubKegiatan;

use App\Http\Controllers\Controller;
use App\Models\KeuanganTahunan;
use App\Models\Opd;
use App\Models\PeriodePerTahun;
use App\Models\Renstra\Kegiatan\KegiatanRenstra;
use App\Models\Renstra\SubKegiatan\SubKegiatanRenstra;
use App\Models\SumberAnggaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class SubKegiatanController extends Controller
{
    public function index(Request $request) {
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        Gate::authorize('access-opd', $selectedOpd);
        $kegiatan = KegiatanRenstra::whereHas(
            'programRenstra.sasaranRenstra.renstraTujuan.opd',
            function($query) use($selectedOpd) {
                $query->where('id', $selectedOpd);
            }
        )->with([
            'mstKegiatan',
            'subKegiatan.anggaranRenstra',
            'subKegiatan.anggaranRenja',
            'subKegiatan.sumberAnggaran',
            'subKegiatan.mstSubKegiatan',
            'programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.tujuan.misi.periode.periodePerTahun',
            'programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'programRenstra.sasaranRenstra.renstraTujuan.opd',
            'programRenstra.programRpjmd.mstProgram',
        ])->get();
        $periode = periode();
        return view('renstra.sub-kegiatan.sub-kegiatan.index', compact('opds', 'selectedOpd', 'kegiatan', 'periode'));
    }

    public function create() {
        $opds = getAllowedOpds();
        $sumberAnggaran = SumberAnggaran::all();
        return view('renstra.sub-kegiatan.sub-kegiatan.create', compact('opds', 'sumberAnggaran'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'kegiatan' => 'required|exists:renstra_kegiatan,id',
            'sub_kegiatan' => 'required|exists:mst_sub_kegiatan,id',
            'anggaran' => 'required|array|min:1',
            'anggaran.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'anggaran.*.value' => 'required|integer',
            'sumber' => 'required|array|min:1',
            'sumber.*' => 'required|exists:sumber_anggaran,id',
        ]);

        $subKegiatan = SubKegiatanRenstra::create([
            'id_renstra_kegiatan' => $request->get('kegiatan'),
            'id_mst_sub_kegiatan' => $request->get('sub_kegiatan'),
        ]);

        $anggaranRenstraMap = collect($request->get('anggaran'))->map(function($anggaran) {
            return [
                'nilai' => $anggaran['value'],
                'id_periode_per_tahun' => $anggaran['id_tahun'],
                'assignable_kind' => KeuanganTahunan::KIND_ANGGARAN_RENSTRA
            ];
        });

        $anggaranRenjaMap = $subKegiatan->getTahunPeriode()->map(function(PeriodePerTahun $periode){
            return [
                'nilai' => 0,
                'id_periode_per_tahun' => $periode->id,
                'assignable_kind' => KeuanganTahunan::KIND_ANGGARAN_RENJA
            ];
        });

        $subKegiatan->anggaranRenstra()->createMany($anggaranRenstraMap);
        $subKegiatan->anggaranRenja()->createMany($anggaranRenjaMap);

        $subKegiatan->sumberAnggaran()->sync($request->get('sumber'));

        $anggaranSubKegiatan = $subKegiatan->anggaranRenstra;
        $anggaranKegiatan = $subKegiatan->kegiatan->anggaranRenstra;
        $anggaranProgram = $subKegiatan->kegiatan->programRenstra->anggaranRenstra;
        $anggaranSasaran = $subKegiatan->kegiatan->programRenstra->sasaranRenstra->anggaranRenstra;

        foreach ($anggaranKegiatan as $anggaran) {
             $anggaranSub = $anggaranSubKegiatan->firstWhere('id_periode_per_tahun', $anggaran->id_periode_per_tahun);

             if($anggaranSub) {
                 $anggaran->nilai = $anggaran->nilai + $anggaranSub->nilai;
                 $anggaran->save();
             }
        }

        foreach ($anggaranProgram as $anggaran) {
            $anggaranSub = $anggaranSubKegiatan->firstWhere('id_periode_per_tahun', $anggaran->id_periode_per_tahun);

            if($anggaranSub) {
                $anggaran->nilai = $anggaran->nilai + $anggaranSub->nilai;
                $anggaran->save();
            }
        }

        foreach ($anggaranSasaran as $anggaran) {
            $anggaranSub = $anggaranSubKegiatan->firstWhere('id_periode_per_tahun', $anggaran->id_periode_per_tahun);

            if ($anggaranSub) {
                $anggaran->nilai = $anggaran->nilai + $anggaranSub->nilai;
                $anggaran->save();
            }
        }

        session()->flash('message', 'Berhasil di buat');

        return redirect()->route('renstra.sub_kegiatan.sub_kegiatan.index');
    }

    public function submitRenja(SubKegiatanRenstra $subKegiatan, Request $request) {
        $this->validate($request, [
            'renja' => 'required|array|min:1',
            'renja.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'renja.*.value' => 'required|integer',
        ]);

        $renjaInput = $request->get('renja');
        $anggaranSubKegiatan = $subKegiatan->anggaranRenja;
        $anggaranKegiatan = $subKegiatan->kegiatan->anggaranRenja;
        $anggaranProgram = $subKegiatan->kegiatan->programRenstra->anggaranRenja;

        foreach ($renjaInput as $renja) {
            $selectedAnggaranSubKegiatan = $anggaranSubKegiatan->firstWhere('id_periode_per_tahun', $renja['id_tahun']);
            $selisihAngka = $renja['value'] - $selectedAnggaranSubKegiatan->nilai;

            $selectedAnggaranSubKegiatan->nilai = $renja['value'];
            $selectedAnggaranSubKegiatan->save();

            $selectedAnggaranKegiatan = $anggaranKegiatan->firstWhere('id_periode_per_tahun', $renja['id_tahun']);
            $selectedAnggaranKegiatan->nilai = $selectedAnggaranKegiatan->nilai + $selisihAngka;
            $selectedAnggaranKegiatan->save();

            $selectedAnggaranProgram = $anggaranProgram->firstWhere('id_periode_per_tahun', $renja['id_tahun']);
            $selectedAnggaranProgram->nilai = $selectedAnggaranProgram->nilai + $selisihAngka;
            $selectedAnggaranProgram->save();
        }

        session()->flash('message', 'Renja Berhasil di input');

        return redirect()->back();
    }

    public function destroy(SubKegiatanRenstra $subKegiatan) {

        $this->validateParentModelDeletion($subKegiatan, 'Sub Kegiatan', ['Sasaran Sub Kegiatan']);

        $subKegiatan->delete();

        session()->flash('message', 'Berhasil di hapus');

        return redirect()->back();
    }

    public function edit(SubKegiatanRenstra $subKegiatan) {
        $opds = Opd::all();
        $sumberAnggaran = SumberAnggaran::all();

        return view('renstra.sub-kegiatan.sub-kegiatan.edit', compact('opds', 'sumberAnggaran', 'subKegiatan'));
    }

    public function update(SubKegiatanRenstra $subKegiatan, Request $request) {
        $this->validate($request, [
            'kegiatan' => 'required|exists:renstra_kegiatan,id',
            'sub_kegiatan' => 'required|exists:mst_sub_kegiatan,id',
            'anggaran' => 'required|array|min:1',
            'anggaran.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'anggaran.*.value' => 'required|integer',
            'anggaran.*.value' => 'required|integer',
            'sumber' => 'required|array|min:1',
            'sumber.*' => 'required|exists:sumber_anggaran,id',
        ]);

        $subKegiatan->update([
            'id_renstra_kegiatan' => $request->get('kegiatan'),
            'id_mst_sub_kegiatan' => $request->get('sub_kegiatan'),
        ]);

        $oldAnggaranSubKegiatan = $subKegiatan->anggaranRenstra;
        $oldAnggaranKegiatan = $subKegiatan->kegiatan->anggaranRenstra;
        $oldAnggaranProgram = $subKegiatan->kegiatan->programRenstra->anggaranRenstra;
        $oldAnggaranSasaran = $subKegiatan->kegiatan->programRenstra->sasaranRenstra->anggaranRenstra;

        collect($request->get('anggaran'))->each(function($anggaran) use($oldAnggaranSubKegiatan, $oldAnggaranKegiatan, $oldAnggaranProgram, $oldAnggaranSasaran) {
            $selectedOldAnggaranSubKegiatan = $oldAnggaranSubKegiatan->firstWhere('id_periode_per_tahun', $anggaran['id_tahun']);
            $selectedOldAnggaranKegiatan = $oldAnggaranKegiatan->firstWhere('id_periode_per_tahun', $anggaran['id_tahun']);
            $selectedOldAnggaranProgram = $oldAnggaranProgram->firstWhere('id_periode_per_tahun', $anggaran['id_tahun']);
            $selectedoldanggaranSasaran = $oldAnggaranSasaran->firstWhere('id_periode_per_tahun', $anggaran['id_tahun']);

            if($selectedOldAnggaranSubKegiatan) {
                $selisihAngka = $anggaran['value'] - $selectedOldAnggaranSubKegiatan->nilai;
                $selectedOldAnggaranSubKegiatan->nilai = $anggaran['value'];
                $selectedOldAnggaranSubKegiatan->save();

                $selectedOldAnggaranKegiatan->nilai = $selectedOldAnggaranKegiatan->nilai + $selisihAngka;
                $selectedOldAnggaranKegiatan->save();

                $selectedOldAnggaranProgram->nilai = $selectedOldAnggaranProgram->nilai + $selisihAngka;
                $selectedOldAnggaranProgram->save();

                $selectedoldanggaranSasaran->nilai = $selectedoldanggaranSasaran->nilai + $selisihAngka;
                $selectedoldanggaranSasaran->save();
            } else {
                throw new \Exception('Inkonsistensi data, ada anggaran yang tidak ada. seharusnya semua data anggaran otomatis ke built');
            }
        });

        $subKegiatan->sumberAnggaran()->sync($request->get('sumber'));

        session()->flash('message', 'Berhasil di buat');

        return redirect()->route('renstra.sub_kegiatan.sub_kegiatan.index');
    }
}
