<?php

namespace App\Http\Controllers\Renstra\Sasaran;

use App\Http\Controllers\Controller;
use App\Models\Renstra\SasaranIndikatorPenanggungJawabRenstra;
use App\Models\Renstra\SasaranRenstraIndikator;
use Illuminate\Http\Request;

class PenanggungJawabSasaranRenstraController extends Controller
{
    public function index(Request $request) {
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        $sasaranIndikator = SasaranRenstraIndikator::with([
            'sasaran.renstraTujuan.opd',
            'sasaran.renstraTujuan.rpjmdSasaran.tujuan.misi',
            'sasaran.renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'sasaranIndikatorPenanggungJawab.pegawai',
            'sasaranIndikatorPenanggungJawab.target',
            'target'
        ])->whereHas(
            'sasaran.renstraTujuan.opd',
            function($query) use($selectedOpd) {
                return $query->where('id', $selectedOpd);
            }
        )->get();

        $periode = periode();

        return view('renstra.sasaran.penanggung-jawab.index', compact(
            'sasaranIndikator',
            'selectedOpd',
            'opds',
            'periode'
        ));
    }

    public function massUpdate(Request $request) {
        $this->validate($request, [
            'indikator' => 'required|array|min:1',
            'indikator.*.id' => 'required|exists:renstra_sasaran_indikator,id',
            'indikator.*.penanggung_jawab' => 'nullable|array|min:1',
            'indikator.*.penanggung_jawab.*.id' => 'nullable|exists:renstra_sasaran_indikator_penanggung_jawab,id',
            'indikator.*.penanggung_jawab.*.pegawai_id' => 'nullable|exists:mst_pegawai,id',
            'indikator.*.penanggung_jawab.*.target' => 'array',
            'indikator.*.penanggung_jawab.*.target.*.periode_id' => 'exists:mst_periode_per_tahun,id',
            'indikator.*.penanggung_jawab.*.target.*.target' => 'nullable|string',
        ]);



        $parameters = collect($request->get('indikator'));
        $indikatorIds = $parameters->map(function($indikator) {return $indikator['id'];});
        $indikators = SasaranRenstraIndikator::whereIn('id', $indikatorIds)->get();

        foreach ($indikators as $indikator) {
            /** @var SasaranRenstraIndikator $indikator  */
            $data = $parameters->where('id', $indikator->id)->first();
            $penanggungJawabCollection = collect($data['penanggung_jawab'] ?? [])
                ->filter(function($penanggungJawab) {
                    return isset($penanggungJawab['pegawai_id']);
                });

            $penanggungJawabToDelete = $indikator->sasaranIndikatorPenanggungJawab()
                ->whereNotIn('id', $penanggungJawabCollection->pluck('id'))->get();

            foreach ($penanggungJawabToDelete as $penanggungJawab) {
                $penanggungJawab->delete();
            }

            foreach ($penanggungJawabCollection as $penanggungJawab) {

                $targetCollection = $penanggungJawab['target'];
                /** @var SasaranIndikatorPenanggungJawabRenstra */
                $pivot = $penanggungJawab['id'] == null
                    ? SasaranIndikatorPenanggungJawabRenstra::create([
                        'id_pegawai' => $penanggungJawab['pegawai_id'],
                        'id_renstra_sasaran_indikator' => $indikator->id
                    ])
                    : $indikator->sasaranIndikatorPenanggungJawab()->updateOrCreate(
                        ['id' => $penanggungJawab['id']],
                        ['id_pegawai' => $penanggungJawab['pegawai_id']]
                    );

                foreach ($targetCollection as $target) {
                    $pivot->target()->updateOrCreate(
                        ['id_periode_per_tahun' => $target['periode_id']],
                        ['target' => $target['target']]
                    );
                }
            }
        }



        return redirect()->back();
    }
}
