<?php

namespace App\Http\Controllers\Renstra;

use App\Http\Controllers\Controller;
use App\Models\Opd;
use App\Models\PeriodePerTahun;
use App\Models\Renstra\TujuanRenstra;
use App\Models\Renstra\SasaranRenstra;
use App\Models\Renstra\SasaranRenstraIndikator;
use App\Models\Renstra\SasaranRenstraTarget;
use App\Models\Renstra\StrategiRenstra;
use App\Models\Renstra\KebijakanRenstra;
use App\Models\Satuan;
use App\Models\Tujuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Datatables;
use Illuminate\Support\Facades\Gate;

use function PHPUnit\Framework\isNull;

class SasaranRenstraController extends Controller
{
    public function index(Request $request)
    {
        $hierarki = TujuanRenstra::with([
            'rpjmdSasaran', 'rpjmdSasaran.tujuan.misi', 'rpjmdSasaran.prioritasPembangunan',
            'rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'renstraSasaran.renstraTujuan.opd', 'renstraSasaran.indikator', 'renstraSasaran.indikator.target',
            'renstraSasaran.indikator.satuan', 'opd',
        ])->where('id_periode', $this->id_periode)->get();

        $opd = getAllowedOpds();
        $opd_selected = $request->opd ?? auth()->user()->id_opd ?? $opd->first()->id;

        $hierarki = $hierarki->where('id_mst_opd', $opd_selected);

        Gate::authorize('access-opd', $opd_selected);

        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();
        $title = 'sasaran';

        return view('renstra/sasaran/sasaran', [
            'hierarki' => $hierarki,
            'periode_per_tahun' => $periode_per_tahun,
            'title' => $title,
            'opd' => $opd,
            'opd_selected' => !is_null($opd_selected) ? $opd_selected : 0
        ]);
    }

    public function create()
    {
        $opd = getAllowedOpds();
        $tujuan = TujuanRenstra::with(['opd'])
            ->where('id_periode', $this->id_periode)
            ->whereHas('opd', function($query) use ($opd) {
                return $query->whereIn('mst_opd.id', $opd->pluck('id'));
            })
            ->get();
        $satuan = Satuan::all();
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        return view('renstra.sasaran.form-add', [
            'tujuan' => $tujuan,
            'satuan' => $satuan,
            'periode_per_tahun' => $periode_per_tahun
        ]);
    }

    public function store(Request $request)
    {
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        $request->validate([
            'tujuan' => 'required|string',
            'sasaran' => 'required|string',
            'indikator.*' => 'required|string',
            'pengukuran.*' => 'required|string',
            'satuan.*' => 'required|string',
            'cara_pengukuran.*' => 'required|string'
        ]);

        $data = $this->objDataRequest($request->all());
        $sasaran = new SasaranRenstra();
        $sasaran->name = $data->sasaran;
        $sasaran->id_renstra_tujuan = $data->tujuan;
        $sasaran->id_periode = $this->id_periode;
        $sasaran->save();

        $this->insertDetail($data->data_detail, $sasaran, $periode_per_tahun);

        return redirect('renstra/sasaran')->with([
            'success' => 'Data berhasil disimpan',
        ]);
    }

    private function objDataRequest($data)
    {
        $data_form = [];
        $indikator_count = count($data['indikator']);
        $target = array_chunk($data['target'], count($data['target']) / $indikator_count);
        for ($i = 0; $i < $indikator_count; $i++) {
            $data_detail[] = (object)[
                'indikator' => $data['indikator'][$i],
                'satuan' => $data['satuan'][$i],
                'pk' => $data['pk'][$i],
                'ir' => $data['ir'][$i],
                'iku' => $data['iku'][$i],
                'awal' => $data['awal'][$i],
                'target' => $target[$i],
                'akhir' => $data['akhir'][$i],
                'cara_pengukuran' => $data['cara_pengukuran'][$i],
            ];
        }

        $data_form = (object)[
            'tujuan' => $data['tujuan'],
            'sasaran' => $data['sasaran'],
            'data_detail' => $data_detail
        ];

        return $data_form;
    }

    private function insertDetail($arr_detail, $sasaran, $periode_per_tahun)
    {
        foreach ($arr_detail as $item) {
            $sasaran_indikator = new SasaranRenstraIndikator;
            $sasaran_indikator->indikator = $item->indikator;
            $sasaran_indikator->id_satuan = $item->satuan;
            $sasaran_indikator->id_sasaran = $sasaran->id;
            $sasaran_indikator->is_pk = $item->pk;
            $sasaran_indikator->is_iku = $item->iku;
            $sasaran_indikator->is_ir = $item->ir;
            $sasaran_indikator->awal = $item->awal;
            $sasaran_indikator->akhir = $item->akhir;
            $sasaran_indikator->cara_pengukuran = $item->cara_pengukuran;
            $sasaran_indikator->save();

            foreach ($item->target as $idx => $val) {
                $sasaran_target = new SasaranRenstraTarget;
                $sasaran_target->target = !is_null($val) ? $val : '-';
                $sasaran_target->id_indikator_sasaran = $sasaran_indikator->id;
                $sasaran_target->id_periode_per_tahun = $periode_per_tahun[$idx]->id;
                $sasaran_target->save();
            }
        }
    }

    public function edit($id)
    {
        $data = SasaranRenstra::with(['renstraTujuan', 'indikator', 'indikator.target', 'indikator.satuan'])
            ->find(base64_decode($id));

        $opd = getAllowedOpds();
        $tujuan = TujuanRenstra::with(['opd'])
            ->where('id_periode', $this->id_periode)
            ->whereHas('opd', function($query) use ($opd) {
                return $query->whereIn('mst_opd.id', $opd->pluck('id'));
            })
            ->get();
        $satuan = Satuan::all();
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        return view('renstra.sasaran.form-edit', [
            'data' => $data,
            'tujuan' => $tujuan,
            'satuan' => $satuan,
            'periode_per_tahun' => $periode_per_tahun
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $this->objDataRequest($request->all());
        $data_edit = SasaranRenstra::with(['indikator', 'indikator.target'])->find(base64_decode($id));
        $data_edit->name = $data->sasaran;
        $data_edit->id_renstra_tujuan = $data->tujuan;
        $data_edit->save();

        // get id indikator
        $id_indikator = [];
        foreach ($data_edit->indikator as $item) {
            $id_indikator[] = $item->id;
        }

        // create part add (jika ada)
        if (count($data->data_detail) > $data_edit->indikator->count()) {
            $part_add = array_splice($data->data_detail, $data_edit->indikator->count());
            $this->insertDataInUpdate($part_add, base64_decode($id));
        }

        // create part delete (jika ada)
        if (count($data->data_detail) < $data_edit->indikator->count()) {
            $part_delete = array_splice($id_indikator, count($data->data_detail));
            $this->deleteDataInUpdate($part_delete);
        }

        // update data after filter (part add & part delete)
        foreach ($data->data_detail as $idx => $item) {
            // update data indikator
            $indikator = SasaranRenstraIndikator::find($id_indikator[$idx]);
            $indikator->indikator = $item->indikator;
            $indikator->id_satuan = $item->satuan;
            $indikator->id_sasaran = base64_decode($id);
            $indikator->is_pk = $item->pk;
            $indikator->is_iku = $item->iku;
            $indikator->is_ir = $item->ir;
            $indikator->awal = $item->awal;
            $indikator->akhir = $item->akhir;
            $indikator->cara_pengukuran = $item->cara_pengukuran;
            $indikator->update();

            // get data target for update
            $data_target = SasaranRenstraTarget::where('id_indikator_sasaran', $id_indikator[$idx])->get();
            foreach ($item->target as $key => $val) {
                // update data target
                $target = SasaranRenstraTarget::find($data_target[$key]->id);
                $target->target = !is_null($val) ? $val : '-';
                $target->update();
            }
        }

        return redirect('renstra/sasaran')->with([
            'success' => 'Data berhasil diubah',
        ]);
    }

    private function insertDataInUpdate($data_request, $id_sasaran)
    {
        // get periode (tahun)
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        foreach ($data_request as $item) {
            // insert data indikator
            $indikator = new SasaranRenstraIndikator;
            $indikator->indikator = $item->indikator;
            $indikator->id_satuan = $item->satuan;
            $indikator->id_sasaran = $id_sasaran;
            $indikator->is_pk = $item->pk;
            $indikator->is_iku = $item->iku;
            $indikator->is_ir = $item->ir;
            $indikator->awal = $item->awal;
            $indikator->akhir = $item->akhir;
            $indikator->cara_pengukuran = $item->cara_pengukuran;
            $indikator->save();

            foreach ($item->target as $idx => $val) {
                // insert data target
                $target = new SasaranRenstraTarget;
                $target->target = !is_null($val) ? $val : '-';
                $target->id_indikator_sasaran = $indikator->id;
                $target->id_periode_per_tahun = $periode_per_tahun[$idx]->id;
                $target->save();
            }
        }
    }

    private function deleteDataInUpdate($data_detele)
    {
        foreach ($data_detele as $id) {
            SasaranRenstraIndikator::find($id)->delete();
            SasaranRenstraTarget::where('id_indikator_sasaran', $id)->delete();
        }
    }

    public function destroy($id)
    {
        $sasaranRenstra = SasaranRenstra::find(base64_decode($id));

        $this->validateParentModelDeletion($sasaranRenstra, 'Sasaran Renstra', ['Renstra Strategi', 'Renstra Kebijakan', 'Program Renstra']);

        $sasaranRenstra->delete();

        flashSuccess('Berhasil Menghapus data');

        return redirect()->back();
    }

    public function show($id)
    {
        $data = DB::table('renstra_sasaran as rs')
            ->where('rs.id', '=', $id)
            ->join('renstra_tujuan as rt', 'rs.id_renstra_tujuan', '=', 'rt.id')
            ->join('mst_opd as opd', 'rt.id_mst_opd', '=', 'opd.id')
            ->select('rs.id as id', 'rs.name as nama_sasaran', 'opd.name as opd', 'rs.created_at', 'rs.updated_at')
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Sasaran By ID',
            'data'    => $data
        ],);
    }

    public function api()
    {
        $data = DB::table('renstra_sasaran as rs')
            ->join('renstra_tujuan as rt', 'rs.id_renstra_tujuan', '=', 'rt.id')
            ->join('mst_opd as opd', 'rt.id_mst_opd', '=', 'opd.id')
            ->select('rs.id as id_sasaran', 'rs.name as nama_sasaran', 'opd.name as opd', 'rs.created_at', 'rs.updated_at')
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data Sasaran',
            'data'    => $data,
        ], 200);
    }

    public function submitRenja(SasaranRenstra $sasaran, SasaranRenstraIndikator $indikator, Request $request) {
        $this->validate($request, [
            'renja' => 'required|array|min:1',
            'renja.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'renja.*.value' => 'required|string',
        ]);

        $indikator->targetRenja()->delete();

        collect($request->get('renja'))->each(function($renja) use($indikator){
            $indikator->targetRenja()->create([
                "id_periode_per_tahun" => $renja["id_tahun"],
                "nilai" => $renja["value"]
            ]);
        });

        session()->flash('message', 'Renja Berhasil di input');

        return redirect()->back();
    }

}
