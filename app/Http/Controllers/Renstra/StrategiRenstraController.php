<?php

namespace App\Http\Controllers\Renstra;

use App\Http\Controllers\Controller;
use App\Models\Renstra\SasaranRenstra;
use App\Models\Renstra\StrategiRenstra;
use App\Models\Renstra\TujuanRenstra;
use App\Models\Opd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class StrategiRenstraController extends Controller
{
    function index(Request $request)
    {
        $q_hierarki = SasaranRenstra::with([
            'renstraTujuan.rpjmdSasaran.tujuan.misi', 'renstraTujuan.opd', 'renstraStrategi',
            'renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
        ])->where('id_periode', $this->id_periode);
        $tujuan_count = TujuanRenstra::where('id_periode', $this->id_periode)->count();

        $opd = getAllowedOpds();
        $opd_selected = $request->opd ?? auth()->user()->id_opd ?? $opd->first()->id;

        $q_hierarki->whereHas('renstraTujuan', function($q) use ($opd_selected) {
            $q->where('id_mst_opd', $opd_selected);
        });

        Gate::authorize('access-opd', $opd_selected);

        $hierarki = $q_hierarki->get();

        return view('renstra.strategi.strategi', [
            'hierarki' => $hierarki,
            'opd' => $opd,
            'tujuan_count' => $tujuan_count,
            'opd_selected' => $opd_selected
        ]);
    }

    function store(Request $request)
    {
        $data = $this->dataObj($request->all());

        foreach ($data as $item) {
            $strategi_data = StrategiRenstra::where('id_renstra_sasaran', $item['sasaran'])->get();
            $strategi_count = $strategi_data->count();
            $request_count = count($item['strategi']);

            // get id strategi (if exist)
            $id_strategi = [];
            if($strategi_count > 0) {
                foreach($strategi_data as $val) {
                    $id_strategi[] = $val->id;
                }
            }

            // part add
            if($strategi_count < $request_count) {
                $part_add = array_splice($item['strategi'], $strategi_count);
                foreach ($part_add as $val) {
                    $strategi = new StrategiRenstra;
                    $strategi->name = $val;
                    $strategi->id_renstra_sasaran = $item['sasaran'];
                    $strategi->id_periode = $this->id_periode;
                    $strategi->save();
                }
            }

            // part delete
            if($strategi_count > $request_count) {
                $part_delete = array_splice($id_strategi, $request_count);
                foreach($part_delete as $val) {
                    StrategiRenstra::find($val)->delete();
                }
            }

            // part update
            foreach($item['strategi'] as $idx => $val) {
                StrategiRenstra::find($id_strategi[$idx])->update([
                    'name' => $val,
                    'id_renstra_sasaran' => $item['sasaran'],
                    'id_periode' => $this->id_periode,
                ]);
            }
        }

        flashSuccess('Data berhasil di simpan');

        return redirect()->back();
    }

    private function dataObj($data)
    {
        $opdSelected = $data['opd_selected'];
        $sasaran_renstra_count = SasaranRenstra::where('id_periode', $this->id_periode)
            ->whereHas('renstraTujuan', function($q) use ($opdSelected) {
                $q->where('id_mst_opd', $opdSelected);
            })
            ->count();
        for ($i = 0; $i < $sasaran_renstra_count; $i++) {
            $str_sasaran = 'sasaran_' . $i;
            $str_strategi = 'strategi_' . $i;

            $data_form[$str_strategi] = [
                'strategi' => $data[$str_strategi],
                'sasaran' => $data[$str_sasaran]
            ];
        }

        return $data_form;
    }
}
