<?php

namespace App\Http\Controllers\Renstra;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Opd;
use App\Models\PeriodePerTahun;
use App\Models\Renstra\Program\ProgramRenstra;
use App\Models\Renstra\SasaranRenstra;
use App\Models\Rpjmd\Program\ProgramRpjmd;
use Illuminate\Support\Facades\Gate;

class ProgramRenstraController extends Controller
{
    public function index(Request $request)
    {
        $q_hierarki = SasaranRenstra::with([
            'renstraTujuan.rpjmdSasaran.prioritasPembangunan', 'renstraTujuan.rpjmdSasaran.tujuan.misi', 'programRenstra'
        ])->where('id_periode', $this->id_periode);

        $opd = getAllowedOpds();
        $selected_opd = $request->opd ?? auth()->user()->id_opd ?? $opd->first()->id;

        Gate::authorize('access-opd', $selected_opd);

        $q_hierarki->whereHas('renstraTujuan', function ($q) use ($selected_opd) {
            $q->where('id_mst_opd', $selected_opd);
        });

        $hierarki = $q_hierarki->get();

        $title = 'Renstra Program';

        $sasaran = SasaranRenstra::with([
            'renstraTujuan.opd'
        ])->whereHas('renstraTujuan.opd', function($query) use ($selected_opd){
            return $query->where('mst_opd.id', $selected_opd);
        })->get();
        $program = ProgramRpjmd::with(['mstProgram'])->whereHas('opd', function($query) use ($selected_opd){
            $query->where('mst_opd.id', $selected_opd);
        })->get();
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        return view('renstra.program.program', [
            'title' => $title,
            'opd' => $opd,
            'selected_opd' => $selected_opd,
            'sasaran' => $sasaran,
            'program' => $program,
            'periode_per_tahun' => $periode_per_tahun,
            'hierarki' => $hierarki
        ]);
    }

    public function store(Request $request)
    {
        $program = new ProgramRenstra;
        $program->id_renstra_sasaran = $request->sasaran;
        $program->id_rpjmd_program = $request->program;
        $program->id_periode = $this->id_periode;
        $program->save();

        return redirect('/renstra/program/program')->with([
            'success' => 'Data berhasil di tambahkan',
        ]);
    }

    function update(Request $request, $id)
    {
        ProgramRenstra::find(base64_decode($id))->update([
            'id_renstra_sasaran' => $request->sasaran,
            'id_rpjmd_program' => $request->program
        ]);

        return redirect('/renstra/program/program')->with([
            'success' => 'Data berhasil di ubah',
        ]);
    }

    function destroy($id)
    {
        $program = ProgramRenstra::find(base64_decode($id));

        $this->validateParentModelDeletion($program, 'Program Renstra', [ 'Sasaran program','Kegiatan Renstra']);

        $program->delete();

        return redirect('/renstra/program/program')->with([
            'success' => 'Data berhasil di hapus',
        ]);
    }
}
