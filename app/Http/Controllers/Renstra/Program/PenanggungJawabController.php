<?php

namespace App\Http\Controllers\Renstra\Program;

use App\Http\Controllers\Controller;
use App\Models\Opd;
use App\Models\Renstra\Program\SasaranProgramIndikatorPenanggungJawabRenstra;
use App\Models\Renstra\Program\SasaranProgramIndikatorRenstra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class PenanggungJawabController extends Controller
{
    public function index(Request $request){
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        $sasaranProgramIndikator = SasaranProgramIndikatorRenstra::with([
            'sasaranProgram.opd',
            'sasaranProgramIndikatorPenanggungJawab.pegawai',
            'sasaranProgramIndikatorPenanggungJawab.target',
            'target',
            'sasaranProgram.program.programRpjmd.mstProgram',
            'sasaranProgram.program.sasaranRenstra.renstraTujuan.rpjmdSasaran.tujuan.misi',
            'sasaranProgram.program.sasaranRenstra.renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan'
        ])->whereHas('sasaranProgram.opd', function($query) use($selectedOpd){
            $query->where('mst_opd.id', $selectedOpd);
        })->get();

        Gate::authorize('access-opd', $selectedOpd);

        $periode = periode();

        return view('renstra.program.penanggung-jawab.index', compact(
            'sasaranProgramIndikator',
            'selectedOpd',
            'opds',
            'periode'
        ));
    }

    public function massUpdate(Request $request) {
        $this->validate($request, [
            'indikator' => 'required|array|min:1',
            'indikator.*.id' => 'required|exists:renstra_sasaran_program_indikator,id',
            'indikator.*.penanggung_jawab' => 'nullable|array',
            'indikator.*.penanggung_jawab.*.id' => 'nullable|exists:renstra_sasaran_program_indikator_penanggung_jawab,id',
            'indikator.*.penanggung_jawab.*.pegawai_id' => 'nullable|exists:mst_pegawai,id',
            'indikator.*.penanggung_jawab.*.target' => 'array',
            'indikator.*.penanggung_jawab.*.target.*.periode_id' => 'exists:mst_periode_per_tahun,id',
            'indikator.*.penanggung_jawab.*.target.*.target' => 'nullable|string',
        ]);

        $parameters = collect($request->get('indikator'));
        $indikatorIds = $parameters->map(function($indikator) {return $indikator['id'];});
        $indikators = SasaranProgramIndikatorRenstra::whereIn('id', $indikatorIds)->get();

        foreach ($indikators as $indikator) {
            /** @var SasaranProgramIndikatorRenstra $indikator  */
            $data = $parameters->where('id', $indikator->id)->first();
            $penanggungJawabCollection = collect($data['penanggung_jawab'] ?? [])
                ->filter(function($penanggungJawab) {
                    return isset($penanggungJawab['pegawai_id']);
                });

            $penanggungJawabToDelete = $indikator->sasaranProgramIndikatorPenanggungJawab()
                ->whereNotIn('id', $penanggungJawabCollection->pluck('id'))->get();

            foreach ($penanggungJawabToDelete as $penanggungJawab) {
                /** @var SasaranProgramIndikatorPenanggungJawabRenstra $penanggungJawab */
                $penanggungJawab->target()->delete();
                $penanggungJawab->delete();
            }

            foreach ($penanggungJawabCollection as $penanggungJawab) {

                $targetCollection = $penanggungJawab['target'];
                /** @var SasaranProgramIndikatorPenanggungJawabRenstra */
                $pivot = $penanggungJawab['id'] == null
                    ? SasaranProgramIndikatorPenanggungJawabRenstra::create([
                        'id_pegawai' => $penanggungJawab['pegawai_id'],
                        'id_renstra_sasaran_program_indikator' => $indikator->id
                    ])
                    : $indikator->sasaranProgramIndikatorPenanggungJawab()->updateOrCreate(
                        ['id' => $penanggungJawab['id']],
                        ['id_pegawai' => $penanggungJawab['pegawai_id']]
                    );

                foreach ($targetCollection as $target) {
                    $pivot->target()->updateOrCreate(
                        ['id_periode_per_tahun' => $target['periode_id']],
                        ['target' => $target['target']]
                    );
                }
            }
        }

        flashSuccess('Data berhasil di simpan');

        return redirect()->back();
    }
}
