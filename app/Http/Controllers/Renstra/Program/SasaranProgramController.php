<?php

namespace App\Http\Controllers\Renstra\Program;

use App\Http\Controllers\Controller;
use App\Models\Opd;
use App\Models\PeriodePerTahun;
use App\Models\Renstra\Program\ProgramRenstra;
use App\Models\Renstra\Program\SasaranProgramIndikatorRenstra;
use App\Models\Renstra\Program\SasaranProgramRenstra;
use App\Models\Renstra\Program\SasaranProgramTargetRenstra;
use App\Models\Satuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class SasaranProgramController extends Controller
{
    function index(Request $request)
    {
        $title = 'Sasaran Program';
        $opd = getAllowedOpds();
        $opd_selected = $request->opd ?? auth()->user()->id_opd ?? $opd->first()->id;

        Gate::authorize('access-opd', $opd_selected);

        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        $relation = 'program.sasaranRenstra.renstraTujuan.rpjmdSasaran';
        $q_hierarki = SasaranProgramRenstra::with([
            "$relation.tujuan.misi",
            "$relation.prioritasPembangunan",
            "opd",
            "indikator.target"
        ])->where('id_opd', $opd_selected)->where('id_periode', $this->id_periode);

        $q_hierarki->where('id_opd', $opd_selected);
        $hierarki = $q_hierarki->get();

        return view('renstra.program.sasaran-program.sasaran-program', [
            'title' => $title,
            'opd' => $opd,
            'hierarki' => $hierarki,
            'opd_selected' => $opd_selected,
            'periode_per_tahun' => $periode_per_tahun
        ]);
    }

    function create()
    {
        $opd = getAllowedOpds();
        $program = ProgramRenstra::with([
            'sasaranRenstra', 'programRpjmd.mstProgram'
        ])->where('id_periode', $this->id_periode)
        ->whereHas('sasaranRenstra.renstraTujuan', function($query) use($opd){
            $query->whereIn('id_mst_opd', $opd->pluck('id'));
        })->get();

        $satuan = Satuan::all();
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        return view('renstra.program.sasaran-program.sasaran-program-add', [
            'opd' => $opd,
            'program' => $program,
            'satuan' => $satuan,
            'periode_per_tahun' => $periode_per_tahun
        ]);
    }

    function store(Request $request)
    {
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        $request->validate([
            'program' => 'required|string',
            'sasaran_program' => 'required|string',
            'indikator.*' => 'required|string',
            'pengukuran.*' => 'required|string',
            'satuan.*' => 'required|string',
            'cara_pengukuran.*' => 'required|string'
        ]);

        $data = $this->objDataRequest($request->all());
        $sasaran_program = new SasaranProgramRenstra();
        $sasaran_program->name = $data->sasaran_program;
        $sasaran_program->id_opd = $data->opd;
        $sasaran_program->id_program_renstra = $data->program;
        $sasaran_program->id_periode = $this->id_periode;
        $sasaran_program->save();

        $this->insertDetail($data->data_detail, $sasaran_program, $periode_per_tahun);

        return redirect('renstra/program/sasaran-program')->with([
            'success' => 'Data berhasil disimpan',
        ]);
    }

    private function objDataRequest($data)
    {
        $data_form = [];
        $indikator_count = count($data['indikator']);
        $target = array_chunk($data['target'], count($data['target']) / $indikator_count);
        for ($i = 0; $i < $indikator_count; $i++) {
            $data_detail[] = (object)[
                'indikator' => $data['indikator'][$i],
                'satuan' => $data['satuan'][$i],
                'pk' => $data['pk'][$i],
                'iku' => $data['iku'][$i],
                'awal' => $data['awal'][$i],
                'target' => $target[$i],
                'akhir' => $data['akhir'][$i],
                'cara_pengukuran' => $data['cara_pengukuran'][$i],
            ];
        }

        $data_form = (object)[
            'opd' => $data['opd'],
            'program' => $data['program'],
            'sasaran_program' => $data['sasaran_program'],
            'data_detail' => $data_detail
        ];

        return $data_form;
    }

    private function insertDetail($arr_detail, $sasaran_program, $periode_per_tahun)
    {
        foreach ($arr_detail as $item) {
            $sasaran_indikator = new SasaranProgramIndikatorRenstra();
            $sasaran_indikator->indikator = $item->indikator;
            $sasaran_indikator->id_satuan = $item->satuan;
            $sasaran_indikator->id_sasaran_program = $sasaran_program->id;
            $sasaran_indikator->is_pk = $item->pk;
            $sasaran_indikator->is_iku = $item->iku;
            $sasaran_indikator->awal = $item->awal;
            $sasaran_indikator->akhir = $item->akhir;
            $sasaran_indikator->cara_pengukuran = $item->cara_pengukuran;
            $sasaran_indikator->save();

            foreach ($item->target as $idx => $val) {
                $sasaran_target = new SasaranProgramTargetRenstra;
                $sasaran_target->target = !is_null($val) ? $val : '-';
                $sasaran_target->id_sasaran_program_indikator = $sasaran_indikator->id;
                $sasaran_target->id_periode_per_tahun = $periode_per_tahun[$idx]->id;
                $sasaran_target->save();
            }
        }
    }

    function edit($id)
    {
        $id = base64_decode($id);
        $data = SasaranProgramRenstra::with('indikator.target')->where('id', $id)->get();
        $opd = Opd::all();

        $program = ProgramRenstra::with([
            'sasaranRenstra', 'programRpjmd.mstProgram'
        ])->where('id_periode', $this->id_periode)->get();

        $satuan = Satuan::all();
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        return view('renstra.program.sasaran-program.sasaran-program-edit', [
            'opd' => $opd,
            'program' => $program,
            'satuan' => $satuan,
            'periode_per_tahun' => $periode_per_tahun,
            'data' => $data,
        ]);
    }

    function update(Request $request, $id)
    {
        $data = $this->objDataRequest($request->all());
        $data_edit = SasaranProgramRenstra::with(['indikator.target'])->find(base64_decode($id));
        $data_edit->name = $data->sasaran_program;
        $data_edit->id_program_renstra = $data->program;
        $data_edit->id_opd = $data->opd;
        $data_edit->save();

        // get id indikator
        $id_indikator = [];
        foreach ($data_edit->indikator as $item) {
            $id_indikator[] = $item->id;
        }

        // create part add (jika ada)
        if (count($data->data_detail) > $data_edit->indikator->count()) {
            $part_add = array_splice($data->data_detail, $data_edit->indikator->count());
            $this->insertDataInUpdate($part_add, base64_decode($id));
        }

        // create part delete (jika ada)
        if (count($data->data_detail) < $data_edit->indikator->count()) {
            $part_delete = array_splice($id_indikator, count($data->data_detail));
            $this->deleteDataInUpdate($part_delete);
        }

        // update data after filter (part add & part delete)
        foreach ($data->data_detail as $idx => $item) {
            // update data indikator
            $indikator = SasaranProgramIndikatorRenstra::find($id_indikator[$idx]);
            $indikator->indikator = $item->indikator;
            $indikator->id_satuan = $item->satuan;
            $indikator->id_sasaran_program = base64_decode($id);
            $indikator->is_pk = $item->pk;
            $indikator->is_iku = $item->iku;
            $indikator->awal = $item->awal;
            $indikator->akhir = $item->akhir;
            $indikator->cara_pengukuran = $item->cara_pengukuran;
            $indikator->update();

            // get data target for update
            $data_target = SasaranProgramTargetRenstra::where('id_sasaran_program_indikator', $id_indikator[$idx])->get();
            foreach ($item->target as $key => $val) {
                // update data target
                $target = SasaranProgramTargetRenstra::find($data_target[$key]->id);
                $target->target = !is_null($val) ? $val : '-';
                $target->update();
            }
        }

        flashSuccess('Data berhasil di ubah');

        return redirect('renstra/program/sasaran-program')->with([
            'success' => 'Data berhasil diubah',
        ]);
    }

    private function insertDataInUpdate($data_request, $id_sasaran)
    {
        // get periode (tahun)
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        foreach ($data_request as $item) {
            // insert data indikator
            $indikator = new SasaranProgramIndikatorRenstra;
            $indikator->indikator = $item->indikator;
            $indikator->id_satuan = $item->satuan;
            $indikator->id_sasaran_program = $id_sasaran;
            $indikator->is_pk = $item->pk;
            $indikator->is_iku = $item->iku;
            $indikator->awal = $item->awal;
            $indikator->akhir = $item->akhir;
            $indikator->cara_pengukuran = $item->cara_pengukuran;
            $indikator->save();

            foreach ($item->target as $idx => $val) {
                // insert data target
                $target = new SasaranProgramTargetRenstra;
                $target->target = !is_null($val) ? $val : '-';
                $target->id_sasaran_program_indikator = $indikator->id;
                $target->id_periode_per_tahun = $periode_per_tahun[$idx]->id;
                $target->save();
            }
        }
    }

    private function deleteDataInUpdate($data_detele)
    {
        foreach ($data_detele as $id) {
            SasaranProgramIndikatorRenstra::find($id)->delete();
        }
    }

    function destroy($id)
    {
        $id = base64_decode($id);

        SasaranProgramRenstra::find($id)->delete();

        return redirect('renstra/program/sasaran-program')->with([
            'success' => 'Data berhasil di hapus'
        ]);
    }

    public function api()
    {
        $data = DB::table('renstra_program as rp')
            ->join('renstra_sasaran as rs', 'rp.id_renstra_sasaran', '=', 'rs.id')
            ->join('rpjmd_program as prog', 'rp.id_rpjmd_program', '=', 'prog.id')
            ->join('mst_program as m_prog', 'prog.id_mst_program', '=', 'm_prog.id')
            ->join('mst_periode as mp', 'rp.id_periode', '=', 'mp.id')
            ->select('rp.id as id_program', 'rs.name as renstra_sasaran', 'm_prog.name as rpjmd_program', 'mp.dari as dari', 'mp.sampai as sampai')
            ->get();

        return response()->json($data, 200);
    }

    public function submitRenja(SasaranProgramRenstra $sasaranProgram, SasaranProgramIndikatorRenstra $indikator, Request $request) {
        $this->validate($request, [
            'renja' => 'required|array|min:1',
            'renja.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'renja.*.value' => 'required|string',
        ]);

        $indikator->targetRenja()->delete();

        collect($request->get('renja'))->each(function($renja) use($indikator){
            $indikator->targetRenja()->create([
                "id_periode_per_tahun" => $renja["id_tahun"],
                "nilai" => $renja["value"]
            ]);
        });

        session()->flash('message', 'Renja Berhasil di input');

        return redirect()->back();
    }

}
