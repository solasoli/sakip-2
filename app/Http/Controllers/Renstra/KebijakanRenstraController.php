<?php

namespace App\Http\Controllers\Renstra;

use App\Http\Controllers\Controller;
use App\Models\Renstra\StrategiRenstra;
use App\Models\Renstra\KebijakanRenstra;
use App\Models\Opd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class KebijakanRenstraController extends Controller
{
    public function index(Request $request)
    {
        $q_hierarki = StrategiRenstra::with([
            'sasaranRenstra.renstraTujuan.rpjmdSasaran.tujuan.misi',
            'sasaranRenstra.renstraTujuan.opd',
            'sasaranRenstra.renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'sasaranRenstra.renstraStrategi.renstraKebijakan',
        ])->where('id_periode', $this->id_periode);

        $opd = getAllowedOpds();
        $opd_selected = $request->opd ?? auth()->user()->id_opd ?? $opd->first()->id;

        $q_hierarki->whereHas('sasaranRenstra.renstraTujuan', function($q) use ($opd_selected) {
            $q->where('id_mst_opd', $opd_selected);
        });

        Gate::authorize('access-opd', $opd_selected);

        $hierarki = $q_hierarki->get();
        return view('renstra.kebijakan.kebijakan', [
            'hierarki' => $hierarki,
            'opd' => $opd,
            'opd_selected' => $opd_selected
        ]);
    }

    public function store(Request $request)
    {
        $data = $this->dataObj($request->all());

        foreach ($data as $item)
        {
            $kebijakan_data = KebijakanRenstra::where('id_renstra_strategi', $item['strategi'])->get();
            $kebijakan_count = $kebijakan_data->count();
            $request_count = count($item['kebijakan']);

            $id_kebijakan = [];
            if($kebijakan_count > 0)
            {
                foreach($kebijakan_data as $val)
                {
                    $id_kebijakan[] = $val->id;
                }
            }

            if($kebijakan_count < $request_count)
            {
                $part_add = array_splice($item['kebijakan'], $kebijakan_count);
                foreach ($part_add as $val)
                {
                    $kebijakan = new KebijakanRenstra;
                    $kebijakan->name = $val;
                    $kebijakan->id_renstra_strategi = $item['strategi'];
                    $kebijakan->id_periode = $this->id_periode;
                    $kebijakan->save();
                }
            }

            if($kebijakan_count > $request_count)
            {
                $part_delete = array_splice($id_kebijakan, $request_count);
                foreach($part_delete as $val) {
                    KebijakanRenstra::find($val)->delete();
                }
            }

            foreach($item['kebijakan'] as $idx => $val)
            {
                KebijakanRenstra::find($id_kebijakan[$idx])->update([
                    'name' => $val,
                    'id_renstra_strategi' => $item['strategi'],
                    'id_periode' => $this->id_periode,
                ]);
            }
        }

        flashSuccess('Data berasil di simpan');

        return redirect()->back();

    }

    private function dataObj($data)
    {
        $opdSelected = $data['opd_selected'];
        $strategi_renstra = StrategiRenstra::where('id_periode', $this->id_periode)
            ->whereHas('sasaranRenstra.renstraTujuan', function($q) use ($opdSelected) {
                $q->where('id_mst_opd', $opdSelected);
            })
            ->count();
        for ($i = 0; $i < $strategi_renstra; $i++) {
            $str_strategi = 'strategi_' . $i;
            $str_kebijakan = 'kebijakan_' . $i;

            $data_form[$str_kebijakan] = [
                'kebijakan' => $data[$str_kebijakan],
                'strategi' => $data[$str_strategi]
            ];
        }

        return $data_form;
    }
}
