<?php

namespace App\Http\Controllers\Renstra\Kegiatan;

use App\Http\Controllers\Controller;
use App\Models\Renstra\Kegiatan\SasaranKegiatanIndikatorPenanggungJawabRenstra;
use App\Models\Renstra\Kegiatan\SasaranKegiatanIndikatorRenstra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class PenanggungJawabKegiatanController extends Controller
{
    public function index(Request $request){
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        Gate::authorize('access-opd', $selectedOpd);
        $sasaranKegiatanIndikator = SasaranKegiatanIndikatorRenstra::with([
            'sasaranKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.opd',
            'sasaranKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.tujuan.misi',
            'sasaranKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'sasaranKegiatanIndikatorPenanggungJawab.pegawai',
            'sasaranKegiatanIndikatorPenanggungJawab.target',
            'target'
        ])->whereHas(
            'sasaranKegiatan.kegiatan.programRenstra.sasaranRenstra.renstraTujuan.opd',
            function($query) use($selectedOpd) {
                $query->where('id', $selectedOpd);
            }
        )->get();

        $periode = periode();

        return view('renstra.kegiatan.penanggung-jawab.index', compact(
            'sasaranKegiatanIndikator',
            'selectedOpd',
            'opds',
            'periode'
        ));
    }


    public function massUpdate(Request $request) {
        $this->validate($request, [
            'indikator' => 'required|array',
            'indikator.*.id' => 'required|exists:renstra_sasaran_kegiatan_indikator,id',
            'indikator.*.penanggung_jawab' => 'nullable|array|min:1',
            'indikator.*.penanggung_jawab.*.id' => 'nullable|exists:renstra_sasaran_kegiatan_indikator_penanggung_jawab,id',
            'indikator.*.penanggung_jawab.*.pegawai_id' => 'nullable|exists:mst_pegawai,id',
            'indikator.*.penanggung_jawab.*.target' => 'array',
            'indikator.*.penanggung_jawab.*.target.*.periode_id' => 'exists:mst_periode_per_tahun,id',
            'indikator.*.penanggung_jawab.*.target.*.target' => 'nullable|string',
        ]);

        $parameters = collect($request->get('indikator'));
        $indikatorIds = $parameters->map(function($indikator) {return $indikator['id'];});
        $indikators = SasaranKegiatanIndikatorRenstra::whereIn('id', $indikatorIds)->get();

        foreach ($indikators as $indikator) {
            /** @var SasaranKegiatanIndikatorRenstra $indikator  */
            $data = $parameters->where('id', $indikator->id)->first();
            $penanggungJawabCollection = collect($data['penanggung_jawab'] ?? [])
                ->filter(function($penanggungJawab) {
                    return isset($penanggungJawab['pegawai_id']);
                });

            $penanggungJawabToDelete = $indikator->sasaranKegiatanIndikatorPenanggungJawab()
                ->whereNotIn('id', $penanggungJawabCollection->pluck('id'))->get();

            foreach ($penanggungJawabToDelete as $penanggungJawab) {
                /** @var SasaranKegiatanIndikatorPenanggungJawabRenstra $penanggungJawab */
                $penanggungJawab->target()->delete();
                $penanggungJawab->delete();
            }

            foreach ($penanggungJawabCollection as $penanggungJawab) {

                $targetCollection = $penanggungJawab['target'];
                /** @var SasaranProgramIndikatorPenanggungJawab */
                $pivot = $penanggungJawab['id'] == null
                    ? SasaranKegiatanIndikatorPenanggungJawabRenstra::create([
                        'id_pegawai' => $penanggungJawab['pegawai_id'],
                        'id_renstra_sasaran_kegiatan_indikator' => $indikator->id
                    ])
                    : $indikator->sasaranKegiatanIndikatorPenanggungJawab()->updateOrCreate(
                        ['id' => $penanggungJawab['id']],
                        ['id_pegawai' => $penanggungJawab['pegawai_id']]
                    );

                foreach ($targetCollection as $target) {
                    $pivot->target()->updateOrCreate(
                        ['id_periode_per_tahun' => $target['periode_id']],
                        ['target' => $target['target']]
                    );
                }
            }
        }

        flashSuccess('Data berhasil di simpan');

        return redirect()->back();
    }
}
