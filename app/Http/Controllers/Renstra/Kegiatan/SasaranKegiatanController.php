<?php

namespace App\Http\Controllers\Renstra\Kegiatan;

use App\Http\Controllers\Controller;
use App\Models\Opd;
use App\Models\Renstra\Kegiatan\SasaranKegiatanIndikatorRenstra;
use App\Models\Renstra\Kegiatan\SasaranKegiatanIndikatorTargetRenstra;
use App\Models\Renstra\Kegiatan\SasaranKegiatanRenstra;
use App\Models\Satuan;
use App\Transformers\KegiatanRenstraTransformer;
use App\Transformers\SatuanTransformer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class SasaranKegiatanController extends Controller
{
    public function index(Request $request) {
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        Gate::authorize('access-opd', $selectedOpd);
        $sasaranKegiatan = SasaranKegiatanRenstra::whereHas(
            'kegiatan.programRenstra.sasaranRenstra.renstraTujuan.opd',
            function($query) use($selectedOpd) {
                $query->where('id', $selectedOpd);
            }
        )->with([
            'kegiatan.mstKegiatan',
            'kegiatan.programRenstra.programRpjmd.mstProgram',
            'kegiatan.programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.tujuan.misi',
            'kegiatan.programRenstra.sasaranRenstra.renstraTujuan.rpjmdSasaran.prioritasPembangunan.mstPrioritasPembangunan',
        ])->get();

        return view('renstra.kegiatan.sasaran-kegiatan.index', compact(
            'selectedOpd',
            'opds',
            'sasaranKegiatan'
        ));
    }

    public function create(Request $request) {
        $opds = getAllowedOpds();
        $selectedOpd = $request->opd ?? auth()->user()->id_opd ?? $opds->first()->id;
        $satuan = fractal(Satuan::all(), new SatuanTransformer);
        return view('renstra.kegiatan.sasaran-kegiatan.create', compact(
            'selectedOpd',
            'opds',
            'satuan'
        ));
    }

    public function store(Request $request) {
        $this->validate($request, [
            "kegiatan" => 'required|exists:renstra_kegiatan,id',
            "sasaran" => 'required|string',
            "indikator" => 'required|array|min:1',
            "indikator.*.nama" => "required|string",
            "indikator.*.satuan" => "required|exists:mst_satuan,id",
            "indikator.*.is_pk" => "in:on",
            "indikator.*.is_iku" => "in:on",
            "indikator.*.cara_pengukuran" => "required|string",
            "indikator.*.target_awal" => "required|string",
            "indikator.*.target_akhir" => "required|string",
            "indikator.*.target" => "required|array|min:1",
            "indikator.*.target.*.id_tahun" => "required|exists:mst_periode_per_tahun,id",
            "indikator.*.target.*.value" => "required|string",
        ]);

        $sasaranKegiatanRenstra = SasaranKegiatanRenstra::create([
            "id_renstra_kegiatan" => $request->get('kegiatan'),
            "name" => $request->get('sasaran'),
        ]);

        collect($request->get('indikator'))->each(function($indikator) use($sasaranKegiatanRenstra) {
            $indikatorModel = SasaranKegiatanIndikatorRenstra::create([
                'name' => $indikator["nama"],
                'id_sasaran_kegiatan' => $sasaranKegiatanRenstra->id,
                'id_satuan' => $indikator["satuan"],
                'is_pk' => ($indikator["is_pk"] ?? null) == "on",
                'is_iku' => ($indikator["is_iku"] ?? null) == "on",
                'cara_pengukuran' => $indikator["cara_pengukuran"],
                'target_awal' => $indikator["target_awal"],
                'target_akhir' => $indikator["target_akhir"],
            ]);

            collect($indikator["target"])->each(function($target) use ($indikatorModel){
                SasaranKegiatanIndikatorTargetRenstra::create([
                    "id_periode_per_tahun" => $target["id_tahun"],
                    "id_sasaran_kegiatan_indikator" => $indikatorModel->id,
                    "target" => $target["value"],
                ]);
            });
        });

        return redirect()->route('renstra.kegiatan.sasaran_kegiatan.index');
    }

    public function submitRenja(SasaranKegiatanRenstra $sasaranKegiatan, SasaranKegiatanIndikatorRenstra $indikator, Request $request) {
        $this->validate($request, [
            'renja' => 'required|array|min:1',
            'renja.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'renja.*.value' => 'required|string',
        ]);

        $indikator->targetRenja()->delete();

        collect($request->get('renja'))->each(function($renja) use($indikator){
            $indikator->targetRenja()->create([
                "id_periode_per_tahun" => $renja["id_tahun"],
                "nilai" => $renja["value"]
            ]);
        });

        session()->flash('message', 'Renja Berhasil di input');

        return redirect()->back();
    }

    public function destroy(SasaranKegiatanRenstra $kegiatan) {
        $kegiatan->delete();

        flashSuccess('Berhasil di hapus');

        return redirect()->back();
    }

    public function edit(SasaranKegiatanRenstra $kegiatan, Request $request) {
        $opds = Opd::all();
        $satuan = fractal(Satuan::all(), new SatuanTransformer);
        $transformedKegiatan = fractal($kegiatan->kegiatan, new KegiatanRenstraTransformer)->parseIncludes(['mst_kegiatan', 'tahun_periode']);
        return view('renstra.kegiatan.sasaran-kegiatan.edit', compact(
            'opds',
            'satuan',
            'kegiatan',
            'transformedKegiatan'
        ));
    }

    public function update(SasaranKegiatanRenstra $sasaranKegiatanRenstra, Request $request) {
        $this->validate($request, [
            "kegiatan" => 'required|exists:renstra_kegiatan,id',
            "sasaran" => 'required|string',
            "indikator" => 'required|array|min:1',
            "indikator.*.id" => "nullable|exists:renstra_sasaran_kegiatan_indikator,id",
            "indikator.*.nama" => "required|string",
            "indikator.*.satuan" => "required|exists:mst_satuan,id",
            "indikator.*.is_pk" => "in:on",
            "indikator.*.is_iku" => "in:on",
            "indikator.*.cara_pengukuran" => "required|string",
            "indikator.*.target_awal" => "required|string",
            "indikator.*.target_akhir" => "required|string",
            "indikator.*.target" => "required|array|min:1",
            "indikator.*.target.*.id" => "nullable|exists:renstra_sasaran_kegiatan_indikator_target,id",
            "indikator.*.target.*.id_tahun" => "required|exists:mst_periode_per_tahun,id",
            "indikator.*.target.*.value" => "required|string",
        ]);



        $sasaranKegiatanRenstra->update([
            "id_renstra_kegiatan" => $request->get('kegiatan'),
            "name" => $request->get('sasaran'),
        ]);

        $indikatorItems = collect($request->get('indikator'));
        $indikatorItemIds = $indikatorItems->map(function($indikator){
            return $indikator["id"];
        })->filter(function($indikator){
            return $indikator != null;
        });

        $indikatorToDelete = $sasaranKegiatanRenstra->indikator()->whereNotIn('id', $indikatorItemIds)->get();

        foreach($indikatorToDelete as $indikator) {
            $indikator->delete();
        }

        $indikatorItems->each(function($indikator) use($sasaranKegiatanRenstra) {
            if($indikator["id"] != null) {
                $indikatorModel = $sasaranKegiatanRenstra->indikator()->firstWhere('id', $indikator["id"]);
                $indikatorModel->update([
                    'name' => $indikator["nama"],
                    'id_sasaran_kegiatan' => $sasaranKegiatanRenstra->id,
                    'id_satuan' => $indikator["satuan"],
                    'is_pk' => ($indikator["is_pk"] ?? null) == "on",
                    'is_iku' => ($indikator["is_iku"] ?? null) == "on",
                    'cara_pengukuran' => $indikator["cara_pengukuran"],
                    'target_awal' => $indikator["target_awal"],
                    'target_akhir' => $indikator["target_akhir"],
                ]);
            } else {
                $indikatorModel = $sasaranKegiatanRenstra->indikator()->create([
                    'name' => $indikator["nama"],
                    'id_sasaran_kegiatan' => $sasaranKegiatanRenstra->id,
                    'id_satuan' => $indikator["satuan"],
                    'is_pk' => ($indikator["is_pk"] ?? null) == "on",
                    'is_iku' => ($indikator["is_iku"] ?? null) == "on",
                    'cara_pengukuran' => $indikator["cara_pengukuran"],
                    'target_awal' => $indikator["target_awal"],
                    'target_akhir' => $indikator["target_akhir"],
                ]);
            }

            $targetItems = collect($indikator["target"]);
            $targetItemIds = $targetItems->map(function($target){
                return $target["id"];
            })->filter(function($target){
                return $target != null;
            });

            $targetToDelete = $indikatorModel->target()->whereNotIn('id', $targetItemIds)->get();

            foreach ($targetToDelete as $target) {
                $target->delete();
            }


            $targetItems->each(function($target) use ($indikatorModel){
                if($target["id"] != null ) {
                    $indikatorModel->target()->where('id', $target["id"])->update([
                        "id_periode_per_tahun" => $target["id_tahun"],
                        "target" => $target["value"],
                    ]);
                } else {
                    $indikatorModel->target()->create([
                        "id_periode_per_tahun" => $target["id_tahun"],
                        "target" => $target["value"],
                    ]);
                }
            });
        });

        session()->flash('message', 'Data berhasil di ubah');



        return redirect()->route('renstra.kegiatan.sasaran_kegiatan.edit', [ "kegiatan" => $sasaranKegiatanRenstra->id ]);
    }
}
