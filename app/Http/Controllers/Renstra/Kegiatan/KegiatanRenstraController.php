<?php

namespace App\Http\Controllers\Renstra\Kegiatan;

use App\Http\Controllers\Controller;
use App\Models\Kegiatan;
use App\Models\Opd;
use App\Models\PeriodePerTahun;
use App\Models\Renstra\Kegiatan\KegiatanRenstra;
use App\Models\Renstra\Kegiatan\KegiatanSumberAnggaranRenstra;
use App\Models\Renstra\Program\ProgramRenstra;
use App\Models\SumberAnggaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class KegiatanRenstraController extends Controller
{
    function index(Request $request)
    {
        $opd = getAllowedOpds();
        $selected_opd = $request->opd ?? auth()->user()->id_opd ?? $opd->first()->id;
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        Gate::authorize('access-opd', $selected_opd);

        $relate = 'sasaranRenstra.renstraTujuan.rpjmdSasaran';
        $q_hierarki = ProgramRenstra::with([
            "$relate.tujuan.misi", "$relate.prioritasPembangunan", 'kegiatanRenstra.mstKegiatan',
            'kegiatanRenstra.sumberAnggaran.mstSumberAnggaran', 'programRpjmd.mstProgram'
        ])
        ->whereHas('sasaranRenstra.renstraTujuan', function($query) use($selected_opd){
            $query->where('id_mst_opd', $selected_opd);
        })
        ->where('id_periode', '=', $this->id_periode);

        $hierarki = $q_hierarki->get();

        return view('renstra.kegiatan.kegiatan.kegiatan', [
            'opd' => $opd,
            'title' => 'KEGIATAN',
            'periode_per_tahun' => $periode_per_tahun,
            'selected_opd' => $selected_opd,
            'hierarki' => $hierarki,
        ]);
    }

    function create()
    {
        $opd = getAllowedOpds();
        $program = ProgramRenstra::where('id_periode', $this->id_periode)
            ->whereHas('sasaranRenstra.renstraTujuan', function($query) use ($opd){
                $query->whereIn('id_mst_opd', $opd->pluck('id'));
            })
            ->get();
        $kegiatan = Kegiatan::all();
        $anggaran = SumberAnggaran::all();

        return view('renstra.kegiatan.kegiatan.form-add', [
            'opd' => $opd,
            'program' => $program,
            'kegiatan' => $kegiatan,
            'anggaran' => $anggaran
        ]);
    }

    function store(Request $request)
    {
        $kegiatan = new KegiatanRenstra;
        $kegiatan->id_mst_opd = $request->opd;
        $kegiatan->id_mst_kegiatan = $request->kegiatan;
        $kegiatan->id_renstra_program = $request->program;
        $kegiatan->id_periode = $this->id_periode;
        $kegiatan->save();

        foreach($request->anggaran as $item) {
            $sumber_anggaran = new KegiatanSumberAnggaranRenstra;
            $sumber_anggaran->id_renstra_kegiatan = $kegiatan->id;
            $sumber_anggaran->id_mst_sumber_anggaran = $item;
            $sumber_anggaran->save();
        }

        return redirect('renstra/kegiatan/kegiatan')->with([
            'success' => 'Data berhasil ditambahkan',
        ]);
    }

    function edit($id)
    {
        $id = base64_decode($id);
        $opd = Opd::all();
        $program = ProgramRenstra::where('id_periode', $this->id_periode)->get();
        $kegiatan = Kegiatan::all();
        $anggaran = SumberAnggaran::all();

        $data_edit = KegiatanRenstra::with('sumberAnggaran')->where('id', $id)->get();

        return view('renstra.kegiatan.kegiatan.form-edit', [
            'opd' => $opd,
            'program' => $program,
            'kegiatan' => $kegiatan,
            'anggaran' => $anggaran,
            'data' => $data_edit,
        ]);
    }

    function update(Request $request, $id)
    {
        KegiatanRenstra::find($id)->update([
            'id_mst_opd' => $request->opd,
            'id_mst_kegiatan' => $request->kegiatan,
            'id_renstra_program' => $request->program,
        ]);

        return redirect('renstra/kegiatan/kegiatan')->with([
            'success' => 'Data berhasil diubah',
        ]);
    }

    function destroy($id)
    {
        $id = base64_decode($id);
        $kegiatanRenstra = KegiatanRenstra::find($id);

        $this->validateParentModelDeletion($kegiatanRenstra, 'Kegiatan Renstra', ['Sasaran Kegiatan', 'Sub Kegiatan']);

        $kegiatanRenstra->delete();

        flashSuccess('Data berhasil di hapus');

        return redirect('renstra/kegiatan/kegiatan')->with([
            'success' => 'Data berhasil dihapus',
        ]);
    }
}
