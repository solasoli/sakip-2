<?php

namespace App\Http\Controllers\Renstra;

use App\Http\Controllers\Controller;
use App\Models\Opd;
use App\Models\PeriodePerTahun;
use App\Models\Renstra\TujuanRenstra;
use App\Models\Renstra\TujuanRenstraIndikator;
use App\Models\Renstra\TujuanRenstraTarget;
use App\Models\Renstra\SasaranRenstra;
use App\Models\Renstra\StrategiRenstra;
use App\Models\Renstra\KebijakanRenstra;
use App\Models\Rpjmd\Sasaran;
use App\Models\Satuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class TujuanRenstraController extends Controller
{
    public function index(Request $request)
    {
        $tujuan_count = TujuanRenstra::where('id_periode', $this->id_periode)->count();
        $opd = getAllowedOpds();
        /** @var \App\Models\User */
        $loggedInUser = auth()->user();
        $opd_selected = $request->opd ?? $loggedInUser->id_opd ?? $opd->first()->id;

        $q_hierarki = Sasaran::with([
            'tujuan',
            'tujuan.misi',
            'prioritasPembangunan',
            'prioritasPembangunan.mstPrioritasPembangunan',
            'tujuanRenstra' => function ($query) use ($opd_selected) {
                $query->where('id_mst_opd', $opd_selected)->with([
                    'opd',
                    'indikator',
                    'indikator.target',
                    'indikator.satuan',
                ]);
            },
        ])->where('id_periode', $this->id_periode);

        Gate::authorize('access-opd', $opd_selected);

        $hierarki = $q_hierarki->get();

        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();
        return view('renstra.tujuan.tujuan', [
            'hierarki' => $hierarki,
            'periode_per_tahun' => $periode_per_tahun,
            'tujuan_count' => $tujuan_count,
            'opd' => $opd,
            'opd_selected' => $opd_selected
        ]);
    }

    public function create()
    {
        $opd = getAllowedOpds();
        $sasaran = Sasaran::where('id_periode', $this->id_periode)
            ->whereHas('program.opd', function ($query) use ($opd) {
                $query->whereIn('mst_opd.id', $opd->pluck('id'));
            })
            ->get();
        $satuan = Satuan::all();
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        return view('renstra.tujuan.form-add', [
            'sasaran' => $sasaran,
            'opd' => $opd,
            'satuan' => $satuan,
            'periode_per_tahun' => $periode_per_tahun,
        ]);
    }

    public function store(Request $request)
    {
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        $request->validate([
            'opd' => 'required',
            'tujuan' => 'required',
            'sasaran' => 'required',
            'indikator.*' => 'required|string',
            'pengukuran.*' => 'required|string',
            'satuan.*' => 'required|string',
            'awal.*' => 'required',
            'akhir.*' => 'required',
        ]);

        $data = $this->objDataRequest($request->all());
        $tujuan = new TujuanRenstra;
        $tujuan->name = $data->tujuan;
        $tujuan->id_rpjmd_sasaran = $data->sasaran;
        $tujuan->id_periode = $this->id_periode;
        $tujuan->id_mst_opd = $data->opd;
        $tujuan->save();

        $this->insertDetail($data->data_detail, $tujuan, $periode_per_tahun);

        return redirect('renstra/tujuan')->with([
            'success' => 'Data berhasil disimpan',
        ]);
    }

    private function objDataRequest($data)
    {
        $data_form = [];
        $indikator_count = count($data['indikator']);
        $target = array_chunk($data['target'], count($data['target']) / $indikator_count);
        for ($i = 0; $i < $indikator_count; $i++) {
            $data_detail[] = (object)[
                'indikator' => $data['indikator'][$i],
                'satuan' => $data['satuan'][$i],
                'awal' => $data['awal'][$i],
                'target' => $target[$i],
                'akhir' => $data['akhir'][$i]
            ];
        }

        $data_form = (object)[
            'opd' => $data['opd'],
            'tujuan' => $data['tujuan'],
            'sasaran' => $data['sasaran'],
            'data_detail' => $data_detail
        ];

        return $data_form;
    }

    private function insertDetail($arr_detail, $tujuan, $periode_per_tahun)
    {
        foreach ($arr_detail as $item) {
            $tujuan_indikator = new TujuanRenstraIndikator;
            $tujuan_indikator->indikator = $item->indikator;
            $tujuan_indikator->id_satuan = $item->satuan;
            $tujuan_indikator->id_tujuan = $tujuan->id;
            $tujuan_indikator->kondisi_awal = $item->awal;
            $tujuan_indikator->kondisi_akhir = $item->akhir;
            $tujuan_indikator->save();

            foreach ($item->target as $idx => $val) {
                $sasaran_target = new TujuanRenstraTarget;
                $sasaran_target->target = !is_null($val) ? $val : '-';
                $sasaran_target->id_indikator_tujuan = $tujuan_indikator->id;
                $sasaran_target->id_periode_per_tahun = $periode_per_tahun[$idx]->id;
                $sasaran_target->save();
            }
        }
    }

    public function edit($id)
    {
        $tujuan = TujuanRenstra::with(['indikator', 'indikator.target', 'opd'])->where([
            ['id_periode', $this->id_periode],
            ['id', base64_decode($id)]
        ])->get();

        $opd = getAllowedOpds();
        $sasaran = Sasaran::where('id_periode', $this->id_periode)
            ->whereHas('program.opd', function ($query) use ($opd) {
                $query->whereIn('mst_opd.id', $opd->pluck('id'));
            })
            ->get();
        $satuan = Satuan::all();
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        return view('renstra/tujuan/form-edit', [
            'sasaran' => $sasaran,
            'opd' => $opd,
            'satuan' => $satuan,
            'periode_per_tahun' => $periode_per_tahun,
            'tujuan' => $tujuan
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = $this->objDataRequest($request->all());
        $data_edit = TujuanRenstra::with(['indikator', 'indikator.target', 'opd'])->find(base64_decode($id));
        $data_edit->name = $data->tujuan;
        $data_edit->id_rpjmd_sasaran = $data->sasaran;
        $data_edit->id_mst_opd = $data->opd;
        $data_edit->save();

        // get id indikator
        $id_indikator = [];
        foreach ($data_edit->indikator as $item) {
            $id_indikator[] = $item->id;
        }

        // create part add (jika ada)
        if (count($data->data_detail) > $data_edit->indikator->count()) {
            $part_add = array_splice($data->data_detail, $data_edit->indikator->count());
            $this->insertDataInUpdate($part_add, base64_decode($id));
        }

        // create part delete (jika ada)
        if (count($data->data_detail) < $data_edit->indikator->count()) {
            $part_delete = array_splice($id_indikator, count($data->data_detail));
            $this->deleteDataInUpdate($part_delete);
        }

        // update data after filter (part add & part delete)
        foreach ($data->data_detail as $idx => $item) {
            // update data indikator
            $indikator = TujuanRenstraIndikator::find($id_indikator[$idx]);
            $indikator->indikator = $item->indikator;
            $indikator->id_satuan = $item->satuan;
            $indikator->id_tujuan = base64_decode($id);
            $indikator->kondisi_awal = $item->awal;
            $indikator->kondisi_akhir = $item->akhir;
            $indikator->update();

            // get data target for update
            $data_target = TujuanRenstraTarget::where('id_indikator_tujuan', $id_indikator[$idx])->get();
            foreach ($item->target as $key => $val) {
                // update data target
                $target = TujuanRenstraTarget::find($data_target[$key]->id);
                $target->target = !is_null($val) ? $val : "-";
                $target->update();
            }
        }

        return redirect('renstra/tujuan')->with([
            'success' => 'Data berhasil diubah',
        ]);
    }

    private function insertDataInUpdate($data_request, $id_tujuan)
    {
        // get periode (tahun)
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        foreach ($data_request as $item) {
            // insert data indikator
            $indikator = new TujuanRenstraIndikator;
            $indikator->indikator = $item->indikator;
            $indikator->id_satuan = $item->satuan;
            $indikator->id_tujuan = $id_tujuan;
            $indikator->kondisi_awal = $item->awal;
            $indikator->kondisi_akhir = $item->akhir;
            $indikator->save();

            foreach ($item->target as $idx => $val) {
                // insert data target
                $target = new TujuanRenstraTarget;
                $target->target = !is_null($val) ? $val : '-';
                $target->id_indikator_tujuan = $indikator->id;
                $target->id_periode_per_tahun = $periode_per_tahun[$idx]->id;
                $target->save();
            }
        }
    }

    private function deleteDataInUpdate($data_detele)
    {
        foreach ($data_detele as $id) {
            TujuanRenstraIndikator::find($id)->delete();
            TujuanRenstraTarget::where('id_indikator_tujuan', $id)->delete();
        }
    }

    public function destroy($id)
    {
        /** @var TujuanRenstra */
        $tujuan = TujuanRenstra::with(['indikator', 'indikator.target'])->find(base64_decode($id));

        $this->validateParentModelDeletion($tujuan, 'Tujuan Renstra', ['Sasaran Renstra']);

        $tujuan->delete();

        return redirect()->back()->with([
            'success' => 'Data berhasil dihapus',
        ]);
    }

    public function show($id)
    {
        $data = DB::table('renstra_tujuan as rt')
            ->where('rt.id', '=', $id)
            ->join('rpjmd_sasaran as rs', 'rt.id_rpjmd_sasaran', '=', 'rs.id')
            ->join('mst_periode as mp', 'rt.id_periode', '=', 'mp.id')
            ->join('mst_opd as opd', 'rt.id_mst_opd', '=', 'opd.id')
            ->select('rt.id as id_tujuan', 'rt.name as tujuan', 'rs.name as sasaran', 'mp.dari as dari', 'mp.sampai as sampai', 'opd.name as opd')
            ->get();

        return response()->json([
            'success' => true,
            'message' => 'Data Tujuan By ID',
            'data'    => $data,
        ], 200);
    }

    public function api()
    {
        $data = DB::table('renstra_tujuan as rt')
            ->join('rpjmd_sasaran as rs', 'rt.id_rpjmd_sasaran', '=', 'rs.id')
            ->join('mst_periode as mp', 'rt.id_periode', '=', 'mp.id')
            ->join('mst_opd as opd', 'rt.id_mst_opd', '=', 'opd.id')
            ->select('rt.id as id_tujuan', 'rt.name as tujuan', 'rs.name as sasaran', 'mp.dari as dari', 'mp.sampai as sampai', 'opd.name as opd')
            ->get();


        return response()->json([
            'success' => true,
            'message' => 'List Data Tujuan',
            'data'    => $data,
        ], 200);
    }
}
