<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Eselon;
use App\Models\Opd;
use App\Transformers\OpdTransformer;
use App\Transformers\PegawaiTransformer;
use Illuminate\Http\Request;

class OpdsController extends Controller
{
    public function index()
    {
        $opds = Opd::all();
        return jsonResponse([
            "opds" => fractal($opds, new OpdTransformer)
        ]);
    }

    public function getPegawai(Opd $opd, Request $request)
    {

        $nama = $request->get('search');
        $sasaran = $request->get('sasaran');

        $pegawai = $opd->pegawai();

        if ($sasaran == 'renstra') {
            $pegawai->whereIn('id_eselon', [Eselon::II_A, Eselon::II_B]);
        } elseif ($sasaran == 'program') {
            $pegawai->whereIn('id_eselon', [Eselon::II_B, Eselon::III_A, Eselon::III_B,]);
        } elseif ($sasaran == 'kegiatan') {
            $pegawai->whereIn('id_eselon', [Eselon::III_A, Eselon::III_B, Eselon::IV_A, Eselon::IV_B]);
        } elseif ($sasaran == 'sub-kegiatan') {
            $pegawai->where(function ($query) {
                $query->whereIn('id_eselon', [Eselon::IV_A, Eselon::IV_B, null])->orWhereNull('id_eselon');
            });
        }

        if ($nama) {
            $pegawai->where('nama', 'like', "%$nama%");
        }

        return jsonResponse([
            "pegawai" => fractal($pegawai->simplePaginate(), new PegawaiTransformer)
        ]);
    }
}
