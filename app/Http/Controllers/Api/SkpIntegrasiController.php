<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Pegawai;
use App\Models\Renstra\Kegiatan\SasaranKegiatanRenstra;
use App\Models\Renstra\Program\SasaranProgramRenstra;
use App\Models\Renstra\SasaranRenstra;
use App\Models\Renstra\SubKegiatan\SasaranSubKegiatanRenstra;
use App\Transformers\PegawaiTransformer;
use App\Transformers\SasaranKegiatanRenstraTransformer;
use App\Transformers\SasaranProgramRenstraTransformer;
use App\Transformers\SasaranRenstraTransformer;
use App\Transformers\SasaranSubKegiatanRenstraTransformer;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Validation\ValidationException;

class SkpIntegrasiController extends Controller
{
    public function getPegawai()
    {
        $pegawai = Pegawai::with([
            'opd',
            'eselon',
        ])->get();

        return jsonResponse([
            "pegawai" => fractal($pegawai, new PegawaiTransformer)->parseIncludes([
                'opd',
                'eselon',
            ])
        ]);
    }

    public function getSasaranRenstra(Request $request)
    {

        $sasaranRenstra = SasaranRenstra::with([
            'renstraTujuan.opd',
            'indikator.satuan'
        ]);

        if ($request->get('opd')) {
            $sasaranRenstra->whereHas('renstraTujuan.opd', function ($query) use ($request) {
                return $query->where('id_mst_opd', $request->get('opd'));
            });
        }

        return jsonResponse([
            "sasaran_renstra" => fractal($sasaranRenstra->get(), new SasaranRenstraTransformer)->parseIncludes([
                'renstra_tujuan.opd',
                'renstra_indikator.satuan'
            ])
        ]);
    }

    public function getSasaranProgramRenstra()
    {

        $sasaranProgramRenstra = SasaranProgramRenstra::with([
            'program.sasaranRenstra'
        ])->wherehas('program.sasaranRenstra');

        return jsonResponse([
            "sasaran_program_renstra" => fractal($sasaranProgramRenstra->get(), new SasaranProgramRenstraTransformer)->parseIncludes([
                'indikator',
                'program.sasaran_renstra'
            ])
        ]);
    }

    public function getSasaranKegiatanRenstra()
    {
        $sasaranKegiatanRenstra = SasaranKegiatanRenstra::with([
            'kegiatan.programRenstra.sasaranProgram',
            'indikator'
        ])->whereHas('kegiatan.programRenstra.sasaranProgram')
            ->whereHas('indikator')
            ->get();


        return jsonResponse([
            "sasaran_kegiatan_renstra" => fractal($sasaranKegiatanRenstra, new SasaranKegiatanRenstraTransformer)->parseIncludes([
                'kegiatan.program_renstra.sasaran_program',
                'indikator'
            ])
        ]);
    }

    public function getSasaranSubKegiatanRenstra()
    {
        $sasaranSubKegiatanRenstra = SasaranSubKegiatanRenstra::with([
            'subKegiatan.kegiatan.sasaranKegiatan',
            'output'
        ])->whereHas('subKegiatan.kegiatan.sasaranKegiatan',)
            ->whereHas('output')
            ->get();


        return jsonResponse([
            "sasaran_sub_kegiatan_renstra" => fractal($sasaranSubKegiatanRenstra, new SasaranSubKegiatanRenstraTransformer)->parseIncludes([
                'sub_kegiatan.kegiatan.sasaran_kegiatan',
                'output'
            ])
        ]);
    }

    public function getPerjanjianKinerja($nip) {
        $pegawai = Pegawai::where('nip', $nip)->firstOrFail();

        /** @var Collection */
        $targetPkRenja = $pegawai->targetPkRenjaTahunan()
            ->with([
                'pkRenjaTahunan.pk.sasaran',
                'satuan'
            ])
            ->whereHas('pkRenjaTahunan')
            ->get();

        $penanggungJawab = [
            "id" => $pegawai->id,
            "nip" => $pegawai->nip,
            "name" => $pegawai->nama,
        ];

        $pk = $targetPkRenja
            ->groupBy(function($target){
                return $target->pkRenjaTahunan->pk->sasaran->getTable() .'_'. $target->pkRenjaTahunan->pk->sasaran->id;
            })
            ->map(function(Collection $targetPkRenjaCollection){

                $sasaran = $targetPkRenjaCollection->first()->pkRenjaTahunan->pk->sasaran;

                $sasaranData = [
                    "id" => $sasaran->id,
                    "name" => $sasaran->name,
                    "target_pk" => $targetPkRenjaCollection->map(fn ($targetPkRenja) => [
                        "id" => $targetPkRenja->id,
                        "atasan" => [
                            "id" => $targetPkRenja->atasan->id,
                            "nip" => $targetPkRenja->atasan->nip,
                            "name" => $targetPkRenja->atasan->nama,
                        ],
                        "tahun" => [
                            "id" => $targetPkRenja->tahun->id,
                            "name" => $targetPkRenja->tahun->tahun,
                        ],
                        "indikator" => [
                            "id" => $targetPkRenja->pkRenjaTahunan->pk->id,
                            "name" => $targetPkRenja->pkRenjaTahunan->pk->name,
                            "target" => $targetPkRenja->target,
                            "satuan" => [
                                "id" => $targetPkRenja->satuan->id,
                                "name" => $targetPkRenja->satuan->name,
                            ],
                        ]
                    ])
                ];

                $className = get_class($sasaran);

                $anggaran = null;

                if ($className == SasaranRenstra::class) {

                    $anggaran = $targetPkRenjaCollection
                        ->map(function ($data) use ($className) {

                            $programRenstra = $data->pkRenjaTahunan->pk->sasaran->programRenstra;

                            return $programRenstra
                                ->map(function ($program) use ($className)  {
                                    return [
                                        'id' => $program->id,
                                        'type' => $this->jenisAnggaranLevel($className),
                                        'name' => $program->name,
                                        'anggaran' => $program->anggaranRenstra->map(fn($anggaran) => [
                                            "periode" => [
                                                "id" => $anggaran->tahun->id,
                                                "tahun" => $anggaran->tahun->tahun,
                                            ],
                                            "nilai" => $anggaran->nilai
                                        ]),
                                        'total_anggaran' => $program->anggaranRenstra->sum('nilai'),
                                        'sumber' => $program->getSumberAnggaran()
                                    ];
                                });
                        })->flatten(1)->unique('id');
                } else {

                    $anggaran = $targetPkRenjaCollection
                        ->map(function ($data) use ($className)  {
                            $referrencedParent = $data->pkRenjaTahunan->pk->sasaran->parent;
                            return [
                                'id' => $referrencedParent->id,
                                'type' => $this->jenisAnggaranLevel($className),
                                'name' => $referrencedParent->name,
                                'anggaran' => $referrencedParent->anggaranRenstra->map(fn($anggaran) => [
                                    "periode" => [
                                        "id" => $anggaran->tahun->id,
                                        "tahun" => $anggaran->tahun->tahun,
                                    ],
                                    "nilai" => $anggaran->nilai
                                ]),
                                'total_anggaran' => $referrencedParent->anggaranRenstra->sum('nilai'),
                                'sumber' => $referrencedParent->getSumberAnggaran()
                            ];
                        })->unique('id');
                }

                return [
                    "sasaran" => $sasaranData,
                    "anggaran" => $anggaran,
                ];
            });

        return jsonResponse([
            "perjanjian_kinerja" => $pk->values(),
            "penanggung_jawab" => $penanggungJawab
        ]);
    }

    private function jenisAnggaranLevel(string $jenis)
    {
        switch ($jenis) {
            case SasaranRenstra::class:
            case SasaranProgramRenstra::class:
                return 'program';
            case SasaranKegiatanRenstra::class:
                return 'kegiatan';
            case SasaranSubKegiatanRenstra::class:
                return 'sub_kegiatan';
            default:
                throw ValidationException::withMessages(['jenis' => 'Jenis salah']);
        }
    }
}
