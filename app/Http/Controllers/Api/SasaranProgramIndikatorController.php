<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Eselon;
use App\Models\Renstra\Program\SasaranProgramIndikatorRenstra;
use App\Transformers\PegawaiTransformer;
use Illuminate\Http\Request;

class SasaranProgramIndikatorController extends Controller
{
    public function pegawaiTersedia(SasaranProgramIndikatorRenstra $indikator) {
        $opd = $indikator->sasaranProgram->opd;

        $pegawai = $opd->pegawai()->where('id_eselon', Eselon::II_B)->simplePaginate();

        return jsonResponse([
            "pegawai" => fractal($pegawai, new PegawaiTransformer)
        ]);
    }
}
