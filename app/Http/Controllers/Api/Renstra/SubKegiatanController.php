<?php

namespace App\Http\Controllers\Api\Renstra;

use App\Http\Controllers\Controller;
use App\Models\Opd;
use App\Models\Renstra\SubKegiatan\SubKegiatanRenstra;
use App\Transformers\SubKegiatanRenstraTransformer;
use Illuminate\Http\Request;

class SubKegiatanController extends Controller
{
    public function getSubKegiatanDenganOpd(Opd $opd) {
        $subKegiatan = SubKegiatanRenstra::whereHas(
            'kegiatan.programRenstra.sasaranRenstra.renstraTujuan.opd',
            function($query) use ($opd) {
                return $query->where('id', $opd->id);
            }
        )->with([
            'mstSubKegiatan'
        ])->get();

        return jsonResponse([
            "sub_kegiatan" => fractal($subKegiatan, new SubKegiatanRenstraTransformer)->parseIncludes([
                'mst_sub_kegiatan',
                'tahun_periode'
            ])
        ]);
    }
}
