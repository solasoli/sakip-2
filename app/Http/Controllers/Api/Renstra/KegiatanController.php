<?php

namespace App\Http\Controllers\Api\Renstra;

use App\Http\Controllers\Controller;
use App\Models\Opd;
use App\Models\Renstra\Kegiatan\KegiatanRenstra;
use App\Models\SubKegiatan;
use App\Transformers\KegiatanRenstraTransformer;
use App\Transformers\SubKegiatanTransformer;
use Illuminate\Http\Request;

class KegiatanController extends Controller
{
    public function kegiatanDenganOpd(Opd $opd, Request $request)
    {
        $keyword = $request->term;
        $kegiatan = KegiatanRenstra::whereHas(
            'programRenstra.sasaranRenstra.renstraTujuan.opd',
            function ($query) use ($opd) {
                $query->where('id', $opd->id);
            },
        )->whereHas(
            'mstKegiatan',
            function ($query) use ($keyword) {
                if ($keyword)
                    $query->where('mst_kegiatan.name', 'like', "%$keyword%");
            }
        )
            ->with('mstKegiatan')->simplePaginate(70);

        return jsonResponse([
            "kegiatan" => fractal($kegiatan, new KegiatanRenstraTransformer)->parseIncludes(['mst_kegiatan', 'tahun_periode'])
        ]);
    }

    public function getSubKegiatan(KegiatanRenstra $kegiatan, Request $request)
    {
        $keyword = $request->term;
        $kegiatan = SubKegiatan::whereHas(
            'kegiatan.renstraKegiatan',
            function ($query) use ($kegiatan) {
                $query->where('id', $kegiatan->id);
            }
        );

        if ($keyword && $keyword != '') {
            $kegiatan->where(function ($query) use ($keyword) {
                $query->where('name', 'like', "%$keyword%");

                if (is_numeric($keyword)) {
                    $query->orWhere('kode_sub_kegiatan', str_pad($keyword, 2, 0, STR_PAD_LEFT));
                }
            });
        }

        return jsonResponse([
            "sub_kegiatan" => fractal($kegiatan->simplePaginate(70), new SubKegiatanTransformer)
        ]);
    }
}
