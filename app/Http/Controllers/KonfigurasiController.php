<?php

namespace App\Http\Controllers;

use App\Models\PeriodePerTahun;
use App\Models\PeriodeWalkot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KonfigurasiController extends Controller
{
    public function test()
    {
        return view('template');
    }
    public function index()
    {
        $periode = PeriodeWalkot::all();
        return view('konfigurasi', [
            'periode' => $periode,
        ]);
    }

    public function getTahunPeriode($id_periode)
    {
        $tahun = PeriodePerTahun::where('id_periode', $id_periode)->get();
        return $tahun;
    }

    public function update(Request $request)
    {
        $id_periode = $request->periode;
        $selectedPeriode = PeriodeWalkot::where('is_selected', 1)->get();
        $countSelected = $selectedPeriode->count();
        if ($countSelected > 0) {
            DB::table('mst_periode as mp')
                ->where('mp.is_selected', 1)
                ->update([
                    'is_selected' => 0,
                ]);
        }

        DB::table('mst_periode as mp')
            ->where('mp.id', $id_periode)
            ->update([
                'is_selected' => 1,
            ]);

        return redirect()->back()->with([
            'selected' => 'Periode berhasil di set',
        ]);
    }
}
