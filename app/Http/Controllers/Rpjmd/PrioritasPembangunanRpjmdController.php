<?php

namespace App\Http\Controllers\Rpjmd;

use App\Http\Controllers\Controller;
use App\Models\Misi;
use App\Models\PrioritasPembangunan;
use App\Models\Rpjmd\PrioritasPembangunanRpjmd;
use App\Models\Rpjmd\Sasaran;
use App\Models\Tujuan;
use Illuminate\Http\Request;

class PrioritasPembangunanRpjmdController extends Controller
{
    function index()
    {
        $misi = Misi::where('id_periode', $this->id_periode)->get();
        $tujuan = Tujuan::where('id_periode', $this->id_periode)->get();
        $sasaran = Sasaran::with(['sasaranDetail', 'prioritasPembangunan'])
            ->where('id_periode', $this->id_periode)
            ->get();

        $data = Sasaran::where('id_periode', $this->id_periode)->get();
        $mst_prioritas_pembangunan = PrioritasPembangunan::all();

        return view('rpjmd.prioritas-pembangunan.prioritas-pembangunan', [
            'misi' => $misi,
            'tujuan' => $tujuan,
            'sasaran' => $sasaran,
            'data' => $data,
            'mst_prioritas_pembangunan' => $mst_prioritas_pembangunan,
        ]);
    }

    function store(Request $request)
    {
        $sasaran = Sasaran::where('id_periode', $this->id_periode)->get();

        foreach ($sasaran as $idx => $item) {
            $str_prioritas_pembangunan = 'prioritas_pembangunan' . $idx;
            $str_sasaran = 'id_sasaran' . $idx;
            $ppr = PrioritasPembangunanRpjmd::firstOrNew(['id_sasaran' => $item->id]);
            $ppr->id_mst_prioritas_pembangunan = !is_null(request($str_prioritas_pembangunan)) ? request($str_prioritas_pembangunan) : 0;
            $ppr->id_sasaran = $request->$str_sasaran;
            $ppr->id_periode = $this->id_periode;
            $ppr->save();
        }

        return redirect()->back()->with([
            'success' => 'Data berhasil di tetapkan!',
        ]);
    }
}
