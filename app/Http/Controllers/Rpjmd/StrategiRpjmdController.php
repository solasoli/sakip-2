<?php

namespace App\Http\Controllers\Rpjmd;

use App\Http\Controllers\Controller;
use App\Models\Misi;
use App\Models\Rpjmd\Sasaran;
use App\Models\Rpjmd\StrategiRpjmd;
use App\Models\Tujuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StrategiRpjmdController extends Controller
{
    function index()
    {
        $data = DB::table('rpjmd_sasaran as rs')
            ->select(DB::raw('rs.*, rt.name as tujuan, rm.misi'))
            ->join('rpjmd_tujuan as rt', 'rs.id_tujuan', '=', 'rt.id')
            ->join('rpjmd_misi as rm', 'rt.id_misi', '=', 'rm.id')
            ->paginate(5);

        $misi = Misi::where('id_periode', $this->id_periode)->get();
        $tujuan = Tujuan::where('id_periode', $this->id_periode)->get();
        $sasaran = Sasaran::with('sasaranDetail')->where('id_periode', $this->id_periode)->get();
        $strategi = StrategiRpjmd::all();

        return view('rpjmd.strategi.strategi', [
            'data' => $data,
            'misi' => $misi,
            'tujuan' => $tujuan,
            'sasaran' => $sasaran,
            'strategi' => $strategi
        ]);
    }

    function store(Request $request)
    {
        $data_request = $this->dataObj($request, $this->id_periode);
        foreach ($data_request as $item) {

            $data = StrategiRpjmd::where('id_sasaran', $item['id_sasaran'][0]);
            $data_available = $data->get();
            $data_edit = [];
            foreach ($data_available as $val) {
                $data_edit[] = $val->id;
            }

            // tambahkan data (jika ada)
            if ($data_available->count() < count($item['strategi'])) {
                $data_add = array_splice($item['strategi'], $data_available->count());
                foreach ($data_add as $val) {
                    $strategi = new StrategiRpjmd;
                    $strategi->name = $val;
                    $strategi->id_sasaran = $item['id_sasaran'][0];
                    $strategi->id_periode = $this->id_periode;
                    $strategi->save();
                }
            }

            // delete data (jika ada)
            if ($data_available->count() > count($item['strategi'])) {
                $data_delete = array_splice($data_edit, count($item['strategi']));
                foreach ($data_delete as $val) {
                    $strategi = StrategiRpjmd::find($val);
                    $this->validateParentModelDeletion($strategi, "Strategi $strategi->name", ["Kebijakan RPJMD"]);
                }
            }

            // update data
            foreach ($data_edit as $i => $val) {
                StrategiRpjmd::find($val)->update([
                    'name' => $item['strategi'][$i],
                    'id_sasaran' => $item['id_sasaran'][0],
                ]);
            }
        }

        flashSuccess('Data berhasil di simpan');

        return redirect()->back();
    }

    private function dataObj($data, $periode)
    {
        $sasaran = Sasaran::where('id_periode', $periode)->get();

        foreach ($sasaran as $idx => $item) {
            $str_strategi = 'strategi' . $idx;
            $str_id_sasaran = 'id_sasaran' . $idx;

            if (isset($data->$str_strategi)) {
                if (!is_null($data->$str_strategi[0])) {
                    $data_form['data_' . $idx]['strategi'] = $data->$str_strategi;
                    $data_form['data_' . $idx]['id_sasaran'] = $data->$str_id_sasaran;
                }
            }
        }

        return $data_form;
    }
}
