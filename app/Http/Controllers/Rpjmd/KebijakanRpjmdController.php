<?php

namespace App\Http\Controllers\Rpjmd;

use App\Http\Controllers\Controller;
use App\Models\Rpjmd\KebijakanRpjmd;
use App\Models\Rpjmd\StrategiRpjmd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KebijakanRpjmdController extends Controller
{
    function index()
    {
        $strategi = StrategiRpjmd::all();
        $kebijakan = KebijakanRpjmd::all();
        $hierarki = DB::table('rpjmd_strategi as rst')
            ->select(DB::raw('rst.*, rm.misi as misi, rt.name as tujuan, rsa.name as sasaran'))
            ->join('rpjmd_sasaran as rsa', 'rst.id_sasaran', '=', 'rsa.id')
            ->join('rpjmd_tujuan as rt', 'rsa.id_tujuan', '=', 'rt.id')
            ->join('rpjmd_misi as rm', 'rt.id_misi', '=', 'rm.id')
            ->where([
                ['rm.id_periode', $this->id_periode],
            ])->paginate(5);

        return view('rpjmd.kebijakan.kebijakan', [
            'hierarki' => $hierarki,
            'kebijakan' => $kebijakan,
            'strategi' => $strategi
        ]);
    }

    function store(Request $request)
    {
        $data_request = $this->dataObj($request, $this->id_periode);
        foreach ($data_request as $item) {
            $data = KebijakanRpjmd::where('id_strategi', $item['id_strategi']);
            $data_available = $data->get();
            $data_edit = [];
            foreach ($data_available as $val) {
                $data_edit[] = $val->id;
            }

            // tambahkan data (jika ada)
            if ($data_available->count() < count($item['kebijakan'])) {
                $data_add = array_splice($item['kebijakan'], $data_available->count());
                foreach ($data_add as $val) {
                    $kebijakan = new KebijakanRpjmd;
                    $kebijakan->name = $val;
                    $kebijakan->id_strategi = $item['id_strategi'];
                    $kebijakan->id_periode = $this->id_periode;
                    $kebijakan->save();
                }
            }

            // delete data (jika ada)
            if ($data_available->count() > count($item['kebijakan'])) {
                $data_delete = array_splice($data_edit, count($item['kebijakan']));
                foreach ($data_delete as $val) {
                    KebijakanRpjmd::find($val)->delete();
                }
            }

            // update data
            foreach ($data_edit as $i => $val) {
                KebijakanRpjmd::find($val)->update([
                    'name' => $item['kebijakan'][$i],
                    'id_strategi' => $item['id_strategi'],
                ]);
            }
        }

        return redirect()->back()->with([
            'success' => 'Data berhasil di tetapkan!',
        ]);
    }

    private function dataObj($data, $periode)
    {
        $strategi = StrategiRpjmd::all();

        foreach ($strategi as $idx => $item) {
            $str_kebijakan = 'kebijakan' . $idx;
            $str_id_strategi = 'id_strategi' . $idx;

            if (isset($data->$str_kebijakan)) {
                if (!is_null($data->$str_kebijakan[0])) {
                    $data_form['data_' . $idx]['kebijakan'] = $data->$str_kebijakan;
                    $data_form['data_' . $idx]['id_strategi'] = $data->$str_id_strategi;
                }
            }
        }

        return $data_form;
    }
}
