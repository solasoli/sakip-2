<?php

namespace App\Http\Controllers\Rpjmd;

use App\Http\Controllers\Controller;
use App\Models\Visi;
use Illuminate\Http\Request;

class VisiController extends Controller
{
    public function index()
    {
        $data = Visi::with('periode')->where('id_periode', $this->id_periode)->get();
        return view('rpjmd/visi', [
            'data' => $data
        ]);
    }

    public function store(Request $request)
    {
        $clean = Visi::truncate();
        if ($this->id_periode == null) {
            return redirect()->back()->with([
                'not_periode' => 'Tahun periode belum di set',
            ]);
        }

        $visi = new Visi;
        $visi->visi = $request->visi;
        $visi->penjabaran = $request->penjabaran;
        $visi->id_periode = $this->id_periode;
        $visi->save();

        return redirect()->back()->with([
            'successAdd' => 'Success!',
        ]);
    }
}
