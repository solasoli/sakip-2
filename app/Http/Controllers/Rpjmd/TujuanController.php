<?php

namespace App\Http\Controllers\Rpjmd;

use App\Http\Controllers\Controller;
use App\Models\Misi;
use App\Models\PeriodePerTahun;
use App\Models\PeriodeWalkot;
use App\Models\Satuan;
use App\Models\Target;
use App\Models\Tujuan;
use App\Models\TujuanIndikator;
use App\Models\TujuanTarget;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TujuanController extends Controller
{
    function index()
    {
        $periode = null;
        $validate_periode = PeriodeWalkot::all();
        if (!is_null($validate_periode)) {
            $periode = $this->getPeriode();
        }
        $periodePerTahun = PeriodeWalkot::with('periodePerTahun')->where('id', $this->id_periode)->get();
        $misi = Misi::where('id_periode', $this->id_periode)->paginate(5);
        $tujuan = Tujuan::where('id_periode', $this->id_periode)->get();
        $indikator = TujuanIndikator::with(['tujuan', 'satuan', 'target'])
            ->whereHas('tujuan', function ($q) {
                $q->where('id_periode', $this->id_periode);
            })->get();

        $data = DB::table('rpjmd_tujuan_indikator as rti')
            ->selectRaw('*')
            ->join('mst_satuan as s', 's.id', '=', 'rti.id_satuan');

        return view('rpjmd.tujuan.tujuan', [
            'periode' => $periode,
            'misi' => $misi,
            'tujuan' => $tujuan,
            'data' => $data,
            'indikator' => $indikator,
            'periodePerTahun' => $periodePerTahun,
        ]);
    }

    function create()
    {
        $periode = $this->getPeriode();

        $misi = Misi::where('id_periode', $this->id_periode)->get();
        $satuan = Satuan::all();
        $years = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        return view('rpjmd.tujuan.form-tujuan', [
            'periode' => $periode,
            'misi' => $misi,
            'satuan' => $satuan,
            'years' => $years
        ]);
    }

    function store(Request $request)
    {
        // proses input target
        // mendapatkan jumlah tahun pada periode saat ini
        $getAllYearsInPeriode = PeriodePerTahun::where('id_periode', $this->id_periode)->get();
        $countAllYearsInPeriode = count($getAllYearsInPeriode);

        // validasi
        $request->validate([
            'tujuan' => ['required'],
            'misi' => ['required'],
            'indikator.*' => ['required'],
            'satuan.*' => ['required'],
            'awal.*' => ['required'],
            'akhir.*' => ['required']
        ]);

        // memecah request target pertahun
        $placeTarget = array();
        foreach ($request->target as $val) {
            is_null($val) ? array_push($placeTarget, '-') : array_push($placeTarget, $val);
        }

        // input data tujuan
        $tujuan = new Tujuan;
        $tujuan->name = $request->tujuan;
        $tujuan->id_misi = $request->misi;
        $tujuan->id_periode = $this->id_periode;
        $tujuan->save();

        $arrayTarget = array_chunk($placeTarget, $countAllYearsInPeriode);
        foreach ($arrayTarget as $i => $targets) {
            // input data indikator
            $indikator = new TujuanIndikator;
            $indikator->name = $request->indikator[$i];
            $indikator->id_tujuan = $tujuan->id;
            $indikator->id_misi = $request->misi;
            $indikator->id_satuan = $request->satuan[$i];
            $indikator->awal = $request->awal[$i];
            $indikator->akhir = $request->akhir[$i];
            $indikator->save();

            // input data target
            foreach ($targets as $idx => $target) {
                $inputTargets = new TujuanTarget;
                $inputTargets->target = $target;
                $inputTargets->id_periode_per_tahun = $getAllYearsInPeriode[$idx]->id;
                $inputTargets->id_tujuan_indikator = $indikator->id;
                $inputTargets->save();
            }
        }

        flashSuccess('Data berhasil di tambahkan');

        return redirect('/tujuan');
    }

    function edit($id)
    {
        $id = base64_decode($id);
        $periode = $this->getPeriode();
        $indikator = TujuanIndikator::with(['tujuan', 'satuan', 'target'])->where('id_tujuan', $id)->get();
        $misi = Misi::all();
        $tujuan = Tujuan::with('misi')->where('id', $id)->get();
        $satuan = Satuan::all();
        $years = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        return view('rpjmd.tujuan.form-tujuan-edit', [
            'periode' => $periode,
            'misi' => $misi,
            'satuan' => $satuan,
            'years' => $years,
            'indikator' => $indikator,
            'tujuan' => $tujuan,
        ]);
    }

    function update(Request $request, $id_tujuan)
    {
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        $data = $this->objDataRequest($request->all());
        $tujuan = Tujuan::find($id_tujuan);
        $tujuan->name = $request->tujuan;
        $tujuan->id_misi = $request->misi;
        $tujuan->id_periode = $this->id_periode;
        $tujuan->update();

        $data_tujuan_indikator = TujuanIndikator::with('target')
            ->where('id_tujuan', '=', $tujuan->id)->get();

        $tujuan_indikator_count = $data_tujuan_indikator->count();

        $count_request_indikator = count($data->data_detail);
        $data_delete = $data_tujuan_indikator->skip($count_request_indikator);

        if ($count_request_indikator < $tujuan_indikator_count) {
            foreach ($data_delete as $i => $item) {
                TujuanIndikator::find($item->id)->delete();
                foreach ($item->target as $itemTarget) {
                    TujuanTarget::find($itemTarget->id)->delete();
                }
            }
        }

        foreach ($data->data_detail as $idx => $val) {

            if ($idx < $tujuan_indikator_count) {
                TujuanIndikator::find($data_tujuan_indikator[$idx]->id)->update([
                    'name' => $val->indikator,
                    'id_satuan' => $val->satuan,
                    'id_tujuan' => $tujuan->id,
                    'id_misi' => $request->misi,
                    'awal' => $val->awal,
                    'akhir' => $val->akhir,
                ]);

                foreach ($val->target as $i => $item) {
                    $tujuan_target = TujuanTarget::where('id_tujuan_indikator', $data_tujuan_indikator[$idx]->id)->get();
                    TujuanTarget::find($tujuan_target[$i]->id)->update(['target' => is_null($item) ? '-' : $item]);
                }
            } else {
                $data_remainder = array_splice($data->data_detail, ($idx));
                foreach ($data_remainder as $item) {
                    $tujuan_indikator = new TujuanIndikator;
                    $tujuan_indikator->name = $item->indikator;
                    $tujuan_indikator->id_tujuan = $tujuan->id;
                    $tujuan_indikator->id_satuan = $item->satuan;
                    $tujuan_indikator->id_misi = $request->misi;
                    $tujuan_indikator->awal = $item->awal;
                    $tujuan_indikator->akhir = $item->akhir;
                    $tujuan_indikator->save();

                    foreach ($item->target as $idx => $val) {
                        $sasaran_target = new TujuanTarget;
                        $sasaran_target->target = $val;
                        $sasaran_target->id_tujuan_indikator = $tujuan_indikator->id;
                        $sasaran_target->id_periode_per_tahun = $periode_per_tahun[$idx]->id;
                        $sasaran_target->save();
                    }
                }
            }
        }

        flashSuccess('Data berhasil di ubah');

        return redirect('/tujuan');
    }

    static function objDataRequest($data)
    {
        $data_form = [];
        $indikator_count = count($data['indikator']);
        $target = array_chunk($data['target'], count($data['target']) / $indikator_count);
        for ($i = 0; $i < $indikator_count; $i++) {
            $data_detail[] = (object)[
                'indikator' => $data['indikator'][$i],
                'satuan' => $data['satuan'][$i],
                'awal' => $data['awal'][$i],
                'akhir' => $data['akhir'][$i],
                'target' => $target[$i]
            ];
        }

        $data_form = (object)[
            'tujuan' => $data['tujuan'],
            'data_detail' => $data_detail
        ];

        return $data_form;
    }

    function destroyIndikator($id)
    {
        $tujuanIndikator = TujuanIndikator::findOrFail($id);

        $tujuanIndikator->delete();

        flashSuccess('Indikator berhasil di hapus');

        return redirect()->back();
    }

    function destroy($id)
    {
        $tujuan = Tujuan::findOrFail($id);

        $this->validateParentModelDeletion($tujuan, 'Tujuan RPJMD', ['Sasaran RPJMD']);

        $tujuan->delete();

        flashSuccess('Data berhasil di hapus');

        return redirect()->back();
    }
}
