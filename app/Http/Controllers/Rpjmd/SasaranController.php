<?php

namespace App\Http\Controllers\Rpjmd;

use App\Http\Controllers\Controller;
use App\Models\PeriodePerTahun;
use App\Models\Rpjmd\Sasaran;
use App\Models\Rpjmd\SasaranDetail;
use App\Models\Rpjmd\SasaranTarget;
use App\Models\Satuan;
use App\Models\Tujuan;
use Illuminate\Http\Request;

class SasaranController extends Controller
{
    function index()
    {
        $data = Sasaran::with([
                'tujuan',
                'tujuan.misi',
                'sasaranDetail.sasaranTarget',
                'sasaranDetail.satuan',
                'sasaranDetail.targetRkpd.target'
            ])
            ->where('id_periode', $this->id_periode)
            ->paginate(5);

        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();
        $tujuan = Tujuan::where('id_periode', $this->id_periode)->get();

        $periode = periode();
        $satuan = Satuan::all();

        return view('rpjmd.sasaran.sasaran', [
            'data' => $data,
            'periode' => $periode,
            'periode_per_tahun' => $periode_per_tahun,
            'tujuan' => $tujuan,
            'satuan' => $satuan
        ]);
    }

    function create()
    {
        $tujuan = Tujuan::where('id_periode', $this->id_periode)->get();
        $satuan = Satuan::all();
        $years = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        return view('rpjmd.sasaran.form-sasaran-add', [
            'tujuan' => $tujuan,
            'satuan' => $satuan,
            'years' => $years
        ]);
    }

    function store(Request $request)
    {
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        $request->validate([
            'tujuan' => 'required',
            'sasaran' => 'required',
            'indikator.*' => 'required|string',
            'pengukuran.*' => 'required|string',
            'satuan.*' => 'required|string'
        ]);

        $data = $this->objDataRequest($request->all());
        $sasaran = new Sasaran;
        $sasaran->name = $data->sasaran;
        $sasaran->id_tujuan = $data->tujuan;
        $sasaran->id_periode = $this->id_periode;
        $sasaran->save();

        $this->insertDetail($data->data_detail, $sasaran, $periode_per_tahun);

        flashSuccess('Data berhasil disimpan');

        return redirect('/sasaran');
    }

    function edit($id)
    {
        $sasaran = Sasaran::with('tujuan', 'sasaranDetail')->where('id', base64_decode($id))->get();
        $sasaran_detail = SasaranDetail::with(['sasaran', 'sasaranTarget', 'satuan'])
            ->whereHas('sasaran', function ($q) {
                $q->where('id_periode', $this->id_periode);
            })->get();

        $tujuan = Tujuan::where('id_periode', $this->id_periode)->get();
        $satuan = Satuan::all();
        $years = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        return view('rpjmd.sasaran.form-sasaran-edit', [
            'sasaran' => $sasaran,
            'sasaran_detail' => $sasaran_detail,
            'tujuan' => $tujuan,
            'satuan' => $satuan,
            'years' => $years
        ]);
    }

    function update(Request $request, $id)
    {
        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();

        $data = $this->objDataRequest($request->all());

        $sasaran = Sasaran::find($id);
        $sasaran->name = $data->sasaran;
        $sasaran->id_tujuan = $data->tujuan;
        $sasaran->id_periode = $this->id_periode;
        $sasaran->update();

        $data_sasaran_detail = SasaranDetail::with('sasaranTarget')
            ->where('id_sasaran', '=', $sasaran->id)->get();

        $sasaran_detail_count = $data_sasaran_detail->count();

        $count_request_detail = count($data->data_detail);
        $data_delete = $data_sasaran_detail->skip($count_request_detail);

        if ($count_request_detail < $sasaran_detail_count) {
            foreach ($data_delete as $i => $item) {
                SasaranDetail::find($item->id)->delete();
                foreach ($item->sasaranTarget as $itemTarget) {
                    SasaranTarget::find($itemTarget->id)->delete();
                }
            }
        }

        foreach ($data->data_detail as $idx => $val) {

            if ($idx < $sasaran_detail_count) {
                SasaranDetail::find($data_sasaran_detail[$idx]->id)->update([
                    'indikator' => $val->indikator,
                    'cara_pengukuran' => $val->pengukuran,
                    'id_satuan' => $val->satuan,
                    'id_sasaran' => $sasaran->id,
                    'is_pk' => $val->pk,
                    'is_iku' => $val->iku,
                    'awal' => $val->awal,
                    'akhir' => $val->akhir,
                ]);

                foreach ($val->target as $i => $item) {
                    $sasaran_target = SasaranTarget::where('id_sasaran_detail', $data_sasaran_detail[$idx]->id)->get();
                    SasaranTarget::find($sasaran_target[$i]->id)->update(['target' => $item]);
                }
            } else {
                $data_remainder = array_splice($data->data_detail, ($idx));
                $this->insertDetail($data_remainder, $sasaran, $periode_per_tahun);
            }
        }

        flashSuccess('Data berhasil di ubah');

        return redirect('/sasaran');
    }

    private static function objDataRequest($data)
    {
        $data_form = [];
        $indikator_count = count($data['indikator']);
        $target = array_chunk($data['target'], count($data['target']) / $indikator_count);
        for ($i = 0; $i < $indikator_count; $i++) {
            $data_detail[] = (object)[
                'indikator' => $data['indikator'][$i],
                'satuan' => $data['satuan'][$i],
                'pengukuran' => $data['pengukuran'][$i],
                'pk' => $data['pk'][$i],
                'iku' => $data['iku'][$i],
                'awal' => $data['awal'][$i],
                'akhir' => $data['akhir'][$i],
                'target' => $target[$i]
            ];
        }

        $data_form = (object)[
            'tujuan' => $data['tujuan'],
            'sasaran' => $data['sasaran'],
            'data_detail' => $data_detail
        ];

        return $data_form;
    }

    private static function insertDetail($arr_detail, $sasaran, $periode_per_tahun)
    {
        foreach ($arr_detail as $item) {
            $sasaran_detail = new SasaranDetail;
            $sasaran_detail->indikator = $item->indikator;
            $sasaran_detail->cara_pengukuran = $item->pengukuran;
            $sasaran_detail->id_satuan = $item->satuan;
            $sasaran_detail->id_sasaran = $sasaran->id;
            $sasaran_detail->is_pk = $item->pk;
            $sasaran_detail->is_iku = $item->iku;
            $sasaran_detail->awal = $item->awal;
            $sasaran_detail->akhir = $item->akhir;
            $sasaran_detail->save();

            foreach ($item->target as $idx => $val) {
                $sasaran_target = new SasaranTarget;
                $sasaran_target->target = $val;
                $sasaran_target->id_sasaran_detail = $sasaran_detail->id;
                $sasaran_target->id_periode_per_tahun = $periode_per_tahun[$idx]->id;
                $sasaran_target->save();
            }
        }
    }

    function destroy($id)
    {
        $data = Sasaran::findOrFail($id);

        $this->validateParentModelDeletion($data, 'Sasaran', ['Program RPJMD', 'Tujuan Renstra']);
        $data->delete();

        flashSuccess('Data Berhasil Di Hapus');

        return redirect()->back();
    }

    public function submitTargetRkpd(SasaranDetail $sasaranDetail, Request $request) {
        $this->validate($request, [
            'satuan' => 'required|exists:mst_satuan,id',
            'target' => 'required|array|min:1',
            'target.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'target.*.value' => 'required|string',
        ]);



        $targetRkpd = $sasaranDetail->targetRkpd()->firstOrCreate([], [
            'id_satuan' => $request->get('satuan'),
        ]);

        $targetRkpd->update([
            "id_satuan" => $request->get('satuan')
        ]);

        $targetRkpd->target()->delete();
        foreach ($request->get('target') as $target) {
            $targetRkpd->target()->create([
                "id_periode_per_tahun" => $target["id_tahun"],
                "nilai" => $target["value"]
            ]);
        }

        flashSuccess('Target berhasil di input');

        return redirect()->back();
    }
}
