<?php

namespace App\Http\Controllers\Rpjmd\Program;

use App\Http\Controllers\Controller;
use App\Models\PeriodePerTahun;
use App\Models\Rpjmd\Program\ProgramRpjmd;
use App\Models\Rpjmd\Program\ProgramIndikatorRpjmd;
use App\Models\Rpjmd\Program\ProgramTargetRpjmd;
use App\Models\Satuan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndikatorProgramRpjmdController extends Controller
{
    function index()
    {
        // get data hierarki from programRpjmd and relation
        $data = ProgramRpjmd::with([
            'sasaran.tujuan.misi',
            'sasaran.prioritasPembangunan',
            'mstProgram',
            'indikatorProgram.targetRkpd.target',
            'indikatorProgram.targetProgram',
        ])->whereHas('sasaran.tujuan.misi', function ($query) {
            $query->where('id_periode', $this->id_periode);
        })->paginate(10);

        $satuan = Satuan::all();

        return view('rpjmd.program.indikator-program.indikator-program', [
            'program' => $data,
            'satuan' => $satuan,
        ]);
    }

    function store(Request $request)
    {
        $this->validate($request, [
            'program' => 'required|array',
            'program.*.id' => 'required|exists:rpjmd_program,id',
            'program.*.indikator' => 'nullable|array',
            'program.*.indikator.*.id' => 'nullable|exists:rpjmd_program_indikator',
            'program.*.indikator.*.name' => 'required|string',
            'program.*.indikator.*.satuan_id' => 'required|exists:mst_satuan,id',
            'program.*.indikator.*.kondisi_awal' => 'nullable|string',
            'program.*.indikator.*.kondisi_akhir' => 'nullable|string',
            'program.*.indikator.*.target' => 'nullable|array',
            'program.*.indikator.*.target.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'program.*.indikator.*.target.*.value' => 'required',
        ]);

        foreach ($request->get('program') as $programParam) {
            $program = ProgramRpjmd::with([
                'indikatorProgram'
            ])->firstWhere('id', $programParam['id']);

            $indikatorParams = $programParam['indikator'] ?? [];

            $indikatorIds = collect($indikatorParams)->pluck('id');
            $indikatorToDelete = $program->indikatorProgram->whereNotIn('id', $indikatorIds);

            foreach ($indikatorToDelete as $indikator) {
                $indikator->delete();
            }

            foreach ($indikatorParams as $indikatorParam) {
                $indikator = $indikatorParam['id'] == null
                    ? new ProgramIndikatorRpjmd(['id_rpjmd_program' => $program->id])
                    : $program->indikatorProgram->firstWhere('id', $indikatorParam['id']);

                $indikator->indikator = $indikatorParam['name'];
                $indikator->id_satuan = $indikatorParam['satuan_id'];
                $indikator->awal = $indikatorParam['kondisi_awal'] ?? '';
                $indikator->akhir = $indikatorParam['kondisi_akhir'] ?? '';
                $indikator->save();

                foreach ($indikatorParam["target"] ?? [] as $targetParam) {
                    $indikator->targetProgram()->updateOrCreate([
                        'id_periode_per_tahun' => $targetParam['id_tahun'],
                    ], [
                        'target' => $targetParam['value']
                    ]);
                }
            }
        }

        flashSuccess('Data berhasil di tetapkan');

        return redirect()->back();
    }

    function destroy($id)
    {
        ProgramIndikatorRpjmd::find(base64_decode($id))->delete();

        return redirect()->back()->with(['success' => 'Data berhasil dihapus']);
    }

    public function submitTargetRkpd(ProgramIndikatorRpjmd $indikator, Request $request) {
        $this->validate($request, [
            'satuan' => 'required|exists:mst_satuan,id',
            'target' => 'required|array|min:1',
            'target.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'target.*.value' => 'required|string',
        ]);

        $targetRkpd = $indikator->targetRkpd()->firstOrCreate([], [
            'id_satuan' => $request->get('satuan'),
        ]);

        $targetRkpd->update([
            "id_satuan" => $request->get('satuan')
        ]);

        $targetRkpd->target()->delete();
        foreach ($request->get('target') as $target) {
            $targetRkpd->target()->create([
                "id_periode_per_tahun" => $target["id_tahun"],
                "nilai" => $target["value"]
            ]);
        }

        session()->flash('message', 'Target Berhasil di input');

        return redirect()->back();
    }
}
