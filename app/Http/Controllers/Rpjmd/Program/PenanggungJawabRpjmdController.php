<?php

namespace App\Http\Controllers\Rpjmd\Program;

use App\Http\Controllers\Controller;
use App\Models\Opd;
use App\Models\Rpjmd\Program\PenaggungJawabRpjmd;
use App\Models\Rpjmd\Program\ProgramRpjmd;
use Illuminate\Http\Request;

class PenanggungJawabRpjmdController extends Controller
{
    public function index()
    {
        $data = ProgramRpjmd::with([
            'sasaran.prioritasPembangunan', 'sasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'sasaran.tujuan', 'sasaran.tujuan.misi',
            'mstProgram',
        ])->whereHas('sasaran.tujuan.misi', function ($query) {
            $query->where('id_periode', $this->id_periode);
        })->get();

        $opd = Opd::all();
        $rpjmd_penanggung_jawab = PenaggungJawabRpjmd::where('id_periode', $this->id_periode)->get();

        return view('rpjmd.program.penanggung-jawab.penanggung-jawab', [
            'data' => $data,
            'opd' => $opd,
            'rpjmd_penanggung_jawab' => $rpjmd_penanggung_jawab
        ]);
    }

    public function store(Request $request)
    {
        $data_request = $this->dataObj($request);
        // dd($data_request);
        foreach ($data_request as $item) {
            $data = PenaggungJawabRpjmd::where('id_rpjmd_program', $item['id_program']);
            $data_available = $data->get();
            $data_edit = [];
            foreach ($data_available as $val) {
                $data_edit[] = $val->id;
            }

            // tambahkan data (jika ada)
            if ($data_available->count() < count($item['penanggung_jawab'])) {
                $data_add = array_splice($item['penanggung_jawab'], $data_available->count());
                foreach ($data_add as $val) {
                    $penanggung_jawab = new PenaggungJawabRpjmd;
                    $penanggung_jawab->id_mst_opd = $val;
                    $penanggung_jawab->id_rpjmd_program = $item['id_program'][0];
                    $penanggung_jawab->id_periode = $this->id_periode;
                    $penanggung_jawab->save();
                }
            }

            // delete data (jika ada)
            if ($data_available->count() > count($item['penanggung_jawab'])) {
                $data_delete = array_splice($data_edit, count($item['penanggung_jawab']));
                foreach ($data_delete as $val) {
                    PenaggungJawabRpjmd::find($val)->delete();
                }
            }

            // update data
            foreach ($data_edit as $i => $val) {
                PenaggungJawabRpjmd::find($val)->update([
                    'id_mst_opd' => $item['penanggung_jawab'][$i],
                    'id_rpjmd_program' => $item['id_program'][0],
                ]);
            }
        }

        return redirect()->back()->with([
            'success' => 'Data berhasil di tetapkan!',
        ]);
    }

    static function dataObj($data)
    {
        $program = ProgramRpjmd::all();
        $data_form = [];
        foreach ($program as $idx => $item) {
            $str_program = 'program' . $idx;
            $str_penanggung_jawab = 'opd' . $idx;

            if (!is_null($data->$str_penanggung_jawab[0])) {
                $data_form['data_' . $idx]['id_program'] = $data->$str_program;
                $data_form['data_' . $idx]['penanggung_jawab'] = $data->$str_penanggung_jawab;
            }
        }

        return $data_form;
    }
}
