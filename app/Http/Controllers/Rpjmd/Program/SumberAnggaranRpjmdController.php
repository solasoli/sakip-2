<?php

namespace App\Http\Controllers\Rpjmd\Program;

use App\Http\Controllers\Controller;
use App\Models\Rpjmd\Program\ProgramRpjmd;
use App\Models\Rpjmd\Program\SumberAnggaranRpjmd;
use App\Models\SumberAnggaran;
use Illuminate\Http\Request;

class SumberAnggaranRpjmdController extends Controller
{
    public function index()
    {
        $data = ProgramRpjmd::with([
            'sasaran.prioritasPembangunan', 'sasaran.prioritasPembangunan.mstPrioritasPembangunan',
            'sasaran', 'sasaran.tujuan', 'sasaran.tujuan.misi',
            'mstProgram',
        ])->whereHas('sasaran.tujuan.misi', function ($query) {
            $query->where('id_periode', $this->id_periode);
        })->get();

        $sumber_anggaran_rpjmd = SumberAnggaranRpjmd::where('id_periode', $this->id_periode)->get();
        $sumber_anggaran_mst = SumberAnggaran::all();

        return view('rpjmd.program.sumber-anggaran.sumber-anggaran', [
            'data' => $data,
            'sumber_anggaran_rpjmd' => $sumber_anggaran_rpjmd,
            'sumber_anggaran_mst' => $sumber_anggaran_mst,
        ]);
    }

    public function store(Request $request)
    {
        $data_request = $this->dataObj($request, $this->id_periode);

        foreach ($data_request as $item) {
            $data = SumberAnggaranRpjmd::where('id_rpjmd_program', $item['id_program']);
            $data_available = $data->get();
            $data_edit = [];
            foreach ($data_available as $val) {
                $data_edit[] = $val->id;
            }

            // tambahkan data (jika ada)
            if ($data_available->count() < count($item['sumber_anggaran'])) {
                $data_add = array_splice($item['sumber_anggaran'], $data_available->count());
                foreach ($data_add as $val) {
                    $sumber_anggaran = new SumberAnggaranRpjmd;
                    $sumber_anggaran->id_mst_sumber_anggaran = $val;
                    $sumber_anggaran->id_rpjmd_program = $item['id_program'][0];
                    $sumber_anggaran->id_periode = $this->id_periode;
                    $sumber_anggaran->save();
                }
            }

            // delete data (jika ada)
            if ($data_available->count() > count($item['sumber_anggaran'])) {
                $data_delete = array_splice($data_edit, count($item['sumber_anggaran']));
                foreach ($data_delete as $val) {
                    SumberAnggaranRpjmd::find($val)->delete();
                }
            }

            // update data
            foreach ($data_edit as $i => $val) {
                SumberAnggaranRpjmd::find($val)->update([
                    'id_mst_sumber_anggaran' => $item['sumber_anggaran'][$i],
                    'id_rpjmd_program' => $item['id_program'][0],
                ]);
            }
        }

        return redirect()->back()->with([
            'success' => 'Data berhasil di tetapkan!',
        ]);
    }

    private function dataObj($data, $periode)
    {
        $program = ProgramRpjmd::all();

        foreach ($program as $idx => $item) {
            $str_program = 'program' . $idx;
            $str_sumber_anggaran = 'sumber_anggaran' . $idx;
            if (!is_null($data->$str_sumber_anggaran[0])) {
                $data_form['data_' . $idx]['id_program'] = $data->$str_program;
                $data_form['data_' . $idx]['sumber_anggaran'] = $data->$str_sumber_anggaran;
            }
        }

        return $data_form;
    }
}
