<?php

namespace App\Http\Controllers\Rpjmd\Program;

use App\Http\Controllers\Controller;
use App\Models\PeriodePerTahun;
use App\Models\Program;
use App\Models\Rpjmd\Program\ProgramRpjmd;
use App\Models\Rpjmd\Sasaran;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProgramRpjmdController extends Controller
{
    function index()
    {
        $data = Sasaran::with([
            'prioritasPembangunan', 'prioritasPembangunan.mstPrioritasPembangunan',
            'tujuan', 'tujuan.misi',
            'program.anggaran',
            'program.mstProgram',
        ])->whereHas('tujuan.misi', function ($query) {
            $query->where('id_periode', $this->id_periode);
        })->where('id_periode', $this->id_periode)->get();

        $periode_per_tahun = PeriodePerTahun::where('id_periode', $this->id_periode)->get();
        $sasaran = Sasaran::all();
        $mst_program = Program::whereDoesntHave('rpjmdProgram')->get();

        return view('rpjmd.program.program.program', [
            'data' => $data,
            'periode_per_tahun' => $periode_per_tahun,
            'sasaran' => $sasaran,
            'mst_program' => $mst_program,
        ]);
    }

    function store(Request $request)
    {
        $this->validate($request, [
            'program' => [
                'required',
                Rule::unique('rpjmd_program', 'id_mst_program'),
                'exists:mst_program,id'
            ],
            'sasaran' => 'required|exists:rpjmd_sasaran,id'
        ]);

        $program = new ProgramRpjmd;
        $program->id_mst_program = $request->program;
        $program->id_rpjmd_sasaran = $request->sasaran;
        $program->id_periode = $this->id_periode;
        $program->save();

        flashSuccess('Data Berhasil di tambahkan');

        return redirect()->back();
    }

    function update(Request $request, $id)
    {
        $this->validate($request, [
            'program' => [
                'required',
                Rule::unique('rpjmd_program', 'id_mst_program')->ignore($id),
                'exists:mst_program,id'
            ],
            'sasaran' => 'required|exists:rpjmd_sasaran,id'
        ]);

        $program = ProgramRpjmd::find($id);
        $program->id_rpjmd_sasaran = $request->sasaran;
        $program->id_mst_program = $request->program;
        $program->update();

        flashSuccess('Data Berhasil di ubah');

        return redirect()->back();
    }

    function destroy($id)
    {
        $program = ProgramRpjmd::find(base64_decode($id));

        $this->validateParentModelDeletion($program, 'Program RPJMD', ['Indikator Program RPJMD', 'Program Renstra']);

        $program->delete();

        flashSuccess('Data berhasil di hapus');

        return redirect()->back();
    }

    public function submitAnggaranRkpd(ProgramRpjmd $program, Request $request) {
        $this->validate($request, [
            'anggaran' => 'required|array|min:1',
            'anggaran.*.id_tahun' => 'required|exists:mst_periode_per_tahun,id',
            'anggaran.*.value' => 'required|integer',
        ]);

        $anggaranInput = $request->get('anggaran');

        $program->anggaran()->delete();
        foreach ($anggaranInput as $anggaran) {
            $program->anggaran()->create([
                "id_periode_per_tahun" => $anggaran["id_tahun"],
                "nilai" => $anggaran["value"]
            ]);
        }

        session()->flash('message', 'Anggaran Berhasil di input');

        return redirect()->back();
    }
}
