<?php

namespace App\Http\Controllers\Rpjmd;

use App\Http\Controllers\Controller;
use App\Models\Misi;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class MisiController extends Controller
{
    public function index()
    {
        $misi = Misi::where('id_periode', $this->id_periode)->get();
        return view('rpjmd.misi', [
            'misi' => $misi,
        ]);
    }

    public function store(Request $request)
    {
        if ($this->id_periode == null) {
            throw ValidationException::withMessages(["Tahun periode belum di isi"]);
            return redirect()->back();
        }

        $misi = Misi::where('id_periode', $this->id_periode)->get();
        $check_count = $misi->count();

        $all_request = $request->all()['misi'];
        $request_reminder = array_splice($all_request, $check_count);

        // update old misi
        foreach ($misi as $idx => $item) {
            Misi::find($item->id)->update([
                'misi' => $all_request[$idx],
            ]);
        }

        if (count($request_reminder) > 0) {
            foreach ($request_reminder as $item) {
                // add new misi
                $misi = new Misi;
                $misi->misi = $item;
                $misi->id_periode = $this->id_periode;
                $misi->save();
            }
        }

        flashSuccess('Data berhasil di simpan');

        return redirect()->back();
    }

    function destroy($id)
    {
        $misi = Misi::findOrFail(base64_decode($id));

        $this->validateParentModelDeletion($misi, 'Misi', ['Tujuan RPJMD']);

        $misi->delete();

        flashSuccess('Data berhasil di hapus');

        return redirect()->back();
    }
}
