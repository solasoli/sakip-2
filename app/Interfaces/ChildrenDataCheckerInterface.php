<?php

namespace App\Interfaces;

interface ChildrenDataCheckerInterface {
    public function hasMandatoryChildren() : bool;
}
