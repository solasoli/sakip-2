<?php

namespace App\Providers;

use App\Constants\AuthorizationModule;
use App\Constants\Role;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('access-opd', function(User $user, int $opdId) {
            if($user->role == Role::SUPER_ADMIN)
                return true;

            if($user->role == Role::ADMIN_OPD)
                return $user->id_opd == $opdId;

            return false;
        });

        Gate::define('access-module', function(User $user, string $module) {
            if($user->role == Role::SUPER_ADMIN)
                return true;

            if($user->role == Role::ADMIN_OPD)
                return in_array($module, [
                    AuthorizationModule::RENSTRA,
                    AuthorizationModule::PK,
                    AuthorizationModule::LAPORAN,
                ]);

            return false;
        });

        Gate::define('access-other-opd', function(User $user) {
            return $user->role == Role::SUPER_ADMIN;
        });
    }
}
