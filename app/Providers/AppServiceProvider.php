<?php

namespace App\Providers;

use App\Models\PeriodeWalkot;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('PeriodeAktif', function ($app){
            $periode = PeriodeWalkot::where('is_selected', 1)->first();
            return $periode;
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
