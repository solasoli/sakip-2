<?php

namespace App\Exports\Laporan;

use App\Models\Renstra\Program\ProgramRenstra;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;

class RenstraExport implements FromCollection
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $program = ProgramRenstra::all();

        return $program;
    }
}
