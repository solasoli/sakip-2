<?php

namespace App\Constants;

class AuthorizationModule {
    const MASTER = 'master';
    const RPJMD = 'rpjmd';
    const RENSTRA = 'renstra';
    const PK = 'perjanjian_kinerja';
    const LAPORAN = 'laporan';
    const CONFIGURATION = 'configuration';
}
