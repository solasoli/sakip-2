<?php

namespace App\Constants;

class Role {
    const ADMIN_OPD = 'admin_opd';
    const SUPER_ADMIN = 'super_admin';

    static function get() {
        return collect([
            [
                'name' => 'Admin OPD',
                'value' => self::ADMIN_OPD,
            ],
            [
                'name' => 'Super Admin',
                'value' => self::SUPER_ADMIN,
            ]
        ]);
    }
}
