<?php

namespace App\Models;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\Rpjmd\Sasaran;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Misi extends Model implements ChildrenDataCheckerInterface
{
    use HasFactory;
    protected $table = 'rpjmd_misi';
    protected $guarded = ['id'];

    public static function booted() {
        static::deleting(function(Misi $misi) {
            foreach ($misi->tujuan as $item) {
                $item->delete();
            }
        });
    }

    public function hasMandatoryChildren(): bool
    {
        $this->loadCount(['tujuan']);
        return $this->tujuan_count > 0;
    }

    function tujuanIndikator()
    {
        return $this->hasOne(TujuanIndikator::class, 'id_misi');
    }

    function tujuan()
    {
        return $this->hasMany(Tujuan::class, 'id_misi');
    }

    function sasaran()
    {
        return $this->hasManyThrough(Sasaran::class, Tujuan::class, 'id_misi', 'id_tujuan', 'id', 'id');
    }

    function periode()
    {
        return $this->belongsTo(PeriodeWalkot::class, 'id_periode');
    }
}
