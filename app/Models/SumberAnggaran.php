<?php

namespace App\Models;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\Renstra\Kegiatan\KegiatanRenstra;
use App\Models\Renstra\Kegiatan\KegiatanSumberAnggaranRenstra;
use App\Models\Renstra\SubKegiatan\SubKegiatanRenstra;
use App\Models\Rpjmd\Program\ProgramRpjmd;
use App\Models\Rpjmd\Program\SumberAnggaranRpjmd;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SumberAnggaran extends Model implements ChildrenDataCheckerInterface
{
    protected $table = 'sumber_anggaran';

    public static function booted() {
        static::deleting(function(SumberAnggaran $sumberAnggaran) {

            foreach ($sumberAnggaran->programRpjmd as $item) {
                $item->delete();
            }

            foreach ($sumberAnggaran->kegiatanRenstra as $item) {
                $item->delete();
            }

            foreach ($sumberAnggaran->subKegiatanRenstra as $item) {
                $item->delete();
            }
        });
    }

    public function hasMandatoryChildren(): bool
    {
        return $this->programRpjmd()->count() > 0
            || $this->kegiatanSumberAnggaranRenstra()->count() > 0
            || $this->subKegiatanRenstra()->count() > 0;
    }

    public function kegiatanSumberAnggaranRenstra() {
        return $this->hasMany(KegiatanSumberAnggaranRenstra::class, 'id_mst_sumber_anggaran');
    }

    public function subKegiatanRenstra() {
        return $this->belongsToMany(
            SubKegiatanRenstra::class,
            'renstra_sub_kegiatan_sumber_anggaran',
            'id_sumber_anggaran',
            'id_renstra_sub_kegiatan',
        );
    }

    public function kegiatanRenstra() {
        return $this->belongsToMany(
            KegiatanRenstra::class,
            KegiatanSumberAnggaranRenstra::class,
            'id_mst_sumber_anggaran',
            'id_renstra_kegiatan'
        );
    }

    public function programRpjmd() {
        return $this->belongsToMany(
            ProgramRpjmd::class,
            SumberAnggaranRpjmd::class,
            'id_mst_sumber_anggaran',
            'id_rpjmd_program'
        );
    }

}
