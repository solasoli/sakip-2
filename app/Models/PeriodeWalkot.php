<?php

namespace App\Models;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\Renstra\KebijakanRenstra;
use App\Models\Renstra\Kegiatan\KegiatanRenstra;
use App\Models\Renstra\Program\ProgramRenstra;
use App\Models\Renstra\Program\SasaranProgramRenstra;
use App\Models\Renstra\SasaranRenstra;
use App\Models\Renstra\StrategiRenstra;
use App\Models\Renstra\TujuanRenstra;
use App\Models\Rpjmd\KebijakanRpjmd;
use App\Models\Rpjmd\PrioritasPembangunanRpjmd;
use App\Models\Rpjmd\Program\PenaggungJawabRpjmd;
use App\Models\Rpjmd\Program\ProgramRpjmd;
use App\Models\Rpjmd\Program\SumberAnggaranRpjmd;
use App\Models\Rpjmd\Sasaran;
use App\Models\Rpjmd\StrategiRpjmd;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PeriodeWalkot extends Model implements ChildrenDataCheckerInterface
{
    use HasFactory;
    protected $table = 'mst_periode';

    public static function booted() {
        static::deleting(function(PeriodeWalkot $periode) {
            foreach ($periode->visiRpjmd as $item) {
                $item->delete();
            }

            foreach ($periode->misiRpjmd as $item) {
                $item->delete();
            }

            foreach ($periode->tujuanRpjmd as $item) {
                $item->delete();
            }

            foreach ($periode->periodePerTahun as $item) {
                $item->delete();
            }

            foreach ($periode->sasaranProgramRenstra as $item) {
                $item->delete();
            }
        });
    }

    public function hasMandatoryChildren(): bool
    {
        return $this->periodePerTahun()->count() > 0
            || $this->visiRpjmd()->count() > 0
            || $this->misiRpjmd()->count() > 0
            || $this->tujuanRpjmd()->count() > 0
            || $this->sasaranRpjmd()->count() > 0
            || $this->prioritasPembangunanRpjmd()->count() > 0
            || $this->strategiRpjmd()->count() > 0
            || $this->kebijakanRpjmd()->count() > 0
            || $this->penanggungJawabRpjmd()->count() > 0
            || $this->sumberAnggaranRpjmd()->count() > 0
            || $this->tujuanRenstra()->count() > 0
            || $this->sasaranRenstra()->count() > 0
            || $this->sasaranProgramRenstra()->count() > 0
            || $this->strategiRenstra()->count() > 0
            || $this->programRpjmd()->count() > 0
            || $this->kebijakanRenstra()->count() > 0
            || $this->programRenstra()->count() > 0
            || $this->kegiatanRenstra()->count() > 0;
    }

    public function periodePerTahun()
    {
        return $this->hasMany(PeriodePerTahun::class, 'id_periode', 'id');
    }

    public function visiRpjmd() {
        return $this->hasMany(Visi::class, 'id_periode');
    }

    public function kebijakanRenstra() {
        return $this->hasMany(KebijakanRenstra::class, 'id_periode');
    }

    public function strategiRenstra() {
        return $this->hasMany(StrategiRenstra::class, 'id_periode');
    }

    public function kebijakanRpjmd() {
        return $this->hasMany(KebijakanRpjmd::class, 'id_periode');
    }

    public function kegiatanRenstra() {
        return $this->hasMany(KegiatanRenstra::class, 'id_periode');
    }

    public function strategiRpjmd() {
        return $this->hasMany(StrategiRpjmd::class, 'id_periode');
    }

    public function prioritasPembangunanRpjmd() {
        return $this->hasMany(PrioritasPembangunanRpjmd::class, 'id_periode');
    }

    public function misiRpjmd() {
        return $this->hasMany(Misi::class, 'id_periode');
    }

    public function tujuanRpjmd() {
        return $this->hasMany(Tujuan::class, 'id_periode');
    }

    public function sasaranRpjmd() {
        return $this->hasMany(Sasaran::class, 'id_periode');
    }

    public function tujuanRenstra() {
        return $this->hasMany(TujuanRenstra::class, 'id_periode');
    }

    public function sasaranRenstra() {
        return $this->hasMany(SasaranRenstra::class, 'id_periode');
    }

    public function programRenstra() {
        return $this->hasMany(ProgramRenstra::class, 'id_periode');
    }

    public function programRpjmd() {
        return $this->hasMany(ProgramRpjmd::class, 'id_periode');
    }

    public function penanggungJawabRpjmd() {
        return $this->hasMany(PenaggungJawabRpjmd::class, 'id_periode');
    }

    public function sumberAnggaranRpjmd() {
        return $this->hasMany(SumberAnggaranRpjmd::class, 'id_periode');
    }

    public function sasaranProgramRenstra() {
        return $this->hasMany(SasaranProgramRenstra::class, 'id_periode');
    }
}
