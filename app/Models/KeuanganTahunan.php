<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KeuanganTahunan extends Model
{
    const KIND_ANGGARAN_RENSTRA = 'anggaran_renstra';
    const KIND_ANGGARAN_RENJA = 'anggaran_renja';
    const KIND_ANGGARAN_PK = 'anggaran_pk';

    protected $table = "keuangan_tahunan";
    protected $fillable = [
        'assignable_id',
        'assignable_type',
        'assignable_kind',
        'id_periode_per_tahun',
        'nilai',
        'is_deleted',
    ];

    function assignable() {
        return $this->morphTo();
    }

    function tahun() {
        return $this->belongsTo(PeriodePerTahun::class, 'id_periode_per_tahun');
    }
}
