<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TujuanIndikator extends Model
{
    use HasFactory;
    protected $table = 'rpjmd_tujuan_indikator';
    protected $guarded = ['id'];

    public static function booted() {
        static::deleting(function(TujuanIndikator $indikator) {
            $indikator->target()->delete();
        });
    }

    public function tujuan()
    {
        return $this->hasOne(Tujuan::class, 'id', 'id_tujuan');
    }

    public function misi()
    {
        return $this->belongsTo(Misi::class, 'id_misi');
    }

    public function satuan()
    {
        return $this->belongsTo(Satuan::class, 'id_satuan');
    }

    public function target()
    {
        return $this->hasMany(Target::class, 'id_tujuan_indikator', 'id');
    }
}
