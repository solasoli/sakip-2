<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Eselon extends Model
{
    protected $table = 'mst_eselon';
    protected $guarded = [];

    const II_A = 7;
    const II_B = 1;
    const III_A = 2;
    const III_B = 4;
    const IV_A = 5;
    const IV_B = 6;
    const Bupati = 8;
    use HasFactory;

    public function pegawai()
    {
        return $this->hasMany(Pegawai::class, 'id_eselon', 'id');
    }
}
