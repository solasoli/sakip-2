<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    use HasFactory;
    protected $table = 'mst_pegawai';
    protected $guarded = [];


    public function eselon()
    {
    	return $this->belongsTo(Eselon::class, 'id_eselon');
    }
    public function opd()
    {
    	return $this->belongsTo(Opd::class, 'id_opd');
    }
    public function pangkat()
    {
    	return $this->belongsTo(Pangkat::class, 'id_pangkat');
    }
    public function pangkatGolongan()
    {
    	return $this->belongsTo(PangkatGolongan::class, 'id_pangkat_gol');
    }

    public function targetPkRenjaTahunan() {
        return $this->belongsToMany(
            TargetPkRenjaTahunan::class,
            'target_pk_renja_tahunan_penanggung_jawab',
            'penanggung_jawab_id',
            'target_pk_renja_tahunan_id'
        );
    }

    public function targetPkRenjaTahunanSebagaiAtasan() {
        return $this->hasMany(TargetPkRenjaTahunan::class, 'atasan_id');
    }
}
