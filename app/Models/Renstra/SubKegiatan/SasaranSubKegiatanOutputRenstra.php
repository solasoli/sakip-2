<?php

namespace App\Models\Renstra\SubKegiatan;

use App\Models\Pegawai;
use App\Models\PkRenjaTahunan;
use App\Models\PkRenjaTriwulan;
use App\Models\Satuan;
use App\Models\KeuanganTahunan;
use Illuminate\Database\Eloquent\Model;

class SasaranSubKegiatanOutputRenstra extends Model
{
    protected $table = "renstra_sasaran_sub_kegiatan_output";
    protected $fillable = [
        'name',
        'id_sasaran_sub_kegiatan',
        'id_satuan',
        'is_pk',
        'is_iku',
        'cara_pengukuran',
        'target_awal',
        'target_akhir',
        'is_deleted',
    ];
    protected $casts = [
        "is_pk" => "boolean",
        "is_iku" => "boolean",
        "is_deleted" => "boolean",
    ];

    public function getNameAttribute(){
        return $this->attributes['name'];
    }

    public static function booted() {
        static::deleting(function(SasaranSubKegiatanOutputRenstra $output) {
            $output->targetRenja()->delete();
            $output->target()->delete();

            foreach ($output->sasaranSubKegiatanOutputPenanggungJawab as $penanggungJawab) {
                /** @var SasaranSubKegiatanOutputPenanggungJawabRenstra $penanggungJawab */
                $penanggungJawab->delete();
            }

            if($output->pkRenjaTahunan) $output->pkRenjaTahunan->delete();

            foreach ($output->pkRenjaTriwulan as $pkRenjaTriwulan) {
                /** @var PkRenjaTriwulan $pkRenjaTriwulan */
                $pkRenjaTriwulan->delete();
            }
        });
    }

    function getSumberAnggaran()
    {
        return $this->sasaranSubKegiatan->getSumberAnggaran();
    }

    function getMisi()
    {
        return $this->sasaranSubKegiatan->getMisi();
    }

    function getRpjmdTujuan()
    {
        return $this->sasaranSubKegiatan->getRpjmdTujuan();
    }

    function getRpjmdSasaran()
    {
        return $this->sasaranSubKegiatan->getRpjmdSasaran();
    }

    function getPrioritasPembangunan()
    {
        return $this->sasaranSubKegiatan->getPrioritasPembangunan();
    }

    function getPerangkatDaerah()
    {
        return $this->sasaranSubKegiatan->getPerangkatDaerah();
    }

    function getRenstraTujuan() {
        return $this->sasaranSubKegiatan->getRenstraTujuan();
    }

    function getRenstraSasaran() {
        return $this->sasaranSubKegiatan->getRenstraSasaran();
    }

    public function satuan()
    {
        return $this->belongsTo(Satuan::class, 'id_satuan');
    }

    public function target()
    {
        return $this->hasMany(SasaranSubKegiatanOutputTargetRenstra::class, 'id_sasaran_sub_kegiatan_output');
    }

    public function targetRenja()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable');
    }

    public function sasaranSubKegiatan()
    {
        return $this->belongsTo(SasaranSubKegiatanRenstra::class, 'id_sasaran_sub_kegiatan');
    }

    function penanggungJawab() {
        return $this->belongsToMany(
            Pegawai::class,
            SasaranSubKegiatanOutputPenanggungJawabRenstra::class,
            'id_renstra_sasaran_sub_kegiatan_output',
            'id_pegawai'
        );
    }

    public function sasaranSubKegiatanOutputPenanggungJawab() {
        return $this->hasMany(SasaranSubKegiatanOutputPenanggungJawabRenstra::class, 'id_renstra_sasaran_sub_kegiatan_output');
    }

    public function pkRenjaTahunan()
    {
        return $this->morphOne(PkRenjaTahunan::class, 'pk');
    }

    public function pkRenjaTriwulan()
    {
        return $this->morphMany(PkRenjaTriwulan::class, 'pk');
    }

    public function sasaran()
    {
        return $this->sasaranSubKegiatan();
    }
}
