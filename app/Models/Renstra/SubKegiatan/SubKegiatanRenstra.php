<?php

namespace App\Models\Renstra\SubKegiatan;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\Renstra\Kegiatan\KegiatanRenstra;
use App\Models\SubKegiatan;
use App\Models\SumberAnggaran;
use App\Models\KeuanganTahunan;
use Illuminate\Database\Eloquent\Model;

class SubKegiatanRenstra extends Model implements ChildrenDataCheckerInterface
{
    protected $table = 'renstra_sub_kegiatan';
    protected $fillable = [
        'id_renstra_kegiatan',
        'id_mst_sub_kegiatan',
    ];

    public static function booted() {
        static::deleting(function(SubKegiatanRenstra $subKegiatanRenstra) {

            $anggaranSubKegiatan = $subKegiatanRenstra->anggaranRenstra;
            $anggaranKegiatan = $subKegiatanRenstra->kegiatan->anggaranRenstra;
            $anggaranProgram = $subKegiatanRenstra->kegiatan->programRenstra->anggaranRenstra;

            foreach ($anggaranSubKegiatan as $anggaran) {
                $selectedAnggaranKegiatan = $anggaranKegiatan->firstWhere('id_periode_per_tahun', $anggaran->id_periode_per_tahun);
                $selectedAnggaranProgram = $anggaranProgram->firstWhere('id_periode_per_tahun', $anggaran->id_periode_per_tahun);

                $selectedAnggaranKegiatan->nilai = $selectedAnggaranKegiatan->nilai - $anggaran->nilai;
                $selectedAnggaranKegiatan->save();
                $selectedAnggaranProgram->nilai = $selectedAnggaranProgram->nilai - $anggaran->nilai;
                $selectedAnggaranProgram->save();
            }

            $subKegiatanRenstra->sumberAnggaran()->detach();

            foreach ($subKegiatanRenstra->sasaran as $sasaran) {
                $sasaran->delete();
            }

            $subKegiatanRenstra->anggaranPk()->delete();
            $subKegiatanRenstra->anggaranRenja()->delete();
            $subKegiatanRenstra->anggaranRenstra()->delete();
            $subKegiatanRenstra->sumberAnggaran()->detach();
        });
    }

    public function getNameAttribute() {
        return $this->mstSubKegiatan->name;
    }

    public function getTahunPeriode() {
        return $this->kegiatan->getTahunPeriode();
    }

    public function getPerangkatDaerah() {
        return $this->kegiatan->getPerangkatDaerah();
    }

    public function hasMandatoryChildren(): bool
    {
        $this->loadCount('sasaran');
        return $this->sasaran_count > 0;
    }

    public function sumberAnggaran() {
        return $this->belongsToMany(
            SumberAnggaran::class,
            'renstra_sub_kegiatan_sumber_anggaran',
            'id_renstra_sub_kegiatan',
            'id_sumber_anggaran'
        );
    }

    function getSumberAnggaran()
    {
        return $this->kegiatan->getSumberAnggaran();
    }

    public function sasaran() {
        return $this->hasMany(SasaranSubKegiatanRenstra::class, 'id_renstra_sub_kegiatan');
    }

    public function kegiatan() {
        return $this->belongsTo(KegiatanRenstra::class, 'id_renstra_kegiatan');
    }

    public function mstSubKegiatan() {
        return $this->belongsTo(SubKegiatan::class, 'id_mst_sub_kegiatan');
    }

    public function anggaranRenja() {
        return $this->morphMany(KeuanganTahunan::class, 'assignable')->where('assignable_kind', KeuanganTahunan::KIND_ANGGARAN_RENJA);
    }

    public function anggaranRenstra() {
        return $this->morphMany(KeuanganTahunan::class, 'assignable')->where('assignable_kind', KeuanganTahunan::KIND_ANGGARAN_RENSTRA);
    }

    public function anggaranPk() {
        return $this->morphMany(KeuanganTahunan::class, 'assignable')->where('assignable_kind', KeuanganTahunan::KIND_ANGGARAN_PK);
    }
}
