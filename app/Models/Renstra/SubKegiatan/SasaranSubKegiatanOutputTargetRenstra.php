<?php

namespace App\Models\Renstra\SubKegiatan;

use App\Models\PeriodePerTahun;
use Illuminate\Database\Eloquent\Model;

class SasaranSubKegiatanOutputTargetRenstra extends Model
{
    protected $table = "renstra_sasaran_sub_kegiatan_output_target";
    protected $fillable = [
        "id_periode_per_tahun",
        "id_sasaran_sub_kegiatan_output",
        "target",
    ];

    public function tahun() {
        return $this->belongsTo(PeriodePerTahun::class, 'id_periode_per_tahun');
    }
}
