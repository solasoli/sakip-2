<?php

namespace App\Models\Renstra\SubKegiatan;

use App\Interfaces\ChildrenDataCheckerInterface;
use Illuminate\Database\Eloquent\Model;

class SasaranSubKegiatanRenstra extends Model implements ChildrenDataCheckerInterface
{
    protected $table = 'renstra_sasaran_sub_kegiatan';
    protected $fillable = [
        'id_renstra_sub_kegiatan',
        'name',
        'is_deleted'
    ];

    public static function booted() {
        static::deleting(function(SasaranSubKegiatanRenstra $sasaranSubKegiatanRenstra) {
            foreach($sasaranSubKegiatanRenstra->output as $output){
                $output->delete();
            }
        });
    }

    function getSumberAnggaran()
    {
        return $this->subKegiatan->getSumberAnggaran();
    }

    function getMisi()
    {
        return $this->subKegiatan->kegiatan->getMisi();
    }

    function getRpjmdTujuan()
    {
        return $this->subKegiatan->kegiatan->getRpjmdTujuan();
    }

    function getRpjmdSasaran()
    {
        return $this->subKegiatan->kegiatan->getRpjmdSasaran();
    }

    function getPrioritasPembangunan()
    {
        return $this->subKegiatan->kegiatan->getPrioritasPembangunan();
    }

    function getPerangkatDaerah()
    {
        return $this->subKegiatan->kegiatan->getPerangkatDaerah();
    }

    function getRenstraTujuan() {
        return $this->subKegiatan->kegiatan->getRenstraTujuan();
    }

    function getRenstraSasaran() {
        return $this->subKegiatan->kegiatan->getRenstraSasaran();
    }

    public function hasMandatoryChildren(): bool
    {
        return false;
    }

    public function subKegiatan()
    {
        return $this->belongsTo(SubKegiatanRenstra::class, 'id_renstra_sub_kegiatan');
    }

    public function output()
    {
        return $this->hasMany(SasaranSubKegiatanOutputRenstra::class, 'id_sasaran_sub_kegiatan');
    }

    public function parent()
    {
        return $this->subKegiatan();
    }

}
