<?php

namespace App\Models\Renstra\SubKegiatan;

use App\Models\Pegawai;
use App\Models\Renstra\TargetPenanggungJawabTahunanRenstra;
use Illuminate\Database\Eloquent\Model;

class SasaranSubKegiatanOutputPenanggungJawabRenstra extends Model
{
    protected $table = 'renstra_sasaran_sub_kegiatan_output_penanggung_jawab';
    protected $fillable = [
        'id_renstra_sasaran_sub_kegiatan_output',
        'id_pegawai'
    ];

    public static function booted() {
        static::deleting(function(SasaranSubKegiatanOutputPenanggungJawabRenstra $penanggungJawab) {
            $penanggungJawab->target()->delete();
        });
    }

    public function output() {
        return $this->belongsTo(SasaranSubKegiatanOutputRenstra::class, 'id_renstra_sasaran_program_indikator');
    }

    public function pegawai() {
        return $this->belongsTo(Pegawai::class, 'id_pegawai');
    }

    public function target() {
        return $this->morphMany(TargetPenanggungJawabTahunanRenstra::class, 'renstra');
    }
}
