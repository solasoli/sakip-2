<?php

namespace App\Models\Renstra;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\KeuanganTahunan;
use App\Models\PeriodePerTahun;
use App\Models\Renstra\Program\ProgramRenstra;
use Illuminate\Database\Eloquent\Model;

class SasaranRenstra extends Model implements ChildrenDataCheckerInterface
{
    protected $table = 'renstra_sasaran';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(SasaranRenstra $sasaranRenstra) {
            foreach($sasaranRenstra->indikator as $indikator) {
                $indikator->delete();
            }

            foreach($sasaranRenstra->renstraStrategi as $strategi) {
                $strategi->delete();
            }

            foreach($sasaranRenstra->renstraKebijakan as $kebijakan) {
                $kebijakan->delete();
            }

            foreach ($sasaranRenstra->programRenstra as $program) {
                $program->delete();
            }
        });
    }

    public function getAnggaranRenstraAttribute()
    {
        if (!isset($this->relations['anggaranRenstra'])) {
            $this->load('anggaranRenstra');
        }

        if ($this->relations['anggaranRenstra']->count() == 0) {
            $anggaranMap = $this->getTahunPeriode()->map(function (PeriodePerTahun $periode) {
                return [
                    "id_periode_per_tahun" => $periode->id,
                    "nilai" => 0,
                    "assignable_kind" => KeuanganTahunan::KIND_ANGGARAN_RENSTRA
                ];
            });

            $this->anggaranRenstra()->createMany($anggaranMap);

            $this->load('anggaranRenstra');
        }


        return $this->relations['anggaranRenstra'];
    }

    function getTahunPeriode()
    {
        return $this->renstraTujuan->rpjmdSasaran->tujuan->misi->periode->periodePerTahun;
    }

    public function getPeriode()
    {
        return $this->renstraTujuan->rpjmdSasaran->tujuan->misi->periode;
    }

    function getMisi()
    {
        return $this->renstraTujuan->rpjmdSasaran->tujuan->misi ?? null;
    }

    function getRpjmdTujuan()
    {
        return $this->renstraTujuan->rpjmdSasaran->tujuan ?? null;
    }

    function getRpjmdSasaran()
    {
        return $this->renstraTujuan->rpjmdSasaran ?? null;
    }

    function getPrioritasPembangunan()
    {
        $prioritasPembangunan = $this->renstraTujuan
            ->rpjmdSasaran->prioritasPembangunan
            ->mstPrioritasPembangunan ?? null;
        return $prioritasPembangunan ?? null;
    }

    function getRenstraTujuan() {
        return $this->renstraTujuan ?? null;
    }

    function hasMandatoryChildren(): bool
    {
        $this->loadCount(['renstraStrategi', 'renstraKebijakan', 'programRenstra']);
        return $this->renstra_strategi_count > 0 || $this->renstra_kebijakan_count > 0 || $this->program_renstra_count > 0;
    }

    function getSumberAnggaran() {
        return $this->programRenstra->map(function(ProgramRenstra $program) {
            return $program->getSumberAnggaran();
        })->flatten()->unique();
    }

    function renstraTujuan()
    {
        return $this->belongsTo(TujuanRenstra::class, 'id_renstra_tujuan');
    }

    function indikator()
    {
        return $this->hasMany(SasaranRenstraIndikator::class, 'id_sasaran');
    }

    function renstraStrategi()
    {
        return $this->hasMany(StrategiRenstra::class, 'id_renstra_sasaran');
    }

    function renstraKebijakan()
    {
        return $this->hasMany(KebijakanRenstra::class, 'id_renstra_strategi');
    }

    function programRenstra()
    {
        return $this->hasMany(ProgramRenstra::class, 'id_renstra_sasaran');
    }

    function anggaranRenstra()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable')->where('assignable_kind', KeuanganTahunan::KIND_ANGGARAN_RENSTRA);
    }
}
