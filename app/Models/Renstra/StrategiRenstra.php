<?php

namespace App\Models\Renstra;

use Illuminate\Database\Eloquent\Model;

class StrategiRenstra extends Model
{
    protected $table = 'renstra_strategi';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(StrategiRenstra $strategi) {
            foreach($strategi->renstraKebijakan as $kebijakan) {
                $kebijakan->delete();
            }
        });
    }

    function sasaranRenstra()
    {
        return $this->belongsTo(SasaranRenstra::class, 'id_renstra_sasaran');
    }

    function renstraKebijakan()
    {
        return $this->hasMany(KebijakanRenstra::class, 'id_renstra_strategi');
    }
}
