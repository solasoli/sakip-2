<?php

namespace App\Models\Renstra\Kegiatan;

use App\Models\Pegawai;
use App\Models\PkRenjaTahunan;
use App\Models\PkRenjaTriwulan;
use App\Models\Satuan;
use App\Models\KeuanganTahunan;
use Illuminate\Database\Eloquent\Model;

class SasaranKegiatanIndikatorRenstra extends Model
{
    protected $table = "renstra_sasaran_kegiatan_indikator";
    protected $fillable = [
        'name',
        'id_sasaran_kegiatan',
        'id_satuan',
        'is_pk',
        'is_iku',
        'cara_pengukuran',
        'target_awal',
        'target_akhir',
        'is_deleted',
    ];

    protected $casts = [
        "is_pk" => "boolean",
        "is_iku" => "boolean",
        "is_deleted" => "boolean",
    ];

    public static function booted() {
        static::deleting(function(SasaranKegiatanIndikatorRenstra $indikator) {
            $indikator->targetRenja()->delete();
            $indikator->target()->delete();

            foreach ($indikator->sasaranKegiatanIndikatorPenanggungJawab as $penanggungJawab) {
                $penanggungJawab->delete();
            }

            if($indikator->pkRenjaTahunan) $indikator->pkRenjaTahunan->delete();

            foreach ($indikator->pkRenjaTriwulan as $pkRenjaTriwulan) {
                /** @var PkRenjaTriwulan $pkRenjaTriwulan */
                $pkRenjaTriwulan->delete();
            }
        });
    }

    public function getNameAttribute()
    {
        return $this->attributes['name'];
    }

    function getSumberAnggaran()
    {
        return $this->sasaranKegiatan->getSumberAnggaran();
    }

    function getMisi()
    {
        return $this->sasaranKegiatan->kegiatan->programRenstra->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan->misi ?? null;
    }

    function getRpjmdTujuan()
    {
        return $this->sasaranKegiatan->kegiatan->programRenstra->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan ?? null;
    }

    function getRpjmdSasaran()
    {
        return $this->sasaranKegiatan->kegiatan->programRenstra->sasaranRenstra->renstraTujuan->rpjmdSasaran ?? null;
    }

    function getPrioritasPembangunan()
    {
        $prioritasPembangunan = $this->sasaranKegiatan->kegiatan->programRenstra
            ->sasaranRenstra->renstraTujuan
            ->rpjmdSasaran->prioritasPembangunan
            ->mstPrioritasPembangunan ?? null;
        return $prioritasPembangunan ?? null;
    }

    function getPerangkatDaerah()
    {
        return $this->sasaranKegiatan->kegiatan->programRenstra->sasaranRenstra->renstraTujuan->opd ?? null;
    }

    function getRenstraTujuan() {
        return $this->sasaranKegiatan->kegiatan->programRenstra->sasaranRenstra->renstraTujuan ?? null;
    }

    function getRenstraSasaran() {
        return $this->sasaranKegiatan->kegiatan->programRenstra->sasaranRenstra ?? null;
    }

    public function satuan()
    {
        return $this->belongsTo(Satuan::class, 'id_satuan');
    }

    public function target()
    {
        return $this->hasMany(SasaranKegiatanIndikatorTargetRenstra::class, 'id_sasaran_kegiatan_indikator');
    }

    public function targetRenja()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable');
    }

    public function sasaranKegiatan()
    {
        return $this->belongsTo(SasaranKegiatanRenstra::class, 'id_sasaran_kegiatan');
    }

    function penanggungJawab() {
        return $this->belongsToMany(
            Pegawai::class,
            SasaranKegiatanIndikatorPenanggungJawabRenstra::class,
            'id_renstra_sasaran_kegiatan_indikator',
            'id_pegawai'
        );
    }

    public function sasaranKegiatanIndikatorPenanggungJawab() {
        return $this->hasMany(SasaranKegiatanIndikatorPenanggungJawabRenstra::class, 'id_renstra_sasaran_kegiatan_indikator');
    }

    public function pkRenjaTahunan()
    {
        return $this->morphOne(PkRenjaTahunan::class, 'pk');
    }

    public function pkRenjaTriwulan()
    {
        return $this->morphMany(PkRenjaTriwulan::class, 'pk');
    }

    public function sasaran()
    {
        return $this->sasaranKegiatan();
    }

}
