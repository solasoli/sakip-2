<?php

namespace App\Models\Renstra\Kegiatan;

use App\Models\PeriodePerTahun;
use Illuminate\Database\Eloquent\Model;

class SasaranKegiatanIndikatorTargetRenstra extends Model
{
    protected $table = "renstra_sasaran_kegiatan_indikator_target";
    protected $fillable = [
        "id_periode_per_tahun",
        "id_sasaran_kegiatan_indikator",
        "target",
    ];

    public function tahun() {
        return $this->belongsTo(PeriodePerTahun::class, 'id_periode_per_tahun');
    }
}
