<?php

namespace App\Models\Renstra\Kegiatan;

use App\Models\SumberAnggaran;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KegiatanSumberAnggaranRenstra extends Model
{
    use HasFactory;
    protected $table = 'renstra_kegiatan_sumber_anggaran';
    protected $guarded = [];



    function kegiatan()
    {
        return $this->belongsTo(KegiatanRenstra::class, 'id_renstra_kegiatan', 'id');
    }

    function mstSumberAnggaran()
    {
        return $this->belongsTo(SumberAnggaran::class, 'id_mst_sumber_anggaran', 'id');
    }
}
