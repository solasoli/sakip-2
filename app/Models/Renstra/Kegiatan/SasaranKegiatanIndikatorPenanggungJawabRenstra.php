<?php

namespace App\Models\Renstra\Kegiatan;

use App\Models\Pegawai;
use App\Models\Renstra\TargetPenanggungJawabTahunanRenstra;
use Illuminate\Database\Eloquent\Model;

class SasaranKegiatanIndikatorPenanggungJawabRenstra extends Model
{
    protected $table = 'renstra_sasaran_kegiatan_indikator_penanggung_jawab';
    protected $fillable = [
        'id_renstra_sasaran_kegiatan_indikator',
        'id_pegawai'
    ];

    public static function booted() {
        static::deleting(function(SasaranKegiatanIndikatorPenanggungJawabRenstra $penanggungJawab) {
            $penanggungJawab->target()->delete();
        });
    }

    public function indikator() {
        return $this->belongsTo(SasaranKegiatanIndikatorRenstra::class, 'id_renstra_sasaran_program_indikator');
    }

    public function pegawai() {
        return $this->belongsTo(Pegawai::class, 'id_pegawai');
    }

    public function target() {
        return $this->morphMany(TargetPenanggungJawabTahunanRenstra::class, 'renstra');
    }
}
