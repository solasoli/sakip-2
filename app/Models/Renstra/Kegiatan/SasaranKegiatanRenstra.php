<?php

namespace App\Models\Renstra\Kegiatan;

use Illuminate\Database\Eloquent\Model;

class SasaranKegiatanRenstra extends Model
{
    protected $table = 'renstra_sasaran_kegiatan';
    protected $fillable = [
        'id_renstra_kegiatan',
        'name',
        'is_deleted'
    ];

    public static function booted() {
        static::deleting(function(SasaranKegiatanRenstra $sasaranKegiatanRenstra) {
            foreach ($sasaranKegiatanRenstra->indikator as $indikator) {
                $indikator->delete();
            }
        });
    }

    function getSumberAnggaran()
    {
        return $this->kegiatan->getSumberAnggaran();
    }

    function getMisi()
    {
        return $this->kegiatan->programRenstra->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan->misi ?? null;
    }

    function getRpjmdTujuan()
    {
        return $this->kegiatan->programRenstra->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan ?? null;
    }

    function getRpjmdSasaran()
    {
        return $this->kegiatan->programRenstra->sasaranRenstra->renstraTujuan->rpjmdSasaran ?? null;
    }

    function getPrioritasPembangunan()
    {
        $prioritasPembangunan = $this->kegiatan->programRenstra
            ->sasaranRenstra->renstraTujuan
            ->rpjmdSasaran->prioritasPembangunan
            ->mstPrioritasPembangunan ?? null;
        return $prioritasPembangunan ?? null;
    }

    function getPerangkatDaerah()
    {
        return $this->kegiatan->programRenstra->sasaranRenstra->renstraTujuan->opd ?? null;
    }

    function getRenstraTujuan() {
        return $this->kegiatan->programRenstra->sasaranRenstra->renstraTujuan ?? null;
    }

    function getRenstraSasaran() {
        return $this->kegiatan->programRenstra->sasaranRenstra ?? null;
    }

    public function getPeriode()
    {
        return $this->kegiatan->programRenstra->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan->misi->periode;
    }

    public function getMstProgram()
    {
        return $this->kegiatan->getMstProgram();
    }

    public function getMstKegiatan()
    {
        return $this->kegiatan->mstKegiatan;
    }

    public function kegiatan()
    {
        return $this->belongsTo(KegiatanRenstra::class, 'id_renstra_kegiatan');
    }

    public function indikator()
    {
        return $this->hasMany(SasaranKegiatanIndikatorRenstra::class, 'id_sasaran_kegiatan');
    }

    public function parent()
    {
        return $this->kegiatan();
    }

}
