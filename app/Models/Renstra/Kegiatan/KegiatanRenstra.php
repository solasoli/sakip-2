<?php

namespace App\Models\Renstra\Kegiatan;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\Kegiatan;
use App\Models\KeuanganTahunan;
use App\Models\PeriodePerTahun;
use App\Models\Renstra\Program\ProgramRenstra;
use App\Models\Renstra\SubKegiatan\SubKegiatanRenstra;
use Illuminate\Database\Eloquent\Model;

class KegiatanRenstra extends Model implements ChildrenDataCheckerInterface
{
    protected $table = 'renstra_kegiatan';
    protected $fillable = [
        'id_mst_opd',
        'id_mst_kegiatan',
        'id_renstra_program',
        'id_periode',
    ];

    public static function booted()
    {
        static::deleting(function (KegiatanRenstra $kegiatan) {
            $kegiatan->sumberAnggaran()->delete();

            foreach ($kegiatan->sasaranKegiatan as $sasaran) {
                $sasaran->delete();
            }

            foreach ($kegiatan->subKegiatan as $subKegiatan) {
                $subKegiatan->delete();
            }

            $kegiatan->anggaranPk()->delete();
            $kegiatan->anggaranRenja()->delete();
            $kegiatan->anggaranRenstra()->delete();
            $kegiatan->sumberAnggaran()->delete();
        });
    }

    public function getAnggaranRenstraAttribute()
    {
        if (!isset($this->relations['anggaranRenstra'])) {
            $this->load('anggaranRenstra');
        }

        if ($this->relations['anggaranRenstra']->count() == 0) {
            $anggaranMap = $this->getTahunPeriode()->map(function (PeriodePerTahun $periode) {
                return [
                    "id_periode_per_tahun" => $periode->id,
                    "nilai" => 0,
                    "assignable_kind" => KeuanganTahunan::KIND_ANGGARAN_RENSTRA
                ];
            });

            $this->anggaranRenstra()->createMany($anggaranMap);

            $this->load('anggaranRenstra');
        }


        return $this->relations['anggaranRenstra'];
    }

    public function getAnggaranRenjaAttribute()
    {
        if (!isset($this->relations['anggaranRenja'])) {
            $this->load('anggaranRenja');
        }

        if ($this->relations['anggaranRenja']->count() == 0) {
            $anggaranMap = $this->getTahunPeriode()->map(function (PeriodePerTahun $periode) {
                return [
                    "id_periode_per_tahun" => $periode->id,
                    "nilai" => 0,
                    "assignable_kind" => KeuanganTahunan::KIND_ANGGARAN_RENJA
                ];
            });

            $this->anggaranRenja()->createMany($anggaranMap);

            $this->load('anggaranRenja');
        }


        return $this->relations['anggaranRenja'];
    }

    public function getNameAttribute()
    {
        return $this->mstKegiatan->name;
    }

    function getSumberAnggaran()
    {
        return $this->sumberAnggaran->map(function (KegiatanSumberAnggaranRenstra $sumber) {
            return $sumber->mstSumberAnggaran->name;
        })->unique();
    }

    function getTahunPeriode()
    {
        return $this->programRenstra->getTahunPeriode();
    }

    public function getMisi()
    {
        return $this->programRenstra->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan->misi ?? null;
    }

    function getRpjmdTujuan()
    {
        return $this->programRenstra->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan ?? null;
    }

    function getRpjmdSasaran()
    {
        return $this->programRenstra->sasaranRenstra->renstraTujuan->rpjmdSasaran ?? null;
    }

    function getPrioritasPembangunan()
    {
        $prioritasPembangunan = $this->programRenstra
            ->sasaranRenstra->renstraTujuan
            ->rpjmdSasaran->prioritasPembangunan
            ->mstPrioritasPembangunan ?? null;
        return $prioritasPembangunan ?? null;
    }

    function getPerangkatDaerah()
    {
        return $this->programRenstra->sasaranRenstra->renstraTujuan->opd ?? null;
    }

    function getRenstraTujuan()
    {
        return $this->programRenstra->sasaranRenstra->renstraTujuan ?? null;
    }

    function getRenstraSasaran()
    {
        return $this->programRenstra->sasaranRenstra ?? null;
    }

    function getMstProgram()
    {
        return $this->programRenstra->programRpjmd->mstProgram;
    }

    public function hasMandatoryChildren(): bool
    {
        $this->loadCount(['sasaranKegiatan', 'subKegiatan']);
        return $this->sasaran_kegiatan_count > 0 || $this->sub_kegiatan_count > 0;
    }

    function programRenstra()
    {
        return $this->belongsTo(ProgramRenstra::class, 'id_renstra_program', 'id');
    }

    function sumberAnggaran()
    {
        return $this->hasMany(KegiatanSumberAnggaranRenstra::class, 'id_renstra_kegiatan');
    }

    function mstKegiatan()
    {
        return $this->belongsTo(Kegiatan::class, 'id_mst_kegiatan', 'id');
    }

    function subKegiatan()
    {
        return $this->hasMany(SubKegiatanRenstra::class, 'id_renstra_kegiatan');
    }

    function anggaranRenstra()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable')->where('assignable_kind', KeuanganTahunan::KIND_ANGGARAN_RENSTRA);
    }

    function anggaranRenja()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable')->where('assignable_kind', KeuanganTahunan::KIND_ANGGARAN_RENJA);
    }

    function anggaranPk()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable')->where('assignable_kind', KeuanganTahunan::KIND_ANGGARAN_PK);
    }

    function sasaranKegiatan()
    {
        return $this->hasMany(SasaranKegiatanRenstra::class, 'id_renstra_kegiatan');
    }
}
