<?php

namespace App\Models\Renstra;

use Illuminate\Database\Eloquent\Model;

class SasaranRenstraTarget extends Model
{
    protected $table = 'renstra_sasaran_target';
    protected $guarded = [];

    function indikator()
    {
        return $this->belongsTo(SasaranRenstraIndikator::class, 'id_indikator_sasaran');
    }
}
