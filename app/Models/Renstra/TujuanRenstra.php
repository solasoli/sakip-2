<?php

namespace App\Models\Renstra;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\Rpjmd\Sasaran;
use App\Models\Opd;
use Illuminate\Database\Eloquent\Model;

class TujuanRenstra extends Model implements ChildrenDataCheckerInterface
{
    protected $table = 'renstra_tujuan';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(TujuanRenstra $tujuanRenstra) {
            foreach($tujuanRenstra->indikator as $indikator) {
                $indikator->delete();
            }

            foreach ($tujuanRenstra->renstraSasaran as $sasaran) {
                $sasaran->delete();
            }
        });
    }

    function hasMandatoryChildren(): bool
    {
        $this->loadCount(['renstraSasaran']);
        return $this->renstra_sasaran_count > 0;
    }

    function rpjmdSasaran()
    {
        return $this->belongsTo(Sasaran::class, 'id_rpjmd_sasaran')
            ->where('id_periode', periode()->id);
    }

    function indikator()
    {
        return $this->hasMany(TujuanRenstraIndikator::class, 'id_tujuan');
    }

    function renstraSasaran()
    {
        return $this->hasMany(SasaranRenstra::class, 'id_renstra_tujuan')
            ->where('id_periode', periode()->id);
    }

    function opd()
    {
        return $this->belongsTo(Opd::class, 'id_mst_opd', 'id');
    }
}
