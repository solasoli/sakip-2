<?php

namespace App\Models\Renstra;

use App\Models\PeriodePerTahun;
use Illuminate\Database\Eloquent\Model;

class TargetPenanggungJawabTahunanRenstra extends Model
{
    protected $table = 'renstra_target_penanggung_jawab_tahunan';
    protected $fillable = [
        'renstra_id',
        'renstra_type',
        'id_periode_per_tahun',
        'target'
    ];

    function renstra() {
        $this->morphTo();
    }

    function tahun() {
        return $this->belongsTo(PeriodePerTahun::class, 'id_periode_per_tahun');
    }
}
