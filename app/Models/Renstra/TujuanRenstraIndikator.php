<?php

namespace App\Models\Renstra;

use App\Models\Satuan;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TujuanRenstraIndikator extends Model
{
    use HasFactory;
    protected $table = 'renstra_tujuan_indikator';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(TujuanRenstraIndikator $indikator) {
            $indikator->target()->delete();
        });
    }

    function tujuan()
    {
        return $this->belongsTo(TujuanRenstra::class, 'id_tujuan');
    }

    function target()
    {
        return $this->hasMany(TujuanRenstraTarget::class, 'id_indikator_tujuan');
    }

    function satuan()
    {
        return $this->belongsTo(Satuan::class, 'id_satuan');
    }
}
