<?php

namespace App\Models\Renstra;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TujuanRenstraTarget extends Model
{
    use HasFactory;
    protected $table = 'renstra_tujuan_target';
    protected $guarded = [];

    function tujuanIndikator()
    {
        return $this->belongsTo(TujuanRenstraIndikator::class, 'id_indikator_tujuan');
    }
}
