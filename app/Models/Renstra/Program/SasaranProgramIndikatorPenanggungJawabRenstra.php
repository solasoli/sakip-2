<?php

namespace App\Models\Renstra\Program;

use App\Models\Pegawai;
use App\Models\Renstra\TargetPenanggungJawabTahunanRenstra;
use Illuminate\Database\Eloquent\Model;

class SasaranProgramIndikatorPenanggungJawabRenstra extends Model
{
    protected $table = 'renstra_sasaran_program_indikator_penanggung_jawab';
    protected $fillable = [
        'id_renstra_sasaran_program_indikator',
        'id_pegawai'
    ];

    public static function booted() {
        static::deleting(function(SasaranProgramIndikatorPenanggungJawabRenstra $penanggungJawab) {
            $penanggungJawab->target()->delete();
        });
    }

    public function indikator() {
        return $this->belongsTo(SasaranProgramIndikatorRenstra::class, 'id_renstra_sasaran_program_indikator');
    }

    public function pegawai() {
        return $this->belongsTo(Pegawai::class, 'id_pegawai');
    }

    public function target() {
        return $this->morphMany(TargetPenanggungJawabTahunanRenstra::class, 'renstra');
    }
}
