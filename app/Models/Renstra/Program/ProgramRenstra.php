<?php

namespace App\Models\Renstra\Program;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\KeuanganTahunan;
use App\Models\PeriodePerTahun;
use App\Models\Renstra\Kegiatan\KegiatanRenstra;
use App\Models\Renstra\SasaranRenstra;
use App\Models\Rpjmd\Program\ProgramRpjmd;
use Illuminate\Database\Eloquent\Model;

class ProgramRenstra extends Model implements ChildrenDataCheckerInterface
{
    protected $table = 'renstra_program';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(ProgramRenstra $program) {
            foreach ($program->sasaranProgram as $sasaran) {
                $sasaran->delete();
            }

            foreach ($program->kegiatanRenstra as $kegiatan) {
                $kegiatan->delete();
            }

            $program->anggaranPk()->delete();
            $program->anggaranRenja()->delete();
            $program->anggaranRenstra()->delete();
        });
    }


    public function getAnggaranRenstraAttribute()
    {
        if(!isset($this->relations['anggaranRenstra'])) {
            $this->load('anggaranRenstra');
        }

        if($this->relations['anggaranRenstra']->count() == 0) {
            $anggaranMap = $this->getTahunPeriode()->map(function(PeriodePerTahun $periode) {
                return [
                    "id_periode_per_tahun" => $periode->id,
                    "nilai" => 0,
                    "assignable_kind" => KeuanganTahunan::KIND_ANGGARAN_RENSTRA
                ];
            });

            $this->anggaranRenstra()->createMany($anggaranMap);

            $this->load('anggaranRenstra');
        }


        return $this->relations['anggaranRenstra'];
    }

    public function getNameAttribute()
    {
        return $this->programRpjmd->mstProgram->name;
    }

    public function getAnggaranRenjaAttribute()
    {
        if(!isset($this->relations['anggaranRenja'])) {
            $this->load('anggaranRenja');
        }

        if($this->relations['anggaranRenja']->count() == 0) {
            $anggaranMap = $this->getTahunPeriode()->map(function(PeriodePerTahun $periode) {
                return [
                    "id_periode_per_tahun" => $periode->id,
                    "nilai" => 0,
                    "assignable_kind" => KeuanganTahunan::KIND_ANGGARAN_RENJA
                ];
            });

            $this->anggaranRenja()->createMany($anggaranMap);

            $this->load('anggaranRenja');
        }


        return $this->relations['anggaranRenja'];
    }

    function getSumberAnggaran()
    {
        return $this->kegiatanRenstra->map(function (KegiatanRenstra $kegiatan) {
            return $kegiatan->getSumberAnggaran();
        })->flatten()->unique();
    }

    function getTahunPeriode()
    {
        return $this->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan->misi->periode->periodePerTahun;
    }

    function getMisi()
    {
        return $this->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan->misi ?? null;
    }

    function getRpjmdTujuan()
    {
        return $this->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan ?? null;
    }

    function getRpjmdSasaran()
    {
        return $this->sasaranRenstra->renstraTujuan->rpjmdSasaran ?? null;
    }

    function getPrioritasPembangunan()
    {
        $prioritasPembangunan = $this->sasaranRenstra->renstraTujuan
            ->rpjmdSasaran->prioritasPembangunan
            ->mstPrioritasPembangunan ?? null;
        return $prioritasPembangunan ?? null;
    }

    function getRenstraTujuan() {
        return $this->sasaranRenstra->renstraTujuan ?? null;
    }

    public function hasMandatoryChildren(): bool
    {
        $this->loadCount(['kegiatanRenstra', 'sasaranProgram']);
        return $this->kegiatan_renstra_count > 0 || $this->sasaran_program_count > 0;
    }

    function sasaranRenstra()
    {
        return $this->belongsTo(SasaranRenstra::class, 'id_renstra_sasaran');
    }

    function programRpjmd()
    {
        return $this->belongsTo(ProgramRpjmd::class, 'id_rpjmd_program');
    }

    function sasaranProgram()
    {
        return $this->hasMany(SasaranProgramRenstra::class, 'id_program_renstra');
    }

    function kegiatanRenstra()
    {
        return $this->hasMany(KegiatanRenstra::class, 'id_renstra_program');
    }

    function anggaranRenstra()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable')->where('assignable_kind', KeuanganTahunan::KIND_ANGGARAN_RENSTRA);
    }

    function anggaranRenja()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable')->where('assignable_kind', KeuanganTahunan::KIND_ANGGARAN_RENJA);
    }

    function anggaranPk()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable')->where('assignable_kind', KeuanganTahunan::KIND_ANGGARAN_PK);
    }
}
