<?php

namespace App\Models\Renstra\Program;

use App\Models\Opd;
use Illuminate\Database\Eloquent\Model;

class SasaranProgramRenstra extends Model
{
    protected $table = 'renstra_sasaran_program';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(SasaranProgramRenstra $sasaranProgramRenstra) {
            foreach ($sasaranProgramRenstra->indikator as $indikator) {
                $indikator->delete();
            }
        });
    }

    function getSumberAnggaran()
    {
        return $this->program->getSumberAnggaran();
    }

    public function getPeriode()
    {
        return $this->program->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan->misi->periode;
    }

    function getMisi()
    {
        return $this->program->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan->misi ?? null;
    }

    function getRpjmdTujuan()
    {
        return $this->program->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan ?? null;
    }

    function getRpjmdSasaran()
    {
        return $this->program->sasaranRenstra->renstraTujuan->rpjmdSasaran ?? null;
    }

    function getPrioritasPembangunan()
    {
        $prioritasPembangunan = $this->program->sasaranRenstra->renstraTujuan
            ->rpjmdSasaran->prioritasPembangunan
            ->mstPrioritasPembangunan ?? null;
        return $prioritasPembangunan ?? null;
    }

    function getRenstraTujuan() {
        return $this->program->sasaranRenstra->renstraTujuan ?? null;
    }

    function program()
    {
        return $this->belongsTo(ProgramRenstra::class, 'id_program_renstra');
    }

    function indikator()
    {
        return $this->hasMany(SasaranProgramIndikatorRenstra::class, 'id_sasaran_program');
    }

    function opd()
    {
        return $this->belongsTo(Opd::class, 'id_opd');
    }

    public function parent()
    {
        return $this->program();
    }

}
