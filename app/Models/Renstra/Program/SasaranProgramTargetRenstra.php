<?php

namespace App\Models\Renstra\Program;
use Illuminate\Database\Eloquent\Model;

class SasaranProgramTargetRenstra extends Model
{
    protected $table = 'renstra_sasaran_program_target';
    protected $guarded = [];

    function sasaranProgramIndikator()
    {
        return $this->belongsTo(SasaranProgramIndikatorRenstra::class, 'id_sasaran_program_indikator');
    }
}
