<?php

namespace App\Models\Renstra\Program;

use App\Models\Pegawai;
use App\Models\PkRenjaTahunan;
use App\Models\PkRenjaTriwulan;
use App\Models\Satuan;
use App\Models\KeuanganTahunan;
use Illuminate\Database\Eloquent\Model;

class SasaranProgramIndikatorRenstra extends Model
{
    protected $table = 'renstra_sasaran_program_indikator';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(SasaranProgramIndikatorRenstra $indikator) {
            $indikator->target()->delete();
            $indikator->targetRenja()->delete();

            foreach ($indikator->sasaranProgramIndikatorPenanggungJawab as $penanggungJawab) {
                $penanggungJawab->delete();
            }

            if($indikator->pkRenjaTahunan) $indikator->pkRenjaTahunan->delete();

            foreach ($indikator->pkRenjaTriwulan as $triwulan) {
                $triwulan->delete();
            }
        });
    }

    public function getNameAttribute()
    {
        return $this->attributes['indikator'];
    }

    function getMisi()
    {
        return $this->sasaranProgram->program->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan->misi->misi ?? null;
    }

    function getRpjmdTujuan()
    {
        return $this->sasaranProgram->program->sasaranRenstra->renstraTujuan->rpjmdSasaran->tujuan->name ?? null;
    }

    function getRpjmdSasaran()
    {
        return $this->sasaranProgram->program->sasaranRenstra->renstraTujuan->rpjmdSasaran->name ?? null;
    }

    function getPrioritasPembangunan()
    {
        $prioritasPembangunan = $this->sasaranProgram->program
            ->sasaranRenstra->renstraTujuan
            ->rpjmdSasaran->prioritasPembangunan
            ->mstPrioritasPembangunan ?? null;
        return $prioritasPembangunan->name ?? null;
    }

    function getPerangkatDaerah()
    {
        return $this->sasaranProgram->opd;
    }

    function getRenstraTujuan() {
        return $this->sasaranProgram->program->sasaranRenstra->renstraTujuan->name ?? null;
    }

    function getRenstraSasaran() {
        return $this->sasaranProgram->program->sasaranRenstra->name ?? null;
    }

    function getMstProgram()
    {
        return $this->sasaranProgram->program->programRpjmd->mstProgram;
    }

    function getSumberAnggaran()
    {
        return $this->parent->getSumberAnggaran();
    }

    function penanggungJawab() {
        return $this->belongsToMany(
            Pegawai::class,
            SasaranProgramIndikatorPenanggungJawabRenstra::class,
            'id_renstra_sasaran_program_indikator',
            'id_pegawai'
        );
    }

    public function sasaranProgramIndikatorPenanggungJawab() {
        return $this->hasMany(SasaranProgramIndikatorPenanggungJawabRenstra::class, 'id_renstra_sasaran_program_indikator');
    }

    function sasaranProgram()
    {
        return $this->belongsTo(SasaranProgramRenstra::class, 'id_sasaran_program');
    }

    function target()
    {
        return $this->hasMany(SasaranProgramTargetRenstra::class, 'id_sasaran_program_indikator');
    }

    function satuan()
    {
        return $this->hasOne(Satuan::class, 'id', 'id_satuan');
    }

    public function targetRenja()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable');
    }

    public function pkRenjaTahunan()
    {
        return $this->morphOne(PkRenjaTahunan::class, 'pk');
    }

    public function pkRenjaTriwulan()
    {
        return $this->morphMany(PkRenjaTriwulan::class, 'pk');
    }

    public function sasaran()
    {
        return $this->sasaranProgram();
    }
}
