<?php

namespace App\Models\Renstra;

use App\Models\PkRenjaTahunan;
use App\Models\PkRenjaTriwulan;
use App\Models\Satuan;
use App\Models\KeuanganTahunan;
use Illuminate\Database\Eloquent\Model;

class SasaranRenstraIndikator extends Model
{
    protected $table = 'renstra_sasaran_indikator';
    protected $guarded = [];
    protected $casts = [
        'is_iku' => 'boolean',
        'is_pk' => 'boolean',
    ];

    public static function booted() {
        static::deleting(function(SasaranRenstraIndikator $indikator) {
            $indikator->target()->delete();
            $indikator->targetRenja()->delete();

            foreach ($indikator->sasaranIndikatorPenanggungJawab as $penanggungJawab) {
                $penanggungJawab->delete();
            }

            if($indikator->pkRenjaTahunan) $indikator->pkRenjaTahunan->delete();

            foreach ($indikator->pkRenjaTriwulan as $triwulan) {
                $triwulan->delete();
            }
        });
    }

    public function getNameAttribute()
    {
        return $this->attributes['indikator'];
    }

    function getSumberAnggaran()
    {
        return $this->parent->getSumberAnggaran();
    }

    function getMisi()
    {
        return $this->sasaran->renstraTujuan->rpjmdSasaran->tujuan->misi ?? null;
    }

    function getRpjmdTujuan()
    {
        return $this->sasaran->renstraTujuan->rpjmdSasaran->tujuan ?? null;
    }

    function getRpjmdSasaran()
    {
        return $this->sasaran->renstraTujuan->rpjmdSasaran ?? null;
    }

    function getPrioritasPembangunan()
    {
        $prioritasPembangunan = $this->sasaran->renstraTujuan
            ->rpjmdSasaran->prioritasPembangunan
            ->mstPrioritasPembangunan ?? null;
        return $prioritasPembangunan ?? null;
    }

    function getPerangkatDaerah()
    {
        return $this->sasaran->renstraTujuan->opd ?? null;
    }

    function getRenstraTujuan() {
        return $this->sasaran->renstraTujuan ?? null;
    }

    function getRenstraSasaran() {
        return $this->sasaran ;
    }

    function sasaran()
    {
        return $this->belongsTo(SasaranRenstra::class, 'id_sasaran')
            ->where('id_periode', periode()->id);
    }

    function target()
    {
        return $this->hasMany(SasaranRenstraTarget::class, 'id_indikator_sasaran');
    }

    function satuan()
    {
        return $this->belongsTo(Satuan::class, 'id_satuan');
    }

    public function targetRenja()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable');
    }

    public function pkRenjaTahunan()
    {
        return $this->morphOne(PkRenjaTahunan::class, 'pk');
    }

    public function pkRenjaTriwulan()
    {
        return $this->morphMany(PkRenjaTriwulan::class, 'pk');
    }

    public function sasaranIndikatorPenanggungJawab()
    {
        return $this->hasMany(SasaranIndikatorPenanggungJawabRenstra::class, 'id_renstra_sasaran_indikator');
    }
}
