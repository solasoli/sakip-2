<?php

namespace App\Models\Renstra;

use App\Models\Pegawai;
use Illuminate\Database\Eloquent\Model;

class SasaranIndikatorPenanggungJawabRenstra extends Model
{
    protected $table = 'renstra_sasaran_indikator_penanggung_jawab';
    protected $fillable = [
        'id_renstra_sasaran_indikator',
        'id_pegawai'
    ];

    public static function booted() {
        static::deleting(function(SasaranIndikatorPenanggungJawabRenstra $penanggungJawab) {
            $penanggungJawab->target()->delete();
        });
    }

    public function indikator() {
        return $this->belongsTo(SasaranRenstraIndikator::class, 'id_renstra_sasaran_indikator');
    }

    public function pegawai() {
        return $this->belongsTo(Pegawai::class, 'id_pegawai');
    }

    public function target() {
        return $this->morphMany(TargetPenanggungJawabTahunanRenstra::class, 'renstra');
    }
}
