<?php

namespace App\Models\Renstra;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KebijakanRenstra extends Model
{
    use HasFactory;
    protected $table = "renstra_kebijakan";
    protected $guarded = [];

    function renstraStrategi()
    {
        return $this->belongsTo(StrategiRenstra::class, 'id_renstra_strategi');
    } 
}
