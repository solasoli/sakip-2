<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PangkatGolongan extends Model
{
    protected $table = 'mst_pangkat_golongan';
    protected $guarded = [];
    use HasFactory;

    public function pegawai()
    {
        return $this->hasMany(Pegawai::class,'id_pangkat_gol');
    }
}
