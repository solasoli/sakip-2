<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Opd extends Model
{
    use HasFactory;
    protected $table = 'mst_opd';
    protected $guarded = [];

    public function satuan()
    {
        return $this->hasMany(Satuan::class, 'id_opd');
    }

    public function pegawai()
    {
        return $this->hasMany(Pegawai::class, 'id_opd');
    }

    public function renstraTujuan()
    {
        return $this->hasMany(TujuanRenstra::class, 'id_mst_opd');
    }
    public function user()
    {
        return $this->hasMany(User::class, 'id_opd');
    }
}
