<?php

namespace App\Models;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\Renstra\Kegiatan\SasaranKegiatanIndikatorRenstra;
use App\Models\Renstra\SasaranRenstraIndikator;
use App\Models\Renstra\SubKegiatan\SasaranSubKegiatanOutputRenstra;
use App\Models\Renstra\TujuanRenstraIndikator;
use App\Models\Rpjmd\Program\ProgramIndikatorRpjmd;
use App\Models\Rpjmd\SasaranDetail;
use App\Models\Rpjmd\TargetRkpdRpjmd;
use Illuminate\Database\Eloquent\Model;

class Satuan extends Model implements ChildrenDataCheckerInterface
{
    protected $table = 'mst_satuan';

    public static function booted() {
        static::deleting(function(Satuan $satuan) {
            foreach ($satuan->tujuanIndikatorRpjmd as $item) {
                $item->delete();
            }

            foreach ($satuan->sasaranIndikatorRenstra as $item) {
                $item->delete();
            }

            foreach ($satuan->sasaranKegiatanIndikatorRenstra as $item) {
                $item->delete();
            }

            foreach ($satuan->sasaranSubKegiatanOutputRenstra as $item) {
                $item->delete();
            }

            foreach ($satuan->tujuanIndikatorRenstra as $item) {
                $item->delete();
            }

            foreach ($satuan->programIndikatorRpjmd as $item) {
                $item->delete();
            }

            foreach ($satuan->sasaranDetailRpjmd as $item) {
                $item->delete();
            }

            foreach ($satuan->targetRkpd as $item) {
                $item->delete();
            }
        });
    }

    public function hasMandatoryChildren(): bool
    {
        return $this->tujuanIndikatorRpjmd()->count() > 0
            || $this->sasaranIndikatorRenstra()->count() > 0
            || $this->sasaranKegiatanIndikatorRenstra()->count() > 0
            || $this->sasaranSubKegiatanOutputRenstra()->count() > 0
            || $this->tujuanIndikatorRenstra()->count() > 0
            || $this->programIndikatorRpjmd()->count() > 0
            || $this->sasaranDetailRpjmd()->count() > 0
            || $this->targetRkpd()->count() > 0;
    }

    public function opd()
    {
        return $this->belongsTo(Opd::class, 'id');
    }

    public function tujuanIndikatorRpjmd()
    {
        return $this->hasMany(TujuanIndikator::class, 'id_satuan');
    }

    public function sasaranIndikatorRenstra()
    {
        return $this->hasMany(SasaranRenstraIndikator::class, 'id_satuan');
    }

    public function sasaranKegiatanIndikatorRenstra()
    {
        return $this->hasMany(SasaranKegiatanIndikatorRenstra::class, 'id_satuan');
    }

    public function sasaranSubKegiatanOutputRenstra()
    {
        return $this->hasMany(SasaranSubKegiatanOutputRenstra::class, 'id_satuan');
    }

    public function tujuanIndikatorRenstra()
    {
        return $this->hasMany(TujuanRenstraIndikator::class, 'id_satuan');
    }

    public function programIndikatorRpjmd()
    {
        return $this->hasMany(ProgramIndikatorRpjmd::class, 'id_satuan');
    }

    public function sasaranDetailRpjmd()
    {
        return $this->hasMany(SasaranDetail::class, 'id_satuan');
    }

    public function targetRpkd()
    {
        return $this->hasMany(TargetRkpdRpjmd::class, 'id_satuan');
    }
}
