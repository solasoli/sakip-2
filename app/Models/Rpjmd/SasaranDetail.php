<?php

namespace App\Models\Rpjmd;

use App\Models\Satuan;
use Illuminate\Database\Eloquent\Model;

class SasaranDetail extends Model
{
    protected $table = 'rpjmd_sasaran_detail';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(SasaranDetail $detail) {
            $detail->sasaranTarget()->delete();
        });
    }

    function sasaran()
    {
        return $this->belongsTo(Sasaran::class, 'id_sasaran');
    }

    function satuan()
    {
        return $this->belongsTo(Satuan::class, 'id_satuan');
    }

    function sasaranTarget()
    {
        return $this->hasMany(SasaranTarget::class, 'id_sasaran_detail');
    }

    function targetRkpd()
    {
        return $this->morphOne(TargetRkpdRpjmd::class, 'rpjmd');
    }
}
