<?php

namespace App\Models\Rpjmd;

use App\Models\PrioritasPembangunan;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrioritasPembangunanRpjmd extends Model
{
    use HasFactory;
    protected $table = 'rpjmd_prioritas_pembangunan';
    protected $guarded = [];

    function sasaran()
    {
        return $this->belongsTo(Sasaran::class, 'id_sasaran');
    }

    function mstPrioritasPembangunan()
    {
        return $this->belongsTo(PrioritasPembangunan::class, 'id_mst_prioritas_pembangunan');
    }
}
