<?php

namespace App\Models\Rpjmd;

use App\Models\PeriodePerTahun;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SasaranTarget extends Model
{
    use HasFactory;
    protected $table = 'rpjmd_sasaran_target';
    protected $guarded = [];

    function sasaranDetail()
    {
        return $this->belongsToMany(SasaranDetail::class, 'id_sasaran_detail');
    }

    function periodePerTahun()
    {
        return $this->belongsTo(PeriodePerTahun::class, 'id_periode_per_tahun');
    }
}
