<?php

namespace App\Models\Rpjmd;

use App\Models\KeuanganTahunan;
use App\Models\Satuan;
use Illuminate\Database\Eloquent\Model;

class TargetRkpdRpjmd extends Model
{
    protected $table = 'rpjmd_target_rkpd';
    protected $fillable = [
        'rpjmd_id',
        'rpjmd_type',
        'id_satuan',
        'periode'
    ];

    public static function booted() {
        static::deleting(function(TargetRkpdRpjmd $target) {
            $target->target()->delete();
        });
    }

    public function satuan() {
        return $this->belongsTo(Satuan::class, 'id_satuan');
    }

    public function rpjmd() {
        return $this->morphTo();
    }

    public function target() {
        return $this->morphMany(KeuanganTahunan::class, 'assignable');
    }
}
