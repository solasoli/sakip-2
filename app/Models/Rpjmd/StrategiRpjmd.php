<?php

namespace App\Models\Rpjmd;

use App\Interfaces\ChildrenDataCheckerInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StrategiRpjmd extends Model implements ChildrenDataCheckerInterface
{
    use HasFactory;
    protected $table = 'rpjmd_strategi';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(StrategiRpjmd $strategiRpjmd) {
            foreach ($strategiRpjmd->kebijakan as $item) {
                $item->delete();
            }
        });
    }

    public function hasMandatoryChildren(): bool
    {
        $this->loadCount('kebijakan');
        return $this->kebijakan_count > 0;
    }

    public function kebijakan() {
        return $this->hasMany(KebijakanRpjmd::class, 'id_strategi');
    }
}
