<?php

namespace App\Models\Rpjmd\Program;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramTargetRpjmd extends Model
{
    use HasFactory;
    protected $table = 'rpjmd_program_target';
    protected $guarded = [];

    function indikatorProgram()
    {
        return $this->belongsTo(ProgramIndikatorRpjmd::class, 'id_program_indikator');
    }
}
