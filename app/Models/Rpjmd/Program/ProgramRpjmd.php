<?php

namespace App\Models\Rpjmd\Program;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\KeuanganTahunan;
use App\Models\Opd;
use App\Models\Program;
use App\Models\Renstra\Program\ProgramRenstra;
use App\Models\Rpjmd\Program\ProgramIndikatorRpjmd;
use App\Models\Rpjmd\Sasaran;
use Illuminate\Database\Eloquent\Model;

class ProgramRpjmd extends Model implements ChildrenDataCheckerInterface
{
    protected $table = 'rpjmd_program';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(ProgramRpjmd $programRpjmd) {
            $programRpjmd->programRpjmdAnggaran()->delete();
            $programRpjmd->sumberAnggaranRpjmd()->delete();

            foreach($programRpjmd->indikatorProgram as $indikator){
                $indikator->delete();
            };

            $programRpjmd->anggaran()->delete();
            $programRpjmd->Opd()->detach();

            foreach($programRpjmd->programRenstra as $programRenstra){
                $programRenstra->delete();
            };
        });
    }

    public function getMisi() {
        return $this->sasaran->tujuan->misi;
    }

    public function getRpjmdSasaran() {
        return $this->sasaran;
    }

    public function getRpjmdTujuan() {
        return $this->sasaran->tujuan;
    }

    public function getPrioritasPembangunan() {
        return $this->sasaran->prioritasPembangunan;
    }

    public function hasMandatoryChildren(): bool
    {
        $this->loadCount(['indikatorProgram', 'programRenstra']);
        return $this->indikator_program_count > 0 || $this->program_renstra_count > 0;
    }

    function programRpjmdAnggaran()
    {
        return $this->hasMany(ProgramAnggaranRpjmd::class, 'id');
    }

    function sasaran()
    {
        return $this->belongsTo(Sasaran::class, 'id_rpjmd_sasaran');
    }

    function mstProgram()
    {
        return $this->belongsTo(Program::class, 'id_mst_program');
    }

    function sumberAnggaranRpjmd()
    {
        return $this->hasMany(SumberAnggaranRpjmd::class, 'id_rpjmd_program');
    }

    function indikatorProgram()
    {
        return $this->hasMany(ProgramIndikatorRpjmd::class, 'id_rpjmd_program');
    }

    function anggaran()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable');
    }

    function programRenstra()
    {
        return $this->hasMany(ProgramRenstra::class, 'id_rpjmd_program');
    }

    public function Opd()
    {
        return $this->belongsToMany(
            Opd::class,
            PenaggungJawabRpjmd::class,
            'id_rpjmd_program',
            'id_mst_opd'
        );
    }
}
