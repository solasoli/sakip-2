<?php

namespace App\Models\Rpjmd\Program;

use Illuminate\Database\Eloquent\Model;

class PenaggungJawabRpjmd extends Model
{
    protected $table = 'rpjmd_penanggung_jawab';
    protected $guarded = [];
}
