<?php

namespace App\Models\Rpjmd\Program;

use Illuminate\Database\Eloquent\Model;

class SumberAnggaranRpjmd extends Model
{
    protected $table = 'rpjmd_sumber_anggaran';
    protected $guarded = [];

    function programRpjmd()
    {
        return $this->belongsTo(ProgramRpjmd::class, 'id_rpjmd_program');
    }
}
