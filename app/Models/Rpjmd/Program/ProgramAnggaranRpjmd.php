<?php

namespace App\Models\Rpjmd\Program;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProgramAnggaranRpjmd extends Model
{
    use HasFactory;
    protected $table = 'rpjmd_program_anggaran';
    protected $guarded = [];

    function program()
    {
        return $this->belongsTo(ProgramRpjmd::class, 'id_program');
    }
}
