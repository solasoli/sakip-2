<?php

namespace App\Models\Rpjmd\Program;

use App\Models\Rpjmd\Program\ProgramRpjmd;
use App\Models\Rpjmd\TargetRkpdRpjmd;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Satuan;

class ProgramIndikatorRpjmd extends Model
{
    use HasFactory;
    protected $table = 'rpjmd_program_indikator';
    protected $guarded = [];

    public static function booted()
    {
        static::deleting(function (ProgramIndikatorRpjmd $program) {
            $program->targetProgram()->delete();

            if ($program->targetRkpd) $program->targetRkpd->delete();
        });
    }

    function program()
    {
        return $this->belongsTo(ProgramRpjmd::class, 'id');
    }

    function targetProgram()
    {
        return $this->hasMany(ProgramTargetRpjmd::class, 'id_program_indikator');
    }

    function targetRkpd()
    {
        return $this->morphOne(TargetRkpdRpjmd::class, 'rpjmd');
    }

    function satuan()
    {
        return $this->belongsTo(Satuan::class, 'id_satuan');
    }
}
