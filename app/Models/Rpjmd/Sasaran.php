<?php

namespace App\Models\Rpjmd;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\Renstra\TujuanRenstra;
use App\Models\Rpjmd\Program\ProgramRpjmd;
use App\Models\Satuan;
use App\Models\Tujuan;
use Illuminate\Database\Eloquent\Model;

class  Sasaran extends Model implements ChildrenDataCheckerInterface
{
    protected $table = 'rpjmd_sasaran';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(Sasaran $sasaran) {
            foreach ($sasaran->sasaranDetail as $item) {
                $item->delete();
            }

            if($sasaran->prioritasPembangunan) $sasaran->prioritasPembangunan->delete();

            foreach ($sasaran->program as $item) {
                $item->delete();
            }

            foreach ($sasaran->strategi as $item) {
                $item->delete();
            }

            foreach ($sasaran->tujuanRenstra as $item) {
                $item->delete();
            }
        });
    }

    public function hasMandatoryChildren(): bool
    {
        $this->loadCount(['program', 'tujuanRenstra']);
        return $this->program_count > 0 || $this->tujuan_renstra_count > 0;
    }

    function tujuan()
    {
        return $this->belongsTo(Tujuan::class, 'id_tujuan');
    }

    function sasaranDetail()
    {
        return $this->hasMany(SasaranDetail::class, 'id_sasaran');
    }

    function sasaranTarget()
    {
        return $this->hasManyThrough(SasaranTarget::class, SasaranDetail::class, 'id_sasaran', 'id_sasaran_detail', 'id', 'id');
    }

    function satuan()
    {
        return $this->hasManyThrough(Satuan::class, SasaranDetail::class, 'id_sasaran', 'id', 'id', 'id_sasaran');
    }

    function prioritasPembangunan()
    {
        return $this->hasOne(PrioritasPembangunanRpjmd::class, 'id_sasaran');
    }

    function program()
    {
        return $this->hasMany(ProgramRpjmd::class, 'id_rpjmd_sasaran')->where('id_periode', periode()->id);
    }

    function tujuanRenstra()
    {
        return $this->hasMany(TujuanRenstra::class, 'id_rpjmd_sasaran')->where('id_periode', periode()->id);
    }

    public function strategi() {
        return $this->hasMany(StrategiRpjmd::class, 'id_sasaran');
    }
}
