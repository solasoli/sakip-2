<?php

namespace App\Models\Rpjmd;

use Illuminate\Database\Eloquent\Model;

class KebijakanRpjmd extends Model
{
    protected $table = 'rpjmd_kebijakan';
    protected $guarded = [];

    function strategi()
    {
        return $this->belongsTo(StrategiRpjmd::class, 'id_strategi');
    }
}
