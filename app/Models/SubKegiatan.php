<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubKegiatan extends Model
{
    protected $table = 'mst_sub_kegiatan';
    protected $fillable = [
        "id_kegiatan",
        "kode_sub_kegiatan",
        "name"
    ];

    public function kegiatan() {
        return $this->belongsTo(Kegiatan::class, 'id_kegiatan');
    }
}
