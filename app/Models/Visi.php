<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Visi extends Model
{
    protected $table = 'rpjmd_visi';

    function periode()
    {
        return $this->belongsTo(PeriodeWalkot::class, 'id_periode');
    }
}
