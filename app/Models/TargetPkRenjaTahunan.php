<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TargetPkRenjaTahunan extends Model
{
    protected $table = 'target_pk_renja_tahunan';

    protected $fillable = [
        'pk_renja_tahunan_id',
        'atasan_id',
        'tahun_id',
        'satuan_id',
        'target',
    ];

    public static function booted()
    {
        static::deleting(function (TargetPkRenjaTahunan $target) {
            $target->penanggungJawab()->sync([]);
        });
    }

    public function penanggungJawab() {
        return $this->belongsToMany(
            Pegawai::class,
            'target_pk_renja_tahunan_penanggung_jawab',
            'target_pk_renja_tahunan_id',
            'penanggung_jawab_id'
        );
    }

    public function atasan() {
        return $this->belongsTo(Pegawai::class, 'atasan_id');
    }

    public function tahun() {
        return $this->belongsTo(PeriodePerTahun::class, 'tahun_id');
    }

    public function satuan() {
        return $this->belongsTo(Satuan::class, 'satuan_id');
    }

    public function pkRenjaTahunan() {
        return $this->belongsTo(PkRenjaTahunan::class, 'pk_renja_tahunan_id');
    }

}
