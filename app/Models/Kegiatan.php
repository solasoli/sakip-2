<?php

namespace App\Models;

use App\Models\Renstra\Kegiatan\KegiatanRenstra;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kegiatan extends Model
{
    use HasFactory;
    protected $table = 'mst_kegiatan';

    public function renstraKegiatan() {
        return $this->hasMany(KegiatanRenstra::class, 'id_mst_kegiatan');
    }
}
