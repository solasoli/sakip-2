<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PeriodePerTahun extends Model
{
    use HasFactory;
    protected $table = 'mst_periode_per_tahun';

    public static function booted() {
        static::deleting(function(PeriodePerTahun $periode) {
            foreach ($periode->keuanganTahunan as $item) {
                if($item->assignable) $item->assignable->delete();
                $item->delete();
            }

            if($periode->tujuanTarget) $periode->tujuanTarget->delete();
        });
    }

    public function periode()
    {
        return $this->belongsTo(PeriodeWalkot::class);
    }

    public function tujuanTarget()
    {
        return $this->hasOne(TujuanTarget::class, 'id_periode_per_tahun');
    }

    public function keuanganTahunan()
    {
        return $this->hasMany(KeuanganTahunan::class, 'id_periode_per_tahun');
    }
}
