<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TujuanTarget extends Model
{
    use HasFactory;
    protected $table = 'rpjmd_tujuan_target';
    protected $guarded = [];

    public function tahunPeriode()
    {
        return $this->belongsTo(PeriodePerTahun::class, 'id_periode_per_tahun');
    }
}
