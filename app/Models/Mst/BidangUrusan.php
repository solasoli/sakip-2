<?php

namespace App\Models\Mst;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\Program;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BidangUrusan extends Model implements ChildrenDataCheckerInterface
{
    use HasFactory;
    protected $table = 'mst_bidang_urusan';
    protected $guarded = ['id'];

    public static function booted() {
        static::deleting(function(BidangUrusan $bidangUrusan) {
            foreach ($bidangUrusan->program as $item) {
                $item->delete();
            }
        });
    }

    public function hasMandatoryChildren(): bool
    {
        return $this->program()->count() > 0;
    }

    function urusan()
    {
        return $this->belongsTo(Urusan::class, 'id_urusan');
    }

    function program() {
        return $this->hasMany(Program::class, 'id_bidang_urusan');
    }
}
