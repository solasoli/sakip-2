<?php

namespace App\Models\Mst;

use App\Interfaces\ChildrenDataCheckerInterface;
use Illuminate\Database\Eloquent\Model;

class Urusan extends Model implements ChildrenDataCheckerInterface
{
    protected $table = 'mst_urusan';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(Urusan $urusan) {
            foreach ($urusan->bidangUrusan as $item) {
                $item->delete();
            }
        });
    }

    public function hasMandatoryChildren(): bool
    {
        return $this->bidangUrusan()->count() > 0;
    }

    function bidangUrusan()
    {
        return $this->hasMany(BidangUrusan::class, 'id_urusan');
    }
}
