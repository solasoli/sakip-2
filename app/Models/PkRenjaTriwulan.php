<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PkRenjaTriwulan extends Model
{
    protected $table = 'pk_renja_triwulan';
    protected $fillable = [
        'pk_id',
        'pk_type',
        'id_periode_per_tahun',
        'triwulan',
        'target',
    ];

    public static function booted() {
        static::deleting(function(PkRenjaTriwulan $pkRenjaTriwulan) {
            $pkRenjaTriwulan->penanggungJawab()->detach();
        });
    }

    public function pk() {
        return $this->morphTo();
    }

    public function penanggungJawab() {
        return $this->belongsToMany(Pegawai::class,
            'pk_renja_triwulan_penanggung_jawab',
            'id_pk_renja_triwulan',
            'id_pegawai'
        );
    }
}
