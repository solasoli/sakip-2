<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PkRenjaTahunan extends Model
{
    protected $table = 'pk_renja_tahunan';
    protected $fillable = [
        'pk_id',
        'pk_type',
        'id_satuan',
        'periode'
    ];

    public static function booted()
    {
        static::deleting(function (PkRenjaTahunan $pkRenjaTahunan) {
            $pkRenjaTahunan->targetRenja()->delete();

            foreach ($pkRenjaTahunan->targetPk as $target) {
                $target->delete();
            }
        });
    }

    public function satuan()
    {
        return $this->belongsTo(Satuan::class, 'id_satuan');
    }

    public function pk()
    {
        return $this->morphTo();
    }

    public function targetRenja()
    {
        return $this->morphMany(KeuanganTahunan::class, 'assignable');
    }

    public function targetPk()
    {
        return $this->hasMany(TargetPkRenjaTahunan::class);
    }
}
