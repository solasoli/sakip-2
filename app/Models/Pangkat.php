<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pangkat extends Model
{
    protected $table = 'mst_pangkat';
    protected $guarded = [];
    use HasFactory;

    public function pegawai()
    {
        return $this->hasMany(Pegawai::class,'id_pangkat');
    }
}
