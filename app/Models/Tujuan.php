<?php

namespace App\Models;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\Rpjmd\Sasaran;
use Illuminate\Database\Eloquent\Model;

class Tujuan extends Model implements ChildrenDataCheckerInterface
{
    protected $table = 'rpjmd_tujuan';
    protected $guarded = ['id'];

    public static function booted() {
        static::deleting(function(Tujuan $tujuan) {
            foreach ($tujuan->tujuanIndikator as $item) {
                $item->delete();
            }

            foreach ($tujuan->sasaran as $item) {
                $item->delete();
            }
        });
    }

    public function hasMandatoryChildren(): bool
    {
        $this->loadCount(['sasaran']);
        return $this->sasaran_count > 0;
    }

    function tujuanIndikator()
    {
        return $this->hasMany(TujuanIndikator::class, 'id_tujuan');
    }

    function misi()
    {
        return $this->belongsTo(Misi::class, 'id_misi');
    }

    function sasaran()
    {
        return $this->hasMany(Sasaran::class, 'id_tujuan')->where('id_periode', periode()->id);
    }

}
