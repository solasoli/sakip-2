<?php

namespace App\Models;

use App\Interfaces\ChildrenDataCheckerInterface;
use App\Models\Mst\BidangUrusan;
use App\Models\Rpjmd\Program\ProgramRpjmd;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Program extends Model implements ChildrenDataCheckerInterface
{
    use HasFactory;
    protected $table = 'mst_program';
    protected $guarded = [];

    public static function booted() {
        static::deleting(function(Program $program) {
            foreach ($program->rpjmdProgram as $item) {
                $item->delete();
            }
        });
    }

    public function hasMandatoryChildren(): bool
    {
        return $this->rpjmdProgram != null;
    }

    function bidangUrusan()
    {
        return $this->belongsTo(BidangUrusan::class, 'id_bidang_urusan');
    }

    function rpjmdProgram()
    {
        return $this->hasMany(ProgramRpjmd::class, 'id_mst_program');
    }
}
