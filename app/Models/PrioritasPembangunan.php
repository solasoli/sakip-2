<?php

namespace App\Models;

use App\Models\Rpjmd\PrioritasPembangunanRpjmd;
use Illuminate\Database\Eloquent\Model;

class PrioritasPembangunan extends Model
{
    protected $table = 'prioritas_pembangunan';

    public static function booted() {
        static::deleting(function(PrioritasPembangunan $prioritasPembangunan) {
            foreach($prioritasPembangunan->prioritasPembangunanRpjmd as $item) {
                $item->delete();
            }
        });
    }

    function prioritasPembangunanRpjmd()
    {
        return $this->hasOne(PrioritasPembangunanRpjmd::class, 'id');
    }
}
