<?php

namespace App\View\Components;

use Illuminate\View\Component;

class HierarchyCard extends Component
{

    public $title;
    public $hierarchy;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $hierarchy)
    {
        $this->title = $title;
        $this->hierarchy = $hierarchy;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.hierarchy-card');
    }
}
