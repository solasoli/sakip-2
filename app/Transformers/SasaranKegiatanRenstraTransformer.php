<?php

namespace App\Transformers;

use App\Models\Renstra\Kegiatan\SasaranKegiatanRenstra;
use League\Fractal\TransformerAbstract;

class SasaranKegiatanRenstraTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'kegiatan',
        'indikator'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SasaranKegiatanRenstra $sasaranKegiatanRenstra)
    {
        return [
            "id" => $sasaranKegiatanRenstra->id,
            "id_renstra_kegiatan" => $sasaranKegiatanRenstra->id_renstra_kegiatan,
            "name" => $sasaranKegiatanRenstra->name,
            "kegiatan" => null
        ];
    }

    public function includeKegiatan(SasaranKegiatanRenstra $sasaranKegiatanRenstra) {
        return $this->item($sasaranKegiatanRenstra->kegiatan, new KegiatanRenstraTransformer);
    }

    public function includeIndikator(SasaranKegiatanRenstra $sasaranKegiatanRenstra) {
        return $this->collection($sasaranKegiatanRenstra->indikator, new SasaranKegiatanIndikatorRenstraTransformer);
    }
}
