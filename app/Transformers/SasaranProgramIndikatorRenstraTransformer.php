<?php

namespace App\Transformers;

use App\Models\Renstra\Program\SasaranProgramIndikatorRenstra;
use League\Fractal\TransformerAbstract;

class SasaranProgramIndikatorRenstraTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SasaranProgramIndikatorRenstra $indikator)
    {
        return [
            "id" => $indikator->id,
            "indikator" => $indikator->indikator,
            "is_pk" => $indikator->is_pk,
            "is_iku" => $indikator->is_iku,
            "cara_pengukuran" => $indikator->cara_pengukuran,
        ];
    }
}
