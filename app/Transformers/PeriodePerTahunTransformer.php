<?php

namespace App\Transformers;

use App\Models\PeriodePerTahun;
use League\Fractal\TransformerAbstract;

class PeriodePerTahunTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(PeriodePerTahun $periodePerTahun)
    {
        return [
            "id" => $periodePerTahun->id,
            "id_periode" => $periodePerTahun->id_periode,
            "tahun" => $periodePerTahun->tahun,
        ];
    }
}
