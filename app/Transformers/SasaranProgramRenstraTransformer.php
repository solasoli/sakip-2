<?php

namespace App\Transformers;

use App\Models\Renstra\Program\SasaranProgramRenstra;
use League\Fractal\TransformerAbstract;

class SasaranProgramRenstraTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'indikator',
        'program'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SasaranProgramRenstra $sasaranProgramRenstra)
    {
        return [
            "id" => $sasaranProgramRenstra->id,
            "name" => $sasaranProgramRenstra->name,
            "id_opd" => $sasaranProgramRenstra->id_opd,
            "id_program_renstra" => $sasaranProgramRenstra->id_program_renstra,
            "indikator" => [],
            "program" => null
        ];
    }

    public function includeIndikator(SasaranProgramRenstra $sasaranProgramRenstra)
    {
        return $this->collection($sasaranProgramRenstra->indikator, new SasaranProgramIndikatorRenstraTransformer);
    }

    public function includeProgram(SasaranProgramRenstra $sasaranProgramRenstra)
    {
        return $this->item($sasaranProgramRenstra->program, new ProgramRenstraTransformer);
    }
}
