<?php

namespace App\Transformers;

use App\Models\Renstra\Kegiatan\KegiatanRenstra;
use League\Fractal\TransformerAbstract;

class KegiatanRenstraTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'mst_kegiatan',
        'tahun_periode',
        'program_renstra',
        'sasaran_kegiatan'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(KegiatanRenstra $kegiatanRenstra)
    {
        return [
            "id" => $kegiatanRenstra->id,
            "id_mst_opd" => $kegiatanRenstra->id_mst_opd,
            "id_mst_kegiatan" => $kegiatanRenstra->id_mst_kegiatan,
            "id_renstra_program" => $kegiatanRenstra->id_renstra_program,
            "mst_kegiatan" => null,
            "tahun_periode" => []
        ];
    }

    public function includeMstKegiatan(KegiatanRenstra $kegiatanRenstra)
    {
        return $this->item($kegiatanRenstra->mstKegiatan, new KegiatanTransformer);
    }

    public function includeTahunPeriode(KegiatanRenstra $kegiatanRenstra)
    {
        return $this->collection($kegiatanRenstra->getTahunPeriode(), new PeriodePerTahunTransformer);
    }

    public function includeProgramRenstra(KegiatanRenstra $kegiatanRenstra)
    {
        return $this->item($kegiatanRenstra->programRenstra, new ProgramRenstraTransformer);
    }

    public function includeSasaranKegiatan(KegiatanRenstra $kegiatanRenstra)
    {
        return $this->collection($kegiatanRenstra->sasaranKegiatan, new SasaranKegiatanRenstraTransformer);
    }
}
