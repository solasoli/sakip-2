<?php

namespace App\Transformers;

use App\Models\Pegawai;
use League\Fractal\TransformerAbstract;

class PegawaiTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'opd',
        'eselon',
        'pangkat',
        'golongan'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Pegawai $pegawai)
    {
        return [
            "id" => $pegawai->id,
            "nip" => $pegawai->nip,
            "nama" => $pegawai->nama,
            "gelar_depan" => $pegawai->gelar_depan,
            "gelar_belakang" => $pegawai->gelar_belakang,
            "id_eselon" => $pegawai->id_eselon,
            "jabatan" => $pegawai->jabatan,
            "id_opd" => $pegawai->id_opd,
            "opd" => null,
            "eselon" => null,
        ];
    }

    public function includeOpd(Pegawai $pegawai)
    {
        return $this->item($pegawai->opd, new OpdTransformer);
    }

    public function includeEselon(Pegawai $pegawai)
    {
        return $pegawai->eselon ?  $this->primitive([
            "id" => $pegawai->eselon->id,
            "nama" => $pegawai->eselon->nama,
        ]) : null;
    }

    //     public function includePangkat(Pegawai $pegawai)
    //     {
    //         return $this->primitive([
    //             "id" => $pegawai->pangkat->id,
    //             "nama" => $pegawai->pangkat->nama,
    //         ]);
    //     }

    //     public function includeGolongan(Pegawai $pegawai)
    //     {
    //         return $this->primitive([
    //             "id" => $pegawai->pangkatGolongan->id,
    //             "nama" => $pegawai->pangkatGolongan->nama,
    //         ]);
    //     }
}
