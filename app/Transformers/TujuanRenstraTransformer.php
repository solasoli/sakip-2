<?php

namespace App\Transformers;

use App\Models\Renstra\TujuanRenstra;
use League\Fractal\TransformerAbstract;

class TujuanRenstraTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        "opd"
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TujuanRenstra $tujuanRenstra)
    {
        return [
            "name" => $tujuanRenstra->name,
            "opd" => null
        ];
    }

    public function includeOpd(TujuanRenstra $tujuanRenstra)
    {
        return $this->item($tujuanRenstra->opd, new OpdTransformer);
    }
}
