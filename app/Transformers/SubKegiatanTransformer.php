<?php

namespace App\Transformers;

use App\Models\SubKegiatan;
use League\Fractal\TransformerAbstract;

class SubKegiatanTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SubKegiatan $subKegiatan)
    {
        return [
            "id" => $subKegiatan->id,
            "id_kegiatan" => $subKegiatan->id_kegiatan,
            "kode_sub_kegiatan" => $subKegiatan->kode_sub_kegiatan,
            "name" => $subKegiatan->name
        ];
    }
}
