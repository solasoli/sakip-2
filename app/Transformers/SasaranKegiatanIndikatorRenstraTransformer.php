<?php

namespace App\Transformers;

use App\Models\Renstra\Kegiatan\SasaranKegiatanIndikatorRenstra;
use League\Fractal\TransformerAbstract;

class SasaranKegiatanIndikatorRenstraTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SasaranKegiatanIndikatorRenstra $indikator)
    {
        return [
            'id' => $indikator->id,
            'name' => $indikator->name,
            'id_sasaran_kegiatan' => $indikator->id_sasaran_kegiatan,
            'id_satuan' => $indikator->id_satuan,
            'is_pk' => $indikator->is_pk,
            'is_iku' => $indikator->is_iku,
            'cara_pengukuran' => $indikator->cara_pengukuran,
            'target_awal' => $indikator->target_awal,
            'target_akhir' => $indikator->target_akhir,
            'is_deleted' => $indikator->is_deleted,
        ];
    }
}
