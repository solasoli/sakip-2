<?php

namespace App\Transformers;

use App\Models\Renstra\SasaranRenstra;
use League\Fractal\TransformerAbstract;

class SasaranRenstraTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        "renstra_tujuan",
        "renstra_indikator"
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SasaranRenstra $sasaranRenstra)
    {
        return [
            "id" => $sasaranRenstra->id,
            "name" => $sasaranRenstra->name,
            "renstra_tujuan" => null
        ];
    }

    public function includeRenstraTujuan(SasaranRenstra $sasaranRenstra)
    {
        return $this->item($sasaranRenstra->renstraTujuan, new TujuanRenstraTransformer);
    }

    public function includeRenstraIndikator(SasaranRenstra $sasaranRenstra)
    {
        return $this->collection($sasaranRenstra->indikator, new SasaranRenstraIndikatorTransformer);
    }
}
