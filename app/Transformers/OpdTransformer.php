<?php

namespace App\Transformers;

use App\Models\Opd;
use League\Fractal\TransformerAbstract;

class OpdTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Opd $opd)
    {
        return [
            "id" => $opd->id,
            "name" => $opd->name,
        ];
    }
}
