<?php

namespace App\Transformers;

use App\Models\Renstra\Program\ProgramRenstra;
use League\Fractal\TransformerAbstract;

class ProgramRenstraTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'sasaran_renstra',
        'sasaran_program'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ProgramRenstra $programRenstra)
    {
        return [
            'id'  => $programRenstra->id,
            'id_renstra_sasaran'  => $programRenstra->id_renstra_sasaran,
            'id_rpjmd_program'  => $programRenstra->id_rpjmd_program,
            'id_periode'  => $programRenstra->id_periode,
            'sasaran_renstra' => null
        ];
    }

    public function includeSasaranRenstra(ProgramRenstra $programRenstra)
    {
        return $this->item($programRenstra->sasaranRenstra, new SasaranRenstraTransformer);
    }

    public function includeSasaranProgram(ProgramRenstra $programRenstra)
    {
        return $this->collection($programRenstra->sasaranProgram, new SasaranProgramRenstraTransformer);
    }
}
