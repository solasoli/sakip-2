<?php

namespace App\Transformers;

use App\Models\Renstra\SubKegiatan\SasaranSubKegiatanOutputRenstra;
use League\Fractal\TransformerAbstract;

class SasaranSubKegiatanOutputRenstraTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SasaranSubKegiatanOutputRenstra $output)
    {
        return [
            'id' => $output->id,
            'name' => $output->name,
            'id_sasaran_sub_kegiatan' => $output->id_sasaran_sub_kegiatan,
            'id_satuan' => $output->id_satuan,
            'is_pk' => $output->is_pk,
            'is_iku' => $output->is_iku,
            'cara_pengukuran' => $output->cara_pengukuran,
            'target_awal' => $output->target_awal,
            'target_akhir' => $output->target_akhir,
            'is_deleted' => $output->is_deleted,
        ];
    }
}
