<?php

namespace App\Transformers;

use App\Models\Renstra\SubKegiatan\SasaranSubKegiatanRenstra;
use League\Fractal\TransformerAbstract;

class SasaranSubKegiatanRenstraTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'output',
        'sub_kegiatan',
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SasaranSubKegiatanRenstra $sasaranSubKegiatanRenstra)
    {
        return [
            'id' => $sasaranSubKegiatanRenstra->id,
            'id_renstra_sub_kegiatan' => $sasaranSubKegiatanRenstra->id_renstra_sub_kegiatan,
            'name' => $sasaranSubKegiatanRenstra->name,
        ];
    }

    public function includeSubKegiatan(SasaranSubKegiatanRenstra $sasaranSubKegiatanRenstra)
    {
        return $this->item($sasaranSubKegiatanRenstra->subKegiatan, new SubKegiatanRenstraTransformer);
    }

    public function includeOutput(SasaranSubKegiatanRenstra $sasaranSubKegiatanRenstra)
    {
        return $this->collection($sasaranSubKegiatanRenstra->output, new SasaranSubKegiatanOutputRenstraTransformer);
    }
}
