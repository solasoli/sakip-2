<?php

namespace App\Transformers;

use App\Models\Renstra\SubKegiatan\SubKegiatanRenstra;
use League\Fractal\TransformerAbstract;

class SubKegiatanRenstraTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'mst_sub_kegiatan',
        'tahun_periode',
        'kegiatan'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SubKegiatanRenstra $subKegiatanRenstra)
    {
        return [
            "id" => $subKegiatanRenstra->id,
            "id_renstra_kegiatan" => $subKegiatanRenstra->id_renstra_kegiatan,
            "id_mst_sub_kegiatan" => $subKegiatanRenstra->id_mst_sub_kegiatan,
        ];
    }


    public function includeMstSubKegiatan(SubKegiatanRenstra $subKegiatan)
    {
        return $this->item($subKegiatan->mstSubKegiatan, new SubKegiatanTransformer);
    }

    public function includeTahunPeriode(SubKegiatanRenstra $subKegiatan)
    {
        return $this->collection($subKegiatan->getTahunPeriode(), new PeriodePerTahunTransformer);
    }

    public function includeKegiatan(SubKegiatanRenstra $subKegiatanRenstra)
    {
        return $this->item($subKegiatanRenstra->kegiatan, new KegiatanRenstraTransformer);
    }
}
