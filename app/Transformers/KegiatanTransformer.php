<?php

namespace App\Transformers;

use App\Models\Kegiatan;
use League\Fractal\TransformerAbstract;

class KegiatanTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        //
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Kegiatan $kegiatan)
    {
        return [
            "id" => $kegiatan->id,
            "id_program" => $kegiatan->id_program,
            "kode_kegiatan" => $kegiatan->kode_kegiatan,
            "name" => $kegiatan->name
        ];
    }
}
