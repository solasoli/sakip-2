<?php

namespace App\Transformers;

use App\Models\Renstra\SasaranRenstraIndikator;
use League\Fractal\TransformerAbstract;

class SasaranRenstraIndikatorTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        //
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'satuan'
    ];

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(SasaranRenstraIndikator $indikator)
    {
        return [
            "id" => $indikator->id,
            "indikator" => $indikator->indikator,
            "cara_pengukuran" => $indikator->cara_pengukuran,
            "is_pk" => $indikator->is_pk,
            "is_iku" => $indikator->is_iku,
            "id_satuan" => $indikator->id_satuan,
            "id_sasaran" => $indikator->id_sasaran,
            "satuan" => null,
            "sasaran" => null,
        ];
    }

    public function includeSatuan(SasaranRenstraIndikator $indikator) {
        return $this->item($indikator->satuan, new SatuanTransformer);
    }
}
