//  initial select2
var selectPlaceholder = function(el, str) {
    $(document).find(el).select2({
        placeholder: str,
        width: "100%",
    });
}

var selectUsingParent = function(el, parentEl, str) {
    $(document).find(el).select2({
        placeholder: str,
        width: "100%",
        dropdownParent: $(parentEl),
    });
}
// end select2

// function btn cancel
function cancel(url) {
    window.location.href = url;
}

// fungsi untuk mengisi value pada checkbox
const checkboxValue = function() {
    const checkbox = document.querySelectorAll('input[type=checkbox]');
    checkbox.forEach(res => {
        setBoolValue(res);
    });

    document.addEventListener('click', function() {
        let el = this.activeElement;
        if (el.getAttribute('type') == 'checkbox') {
            setBoolValue(el);
        }
    });
}

const setBoolValue = function(el) {
    if (el.checked == true) {
        el.nextElementSibling.setAttribute('value', 1);
    } else if (el.checked == false) {
        el.nextElementSibling.setAttribute('value', 0);
    }
}
// end function

// function for validate input
function inputValidate(formSelector, type=[], except=null) {
    const form = document.querySelector(formSelector);
    // while form submitted
    form.addEventListener('submit', (e) => {
        // remove text validate if exist
        const elValidate = form.querySelectorAll('.validate');
        elValidate.forEach(res => res.remove());
        // loop type to get element
        type.forEach(t => {
            // loop element to validate
            const el = document.querySelectorAll(t);
            el.forEach(res => {
                const parentEl = res.closest('.form-group');
                const inputName = res.id;
                res.style.border = '1px solid #838383';
                // except element
                if(inputName != except && inputName != '') {
                    // show text validate if value null
                    if(res.value == '' || res.value == undefined || res.value == null) {
                        e.preventDefault();
                        const placeText = document.createElement('small');
                        const text = document.createTextNode(`tidak boleh kosong`);

                        placeText.append(text);
                        placeText.classList.add('text-danger', 'validate');
                        res.style.border = '1px solid #d04c4c';

                        res.parentNode.insertBefore(placeText, res.nextSibling)
                    }
                }
            });
        });
    });
}
// end validate
