<?php

namespace Database\Seeders;

use App\Constants\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User;
        $user->id = 1;
        $user->name = 'admin';
        $user->email = 'admin@admin.com';
        $user->username = 'admin';
        $user->role = Role::SUPER_ADMIN;
        $user->password = Hash::make('12345');
        $user->save();
    }
}
