<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetPkRenjaTahunanPenanggungJawabTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_pk_renja_tahunan_penanggung_jawab', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('target_pk_renja_tahunan_id');
            $table->unsignedBigInteger('penanggung_jawab_id');
            $table->timestamps();

            $table->foreign('target_pk_renja_tahunan_id', 'tprtpj_tprt_id_foreign')
                ->references('id')
                ->on('target_pk_renja_tahunan');

            $table->foreign('penanggung_jawab_id', 'tprtpj_pj_id')
                ->references('id')
                ->on('mst_pegawai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_pk_renja_tahunan_penanggung_jawab');
    }
}
