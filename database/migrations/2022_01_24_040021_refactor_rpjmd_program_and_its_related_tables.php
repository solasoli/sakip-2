<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RefactorRpjmdProgramAndItsRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::table('rpjmd_program', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_mst_program')->change();
            $table->unsignedBigInteger('id_rpjmd_sasaran')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->foreign('id_mst_program')->on('mst_program')->references('id');
            $table->foreign('id_rpjmd_sasaran')->on('rpjmd_sasaran')->references('id');
            $table->foreign('id_periode')->on('mst_periode')->references('id');
        });

        Schema::table('rpjmd_program_anggaran', function (Blueprint $table) {
            $table->unsignedBigInteger('id_program')->change();
            $table->foreign('id_program')->on('rpjmd_program')->references('id');
        });

        Schema::table('rpjmd_program_indikator', function (Blueprint $table) {
            $table->unsignedBigInteger('id_satuan')->change();
            $table->unsignedBigInteger('id_rpjmd_program')->change();
            $table->foreign('id_satuan')->on('mst_satuan')->references('id');
            $table->foreign('id_rpjmd_program')->on('rpjmd_program')->references('id');
        });

        Schema::table('rpjmd_program_target', function (Blueprint $table) {
            $table->unsignedBigInteger('id_program_indikator')->change();
            $table->unsignedBigInteger('id_periode_per_tahun')->change();
            $table->foreign('id_program_indikator')->on('rpjmd_program_indikator')->references('id');
            $table->foreign('id_periode_per_tahun')->on('mst_periode_per_tahun')->references('id');
        });

        Schema::table('rpjmd_sumber_anggaran', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_mst_sumber_anggaran')->change();
            $table->unsignedBigInteger('id_rpjmd_program')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->foreign('id_mst_sumber_anggaran')->on('sumber_anggaran')->references('id');
            $table->foreign('id_rpjmd_program')->on('rpjmd_program')->references('id');
            $table->foreign('id_periode')->on('mst_periode')->references('id');
        });

        Schema::table('rpjmd_penanggung_jawab', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_mst_opd')->change();
            $table->unsignedBigInteger('id_rpjmd_program')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->foreign('id_mst_opd')->on('mst_opd')->references('id');
            $table->foreign('id_rpjmd_program')->on('rpjmd_program')->references('id');
            $table->foreign('id_periode')->on('mst_periode')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpjmd_program', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_mst_program']);
            $table->dropForeign(['id_rpjmd_sasaran']);
            $table->dropForeign(['id_periode']);
        });

        Schema::table('rpjmd_program_anggaran', function (Blueprint $table) {
            $table->dropForeign(['id_program']);
        });

        Schema::table('rpjmd_program_indikator', function (Blueprint $table) {
            $table->dropForeign(['id_satuan']);
            $table->dropForeign(['id_rpjmd_program']);
        });

        Schema::table('rpjmd_program_target', function (Blueprint $table) {
            $table->dropForeign(['id_program_indikator']);
            $table->dropForeign(['id_periode_per_tahun']);
        });

        Schema::table('rpjmd_sumber_anggaran', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_mst_sumber_anggaran']);
            $table->dropForeign(['id_rpjmd_program']);
            $table->dropForeign(['id_periode']);
        });

        Schema::table('rpjmd_penanggung_jawab', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_mst_opd']);
            $table->dropForeign(['id_rpjmd_program']);
            $table->dropForeign(['id_periode']);
        });
    }
}
