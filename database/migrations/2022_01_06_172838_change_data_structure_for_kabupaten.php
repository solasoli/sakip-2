<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDataStructureForKabupaten extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_pegawai', function (Blueprint $table) {
            $table->string('gelar_depan', 64)->nullable(true)->after('nama');
            $table->string('gelar_belakang', 64)->nullable(true)->after('gelar_depan');
            $table->unsignedBigInteger('id_eselon')->nullable(true)->change();
            $table->string('jabatan', 256)->nullable(true)->change();
            $table->unsignedBigInteger('id_opd')->nullable(true)->change();
            $table->dropColumn('unit');
            $table->dropForeign(['id_pangkat']);
            $table->dropForeign(['id_pangkat_gol']);
            $table->dropColumn('id_pangkat');
            $table->dropColumn('id_pangkat_gol');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_pegawai', function (Blueprint $table) {
            $table->integer('id_pangkat')->nullable(true)->after('nama');
            $table->integer('id_pangkat_gol')->nullable(true)->after('id_pangkat');
            $table->unsignedBigInteger('id_eselon')->nullable(true)->change()->after('id_pangkat_gol');
            $table->string('jabatan', 256)->nullable(true)->change()->after('id_eselon');
            $table->string('unit', 256)->nullable(true)->after('jabatan');
            $table->integer('id_opd')->nullable(true)->change()->after('unit');
            $table->dropColumn('gelar_depan');
            $table->dropColumn('gelar_belakang');
        });
    }
}
