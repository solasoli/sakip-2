<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveIdSatuanFromPkRenjaTahunanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pk_renja_tahunan', function (Blueprint $table) {
            $table->dropColumn('id_satuan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pk_renja_tahunan', function (Blueprint $table) {
            $table->unsignedBigInteger('id_satuan')->nullable();
        });
    }
}
