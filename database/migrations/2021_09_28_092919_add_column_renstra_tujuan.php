<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRenstraTujuan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('renstra_tujuan_indikator', function(Blueprint $table) {
            $table->string('kondisi_awal')->after('id_tujuan');
            $table->string('kondisi_akhir')->after('kondisi_awal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_tujuan_indikator', function(Blueprint $table) {
            $table->dropColumn('kondisi_awal');
            $table->dropColumn('kondisi_akhir');
        });
    }
}
