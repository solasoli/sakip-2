<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RefactorRenstraKegiatanAndItsRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::table('renstra_kegiatan', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_mst_opd')->change();
            $table->unsignedBigInteger('id_mst_kegiatan')->change();
            $table->unsignedBigInteger('id_renstra_program')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->foreign('id_mst_opd')->references('id')->on('mst_opd');
            $table->foreign('id_mst_kegiatan')->references('id')->on('mst_kegiatan');
            $table->foreign('id_renstra_program')->references('id')->on('renstra_program');
            $table->foreign('id_periode')->references('id')->on('mst_periode');
        });

        Schema::table('renstra_kegiatan_sumber_anggaran', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_renstra_kegiatan')->change();
            $table->unsignedBigInteger('id_mst_sumber_anggaran')->change();
            $table->foreign('id_renstra_kegiatan')->references('id')->on('renstra_kegiatan');
            $table->foreign('id_mst_sumber_anggaran')->references('id')->on('sumber_anggaran');
        });

        Schema::table('renstra_sasaran_kegiatan', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });

        Schema::table('renstra_sasaran_kegiatan_indikator', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });

        Schema::table('renstra_sasaran_kegiatan_indikator_target', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_kegiatan', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_mst_opd']);
            $table->dropForeign(['id_mst_kegiatan']);
            $table->dropForeign(['id_renstra_program']);
            $table->dropForeign(['id_periode']);
        });

        Schema::table('renstra_kegiatan_sumber_anggaran', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_renstra_kegiatan']);
            $table->dropForeign(['id_mst_sumber_anggaran']);
        });

        Schema::table('renstra_sasaran_kegiatan', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
        });

        Schema::table('renstra_sasaran_kegiatan_indikator', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
        });

        Schema::table('renstra_sasaran_kegiatan_indikator_target', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
        });
    }
}
