<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOnTableRpjmdTujuanIndikator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpjmd_tujuan_indikator', function (Blueprint $table) {
            $table->string('awal')->after('id_satuan');
            $table->string('akhir')->after('awal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpjmd_tujuan_indikator', function (Blueprint $table) {
            $table->dropColumn('awal');
            $table->dropColumn('akhir');
        });
    }
}
