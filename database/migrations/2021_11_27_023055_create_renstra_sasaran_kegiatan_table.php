<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSasaranKegiatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sasaran_kegiatan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_renstra_kegiatan');
            $table->string('name');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();

            $table->foreign('id_renstra_kegiatan', 'renstra_kegiatan_foreign')
                ->references('id')
                ->on('renstra_kegiatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sasaran_kegiatan');
    }
}
