<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RefactorRenstraSubKegiatanAndItsRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::table('renstra_sasaran_sub_kegiatan', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });

        Schema::table('renstra_sasaran_sub_kegiatan_output', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });

        Schema::table('renstra_sasaran_sub_kegiatan_output_target', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_sasaran_sub_kegiatan', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
        });

        Schema::table('renstra_sasaran_sub_kegiatan_output', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
        });

        Schema::table('renstra_sasaran_sub_kegiatan_output_target', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
        });
    }
}
