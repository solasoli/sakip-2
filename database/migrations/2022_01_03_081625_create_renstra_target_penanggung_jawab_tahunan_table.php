<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraTargetPenanggungJawabTahunanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_target_penanggung_jawab_tahunan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('renstra_id');
            $table->string('renstra_type');
            $table->unsignedBigInteger('id_periode_per_tahun');
            $table->string('target')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_target_penanggung_jawab_tahunan');
    }
}
