<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSasaranProgramIndikator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sasaran_program_indikator', function (Blueprint $table) {
            $table->id();
            $table->string('indikator');
            $table->text('cara_pengukuran');
            $table->bigInteger('id_satuan');
            $table->bigInteger('id_sasaran_program');
            $table->boolean('is_pk')->default(0);
            $table->boolean('is_iku')->default(0);
            $table->string('awal');
            $table->string('akhir');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sasaran_program_indikator');
    }
}
