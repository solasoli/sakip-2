<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSasaranProgramTarget extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sasaran_program_target', function (Blueprint $table) {
            $table->id();
            $table->string('target');
            $table->bigInteger('id_periode_per_tahun');
            $table->bigInteger('id_sasaran_program_indikator');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sasaran_program_target');
    }
}
