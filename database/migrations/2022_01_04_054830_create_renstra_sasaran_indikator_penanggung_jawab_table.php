<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSasaranIndikatorPenanggungJawabTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sasaran_indikator_penanggung_jawab', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_renstra_sasaran_indikator');
            $table->unsignedBigInteger('id_pegawai');
            $table->timestamps();

            $table->foreign('id_renstra_sasaran_indikator', 'rsipj_rsi_foreign')
                ->references('id')
                ->on('renstra_sasaran_indikator');

            $table->foreign('id_pegawai')
                ->references('id')
                ->on('mst_pegawai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sasaran_indikator_penanggung_jawab');
    }
}
