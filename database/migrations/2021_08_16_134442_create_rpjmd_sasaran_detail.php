<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRpjmdSasaranDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpjmd_sasaran_detail', function (Blueprint $table) {
            $table->id();
            $table->string('indikator');
            $table->string('cara_pengukuran');
            $table->integer('id_satuan');
            $table->integer('id_sasaran');
            $table->boolean('is_pk')->default(0);
            $table->boolean('is_iku')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpjmd_sasaran_detail');
    }
}
