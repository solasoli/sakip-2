<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSubKegiatanSumberAnggaranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sub_kegiatan_sumber_anggaran', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_renstra_sub_kegiatan');
            $table->unsignedBigInteger('id_sumber_anggaran');
            $table->timestamps();

            $table->foreign('id_renstra_sub_kegiatan', 'rsk_rsksa_id_renstra_sub_kegiatan_foreign')
                ->references('id')
                ->on('renstra_sub_kegiatan');

            $table->foreign('id_sumber_anggaran')
                ->references('id')
                ->on('sumber_anggaran');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sub_kegiatan_sumber_anggaran');
    }
}
