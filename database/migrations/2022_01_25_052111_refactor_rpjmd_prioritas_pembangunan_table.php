<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RefactorRpjmdPrioritasPembangunanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::table('rpjmd_prioritas_pembangunan', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_sasaran')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->unsignedBigInteger('id_mst_prioritas_pembangunan')->change();
            $table->foreign('id_sasaran')->on('rpjmd_sasaran')->references('id');
            $table->foreign('id_periode')->on('mst_periode')->references('id');
            $table->foreign('id_mst_prioritas_pembangunan')->on('prioritas_pembangunan')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpjmd_prioritas_pembangunan', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_sasaran']);
            $table->dropForeign(['id_periode']);
            $table->dropForeign(['id_mst_prioritas_pembangunan']);
        });
    }
}
