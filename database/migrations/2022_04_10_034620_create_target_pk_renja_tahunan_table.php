<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetPkRenjaTahunanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_pk_renja_tahunan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('pk_renja_tahunan_id');
            $table->unsignedBigInteger('atasan_id');
            $table->unsignedBigInteger('tahun_id');
            $table->unsignedBigInteger('satuan_id');
            $table->string('target');
            $table->timestamps();

            $table->foreign('pk_renja_tahunan_id')
                ->references('id')
                ->on('pk_renja_tahunan');

            $table->foreign('atasan_id')
                ->references('id')
                ->on('mst_pegawai');

            $table->foreign('tahun_id')
                ->references('id')
                ->on('mst_periode_per_tahun');

            $table->foreign('satuan_id')
                ->references('id')
                ->on('mst_satuan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_pk_renja_tahunan');
    }
}
