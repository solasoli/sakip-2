
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RefactorRpjmdTujuanAndItsRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::table('rpjmd_tujuan', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_misi')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->foreign('id_misi')->on('rpjmd_misi')->references('id');
            $table->foreign('id_periode')->on('mst_periode')->references('id');
        });

        Schema::table('rpjmd_tujuan_indikator', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_tujuan')->change();
            $table->unsignedBigInteger('id_misi')->change();
            $table->unsignedBigInteger('id_satuan')->change();
            $table->foreign('id_tujuan')->on('rpjmd_tujuan')->references('id');
            $table->foreign('id_misi')->on('rpjmd_misi')->references('id');
            $table->foreign('id_satuan')->on('mst_satuan')->references('id');
        });

        Schema::table('rpjmd_tujuan_target', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_tujuan_indikator')->change();
            $table->unsignedBigInteger('id_periode_per_tahun')->change();
            $table->foreign('id_tujuan_indikator')->on('rpjmd_tujuan_indikator')->references('id');
            $table->foreign('id_periode_per_tahun')->on('mst_periode_per_tahun')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpjmd_tujuan', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_misi']);
            $table->dropForeign(['id_periode']);
        });

        Schema::table('rpjmd_tujuan_indikator', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_tujuan']);
            $table->dropForeign(['id_misi']);
            $table->dropForeign(['id_satuan']);
        });

        Schema::table('rpjmd_tujuan_target', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_tujuan_indikator']);
            $table->dropForeign(['id_periode_per_tahun']);
        });
    }
}
