<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RefactorRenstraProgramAndItsRelatedTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::table('renstra_program', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_renstra_sasaran')->change();
            $table->unsignedBigInteger('id_rpjmd_program')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->foreign('id_renstra_sasaran')->references('id')->on('renstra_sasaran');
            $table->foreign('id_rpjmd_program')->references('id')->on('rpjmd_program');
            $table->foreign('id_periode')->references('id')->on('mst_periode');
        });

        Schema::table('renstra_sasaran_program', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_opd')->change();
            $table->unsignedBigInteger('id_program_renstra')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->foreign('id_opd')->references('id')->on('mst_opd');
            $table->foreign('id_program_renstra')->references('id')->on('renstra_program');
            $table->foreign('id_periode')->references('id')->on('mst_periode');
        });

        Schema::table('renstra_sasaran_program_indikator', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_satuan')->change();
            $table->unsignedBigInteger('id_sasaran_program')->change();
            $table->foreign('id_satuan')->references('id')->on('mst_satuan');
            $table->foreign('id_sasaran_program')->references('id')->on('renstra_sasaran_program');
        });

        Schema::table('renstra_sasaran_program_target', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_periode_per_tahun')->change();
            $table->unsignedBigInteger('id_sasaran_program_indikator')->change();
            $table->foreign('id_periode_per_tahun')->references('id')->on('mst_periode_per_tahun');
            $table->foreign('id_sasaran_program_indikator', 'rspt_rspi_id_sasaran_program_indikator')->references('id')->on('renstra_sasaran_program_indikator');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_program', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_renstra_sasaran']);
            $table->dropForeign(['id_rpjmd_program']);
            $table->dropForeign(['id_periode']);
        });

        Schema::table('renstra_sasaran_program', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_opd']);
            $table->dropForeign(['id_program_renstra']);
            $table->dropForeign(['id_periode']);
        });

        Schema::table('renstra_sasaran_program_indikator', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_satuan']);
            $table->dropForeign(['id_sasaran_program']);
        });

        Schema::table('renstra_sasaran_program_target', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_periode_per_tahun']);
            $table->dropForeign('rspt_rspi_id_sasaran_program_indikator');
        });

    }
}
