<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactorRenstraStrategiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('renstra_strategi', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_renstra_sasaran')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->foreign('id_renstra_sasaran')->on('renstra_sasaran')->references('id');
            $table->foreign('id_periode')->on('mst_periode')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_strategi', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_renstra_sasaran']);
            $table->dropForeign(['id_periode']);
        });
    }
}
