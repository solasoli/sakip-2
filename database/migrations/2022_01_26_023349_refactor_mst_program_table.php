<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactorMstProgramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_program', function (Blueprint $table) {
            $table->unsignedBigInteger('id_bidang_urusan')->change();
            $table->foreign('id_bidang_urusan')->on('mst_bidang_urusan')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_program', function (Blueprint $table) {
            $table->dropForeign(['id_bidang_urusan']);
        });
    }
}
