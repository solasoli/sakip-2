<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RefactorMstPegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::table('mst_pegawai', function (Blueprint $table) {
            // $table->unsignedBigInteger('id_pangkat')->change();
            // $table->unsignedBigInteger('id_pangkat_gol')->change();
            $table->unsignedBigInteger('id_eselon')->change();
            $table->unsignedBigInteger('id_opd')->change();
            // $table->foreign('id_pangkat')->on('mst_pangkat')->references('id');
            // $table->foreign('id_pangkat_gol')->on('mst_pangkat_golongan')->references('id');
            $table->foreign('id_eselon')->on('mst_eselon')->references('id');
            $table->foreign('id_opd')->on('mst_opd')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_pegawai', function (Blueprint $table) {
            // $table->dropForeign(['id_pangkat']);
            // $table->dropForeign(['id_pangkat_gol']);
            $table->dropForeign(['id_eselon']);
            $table->dropForeign(['id_opd']);
        });
    }
}
