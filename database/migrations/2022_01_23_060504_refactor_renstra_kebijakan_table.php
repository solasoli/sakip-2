<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactorRenstraKebijakanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('renstra_kebijakan', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_renstra_strategi')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->foreign('id_renstra_strategi')->references('id')->on('renstra_strategi');
            $table->foreign('id_periode')->references('id')->on('mst_periode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_kebijakan', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_renstra_strategi']);
            $table->dropForeign(['id_periode']);
        });
    }
}
