<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RevertPreviouslyMigratedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('pk_renja_tahunan', function (Blueprint $table) {
            $table->dropForeign(['atasan_id']);
            $table->dropColumn('atasan_id');
        });

        Schema::table('pk_renja_penanggung_jawab', function (Blueprint $table) {
            $table->dropIndex(['pk_renja_id', 'pk_renja_type']);
            $table->dropForeign(['pegawai_id']);
        });

        DB::table('pk_renja_penanggung_jawab')->where('pk_renja_type', 'App\Models\PkRenjaTahunan')->delete();

        Schema::table('pk_renja_penanggung_jawab', function (Blueprint $table) {
            $table->dropColumn('pk_renja_type');
            $table->renameColumn('pk_renja_id', 'id_pk_renja_triwulan');
            $table->renameColumn('pegawai_id', 'id_pegawai');
        });

        Schema::rename('pk_renja_penanggung_jawab', 'pk_renja_triwulan_penanggung_jawab');

        Schema::table('pk_renja_triwulan_penanggung_jawab', function (Blueprint $table) {
            $table->foreign('id_pegawai')
                ->references('id')
                ->on('mst_pegawai');

            $table->foreign('id_pk_renja_triwulan')
                ->references('id')
                ->on('pk_renja_triwulan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pk_renja_tahunan', function (Blueprint $table) {
            $table->unsignedBigInteger('atasan_id')->nullable();

            $table->foreign('atasan_id')->references('id')->on('mst_pegawai');
        });

        Schema::table('pk_renja_triwulan_penanggung_jawab', function (Blueprint $table) {
            $table->dropForeign(['id_pk_renja_triwulan']);
            $table->dropForeign(['id_pegawai']);
        });

        Schema::rename('pk_renja_triwulan_penanggung_jawab', 'pk_renja_penanggung_jawab');

        Schema::table('pk_renja_penanggung_jawab', function (Blueprint $table) {

            $table->string('pk_renja_type')->after('id_pk_renja_triwulan')->nullable();
            $table->renameColumn('id_pk_renja_triwulan', 'pk_renja_id');
            $table->renameColumn('id_pegawai', 'pegawai_id');

            $table->foreign('pegawai_id')->references('id')->on('mst_pegawai');
            $table->index(['pk_renja_id', 'pk_renja_type']);
        });

        DB::table('pk_renja_penanggung_jawab')->update([
            'pk_renja_type' => "App\Models\PkRenjaTriwulan"
        ]);
    }
}
