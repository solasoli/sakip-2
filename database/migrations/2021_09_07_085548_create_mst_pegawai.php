<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstPegawai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * 
     */

    public function up()
    {
        Schema::create('mst_pegawai', function (Blueprint $table) {
            $table->id();
            $table->string('nip');
            $table->string('nama',256);
            $table->integer('id_pangkat');
            $table->integer('id_pangkat_gol');
            $table->integer('id_eselon');
            $table->string('jabatan', 256);
            $table->string('unit', 256);
            $table->integer('id_opd');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_pegawai');
    }
}
