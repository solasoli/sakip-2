<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RefactorRpjmdVisiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::table('rpjmd_visi', function (Blueprint $table) {
            $table->unsignedBigInteger('id_periode')->change();
            $table->foreign('id_periode')->on('mst_periode')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpjmd_visi', function (Blueprint $table) {
            $table->dropForeign(['id_periode']);
        });
    }
}
