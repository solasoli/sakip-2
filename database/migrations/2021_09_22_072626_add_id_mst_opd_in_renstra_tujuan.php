<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdMstOpdInRenstraTujuan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('renstra_tujuan', function (Blueprint $table) {
            $table->foreignId('id_mst_opd')->preference('id')->on('mst_opd')->after('id_periode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_tujuan', function (Blueprint $table) {
            $table->dropColumn('id_mst_opd');
        });
    }
}
