<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRpjmdTargetRkpdTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpjmd_target_rkpd', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('rpjmd_id');
            $table->string('rpjmd_type');
            $table->unsignedBigInteger('id_satuan');
            $table->timestamps();

            $table->foreign('id_satuan')
                ->references('id')
                ->on('mst_satuan');

            $table->index(['rpjmd_id', 'rpjmd_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpjmd_target_rkpd');
    }
}
