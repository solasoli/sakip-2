<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSasaranSubKegiatanOutputPenanggungJawabTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sasaran_sub_kegiatan_output_penanggung_jawab', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_renstra_sasaran_sub_kegiatan_output');
            $table->unsignedBigInteger('id_pegawai');
            $table->timestamps();

            $table->foreign('id_renstra_sasaran_sub_kegiatan_output', 'rssko_rsskopj_foreign')
                ->references('id')
                ->on('renstra_sasaran_sub_kegiatan_output');

            $table->foreign('id_pegawai', 'p_rsskopj_foreign')
                ->references('id')
                ->on('mst_pegawai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sasaran_sub_kegiatan_output_penanggung_jawab');
    }
}
