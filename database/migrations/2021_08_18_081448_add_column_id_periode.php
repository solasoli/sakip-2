<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIdPeriode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpjmd_sasaran_detail', function (Blueprint $table) {
            $table->boolean('is_deleted')->after('is_iku')->default(0);
        });
        Schema::table('rpjmd_sasaran_target', function (Blueprint $table) {
            $table->boolean('is_deleted')->after('id_periode_per_tahun')->default(0);
        });
        Schema::table('rpjmd_tujuan', function (Blueprint $table) {
            $table->integer('id_periode')->after('id_misi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpjmd_sasaran_detail', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });
        Schema::table('rpjmd_sasaran_target', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });
        Schema::table('rpjmd_tujuan', function (Blueprint $table) {
            $table->dropColumn('id_periode');
        });
    }
}
