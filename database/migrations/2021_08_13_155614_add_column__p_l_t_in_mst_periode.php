<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPLTInMstPeriode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_periode', function (Blueprint $table) {
            $table->boolean('is_plt')->default(0)->after('sampai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_periode', function (Blueprint $table) {
            $table->dropColumn('is_plt');
        });
    }
}
