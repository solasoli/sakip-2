<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraKegiatanSumberAnggaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_kegiatan_sumber_anggaran', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_renstra_kegiatan');
            $table->bigInteger('id_mst_sumber_anggaran');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_kegiatan_sumber_anggaran');
    }
}
