<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactorMstBidangUrusanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_bidang_urusan', function (Blueprint $table) {
            $table->unsignedBigInteger('id_urusan')->change();
            $table->foreign('id_urusan')->on('mst_urusan')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_bidang_urusan', function (Blueprint $table) {
            $table->dropForeign(['id_urusan']);
        });
    }
}
