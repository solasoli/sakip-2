<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSubKegiatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sub_kegiatan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_renstra_kegiatan');
            $table->unsignedBigInteger('id_mst_sub_kegiatan');
            $table->timestamps();

            $table->foreign('id_renstra_kegiatan')
                ->references('id')
                ->on('renstra_kegiatan');

            $table->foreign('id_mst_sub_kegiatan')
                ->references('id')
                ->on('mst_sub_kegiatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sub_kegiatan');
    }
}
