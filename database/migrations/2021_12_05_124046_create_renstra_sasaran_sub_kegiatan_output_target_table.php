<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSasaranSubKegiatanOutputTargetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sasaran_sub_kegiatan_output_target', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_periode_per_tahun');
            $table->unsignedBigInteger('id_sasaran_sub_kegiatan_output');
            $table->string('target');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();

            $table->foreign('id_periode_per_tahun', 'rsskot_mppt_foreign')
                ->references('id')
                ->on('mst_periode_per_tahun');

            $table->foreign('id_sasaran_sub_kegiatan_output', 'rsskot_rski_foreign')
                ->references('id')
                ->on('renstra_sasaran_sub_kegiatan_output');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sasaran_sub_kegiatan_output_target');
    }
}
