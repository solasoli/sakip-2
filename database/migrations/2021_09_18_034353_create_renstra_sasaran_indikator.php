<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSasaranIndikator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sasaran_indikator', function (Blueprint $table) {
            $table->id();
            $table->string('indikator');
            $table->bigInteger('id_satuan');
            $table->foreignId('id_sasaran')->references('id')->on('renstra_sasaran')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sasaran_indikator');
    }
}
