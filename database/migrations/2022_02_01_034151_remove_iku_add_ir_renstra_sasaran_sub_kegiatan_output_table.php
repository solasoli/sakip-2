<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveIkuAddIrRenstraSasaranSubKegiatanOutputTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('renstra_sasaran_sub_kegiatan_output', function (Blueprint $table) {
            $table->renameColumn('is_iku', 'is_ir');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_sasaran_sub_kegiatan_output', function (Blueprint $table) {
            $table->renameColumn('is_ir', 'is_iku');
        });
    }
}
