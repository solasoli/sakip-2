<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSasaranSubKegiatanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sasaran_sub_kegiatan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_renstra_sub_kegiatan');
            $table->string('name');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();

            $table->foreign('id_renstra_sub_kegiatan', 'renstra_sub_kegiatan_foreign')
                ->references('id')
                ->on('renstra_sub_kegiatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sasaran_sub_kegiatan');
    }
}
