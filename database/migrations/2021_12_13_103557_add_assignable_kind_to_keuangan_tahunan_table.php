<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAssignableKindToKeuanganTahunanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('keuangan_tahunan', function (Blueprint $table) {
            $table->string('assignable_kind')->after('assignable_type')->nullable();

            $table->index(['assignable_id', 'assignable_type', 'assignable_kind'], 'assignable_id_type_kind_index');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('keuangan_tahunan', function (Blueprint $table) {
            $table->dropColumn('assignable_kind');
        });
    }
}
