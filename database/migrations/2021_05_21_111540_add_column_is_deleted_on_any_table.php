<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIsDeletedOnAnyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpjmd_tujuan', function(Blueprint $table) {
            $table->boolean('is_deleted')->after('id_misi')->default(0);
        });

        Schema::table('rpjmd_tujuan_indikator', function(Blueprint $table) {
            $table->boolean('is_deleted')->after('id_satuan')->default(0);
        });

        Schema::table('rpjmd_tujuan_target', function(Blueprint $table) {
            $table->boolean('is_deleted')->after('id_tujuan_indikator')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpjmd_tujuan', function(Blueprint $table) {
            $table->dropColumn('is_deleted');
        });

        Schema::table('rpjmd_tujuan_indikator', function(Blueprint $table) {
            $table->dropColumn('is_deleted');
        });

        Schema::table('rpjmd_tujuan_target', function(Blueprint $table) {
            $table->dropColumn('is_deleted');
        });
    }
}
