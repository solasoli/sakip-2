<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePkRenjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pk_renja_tahunan', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('pk_id');
            $table->string('pk_type');
            $table->string('id_satuan');
            $table->timestamps();

            $table->index(['pk_id', 'pk_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pk_renja');
    }
}
