<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIdBidangUrusanInTableMstProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_program', function (Blueprint $table) {
            $table->integer('id_bidang_urusan')->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_program', function (Blueprint $table) {
            $table->dropColumn('id_bidang_urusan');
        });
    }
}
