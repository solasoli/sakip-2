<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOnTableRpjmdProgramIndikator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpjmd_program_indikator', function (Blueprint $table) {
            $table->string('awal')->after('id_rpjmd_program');
            $table->string('akhir')->after('awal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpjmd_program_indikator', function (Blueprint $table) {
            $table->dropColumn('awal');
            $table->dropColumn('akhir');
        });
    }
}
