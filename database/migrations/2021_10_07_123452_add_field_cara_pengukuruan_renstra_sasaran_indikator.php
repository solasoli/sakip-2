<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldCaraPengukuruanRenstraSasaranIndikator extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('renstra_sasaran_indikator', function (Blueprint $table) {
            $table->text('cara_pengukuran')->after('indikator');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_sasaran_indikator', function (Blueprint $table) {
            $table->dropColumn('cara_pengukuran');
        });
    }
}
