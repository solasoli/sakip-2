<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RefactorRenstraTujuanAndItsRelatedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::table('renstra_tujuan', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_rpjmd_sasaran')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->unsignedBigInteger('id_mst_opd')->change();
            $table->foreign('id_rpjmd_sasaran')->on('rpjmd_sasaran')->references('id');
            $table->foreign('id_periode')->on('mst_periode')->references('id');
            $table->foreign('id_mst_opd')->on('mst_opd')->references('id');
        });

        Schema::table('renstra_tujuan_indikator', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_satuan')->change();
            $table->unsignedBigInteger('id_tujuan')->change();
            $table->foreign('id_satuan')->on('mst_satuan')->references('id');
            $table->foreign('id_tujuan')->on('renstra_tujuan')->references('id');
        });

        Schema::table('renstra_tujuan_target', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_indikator_tujuan')->change();
            $table->unsignedBigInteger('id_periode_per_tahun')->change();
            $table->foreign('id_indikator_tujuan')->on('renstra_tujuan_indikator')->references('id');
            $table->foreign('id_periode_per_tahun')->on('mst_periode_per_tahun')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_tujuan', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_rpjmd_sasaran']);
            $table->dropForeign(['id_periode']);
            $table->dropForeign(['id_mst_opd']);
        });

        Schema::table('renstra_tujuan_indikator', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_satuan']);
            $table->dropForeign(['id_tujuan']);
        });

        Schema::table('renstra_tujuan_target', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_indikator_tujuan']);
            $table->dropForeign(['id_periode_per_tahun']);
        });
    }
}
