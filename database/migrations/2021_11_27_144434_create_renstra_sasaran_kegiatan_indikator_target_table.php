<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSasaranKegiatanIndikatorTargetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sasaran_kegiatan_indikator_target', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_periode_per_tahun');
            $table->unsignedBigInteger('id_sasaran_kegiatan_indikator');
            $table->string('target');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();

            $table->foreign('id_periode_per_tahun', 'periode_per_tahun_foreign')
                ->references('id')
                ->on('mst_periode_per_tahun');

            $table->foreign('id_sasaran_kegiatan_indikator', 'sasaran_kegiatan_indikator_foreign')
                ->references('id')
                ->on('renstra_sasaran_kegiatan_indikator');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sasaran_kegiatan_indikator_target');
    }
}
