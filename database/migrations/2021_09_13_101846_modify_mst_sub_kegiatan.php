<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyMstSubKegiatan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mst_sub_kegiatan', function (Blueprint $table) {
            $table->string('kode_sub_kegiatan')->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mst_sub_kegiatan', function (Blueprint $table) {
            $table->string('kode_sub_kegiatan')->unique();
        });
    }
}
