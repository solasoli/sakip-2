<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RefactorRpjmdKebijakanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('rpjmd_kebijakan', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_strategi')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->foreign('id_strategi')->on('rpjmd_strategi')->references('id');
            $table->foreign('id_periode')->on('mst_periode')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpjmd_kebijakan', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_strategi']);
            $table->dropForeign(['id_periode']);
        });
    }
}
