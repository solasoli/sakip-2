<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetRenjaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('target_renja', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('assignable_id');
            $table->string('assignable_type');
            $table->unsignedBigInteger('id_periode_per_tahun');
            $table->string('target');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();
            $table->index(['assignable_id', 'assignable_type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('target_renja');
    }
}
