<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRpjmdTujuan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rpjmd_tujuan', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('rpjmd_tujuan_indikator', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('id_tujuan');
            $table->integer('id_misi');
            $table->integer('id_satuan');
            $table->timestamps();
        });

        Schema::create('rpjmd_tujuan_target', function (Blueprint $table) {
            $table->id();
            $table->string('target');
            $table->integer('id_periode_per_tahun');
            $table->integer('id_tujuan_indikator');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rpjmd_tujuan');
        Schema::dropIfExists('rpjmd_tujuan_indikator');
        Schema::dropIfExists('rpjmd_tujuan_target');
    }
}
