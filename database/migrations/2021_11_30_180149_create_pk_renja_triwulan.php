<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePkRenjaTriwulan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pk_renja_triwulan', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('pk_id');
            $table->string('pk_type');
            $table->string('id_periode_per_tahun');
            $table->integer('triwulan');
            $table->string('target');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pk_renja_triwulan');
    }
}
