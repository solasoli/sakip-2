<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSasaranKegiatanIndikatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sasaran_kegiatan_indikator', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_sasaran_kegiatan');
            $table->unsignedBigInteger('id_satuan');
            $table->string('name');
            $table->boolean('is_pk')->default(false);
            $table->boolean('is_iku')->default(false);
            $table->longText('cara_pengukuran');
            $table->string('target_awal');
            $table->string('target_akhir');
            $table->boolean('is_deleted')->default(0);
            $table->timestamps();

            $table->foreign('id_satuan')
                ->references('id')
                ->on('mst_satuan');

            $table->foreign('id_sasaran_kegiatan')
                ->references('id')
                ->on('renstra_sasaran_kegiatan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sasaran_kegiatan_indikator');
    }
}
