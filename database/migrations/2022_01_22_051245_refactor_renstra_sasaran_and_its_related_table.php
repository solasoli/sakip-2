<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RefactorRenstraSasaranAndItsRelatedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Schema::table('renstra_sasaran', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_renstra_tujuan')->change();
            $table->unsignedBigInteger('id_periode')->change();
            $table->foreign('id_renstra_tujuan')->references('id')->on('renstra_tujuan');
            $table->foreign('id_periode')->references('id')->on('mst_periode');
        });

        Schema::table('renstra_sasaran_indikator', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_satuan')->change();
            $table->foreign('id_satuan')->references('id')->on('mst_satuan');
        });

        Schema::table('renstra_sasaran_target', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
            $table->unsignedBigInteger('id_periode_per_tahun')->change();
            $table->foreign('id_periode_per_tahun')->references('id')->on('mst_periode_per_tahun');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_sasaran', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_renstra_tujuan']);
            $table->dropForeign(['id_periode']);
        });

        Schema::table('renstra_sasaran_indikator', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_satuan']);
        });

        Schema::table('renstra_sasaran_target', function (Blueprint $table) {
            $table->boolean('is_deleted')->default(0);
            $table->dropForeign(['id_periode_per_tahun']);
        });
    }
}
