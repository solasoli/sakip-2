<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnOnRenstraSasaran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('renstra_sasaran_indikator', function (Blueprint $table) {
            $table->boolean('is_pk')->default(0)->after('id_sasaran');
            $table->boolean('is_iku')->default(0)->after('is_pk');
            $table->string('awal')->after('is_iku');
            $table->string('akhir')->after('awal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_sasaran_indikator', function (Blueprint $table) {
            $table->dropColumn('is_pk');
            $table->dropColumn('is_iku');
            $table->dropColumn('awal');
            $table->dropColumn('akhir');
        });
    }
}
