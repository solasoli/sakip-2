<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePkRenjaTriwulanPenanggungJawabTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pk_renja_triwulan_penanggung_jawab', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_pegawai');
            $table->unsignedBigInteger('id_pk_renja_triwulan');
            $table->timestamps();

            $table->foreign('id_pegawai')
                ->references('id')
                ->on('mst_pegawai');

            $table->foreign('id_pk_renja_triwulan')
                ->references('id')
                ->on('pk_renja_triwulan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pk_renja_triwulan_penanggung_jawab');
    }
}
