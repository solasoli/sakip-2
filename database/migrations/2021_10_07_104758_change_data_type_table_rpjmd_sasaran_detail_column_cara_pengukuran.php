<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDataTypeTableRpjmdSasaranDetailColumnCaraPengukuran extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rpjmd_sasaran_detail', function (Blueprint $table) {
            $table->text('cara_pengukuran')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rpjmd_sasaran_detail', function (Blueprint $table) {
            $table->string('cara_pengukuran')->change();
        });
    }
}
