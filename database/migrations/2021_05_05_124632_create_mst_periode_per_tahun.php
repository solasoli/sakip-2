<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstPeriodePerTahun extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mst_periode_per_tahun', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_periode');
            $table->foreign('id_periode')->references('id')->on('mst_periode')->cascadeOnDelete()->cascadeOnUpdate();
            $table->integer('tahun');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mst_periode_per_tahun');
    }
}
