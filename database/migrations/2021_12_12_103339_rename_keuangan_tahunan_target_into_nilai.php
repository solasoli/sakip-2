<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameKeuanganTahunanTargetIntoNilai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('keuangan_tahunan', function (Blueprint $table) {
            $table->renameColumn('target', 'nilai');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('keuangan_tahunan', function (Blueprint $table) {
            $table->renameColumn('nilai', 'target');
        });
    }
}
