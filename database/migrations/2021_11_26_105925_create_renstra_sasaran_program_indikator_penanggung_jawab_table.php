<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRenstraSasaranProgramIndikatorPenanggungJawabTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('renstra_sasaran_program_indikator_penanggung_jawab', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_renstra_sasaran_program_indikator');
            $table->unsignedBigInteger('id_pegawai');
            $table->timestamps();

            $table->foreign('id_renstra_sasaran_program_indikator', 'rspi_foreign')
                ->references('id')
                ->on('renstra_sasaran_program_indikator');

            $table->foreign('id_pegawai', 'mst_pegawai_foreign')
                ->references('id')
                ->on('mst_pegawai');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('renstra_sasaran_program_indikator_penanggung_jawab');
    }
}
