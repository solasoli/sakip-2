<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIrRenstraSasaranIndikatorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('renstra_sasaran_indikator', function (Blueprint $table) {
            $table->boolean('is_ir')->default(0)->after('is_iku');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('renstra_sasaran_indikator', function (Blueprint $table) {
            $table->dropColumn('is_ir');
        });
    }
}
